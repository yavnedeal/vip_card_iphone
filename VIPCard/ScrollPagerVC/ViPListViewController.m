//
//  ViPListViewController.m
//  VIPCard
//
//  Created by SanC on 22/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "ViPListViewController.h"
#import "MainTableViewCell.h"
#import "WSCouponsResponse.h"
#import "ScrollViewController.h"

@interface ViPListViewController ()
{
    
}
@end

@implementation ViPListViewController
@synthesize dealsResponseObj, filteredCouponsList,str_module;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBackButton:YES];
    filteredCouponsList = self.dealsResponseObj.couponsList;
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if(!_isSearch)
    {
        filteredCouponsList = self.dealsResponseObj.couponsList;
    }
    else
    {
        _isSearch = NO;
    }
    
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1; //array count returns 10
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [filteredCouponsList count] *2;// this should be one because it will create space between two cells if you want space between 4 cells you can modify it.
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // To "clear" the footer view
    return [UIView new] ;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.row%2==0)
	{
		MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"mainCell"];
       
		if (cell==nil)
		{
			NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MainTableViewCell" owner:self options:nil];
			cell=[nib objectAtIndex:0];
		}
		[cell setBackgroundColor:[UIColor clearColor]];
        WSCouponsResponse *couponseResponse = [filteredCouponsList objectAtIndex:indexPath.row/2];
        NSLog(@"CouponseResponse : %@",couponseResponse.id);
        [cell populateData:couponseResponse andState:NO];
      //  CGSize expectedNameSize = [self getStringSize:cell.name andString:cell.name.text];
       // CGSize expectedDescSize = [self getStringSize:cell.desc andString:cell.desc.text];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        if(expectedNameSize.height > 21)
//        {
//            cell.constaintLblNameHeight.constant = expectedNameSize.height;
//        }
//        if(expectedDescSize.height > 21)
//        {
//            cell.constaintLblDescHeight.constant = expectedDescSize.height;
//        }
        cell.layer.borderColor = [UIColor blackColor].CGColor;
        
        cell.layer.borderWidth = 0.6;
//        [self.mainPrice setStrikethrough:YES];
        [cell.mainPrice setStrikethrough:YES];

		return cell;
	}
	else{
		MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"mainCell"];
       // cell.layer.borderColor = [UIColor blackColor].CGColor;
       // cell.layer.borderWidth = 0.5;
       
		if (cell==nil)
		{
			NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MainTableViewCell" owner:self options:nil];
			cell=[nib objectAtIndex:0];
		}
        [cell setBackgroundColor:COLOR_LIGHT_GRAY];

        [cell populateData:nil andState:YES];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        /*CGSize expectedNameSize = [self getStringSize:cell.name andString:cell.name.text];
        CGSize expectedDescSize = [self getStringSize:cell.desc andString:cell.desc.text];
        [cell.mainPrice setStrikethrough:YES];
        if(expectedNameSize.height > 21)
        {
            cell.con_NameHeight.constant = expectedNameSize.height;
        }
        if(expectedDescSize.height > 21)
        {
            cell.con_DescriptionHeight.constant = expectedDescSize.height;
        }*/


		return cell;
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.row%2==0)
	{
		//	MainTableViewCell *cell = (MainTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
		/*NSMutableString *nameString=[[NSMutableString alloc]init];
        NSMutableString *descString=[[NSMutableString alloc]init];
        
		CGSize max_Name = CGSizeMake(cell.name.frame.size.width,9999);
        CGSize max_Desc = CGSizeMake(cell.desc.frame.size.width,9999);
         
         CGSize expectedNameSize = [nameString sizeWithFont:cell.name.font
         constrainedToSize:max_Name
         lineBreakMode:cell.name.lineBreakMode];
         
         CGSize expectedDescSize = [descString sizeWithFont:cell.desc.font
         constrainedToSize:max_Desc
         lineBreakMode:cell.desc.lineBreakMode];
 
         */

		// CGSize imageHeight = cell.nameBussiness.bounds.size;
		// NSLog(@"Profile Image Height : %f",imageHeight.height);
		WSCouponsResponse *couponseResponse = [filteredCouponsList objectAtIndex:indexPath.row/2];
		CGFloat expectedNameSize = 21.f;
		CGFloat expectedDescSize = 21.f;
		float totalHeight = 284.0f;
		CGFloat w = CGRectGetWidth(self.view.bounds) - 30;
		if(![NSString isEmpty:couponseResponse.name])
		{
			CGSize xSize = [couponseResponse.name sizeWithFont:helveticaFont(25.f)
				            constrainedToSize:CGSizeMake(w,NSIntegerMax)
					        lineBreakMode:NSLineBreakByWordWrapping];
			expectedNameSize = xSize.height;
		}
		if(![NSString isEmpty:couponseResponse.des])
		{
			CGSize xSize = [couponseResponse.des sizeWithFont:helveticaFont(20.f)
											 constrainedToSize:CGSizeMake(w,NSIntegerMax)
					        lineBreakMode:NSLineBreakByWordWrapping];
			expectedDescSize = xSize.height;
		}

		if(![NSString isEmpty:couponseResponse.nameBusiness])
		{
			CGSize xSize = [couponseResponse.nameBusiness sizeWithFont:helveticaFont(17.f)
											constrainedToSize:CGSizeMake(w,NSIntegerMax)
												lineBreakMode:NSLineBreakByWordWrapping];
			if (xSize.height > 21)
			   totalHeight = totalHeight + xSize.height - 21;
		}
        
        if([couponseResponse.idTypeSeals integerValue] > 1)
        {
            CGSize xSize = [[couponseResponse dealPriceFormatedValue] sizeWithFont:helveticaBoldFont(17.f)
                                                     constrainedToSize:CGSizeMake((w-90),NSIntegerMax)
                                                         lineBreakMode:NSLineBreakByWordWrapping];
            if (xSize.height > 21)
                totalHeight = totalHeight + xSize.height - 21;
        }

		//[myString sizeWithFont:aLabel.font
		//	 constrainedToSize:maximumLabelSize
		//		 lineBreakMode:aLabel.lineBreakMode];

		// CGSize expectedNameSize = [self getStringSize:cell.name andString:cell.name.text];
		//CGSize expectedDescSize = [self getStringSize:cell.desc andString:cell.desc.text];
		//adjust the label the the new height.
        

        
        if(expectedNameSize > 21)
        {
            totalHeight += expectedNameSize;
        }
        else{
            totalHeight += 21;
        }
        if(expectedDescSize > 21)
        {
            totalHeight += expectedDescSize;
        }
        else{
            totalHeight += 21;
        }
		//totalHeight += 284;
		// if(totalHeight > 350)
		    return totalHeight;
		// return 350;
		//return (cell.nameBussiness.frame.origin.y+cell.nameBussiness.frame.size.height+20);

	}
	return 8;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WSCouponsResponse *couponRes = [filteredCouponsList objectAtIndex:indexPath.row/2];
	ScrollViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kScrollDetail_SB_ID];
    vc.couponObj = couponRes;
    vc.isCoupneShow = _isCoupns;
	vc.str_module=self.str_module;
	[self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end