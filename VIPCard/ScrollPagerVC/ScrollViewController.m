//
//  ScrollViewController.m
//  VIPCard
//  Desc : This is used for COUPON and DEALS Detail Screen
//  Created by SanC on 23/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "ScrollViewController.h"
#import "CategoryForMealDetailVC.h"
#import "CategoryForMealHolderVC.h"

#import "ShoppingCartSummaryVC.h"
#import "WSBusinessCatResponse.h"
#import "WSCategoryMealResponse.h"
#import "AppDelegate.h"
#import "ReportError.h"

@interface ScrollViewController ()
{
    AppDelegate *appDelegate;
    ReportError *reportErr;
}
@end

@implementation ScrollViewController
@synthesize couponObj,dealObj,str_module;


- (void)viewDidLoad {
    [super viewDidLoad];
	couponObj.isbarcode = NO;
	//NSLog(@"Coupan object=%@",self.couponObj);
    [self showBackButton:YES];

	if(_isCoupneShow)
		_btnAdd.hidden = YES;
	else
		_btnAdd.hidden = NO;

    [self populateData];
     _popupBaseView.hidden = YES;
    _btnSignIn.layer.cornerRadius = 10.0;
    _btnSignIn.layer.masksToBounds = YES;
    _btnSignUp.layer.cornerRadius = 10.0;
    _btnSignUp.layer.masksToBounds = YES;

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopupWindow)];
    gesture.numberOfTapsRequired = 1;
    [_popupBaseView addGestureRecognizer:gesture];
    
       _btnAdd.layer.cornerRadius = 10.0;
    _btnAdd.layer.masksToBounds = YES;
    // Do any additional setup after loading the view.
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.detailScrollView.scrollsToTop = FALSE;
}



#pragma mark-UITextFiledDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[self.view endEditing:YES];

	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	[self.view endEditing:YES];

}
-(void)closePopupWindow
{
    _popupBaseView.hidden = YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	//self.detailScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;

}


-(void) updateLblSize{
	CGRect f = self.lblName.frame;
	[self.lblName sizeToFit];
	f.size.height = self.lblName.frame.size.height;

	self.consLabelName_Height.constant = f.size.height;

	//lblOffer
	f = self.lblOffer.frame;
	[self.lblOffer sizeToFit];
	f.size.height = self.lblOffer.frame.size.height;
	self.consLabelOffer_Height.constant = f.size.height;

	f=self.lblMarks.frame;
	[self.lblMarks sizeToFit];
	f.size.height = self.lblMarks.frame.size.height;
	self.consLabelMark_Height.constant = f.size.height;
}

-(void)populateData
{
	if(IS_OS_8_OR_LATER)
	{
		self.cons_viewdetailHeight.active = YES;
		self.consLabelMark_Height.active = YES;
		self.con_BarcodeHeight.active = YES;
		self.consLabelName_Height.active = YES;
	}
    if (couponObj!=nil)
     {

   // CGSize stringSize;
   // stringSize = [self getStringSize:self.lblName andString:self.couponObj.name];
    [self.lblName setText:self.couponObj.name];
   // [self.lblName sizeToFit];
   // CGFloat tmpHeight = CGRectGetHeight(self.lblName.frame) + 2;
   // self.consLabelName_Height.constant = tmpHeight;


		 //stringSize = [self getStringSize:self.lblOffer andString:self.couponObj.offer];
    [self.lblOffer setText:self.couponObj.offer];
		 //[self.lblOffer sizeToFit];
   // tmpHeight = CGRectGetHeight(self.lblOffer.frame) + 2;
   //self.lbl_OfferHeight.constant = tmpHeight;

    self.lblOfferTitle.text = [NSString stringWithFormat:NSLocalizedString(@"OfferTitle", @"OfferTitle")];
    self.lblOfferTitle.textColor = COLOR_TITLE_NEVY_BLUE;
    //stringSize = [self getStringSize:self.lblDesc andString:self.couponObj.des];

    [self.lblDesc setText:self.couponObj.des];
    self.lblDescTitle.text = [NSString stringWithFormat:NSLocalizedString(@"ExtendedInformation", @"ExtendedInformation")];
         self.lblDesc.hidden=YES;
         self.lblDescTitle.hidden=YES;
		 //self.consLabelDesc_Height.constant =  stringSize.height; //FIXED - 1.0(7) by RR


    self.consLabelDesc_Height.constant = 0.0;
    self.consDesc_Title_Height.constant = 0.0;

    self.lblMarksTitle.text = [NSString stringWithFormat:@"%@:",NSLocalizedString(@"lbl_marks", @"lbl_marks")];
		 //stringSize = [self getStringSize:self.lblMarks andString:self.couponObj.marks];
		 [self.lblMarks setText:self.couponObj.marks];
		 //[self.lblMarks sizeToFit];
		 //tmpHeight = CGRectGetHeight(self.lblMarks.frame) + 2;
		 //self.cons_lblMarkHeight.constant = tmpHeight;

	[self updateLblSize];

    if([self.couponObj.price1 integerValue] == 0)
        self.lblPrice1.hidden = YES;
    if([self.couponObj.price2 integerValue] == 0)
        self.lblPrice2.hidden = YES;
         
         [self.lblPrice1 setTextColor:[UIColor lightGrayColor]];
         [self.lblPrice1 setStrikethrough:YES];
         [self.lblPrice2 setTextColor:COLOR_MAIN_PRICE];
         
         if([self.couponObj.idTypeSeals integerValue] == 1)
         {
             self.lblPrice1.hidden = NO;
              [self.lblPrice1 setText: [NSString stringWithFormat:@"%@%@",SHEKEL,self.couponObj.price1]];
             [self.lblPrice2 setText:[NSString stringWithFormat:@"%@%@",SHEKEL,self.couponObj.price2]];
         }
         else {
             self.lblPrice1.hidden = YES;
             self.lblPrice2.text = [self.couponObj dealPriceFormatedValue];
         }
		 
         
         [self.lblPrice2 sizeToFit];
         self.constatintDiscPriceHeight.constant = CGRectGetHeight(self.lblPrice2.frame);
         

    [self.logoImg sd_setImageWithURL:[self baseImageURL:self.couponObj.logobusiness] placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
    [self.bussinessImage sd_setImageWithURL:[self baseImageURL:self.couponObj.image] placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
		self.bussinessImage.image= [self imageWithImage:self.bussinessImage.image scaledToWidth:320.0f];


      if ([self.str_module isEqualToString:@"Deal"])
      {
       [self.btnAdd setTitle:NSLocalizedString(@"btn_red_order", @"btn_red_order") forState:UIControlStateNormal];
      }
      else {
          [self.btnAdd setTitle:NSLocalizedString(@"btn_use_coupon", @"btn_use_coupon") forState:UIControlStateNormal];
          
      }

		 // NSString *idMeal = (couponObj != nil ) ? couponObj.idMeal : dealObj.idMeal;


		 if (![NSString isEmpty: self.couponObj.idMeal] && [self.couponObj.idMeal integerValue]>0)
		 {
			_btnAdd.hidden = NO;

		 }

    
    //cell.lblDate.text = [NSString stringWithFormat:@"%@-%@",NSLocalizedString(@"lbl_expiry_date", @"lbl_expiry_date"),[DateTimeUtil displayDateFromServerGraphDateStr:dealOfDayResponse.expiredDate]];

    
    self.lblExpire.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"EffectUntil", @"EffectUntil"),[DateTimeUtil displayDateFromServerGraphDateStr:self.couponObj.expiredDate]];
    self.lblComment.text = [NSString stringWithFormat:@"%@:",NSLocalizedString(@"ForClarification", @"ForClarification")];
    
    
         if(couponObj.isbarcode)
         {
             self.imgBarcode.image = [self generateCode:couponObj.barcode];
             self.imgBarcode.hidden = NO;
            self.lblBarcodeText.text=couponObj.barcode;
             self.con_BarcodeHeight.constant =128;
            self.barcodeView.hidden = NO;
         }
         else
         {
             self.imgBarcode.image = [self generateCode:couponObj.barcode];
             self.imgBarcode.hidden = YES;
             
           self.con_BarcodeHeight.constant = 0;
             self.barcodeView.hidden = YES;
         }

		 // self.cons_viewdetailHeight.constant = CGRectGetMaxY(self.lblMarks.frame)+self.lblMarks.frame.origin.y+self.lblMarks.frame.size.height+40;
		 [self.view_DetailBck updateConstraints];
		 [self.view_DetailBck layoutIfNeeded];
        self.cons_viewdetailHeight.constant=CGRectGetMaxY(self.lblComment.frame)+20;
		[self.view layoutIfNeeded];

     }
     else
     {

		// CGSize stringSize;
		 // stringSize = [self getStringSize:self.lblName andString:self.dealObj.name];
		 [self.lblName setText:self.dealObj.name];
		 // self.consLabelName_Height.constant = stringSize.height;
		 // stringSize = [self getStringSize:self.lblOffer andString:self.dealObj.offer];
		 [self.lblOffer setText:self.dealObj.offer];
		 //self.lbl_OfferHeight.constant = stringSize.height;
		 self.lblOfferTitle.text = [NSString stringWithFormat:NSLocalizedString(@"OfferTitle", @"OfferTitle")];
		 self.lblOfferTitle.textColor = COLOR_TITLE_NEVY_BLUE;
		// stringSize = [self getStringSize:self.lblDesc andString:self.dealObj.des];

		 [self.lblDesc setText:self.dealObj.des];
		 self.lblDescTitle.text = [NSString stringWithFormat:NSLocalizedString(@"ExtendedInformation", @"ExtendedInformation")];
		 self.lblDesc.hidden=YES;
		 self.lblDescTitle.hidden=YES;
		 //self.consLabelDesc_Height.constant =  stringSize.height; //FIXED - 1.0(7) by RR


		 self.consLabelDesc_Height.constant = 0.0;
		 self.consDesc_Title_Height.constant = 0.0;

		 self.lblMarksTitle.text = [NSString stringWithFormat:@"%@:",NSLocalizedString(@"lbl_marks", @"lbl_marks")];
		 // stringSize = [self getStringSize:self.lblMarks andString:self.dealObj.marks];
		 [self.lblMarks setText:self.dealObj.marks];
		 //self.cons_lblMarkHeight.constant = stringSize.height;
		 [self updateLblSize];
		 if([self.dealObj.price1 integerValue] == 0)
			 self.lblPrice1.hidden = YES;
		 if([self.dealObj.price2 integerValue] == 0)
			 self.lblPrice2.hidden = YES;

		 [self.lblPrice1 setTextColor:[UIColor lightGrayColor]];
		 [self.lblPrice1 setStrikethrough:YES];
		 [self.lblPrice2 setTextColor:COLOR_MAIN_PRICE];

		 if([self.dealObj.idTypeSeals integerValue] == 1)
		 {
			 self.lblPrice1.hidden = NO;
			 [self.lblPrice1 setText: [NSString stringWithFormat:@"%@%@",SHEKEL,self.dealObj.price1]];
			 [self.lblPrice2 setText:[NSString stringWithFormat:@"%@%@",SHEKEL,self.dealObj.price2]];
		 }
		 else {
			 self.lblPrice1.hidden = YES;
			 self.lblPrice2.text = [self.dealObj dealPriceFormatedValue];
		 }

		 [self.lblPrice2 sizeToFit];
		 self.constatintDiscPriceHeight.constant = CGRectGetHeight(self.lblPrice2.frame);

		 [self.logoImg sd_setImageWithURL:[self baseImageURL:self.dealObj.logobusiness] placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
		 [self.bussinessImage sd_setImageWithURL:[self baseImageURL:self.dealObj.image] placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
		 self.bussinessImage.image= [self imageWithImage:self.bussinessImage.image scaledToWidth:320.0f];


		 if ([self.str_module isEqualToString:@"Deal"])
		 {
			 [self.btnAdd setTitle:NSLocalizedString(@"btn_red_order", @"btn_red_order") forState:UIControlStateNormal];
		 }
		 else {
			 [self.btnAdd setTitle:NSLocalizedString(@"btn_use_coupon", @"btn_use_coupon") forState:UIControlStateNormal];

		 }


		 if (![NSString isEmpty: self.dealObj.idMeal] && [self.dealObj.idMeal integerValue]>0)
		 {
			 _btnAdd.hidden = NO;

		 }
		 

		 self.lblExpire.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"EffectUntil", @"EffectUntil"),[DateTimeUtil displayDateFromServerGraphDateStr:self.dealObj.expiredDate]];
		 self.lblComment.text = [NSString stringWithFormat:@"%@:",NSLocalizedString(@"ForClarification", @"ForClarification")];


		 if(couponObj.isbarcode)
		 {
			 self.imgBarcode.image = [self generateCode:dealObj.barcode];
			 self.imgBarcode.hidden = NO;
			 self.lblBarcodeText.text=couponObj.barcode;
			 self.con_BarcodeHeight.constant =128;
			 self.barcodeView.hidden = NO;
		 }
		 else
		 {
			 self.imgBarcode.image = [self generateCode:dealObj.barcode];
			 self.imgBarcode.hidden = YES;

			 self.con_BarcodeHeight.constant = 0;
			 self.barcodeView.hidden = YES;
		 }

		 // self.cons_viewdetailHeight.constant = CGRectGetMaxY(self.lblMarks.frame)+self.lblMarks.frame.origin.y+self.lblMarks.frame.size.height+40;

		 // self.cons_viewdetailHeight.constant=self.lblMarks.frame.origin.y+self.lblMarks.frame.size.height+self.lblOffer.frame.origin.y+self.lblOffer.frame.size.height+50;
		 [self.view_DetailBck updateConstraints];
		 [self.view_DetailBck layoutIfNeeded];
		 self.cons_viewdetailHeight.constant=CGRectGetMaxY(self.lblComment.frame)+20;
		 [self.view layoutIfNeeded];

	 }
	[self performSelector:@selector(resetScrollContentSize) withObject:nil afterDelay:0.0];
}

-(void) resetScrollContentSize{
	CGFloat h = CGRectGetMaxY(self.BottomView.frame);
	CGSize oldSize = self.detailScrollView.contentSize;
	oldSize.height = h + 20;
	self.detailScrollView.contentSize = oldSize;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
	float oldWidth = sourceImage.size.width;
	float scaleFactor = i_width / oldWidth;

	float newHeight = sourceImage.size.height * scaleFactor;
	float newWidth = oldWidth * scaleFactor;

	UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
	[sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}

//AddButon : Click handling
- (IBAction)onClickAddButton:(id)sender
{
    NSString *idMeal = (couponObj != nil ) ? couponObj.idMeal : dealObj.idMeal;
    
    if (idMeal && ![idMeal isEmpty])
    {
		//UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		NSString *subCat = (couponObj)?couponObj.idCategor:dealObj.idCategor; //

        if([subCat integerValue] == 64)
        {
              [self fetchMealsOfBusiness:couponObj.idBusiness subCategory:couponObj.idCategor];//Call API
                //CALL API TO FETCH MEALS OF BUSINESS ON SUCCESS  SEND TO HOLDERVC
           
            return;
        }
        else if([idMeal integerValue] > 0 )
        {
			//if([[[AppGlobalData sharedManager] getBussinessCart:couponObj.idBusiness] count] > 0)
			///{
				ShoppingCartSummaryVC *summaryVC = [self.storyboard instantiateViewControllerWithIdentifier:kShoppingCartSummaryVC_SB_ID];
				summaryVC.isFromCart = NO;
			    summaryVC.businessId = (couponObj)?couponObj.idBusiness:dealObj.idBusiness;
				summaryVC.selectedBussiness = nil;
				summaryVC.wsCouponsResponse = (couponObj)?couponObj:dealObj;
				[self.navigationController pushViewController:summaryVC animated:YES];
			//}

            return;
        }
     }
    
     //TODO  : will add barcode later
    [self askBarcodePassword];
    
}
#pragma mark-API call
-(void)fetchMealsOfBusiness:(NSString *)bussinessId subCategory:(NSString *)subCategory
{
	[[ConnectionsManager sharedManager] fetchMealsOfBusiness_withdelegate:bussinessId subCategory:subCategory delegate:self];
	[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}

#pragma mark-API Response
- (void) success:(WSBaseResponse *)response
{
	[DejalActivityView removeView];

	if([response.method isEqualToString:mFetchMealForBusinessByCatSub])
	{
 		WSBusinessCatResponse *bussResponse = [[WSBusinessCatResponse alloc] initWithDictionary:response.respondeBody];
            
		if(bussResponse.meals && bussResponse.meals.count)
		{
		CategoryForMealHolderVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:kCategoryForMealHolder_SB_ID];
		vc.selectedBussiness = bussResponse.business;
		vc.categoryList = bussResponse.meals;
		vc.baseviewTitle = bussResponse.business.name;
            vc.isFromDealCouponScreen = TRUE; //SanC
          NSString *idMeal = (couponObj != nil ) ? couponObj.idMeal : dealObj.idMeal;
            if(![idMeal isEmpty] && [idMeal intValue] > 0)
            {
                vc.dealCouponResponse = (couponObj != nil ) ? couponObj : dealObj;
              //  [self.navigationController pushViewController:vc animated:NO];
                [self pushToCategoryHolderVC:vc flag:TRUE];
            }
            else{
                [self.navigationController pushViewController:vc animated:YES];
            }
            
		
		}
		else {
			//put message no coupon founds TOAST
		}

	}
}

//SanC
-(void) pushToCategoryHolderVC:(CategoryForMealHolderVC *) vc flag:(BOOL) pushToInnerPage
{
    if(pushToInnerPage)
    {
        
        CategoryForMealDetailVC *catMealDetailVC = nil;
        for (WSCategoryMealResponse *catMeal in vc.categoryList)
        {
            for(WSMealsResponse *mealObj in catMeal.mealsList)
            {
                if([mealObj.id isEqualToString:vc.dealCouponResponse.idMeal])
                {
                    catMealDetailVC = [self getVCWithSB_ID:kCategoryForMealDetailVC_SB_ID];
                    //Fixed issue : when same item added to cart twice
                    catMealDetailVC.mealResponse = [mealObj copyObject];
                    catMealDetailVC.businessId = catMeal.idBusiness;
                    break;
                }
            }
            
        }
        
        if(catMealDetailVC)
        {
            NSMutableArray *navArr = [self.navigationController.viewControllers mutableCopy];
            [navArr addObject:vc];
            [navArr addObject:catMealDetailVC];
            [self.navigationController setViewControllers:navArr animated:YES];
            return;
        }
        
        
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) failure:(WSBaseResponse *)response
{
	[DejalActivityView removeView];
	if([response.method isEqualToString:mFetchMealForBusinessByCatSub])
	{

	}
}



-(void) askBarcodePassword{
    UIAlertView *inputAlterView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirmation", @"Confirmation") message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
    inputAlterView.alertViewStyle = UIAlertViewStylePlainTextInput;
    inputAlterView.tag=100;
    [inputAlterView show];
    
}

-(void) reTryAlert{
    UIAlertView *inputAlterView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"msg_wrong_passord", @"You want to try again?") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"lbl_try_again", @"Try Again") otherButtonTitles:NSLocalizedString(@"lbl_btn_close", @"lbl_btn_close"), nil];
    
    inputAlterView.tag=200;
    [inputAlterView show];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if(alertView.tag == 100)
    {
        [self.view resignFirstResponder];
        if (alertView.cancelButtonIndex == buttonIndex)
        {
            NSString *title = [alertView textFieldAtIndex:0].text;
            NSString *code = (couponObj != nil ) ? couponObj.code : dealObj.code;
            
            if([title isEqualToString:code])
            {
                if(couponObj)
                {
                    couponObj.isbarcode = YES;
                    _btnAdd.hidden = YES; // when barcode shown hide button
                    [self displayBarcodeImage];
                }
            }
            else
            {
               // [[[[iToast makeText:NSLocalizedString(@"wrongBarCode", @"wrongBarCode")] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] show];
                
                [self performSelector:@selector(reTryAlert) withObject:nil afterDelay:0.0];
                
            }
        }
    }
    else if(alertView.tag == 200)
    {
        
        if (alertView.cancelButtonIndex == buttonIndex)
        {
            [self performSelector:@selector(askBarcodePassword) withObject:nil afterDelay:0.0];
            
        }
    }
}


-(void) displayBarcodeImage
{
    self.imgBarcode.image =  [self generateCode:couponObj.barcode];
    self.imgBarcode.hidden = NO;
    self.lblBarcodeText.text=couponObj.barcode;
    self.con_BarcodeHeight.constant =128;
    self.barcodeView.hidden = NO;
}


- (UIImage*)generateCode:(NSString*)barcode {
    
    NSString *filtername = nil;
            filtername = @"CICode128BarcodeGenerator";
            
    CIFilter *filter = [CIFilter filterWithName:filtername];
    [filter setDefaults];
    
    NSData *data = [kText dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKey:@"inputMessage"];
    
    CIImage *outputImage = [filter outputImage];
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:outputImage
                                       fromRect:[outputImage extent]];
    UIImage *image = [UIImage imageWithCGImage:cgImage
                                         scale:1.
                                   orientation:UIImageOrientationUp];
    
    // Resize without interpolating
    CGFloat scaleRate = self.imgBarcode.frame.size.width / image.size.width;
    UIImage *resized = [self resizeImage:image
                             withQuality:kCGInterpolationNone
                                    rate:scaleRate];
    
   // self.imgBarcode.image = resized;
    
    CGImageRelease(cgImage);
    
    return resized;
}

- (UIImage *)resizeImage:(UIImage *)image
             withQuality:(CGInterpolationQuality)quality
                    rate:(CGFloat)rate
{
    UIImage *resized = nil;
    CGFloat width = image.size.width * rate;
    CGFloat height = image.size.height * rate;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, quality);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    resized = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resized;
}


- (IBAction)onClickMazeButton:(id)sender
{
    if (couponObj!=nil)
     {
		 // NSLog(@"coupan object=%@",self.couponObj);
         [self navigateToLatitude:[self.couponObj.latitude doubleValue] longitude:[self.couponObj.longitude doubleValue] address:@""];
    }
    else
    {
		// NSLog(@"coupan object=%@",self.dealObj);
        [self navigateToLatitude:[self.dealObj.latitude doubleValue] longitude:[self.dealObj.longitude doubleValue] address:@""];
    }
	
}
- (IBAction)onClickCallButton:(id)sender
{
    if (couponObj!=nil)
     {
         [self actionToCall:self.couponObj.phone];
    }
    else
    {
        [self actionToCall:self.dealObj.phone];
    }
}
- (IBAction)showinfo:(id)sender
{
    [appDelegate.window addSubview:[self getReportErrView]];
}
-(ReportError *) getReportErrView{
    
    //WSCouponsResponse *wsCoiponsRes = [array objectAtIndex:selectedDeal];
    //WSBusinessCatResponse *bussResponse =
    reportErr=[ReportError loadFromNibWithFrame:CGRectMake(0, 0,appDelegate.window.frame.size.width,appDelegate.window.frame.size.height )];
    UITapGestureRecognizer *tapgest = [[UITapGestureRecognizer alloc] initWithTarget:reportErr action:@selector(hideKey)];
    [reportErr addGestureRecognizer:tapgest];
    tapgest.delegate = reportErr;
    [reportErr fillData:couponObj.id];
    return reportErr;
}

@end