//
//  ViewController.h
//  VIPCard
//
//  Created by SanC on 17/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

//#define default_LogoHeightConstant 108.0f
//#define default_YavneHeightConstant 85.0f
//#define default_viewHeightConstant 280.0f
//#define default_skipButtonHeightConstant 40.0f
//#define default_usernameHeightConstant 40.0f
//#define default_passwordHeightConstant 40.0f
//#define default_loginHeightConstant 40.0f
//
//
//#define Iphone4s_LogoHeightConstant 80.0f
//#define Iphone4s_YavneHeightConstant 65.0f
//#define Iphone4s_viewHeightConstant 240.0f
//#define Iphone4s_skipButtonHeightConstant 30.f
//#define Iphone4s_usernameHeightConstant 30.f
//#define Iphone4s_passwordHeightConstant 30.f
//#define Iphone4s_loginHeightConstant 30.f


@interface ViewController : BaseViewController<UITextFieldDelegate>
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeightConstant;
//
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *yavneHeightConstant;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstant;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *skipButtonHeightConstant;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *usernameHeightConstant;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordHeightConstant;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginHeightConstant;


@property (weak, nonatomic) IBOutlet UIButton *btnCheckTermsCond;
- (IBAction)onClickCheckTermsCondButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *checkImg;

- (IBAction)onClickForgotPasswordButton:(id)sender;

-(void)loginAPI:(NSString*)str_email password:(NSString*)str_pwd;
- (IBAction)btn_sign_in:(id)sender;
- (IBAction)btn_sign_up:(id)sender;
@end

