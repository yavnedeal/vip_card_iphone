//
//  BaseCoreService.h
//  ChefsOrder
//
//  Created by Sanjeev Choudhary on 10/01/15.
//  Copyright (c) 2015 @ChefsOrder.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataBase.h"
#import "ConnectionsManager.h"
//#import "AppConstant.h"

#define INVALID_RESPONSE_CODE  9999

@protocol BaseCoreServiceDelegate <NSObject>

@required
- (void) onCoreServiceResponse:(NSString *) opType withData:(id) data withError:(CustomError *)error;
@end

@interface BaseCoreService : NSObject
{
	ConnectionsManager *connManager;
	NSManagedObjectContext *moContext;
	id<BaseCoreServiceDelegate> mainDelegate;
	id currentNewRecord;
}

@property (nonatomic,strong) id<BaseCoreServiceDelegate> mainDelegate;

-(id) initWithDelegate:(id<BaseCoreServiceDelegate>) delegate;

-(void) deleteAllEntityRecords:(NSString *) tableName;
-(NSString *) encodeStringData:(NSString *) unencodedString;

-(void) triggerDelegateWithType:(NSString *) type data:(id) aData error:(CustomError *) error;



@end
