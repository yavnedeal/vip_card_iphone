//
//  KeyConstant.h
//  VIPCard
//
//  Created by Vishal Kolhe on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#ifndef KeyConstant_h
#define KeyConstant_h

#define safeAssign(dest, src) \
{	id value = src; \
if(value) \
dest = value; }

#define SAFE(src) \
src!=nil?src:NSNULL

#define SAFE_DEF(value, default) ((value==nil)?(default):(value))

#define NSNULL [NSNull null]
#define IsNULL(val) (val==nil || [val isKindOfClass:[NSNull class]])


#endif /* KeyConstant_h */
//Constant for story board

#define kViewController_SB_ID @"SB_ID_ViewController"
#define kCommonCollectionVC_SB_ID @"SB_ID_CommonCollectionVC"
#define kScrollViewImageVC_SB_ID @"SB_ID_ScrollViewImageVC"
#define kDetailedVC_SB_ID @"SB_ID_DetailedVC"
#define kCustomTableVC_SB_ID @"SB_ID_CustomTableVC"
#define kVIPListVC_SB_ID @"SB_ID_VIPListVC"
#define kSortAlphabetVC_SB_ID @"SB_ID_SortAlphabetVC"
#define kScrollDetail_SB_ID @"SB_ID_ScrollDetailVC"
#define kRevealVC_SB_ID @"SB_ID_RevealVC"

#define kMainScreenVC_SB_ID @"SB_ID_MainScreenViewController"

#define kFindBussinessList       @"SB_ID_FindBussinessListVC"
#define kDealOfTheDay       @"SB_ID_DealOfDaysVC"
#define kBussinessList_VC   @"SB_ID_BussinessListVC"
#define kAddressFormVC   @"SB_ID_AddressFormVC"
#define kCategoryForMealListVC  @"SB_ID_CategoryForMealListVC"
#define kHomeVC @"SB_ID_HomeVC"
#define kAboutBussiness @"SB_ID_AboutBussiness"
#define kBussinessList  @"SB_ID_BussinessList"
#define kFindBussinessHolderVC @"SB_ID_FindBussinessForMealHolderVC"
#define kFieldCategoryFilterVC @"SB_ID_FieldCategoryFilterVC"
#define kFieldCategoryDetailVC @"SB_ID_FieldCategoryFilterDetailVC"

#define kUserRegistrationFilterVC @"SB_ID_UserRegistrationVC"
#define kUserLoginFilterVC @"SB_ID_UserLoginVC"
#define kPopUPViewFromHomeScreen "SB_ID_PopUpHome"



#define kGalleryVC @"SB_ID_GalleryVC"


//define key for constant used to save on disk
#define kGeoWarningAlertDisplayed @"kGeoWarningAlertDisplayed"
#define kCategoryForMealHolder_SB_ID    @"SB_ID_CategoryForMealHolderVC"
#define kCategoryForMealDetailVC_SB_ID  @"SB_ID_CategoryForMealDetailVC"
#define kMarketPlaceHolderVC_SB_ID  @"SB_ID_MarketPlaceHolderVC"
#define kMarketProductVC  @"SB_ID_MarketProductVC"

//Segue Constant

#define kSegueToParentHome  @"toHomeSegue"
#define kSegueToDriverHome @"toDriverHomeSegue"

#define kShoppingCartSummaryVC_SB_ID   @"SB_ID_ShoppingCartSummaryVC"

#define kLastLoggedAppUserInfo @"kLastLoggedAppUserInfo"
#define kAppUserSettingInfo @"kAppUserSettingInfo"

#define kNotificationApplicationModeChanged @"NotificationApplicationModeChanged"

#define kMarketDealDetailVC_SB_ID   @"SB_ID_MarketDealDetailVC"

#define WINDOW  [[UIApplication sharedApplication] keyWindow]


//Defaut key

#define KEY_USER_LOGIN_EMAIL @"email"
#define KEY_USER_LOGIN_EXPIRY_TIME @"expiry_time"
#define KEY_USER_LOGIN_NAME @"name"
#define KEY_USER_LOGIN_TOKEN @"token"
#define KEY_USER_LOGIN_USERID @"userid"




