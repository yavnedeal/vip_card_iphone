//
//  ViewController.m
//  VIPCard
//
//  Created by SanC on 17/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()<ServerResponseDelegate>
{
	NSMutableData *receivedData;
	NSDictionary *jsonDictionary;
	NSDictionary *jsonDict;
    
    BOOL isChecked;
}
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *password;

- (IBAction)enterButton:(id)sender;



- (IBAction)skipButton:(id)sender;


@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
        [self showBackButton:YES];
	 receivedData=[[NSMutableData alloc]init];

//	NSError *error;
//	jsonDictionary=[NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&error];


	//[self customScreen];
//	NSString *post = [NSString stringWithFormat:@"Username=%@&Password=%@",@"username",@"password"];
//	NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//	NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
//	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//	[request setURL:[NSURL URLWithString:@"http://test.enovate-it.com/vipCards/api/login"]];
//	[request setHTTPMethod:@"POST"];
//	[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//	[request setHTTPBody:postData];
//	NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//	if(conn) {
//		NSLog(@"Connection Successful");
//	} else {
//		NSLog(@"Connection could not be made");
//	}
	// This method is used to receive the data which we get using post method.
    
    
    
    

}

-(void)loginAPI:(NSString*)str_email password:(NSString*)str_pwd
{
    NSMutableDictionary *dict_login=[[NSMutableDictionary alloc] init];
    [dict_login setObject:str_email forKey:@"email"];
    [dict_login setObject:str_pwd forKey:@"password"];
    [[ConnectionsManager sharedManager]login_withdelegate:dict_login delegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
    
    
}

- (IBAction)btn_sign_in:(id)sender
{
    if (self.userName.text.length==0 ||[self.userName.text isEqualToString:@""])
     {
         [self showAlertview:NSLocalizedString(@"err_reg_email_none","err_reg_email_none")];
     }
    
    else if (![self NSStringIsValidEmail:self.userName.text])
    {
       [self showAlertview:NSLocalizedString(@"err_reg_email_x","err_reg_email_x")];
    }
    
    else if (self.password.text.length==0||[self.password.text isEqualToString:@""])
    
    {
        [self showAlertview:NSLocalizedString(@"err_reg_password_valide","err_reg_password_valide")];
    }
    
    else{
        NSString *str_Email=self.userName.text;
        NSString *str_pwd=self.password.text;
        [self loginAPI:str_Email password:str_pwd];
    }

//    UIViewController *dealVC = [self getVCWithSB_ID:kCommonCollectionVC_SB_ID];
//    [self.navigationController pushViewController:dealVC animated:YES];
   
}

- (IBAction)btn_sign_up:(id)sender {
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Connection Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
	UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Received Data" message:receivedString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
	NSError *error;
	jsonDictionary=[NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingMutableLeaves error:&error];
	jsonDict=[jsonDictionary objectForKey:@"result"];
	//jsonDict=[jsonDictionary objectForKey:@"responeBody"];

}

// Do any additional setup after loading the view, typically from a nib.

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self.navigationController.navigationBar setHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self.navigationController.navigationBar setHidden:NO];
}

//
//-(void)customScreen
//{
//	if(!IS_LARGE_SCREEN)
//	{
//		self.logoHeightConstant.constant = Iphone4s_LogoHeightConstant;
//		self.yavneHeightConstant.constant=Iphone4s_LogoHeightConstant;
//		self.skipButtonHeightConstant.constant=Iphone4s_skipButtonHeightConstant;
//		self.viewHeightConstant.constant=Iphone4s_viewHeightConstant;
//		self.usernameHeightConstant.constant=Iphone4s_usernameHeightConstant;
//		self.passwordHeightConstant.constant=Iphone4s_passwordHeightConstant;
//		self.loginHeightConstant.constant=Iphone4s_passwordHeightConstant;
//	}
//}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


-(BOOL)isvalid
{
	if ([self.userName.text isEmpty])
 {
	 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Do you want to say hello?"
													 message:@"More info..."
													delegate:self
										   cancelButtonTitle:@"Cancel"
										   otherButtonTitles:@"Say Hello",nil];
	 [alert show];

	 return NO;

//		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Enter your Username" message:@"Fill Your Details" delegate:self cancelButtonTitle:@"Cancel" , nil];
//	 [alert show];
	}
	 return YES;
}

- (IBAction)enterButton:(id)sender
{
//	NSMutableDictionary *params = [NSMutableDictionary dictionary];
//	[params setObject: @"yanivqs@gmail.com"  forKey: @"email"];
//	[params setObject: @"031787"  forKey: @"password"];
//	NSError *error;
//
//
//	NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
//
//	NSString *postLength = [NSString stringWithFormat:@"%lu", [postData length]];
//
//	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//	[request setURL:[NSURL URLWithString:@"http://test.enovate-it.com/vipCards/api/login"]];
//	[request setHTTPMethod:@"POST"];
//	[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//	[request setHTTPBody:postData];
//
//	NSURLConnection *urlConnection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
//	[urlConnection start];

}

- (IBAction)skipButton:(id)sender
{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE", @"ALERT_TITLE") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"btn_submit", @"btn_submit") otherButtonTitles:@"Cancel", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
     [[alertView textFieldAtIndex:0] setPlaceholder:@"XXXX"];
    [alertView show];
	
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView)
    {
        if(buttonIndex == 0)
        {
            UITextField *textField = [alertView textFieldAtIndex:0];
            
            if([textField.text isEqualToString:@"1234567"])
            {
                AppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
                [appdelegate openMainVC];
            }
            else{
                 [[[[iToast makeText:@"Incorrect passcode"] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            }
            
        }
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[_userName resignFirstResponder];
	return YES;
}

- (IBAction)onClickCheckTermsCondButton:(id)sender {

    if(isChecked)
    {
        [self.checkImg setImage:[UIImage imageNamed:@"checkbox_clicked"]];
    }
    else
    {
        [self.checkImg setImage:[UIImage imageNamed:@"checkbox"]];
    }
    
    isChecked = !isChecked;
}

- (IBAction)onClickForgotPasswordButton:(id)sender {
    AppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
    [appdelegate openMainVC];
}
#pragma mark-UserDefineMethods
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)enter_button:(id)sender {
}
//@gauri added//
//API responce//
- (void) success:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    if([response.method isEqualToString:@"login"])
    {
    
        NSUserDefaults *standard = [NSUserDefaults standardUserDefaults];
        [standard setValue:@"yanivqs@gmail.com" forKeyPath:KEY_USER_LOGIN_EMAIL];
        [standard setValue:@"(long)1464075943" forKeyPath:KEY_USER_LOGIN_EXPIRY_TIME];
        [standard setValue:@"yaniv" forKeyPath:KEY_USER_LOGIN_NAME];
        [standard setValue:@"f1c7d3925cd25239a3c7f45c720fe505" forKeyPath:KEY_USER_LOGIN_TOKEN];
        [standard setValue:@"1" forKeyPath:KEY_USER_LOGIN_USERID];


    UIViewController *dealVC = [self getVCWithSB_ID:kCommonCollectionVC_SB_ID];
     [self.navigationController pushViewController:dealVC animated:YES];

        
    }
}


- (void) failure:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    if([response.method isEqualToString:@"login"])
    {
    }
}

@end