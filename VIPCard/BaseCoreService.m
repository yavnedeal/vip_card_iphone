//
//  BaseCoreService.m
//  ChefsOrder
//
//  Created by Sanjeev Choudhary on 10/01/15.
//  Copyright (c) 2015 @ChefsOrder.com. All rights reserved.
//

#import "BaseCoreService.h"
#import "Toast+UIView.h"

@implementation BaseCoreService
@synthesize mainDelegate = mainDelegate;

-(id) initWithDelegate:(id<BaseCoreServiceDelegate>) delegate
{
	self = [super init];
	if(self)
	{
		[self initilizeScreen];
		if(delegate)
		   mainDelegate = delegate;
		
	}
	return self;
}


-(void) showMessage:(NSString *) msg
{
	[[[[UIApplication sharedApplication] keyWindow] window] makeToast:msg];
}

-(void) initilizeScreen
{
	connManager = [ConnectionsManager sharedManager];
	
	moContext = [CoreDataBase managedObjectContext];

}


-(void) deleteAllEntityRecords:(NSString *) tableName
{

	NSArray *records = [[CoreDataBase sharedInstanse] getListFromDB:tableName withPredicate:nil];
	//error handling goes here
	for (NSManagedObject * record in records)
	{
		[[CoreDataBase managedObjectContext] deleteObject:record];
	}
	NSError *saveError = nil;
	[[CoreDataBase managedObjectContext] save:&saveError];
}

-(NSString *) encodeStringData:(NSString *) unencodedString
{

	return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
																				 (CFStringRef)unencodedString,
																				 NULL,
																				 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
																				 kCFStringEncodingUTF8 ));
	
}

-(void) triggerDelegateWithType:(NSString *) type data:(id) aData error:(CustomError *) error
{
	if(mainDelegate && [mainDelegate respondsToSelector:@selector(onCoreServiceResponse:withData:withError:)]){
		[mainDelegate onCoreServiceResponse:type withData:aData withError:error];
	}

}




@end
