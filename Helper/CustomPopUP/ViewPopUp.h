//
//  ViewPopUp.h
//  VIPCard
//
//  Created by SanC on 13/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ViewPopUpDelegate <NSObject>
-(void)returnData:(NSString*)str;
@end

@interface ViewPopUp : UIView<UITableViewDataSource,UITableViewDelegate>
{
	//NSObject <ViewPopUpDelegate> *popUpDelegate;
	UITapGestureRecognizer *singleFingerTap;
	UIView *transparentView;

}
@property(nonatomic,strong)UITableView *tbl_View;
@property(nonatomic,strong) id <ViewPopUpDelegate> popUpDelegate;
@property(nonatomic,strong)NSArray *array_tblData;
@property(nonatomic,strong)NSDictionary *dict_choice;
@property(nonatomic,strong)NSString *header;


-(void) reloadScreenData;
- (void)adjustHeightOfTableview;





@end
