//
//  DateTimeUtil.m
//  ForumApp
//
//  Created by SanC on 05/03/14.
//  Copyright (c) 2014 Enovate. All rights reserved.
//

#import "DateTimeUtil.h"
#import "AppGlobalConstant.h"

#define ONE_DAY_SEC         86400
#define ONE_HOUR_SEC        3600
#define ONE_MIN_SEC         60

@implementation DateTimeUtil

static NSDateFormatter *dtFormatter;

+(NSDate *) localDateFromServerDateString:(NSString *)dateString
{
    //get source date from the server
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:DATE_FMT_DD_MM_YYYY__HH_MM_SS_12];
    NSDate *sourceDate = [dateFormat dateFromString:dateString];
    
    //set timezones
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithName:@"Asia/Jerusalem"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    //calc time difference
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    //set current real date
    return [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
}

+(NSDate *) serverDateFromLocalDate:(NSDate *) date
{
    
    NSDate *sourceDate = date;
    
    //set timezones
    NSTimeZone* destinationTimeZone = [NSTimeZone timeZoneWithName:@"Asia/Jerusalem"];
    NSTimeZone* sourceTimeZone  = [NSTimeZone systemTimeZone];
    
    //calc time difference
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    //set current real date
    return [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
}

+(NSDate *) localDateFromGMTDate:(NSDate *) date
{
    //set timezones
	NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];

    //calc time difference
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:date];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:date];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;

    //set current real date
    return [[NSDate alloc] initWithTimeInterval:interval sinceDate:date];
}

+(NSString *) stringFromDateTime:(NSDate *)date withFormat:(NSString *) aFormat
{
    if(!dtFormatter)
    {
        dtFormatter =  [[NSDateFormatter alloc] init];
    }
    [dtFormatter setDateFormat:aFormat];
    [dtFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    return [dtFormatter stringFromDate:date];
}

+(NSDate *)  dateFromStringDate:(NSString *) date withFormat:(NSString *) aFormat
{
	if(!dtFormatter)
		{
        dtFormatter =  [[NSDateFormatter alloc] init];
		}
    [dtFormatter setDateFormat:aFormat];
    
    return [dtFormatter dateFromString:date];
}

+(NSString *) getTimeLabel:(int) timeDiff
{
    if(timeDiff > 60)
    {
        int h = timeDiff/3600;
        int m = (timeDiff - h * 3600)/60;
        //  int s = timeDiff%60;
        if(h > 0)
        {
            if(h < 24)
                return [NSString stringWithFormat:@"%d hours ago",h];
            return nil;
        }
        else if(m>0)
        {
            
            return [NSString stringWithFormat:@"%d minutes ago",m];
        }
        
    }
    return [NSString stringWithFormat:@"%d seconds ago",timeDiff];
}


+(NSDate *) getZeroTZDate
{
	NSDate *sourceDate = [NSDate date];
    //set timezones
    NSTimeZone* destinationTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSTimeZone* sourceTimeZone  = [NSTimeZone systemTimeZone];
    
    //calc time difference
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    //set current real date
    NSDate *destDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
	return destDate;
}

+(NSString *) getZeroTZDateStr
{
	return [DateTimeUtil getGMTStringDateFrom:[DateTimeUtil getZeroTZDate]];
}

+(NSString *) getGMTStringDateFrom:(NSDate *) date
{
	return [DateTimeUtil stringFromDateTime:date withFormat:DATE_FMT_DD_MM_YYYY__HH_MM_SS_24];
}

+(NSDate *) getGMTDateFromStrDate:(NSString *) date
{
	return [DateTimeUtil dateFromStringDate:date withFormat:DATE_FMT_DD_MM_YYYY__HH_MM_SS_24];
}



+(NSString *) serverDateFromDisplayDate:(NSDate *) date
{
	return [DateTimeUtil stringFromDateTime:date withFormat:DATE_FMT_DD_MM_YYYY__HH_MM_SS_24];
}
+(NSString *) serverDateFromDisplayDateStr:(NSString *) date
{
	if(!date || [date isEmpty])
		return nil;
	NSDate *date2 = [DateTimeUtil dateFromStringDate:date withFormat:DATE_FMT_DD_MM_YYYY];

	return [DateTimeUtil serverDateFromDisplayDate:date2];

}
+(NSString *) displayDateFromServerDate:(NSDate *) date
{
  return [DateTimeUtil stringFromDateTime:date withFormat:GRAPH_DATE_FMT_DD_MM_YYYY];
}
+(NSString *) displayDateFromServerDateStr:(NSString *) date
{
	if(!date || [date isEmpty])
		return nil;
	NSDate *date2 = [DateTimeUtil dateFromStringDate:date withFormat:DATE_FMT_DD_MM_YYYY__HH_MM_SS_24];

	return [DateTimeUtil displayDateFromServerDate:date2];

}

+(NSString *) displayDateFromServerGraphDateStr:(NSString *) date
{
    if(!date || [date isEmpty])
        return nil;
    

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date1 = [dateFormat dateFromString:date];
    // Convert Date to string
    
    [dateFormat setDateFormat:DATE_FMT_DD_MM_YYYY__HH_MM_SS_24];
    NSLog(@"date =%@",date1);
    

//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:DATE_FMT_DD_MM_YYYY__HH_MM_SS_24];
//    NSDate *date1 = [dateFormat dateFromString:date];
//    
//    
//    NSString *dateString = date;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//    NSDate *dateFromString = [[NSDate alloc] init];
//    [dateFormatter setDateFormat:DATE_FMT_DD_MM_YYYY__HH_MM_SS_24];

    //dateFromString = [dateFormatter dateFromString:dateString];
    
    return [DateTimeUtil displayDateFromServerDate:date1];
}


+(NSString *) displayDateTimeFromServerDate:(NSDate *) date
{
	return [DateTimeUtil stringFromDateTime:date withFormat:DATE_FMT_DD_MM_YYYY__HH_MM_SS_24];
}
+(NSString *) displayDateTimeFromServerDateStr:(NSString *) date
{
	if(!date || [date isEmpty])
		return nil;
	NSDate *date2 = [DateTimeUtil dateFromStringDate:date withFormat:DATE_FMT_DD_MM_YYYY__HH_MM_SS_24];

	return [DateTimeUtil displayDateTimeFromServerDate:date2];

}
+(NSDate *) getTodaysDate
{
NSCalendar* myCalendar = [NSCalendar currentCalendar];
 NSDateComponents* components = [myCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
											fromDate:[NSDate date]];
[components setHour: 0];
[components setMinute: 0];
[components setSecond: 0];
	return [myCalendar dateFromComponents:components];

}
+(NSDate *) getDateAfterAddDays:(NSInteger) day
{
	NSCalendar* myCalendar = [NSCalendar currentCalendar];
	NSDateComponents* components = [myCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
												 fromDate:[NSDate date]];
	[components setDay:(components.day + day)];
	[components setHour: 0];
	[components setMinute: 0];
	[components setSecond: 0];
	return [myCalendar dateFromComponents:components];
}

+(NSDate *) getDateBeforeYears:(NSInteger) year
{
    NSCalendar* myCalendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [myCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                                 fromDate:[NSDate date]];
    [components setYear:-year];
    [components setMonth:components.month];
    [components setDay:components.day];
    
    NSDate * maxDate = [myCalendar dateByAddingComponents: components toDate: [NSDate date] options: 0];
    return maxDate;

}


+(NSDate *) weekStartDate
{
	NSDateComponents *component = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay| NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)  fromDate:[NSDate date]];
	[component setWeekday:1];
	[component setHour:0];
	[component setMinute:0];
	[component setSecond:1];

	return [[NSCalendar currentCalendar] dateFromComponents:component];

}
+(NSDate *) weekEndDate
{
	NSDateComponents *component = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay| NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)  fromDate:[NSDate date]];
	[component setWeekday:7];
	[component setHour:23];
	[component setMinute:59];
	[component setSecond:59];

	return [[NSCalendar currentCalendar] dateFromComponents:component];
}
+(NSDate *) monthStartDate
{
	NSDateComponents *component = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay| NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)  fromDate:[NSDate date]];
	[component setDay:1];
	[component setHour:0];
	[component setMinute:0];
	[component setSecond:1];

	return [[NSCalendar currentCalendar] dateFromComponents:component];
}
+(NSDate *) monthEndDate
{
	NSDateComponents *component = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay| NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)  fromDate:[NSDate date]];
	[component setDay:0];
	[component setMonth:[component month]+1];
	[component setHour:23];
	[component setMinute:59];
	[component setSecond:59];

	return [[NSCalendar currentCalendar] dateFromComponents:component];
}

+(NSDate *) dayStartDate:(NSDate *) date
{
	if(!date)
		date = [NSDate date];

    NSDateComponents *component = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay| NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)  fromDate:date];	[component setHour:0];
	[component setMinute:0];
	[component setSecond:01];

	return [[NSCalendar currentCalendar] dateFromComponents:component];
}

+(NSDate *) dayEndDate:(NSDate *) date
{
	if(!date)
		date = [NSDate date];

	NSDateComponents *component = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay| NSCalendarUnitWeekday|NSCalendarUnitWeekOfMonth|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)  fromDate:date];
	[component setHour:23];
	[component setMinute:59];
	[component setSecond:59];

	return [[NSCalendar currentCalendar] dateFromComponents:component];
}

+(NSString *)dayNameFromDate:(NSDate *)date
{
	if(!dtFormatter)
	{
        dtFormatter =  [[NSDateFormatter alloc] init];
	}
    [dtFormatter setDateFormat:@"EEEE"];

    return [dtFormatter stringFromDate:date];
}
+(NSDate *) getDateForHours:(NSInteger) hour
{
    NSCalendar* myCalendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [myCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                                 fromDate:[NSDate date]];
    NSTimeZone * destinationTZ = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [components setTimeZone:destinationTZ];
  
    [components setHour: hour];
    [components setMinute: 0];
    [components setSecond: 0];
    
    return [myCalendar dateFromComponents:components];
}

+(NSDate *) getDateAfterAddHours:(NSInteger) hour minute:(NSInteger)min date:(NSDate*)finaldate
{
    NSCalendar* myCalendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [myCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond
                                                 fromDate:finaldate];
    NSTimeZone * destinationTZ = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [components setTimeZone:destinationTZ];
    [components setHour: (components.hour + hour)];
    [components setMinute: (components.minute + min)];
    [components setSecond: 0];
    return [myCalendar dateFromComponents:components];
    
    
}
+(NSDate *) getDateAfterAddOneDay:(NSInteger) day hour:(NSInteger)hour
{
    NSCalendar* myCalendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [myCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                                 fromDate:[NSDate date]];
    NSTimeZone * destinationTZ = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [components setTimeZone:destinationTZ];

    [components setDay:(components.day + day)];
    [components setHour: 9];
    [components setMinute: 0];
    [components setSecond: 0];
    return [myCalendar dateFromComponents:components];
}
+(NSDate *) gecrrDate
{
    NSCalendar* myCalendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [myCalendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                                 fromDate:[NSDate date]];
    
    NSTimeZone * destinationTZ = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [components setTimeZone:destinationTZ];
    [components setHour: 0];
    [components setMinute: 0];
    [components setSecond: 0];
    return [myCalendar dateFromComponents:components];
    
}

//NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
//
//int year = [components year];
//int month = [components month];
//int day = [components day];

+(NSInteger) convertIntoSeconds:(NSInteger)h m:(NSInteger)m s:(NSInteger)s {
    
    return  ((h * 60) + m ) * 60 + s;
}

+(NSInteger)currentTimeOfDayFromDate:(NSDate *)date
{
    NSDateComponents *c = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:date];
    
    
    return [DateTimeUtil convertIntoSeconds:c.hour m:c.minute s:c.second];
}

+(NSString *)dayFromDate:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    NSInteger day = [components day];
    return [NSString stringWithFormat:@"%ld",(long)day];
}

+(NSString *)monthsFromDate:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    NSInteger day = [components month];
    return [NSString stringWithFormat:@"%ld",(long)day];

}
+(NSString *)yearFromDate:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    NSInteger day = [components year];
    return [NSString stringWithFormat:@"%ld",(long)day];
}
//[df setDateFormat:@"dd"];

+(NSDate *) toLocalTime:(NSDate *) fromDate
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: fromDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: fromDate];
}


// added by Sarika [01-08-2016]
+(BOOL)validateTime:(NSString*)time1 time:(NSString*)time2
{
    if (![NSString isEmpty:time1] && ![NSString isEmpty:time2] ) {
        
        NSDate *currDate = [self toLocalTime:[NSDate date]];
        NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
        [df1 setDateFormat:@"yyyy-MM-dd"];
        
        NSString *serDate = [df1 stringFromDate:[NSDate date]];
        time1 = [NSString stringWithFormat:@"%@ %@",serDate,time1];
        time2 = [NSString stringWithFormat:@"%@ %@",serDate,time2];
        
        NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
        [df2 setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Jerusalem"]];
        [df2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        //NSString *currtime = [df stringFromDate:currDate];
        NSDate *date1 = [df2 dateFromString:time1];
        NSDate *date2 = [df2 dateFromString:time2];
        date1 = [self toLocalTime:date1];
        date2 = [self toLocalTime:date2];
        
        
        NSComparisonResult result1 = [currDate compare:date1];
        NSComparisonResult result2 = [currDate compare:date2];
        
        if(result1 == NSOrderedDescending && result2 == NSOrderedAscending)
        {
            NSLog(@"currDate is later than date1");
            return YES;
        }
    }else{
        return YES;
    }
    //NSLog(@"time:%@",currtime);
    return NO;
}
+(NSMutableArray*)getYears:(NSDate*)date
{
    NSMutableArray *arrYears= [[NSMutableArray alloc]init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];

    
    
    for (int i =1; i <= 10; i++) {
        
        NSDate *currdate = [gregorian dateFromComponents:components];
        
        NSDateFormatter *df = [[NSDateFormatter alloc]init];
        [df setDateFormat:@"yyyy"];
        NSString *dtstr = [df stringFromDate:currdate];
        [arrYears addObject:dtstr];
        [components setYear:[components year]+1];
    }
    return arrYears;
}
+(NSDictionary*)getMonths
{
    NSCalendar *gregorian = [NSCalendar currentCalendar];//[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [gregorian components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    [components setMonth:1];
    NSDate *currdate = [gregorian dateFromComponents:components];
    NSMutableArray *arrMonths= [[NSMutableArray alloc]init];
    NSMutableArray *arrMonthsNum= [[NSMutableArray alloc]init];
    for (int i =0; i < 12; i++) {
        
                NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setMonth:i];
        NSDate *targetDate = [gregorian dateByAddingComponents:dateComponents toDate:currdate  options:0];
        
        NSDateFormatter *df = [[NSDateFormatter alloc]init];
        [df setDateFormat:@"MMM"];
        NSDateFormatter *df1 = [[NSDateFormatter alloc]init];
        [df1 setDateFormat:@"MM"];
        NSString *dtstr = [df stringFromDate:targetDate];
        [arrMonths addObject:dtstr];
        NSString *dtstr1 = [df1 stringFromDate:targetDate];
        [arrMonthsNum addObject:dtstr1];
    }
    NSDictionary *dicM = [[NSDictionary alloc] initWithObjects:@[arrMonths,arrMonthsNum] forKeys:@[@"arrM",@"arrNum"]];
    return dicM;
}
@end
