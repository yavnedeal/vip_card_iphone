//
//  CreditCardValidation.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 08/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "CreditCardValidation.h"


#define kMagicSubtractionNumber 48 // The ASCII value of 0

#import "CreditCardValidation.h"

@implementation CreditCardValidation
/*
-(BOOL)validateCard:(NSString *)cardNumber

{
    
    int Luhn = 0;
    
    // I'm running through my string backwards
    
    for (int i=0;i < [cardNumber length];i++)
        
    {
        
        NSUInteger count = [cardNumber length]-1; // Prevents Bounds Error and makes characterAtIndex easier to read
        
        int doubled = [[NSNumber numberWithUnsignedChar:[cardNumber characterAtIndex:count-i]] intValue] - kMagicSubtractionNumber;
        
        if (i % 2)
            
        {doubled = doubled*2;}
        
        NSString *double_digit = [NSString stringWithFormat:@"%d",doubled];
        
        if ([[NSString stringWithFormat:@"%d",doubled] length] > 1)
            
        {   Luhn = Luhn + [[NSNumber numberWithUnsignedChar:[double_digit characterAtIndex:0]] intValue]-kMagicSubtractionNumber;
            
            Luhn = Luhn + [[NSNumber numberWithUnsignedChar:[double_digit characterAtIndex:1]] intValue]-kMagicSubtractionNumber;}
        
        else
            
        {Luhn = Luhn + doubled;}
        
    }
    
    if (Luhn%10 == 0) // If Luhn/10's Remainder is Equal to Zero, the number is valid
        
        return true;
    
    else
        
        return false;
    
}
 */


-(BOOL) isIsraCard:(NSString *) cardno {
   
        if(cardno.length < 8 || cardno.length > 9) return false;
    
        NSString *diff = @"987654321";
    long sum = 0;
        if(cardno.length < 9) cardno = [@"0" stringByAppendingString:cardno];
        for(NSUInteger i=1;i<9;i++){
            NSRange range = NSMakeRange(i,1);
            NSString *a = [diff substringWithRange:range];
            NSString *b = [cardno substringWithRange:range];
            sum += [a intValue] * [b intValue];
        }
    if(sum % 11 == 0) return true;
    
    return false;
    
}

-(BOOL) isValidVisa:(NSString *)cardno
{
    //VISA
    NSString *regExPattern = @"^4[0-9]{12}(?:[0-9]{3})?$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:cardno
                                                     options:0
                                                       range:NSMakeRange(0, [cardno length])];
    return (regExMatches != 0) ? TRUE:FALSE;
}

-(BOOL) isValidMaster:(NSString *)cardno
{
    NSString *regExPattern1 = @"^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$";
    NSRegularExpression *regEx1 = [[NSRegularExpression alloc] initWithPattern:regExPattern1 options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches1 = [regEx1 numberOfMatchesInString:cardno
                                                       options:0
                                                         range:NSMakeRange(0, [cardno length])];
    return (regExMatches1 != 0) ? TRUE:FALSE;
}


- (BOOL) validateCard:(NSString*) cardno withType:(int )type {
    

    if(type == 1) //master
    {
        return [self isValidMaster:cardno];
    }
    else if(type == 2) //american
    {
        //American Express
        NSString *regExPattern2 = @"^3[47][0-9]{13}$";
        NSRegularExpression *regEx2 = [[NSRegularExpression alloc] initWithPattern:regExPattern2 options:NSRegularExpressionCaseInsensitive error:nil];
        NSUInteger regExMatches2 = [regEx2 numberOfMatchesInString:cardno
                                                           options:0
                                                             range:NSMakeRange(0, [cardno length])];
         return (regExMatches2 != 0) ? TRUE:FALSE;
    }
    else if(type == 3)
    {
        return [self isValidVisa:cardno];
    }
    else if(type == 4)
    {
       
        return [self isIsraCard:cardno];
    }
    else if(type == 5) //dinner
    {
        NSString *regExPattern3 = @"^3(?:0[0-5]|[68][0-9])[0-9]{11}$";
        NSRegularExpression *regEx3 = [[NSRegularExpression alloc] initWithPattern:regExPattern3 options:NSRegularExpressionCaseInsensitive error:nil];
        NSUInteger regExMatches3 = [regEx3 numberOfMatchesInString:cardno
                                                           options:0
                                                             range:NSMakeRange(0, [cardno length])];
        return (regExMatches3 != 0) ? TRUE:FALSE;
    }
    else if(type == 6)
    {
        return [self isValidVisa:cardno] || [self isValidMaster:cardno];
    }
   
    return true;
}
@end