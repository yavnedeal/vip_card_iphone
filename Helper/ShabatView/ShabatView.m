//
//  ShabatView.m
//  VIPCard
//
//  Created by SanC on 18/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "ShabatView.h"
#import "UIImageView+WebCache.h"

@implementation ShabatView


- (instancetype) initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
    if(self)
    {
        [self setBackgroundColor:[UIColor whiteColor]];
    }
	return self;
}

-(void) showInView:(UIView *) parentView{
	if(!parentView)
	{

		UIWindow *window = [[UIApplication sharedApplication] keyWindow];
		self.frame = window.bounds;
		[window addSubview:self];
		return;
	}

	self.frame = parentView.bounds;
	[parentView addSubview:self];
}

-(void)setImageOnView
{
	self.imgName = @"entryImg";
	//attach event
	self.img_View = [[UIImageView alloc] initWithFrame:self.frame];
	self.img_View.image= [UIImage imageNamed:self.imgName];
    [self.img_View sd_setImageWithURL:[NSURL URLWithString:entryImgURL] placeholderImage:[UIImage imageNamed:self.imgName] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.img_View.image = image;
        timer=[NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(onTick:) userInfo:nil repeats:NO];
    }];
    //self.img_View.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:entryImgURL]]];
	[self addSubview:self.img_View];
	[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
 //timer=[NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(onTick:) userInfo:nil repeats:NO];
	

}


-(void)showCountDownTimer
{
	self.imgName=@"img_Shabad";
	self.img_View.image= [UIImage imageNamed:self.imgName];
    [self.img_View sd_setImageWithURL:[NSURL URLWithString:shabatImgURL] placeholderImage:[UIImage imageNamed:self.imgName] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.img_View.image = image;
        timer=[NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(timerFired) userInfo:nil repeats:NO];
    }];
    
   // self.img_View.image= [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:shabatImgURL]]];
	// timer=[NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(timerFired) userInfo:nil repeats:NO];

}

-(void)timerFired
{
	NSLog(@"timerFired...15");
	exit(0);
}

-(void)onTick:(NSTimer*)timer
{
	[DejalActivityView removeView];
	NSLog(@"Tick...5");
	[self shabbatAPICall];
}

#pragma mark-API call
-(void)shabbatAPICall
{
	[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
	[[ConnectionsManager sharedManager] getShabatData_withdelegate:self];
}


#pragma mark-API Response
- (void) success:(WSBaseResponse *)response
{
	[DejalActivityView removeView];
	if([response.method isEqualToString:mIsShabat])
	{
		NSDictionary *dict_shabad=response.respondeBody;
		NSLog(@"dict=%@",dict_shabad);
		BOOL val = [[dict_shabad objectForKey:@"isShabat"] boolValue];
		if (val)
		{
			[self showCountDownTimer];
		}
		else
		{
			[self removeFromSuperview];
		}

	}
}

- (void) failure:(WSBaseResponse *)response
{
	[DejalActivityView removeView];
	exit(0);
	
}

@end
