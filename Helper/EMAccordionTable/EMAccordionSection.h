//
//  EMAccordionSection.h
//  UChat
//
//  Created by Ennio Masi on 10/01/14.
//  Copyright (c) 2014 Hippocrates Sintech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface EMAccordionSection : NSObject

//@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSString *title;
@property (nonatomic,strong) NSString *middelTitle;
@property (nonatomic, strong) NSObject *sectionData;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic, assign) BOOL isRequired;

@property (nonatomic, readonly) BOOL isMultiLevel;

@property (nonatomic,strong) NSString *txtMiddleTitle;


-(NSString *) dispMiddleTitle;

-(UIColor *) dispTitleColor;

@end
