//
//  EMAccordionSection.m
//  UChat
//
//  Created by Ennio Masi on 10/01/14.
//  Copyright (c) 2014 Hippocrates Sintech. All rights reserved.
//

#import "EMAccordionSection.h"
#import  "WSExtraForMealsResponse.h"
#import "ChoiceTypeGrt0.h"

@implementation EMAccordionSection

//@synthesize backgroundColor = _backgroundColor;
@synthesize items = _items;
@synthesize title = _title;
@synthesize middelTitle=_middelTitle;
@synthesize titleColor = _titleColor;
@synthesize titleFont = _titleFont;
@synthesize txtMiddleTitle = _txtMiddleTitle;
@synthesize isMultiLevel = _isMultiLevel;


-(void) setItems:(NSMutableArray *)items
{
	_items = items;

	//here set middleTitle;
	if(_items != nil && _items.count > 0)
	{
		WSExtraForMealsResponse *item =(WSExtraForMealsResponse *) [_items objectAtIndex:0];
		_txtMiddleTitle = item.nameChoice;
	}
	else
	{
		_txtMiddleTitle = nil;

	}

}

-(void) setSectionData:(NSObject *)sectionData
{
    if(sectionData && [sectionData isKindOfClass:[ChoiceTypeGrt0 class]])
    {
        _isMultiLevel = TRUE;
    }
    else
        _isMultiLevel = FALSE;
    
    _sectionData = sectionData;
}

-(UIColor *) dispTitleColor {

	if(_isMultiLevel && _items != nil && _items.count > 0)
	{
		//do something
		/*NSInteger counter = 0;
		NSInteger filledItem = 0;
		
		for (WSExtraForMealsResponse *item in _items) {
			
			if ([item isRequired])
            {
				counter = counter + 1;
                if([item hasSelectedData])
                {
                    filledItem = filledItem + 1;
                }
			}
			
		}

		if (counter > 0 && counter > filledItem)
		{
			_titleColor = [UIColor redColor];
			return _titleColor;
		}*/
		return [UIColor grayColor];
	}
    else if(!_isMultiLevel && self.sectionData)
    {
    
        WSExtraForMealsResponse *item = (WSExtraForMealsResponse *) self.sectionData;
        
        if([item isRequired] && ![item hasSelectedData])
        {
            _titleColor = [UIColor redColor];
            return _titleColor;
        }
        
    }
    else if( self.isRequired)
    {
        _titleColor = [UIColor redColor];
        return _titleColor;
    }
	_titleColor = [UIColor grayColor];
	return _titleColor;


}



-(NSString *) dispMiddleTitle {
	NSString *dispStr = @"";
	if(_txtMiddleTitle && _isMultiLevel)
		dispStr = _txtMiddleTitle;
	else if(!_isMultiLevel)
	{
		dispStr =  [((WSExtraForMealsResponse *)self.sectionData) dispSelectedData];

	}

	if( !dispStr || [dispStr isEmpty])
		return  _middelTitle;


	return dispStr;


}



@end