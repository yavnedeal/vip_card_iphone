//
//  CustomError.m
//  ReceiptLess
//
//  Created by SanC on 10/03/14.
//  Copyright (c) 2014 Enovate. All rights reserved.
//

#import "CustomError.h"
//#import "AppConstant.h"
#import "NSString+CommonForApp.h"

@implementation CustomError
@synthesize errorMessage = _errorMessage;


-(id) initWithCode:(NSInteger) code withMessage:(NSString *) errorMsg
{
	self = [super initWithDomain:@"com.enovate-it.apps.VIPCards" code:code userInfo:[NSDictionary dictionaryWithObjectsAndKeys:errorMsg,NSLocalizedDescriptionKey, nil]];
	if(self)
	{
	  _errorMessage = errorMsg;
	}
	
	return self;
}

+(CustomError *) customErrorFromError:(NSError *) error
{
	//NSString *errorMsg = [error localizedDescription];
	 NSString *errorMsg = [self userDefineMsgFromError:error];
	

	CustomError *customError =[[CustomError alloc] initWithCode:error.code withMessage:errorMsg];

	return customError;
}

+(CustomError *) customErrorWithMessage:(NSString *) message
{
	return [[CustomError alloc] initWithCode:101 withMessage:message];
}

+ (NSString *)userDefineMsgFromError:(NSError *)error
{
	NSString *errorMsg;
	switch (error.code)
	{
		case NSURLErrorUnsupportedURL:
		case NSURLErrorCannotFindHost:
		case NSURLErrorDNSLookupFailed:
		case NSURLErrorCannotConnectToHost:
		case NSURLErrorBadURL:
		case NSURLErrorBadServerResponse:
		case NSURLErrorDataNotAllowed:
			errorMsg = @"SERVER_NOT_RESPONDING";//[NSString localizedStringForKey:@"SERVER_NOT_RESPONDING"];
			break;
		default:
			errorMsg = [error localizedDescription];
			break;
	}

	return errorMsg;

	}

@end
