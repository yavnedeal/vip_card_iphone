//
//  BillingFormVC.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 02/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BillingFormVC.h"
#import "CardCell.h"
#import "CreditCardValidation.h"
#import "WSOrderData.h"
#import "ServiceManager.h"
#import "JSONUtil.h"
#import "CardData.h"
#import "WSAdditionalExtrasResponse.h"
//Manoj
#import "FactoryListData.h"
#import "FactoryCoreService.h"
#import "FactoryInfoContex.h"
#import "FTPopOverMenu.h"

@interface BillingFormVC ()<ServerResponseDelegate,CardCellDelegate, BaseCoreServiceDelegate>
{
    NSMutableArray *arrcardtypes;
    NSMutableArray *arrMonths,*arrYears,*arrTmp,*arrNum,*arrOrdTot,*arrCardObj;
    UIButton *selsender;
    BOOL restpay,editInfo;
    UITextField *actifText;
    float restCashAmt;
    
    //Manoj
    NSArray *factoryList;
    FactoryCoreService *factoryCoreService;
    FactoryInfoContex *factoryContext;
    
    float currentBaseFactoryHeight;
}
@end

@implementation BillingFormVC
static NSMutableArray *arrSettingTime;


@synthesize lblchk,btnchk,btnsend,btnsplit,tblcards,orderTotal,orderId,cnfMsg,sendEmail,sendFax,sendSms,dicOrder,logo,cartDic,myOrder,isFromMyOrder,botview,selectedBussiness;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    factoryCoreService = [[FactoryCoreService alloc] initWithDelegate:self];
    factoryContext = [FactoryInfoContex sharedManager];
   
    restpay = NO;
    //btnchk.enabled = NO;
    //lblchk.text = NSLocalizedString(@"mark to pay the rest with cash", nil);
    [btnsplit setTitle:NSLocalizedString(@"Click here for more card payment split", nil) forState:UIControlStateNormal];
    btnsplit.layer.borderWidth = 0.5;
    btnsplit.layer.borderColor = [UIColor grayColor].CGColor;
    //[btnsend setTitle:NSLocalizedString(@"Submit_Order", @"Submit_Order") forState:UIControlStateNormal];
    
    [self customiseFactoryUI];
    
    
    noOfCards = 1;
    arrYears = [[NSMutableArray alloc]init];
    //arrcardtypes = [NSArray arrayWithObjects:NSLocalizedString(@"choose card company", @"choose card company"),NSLocalizedString(@"MasterCard", @"MasterCard"),NSLocalizedString(@"American Express", @"American Express"),NSLocalizedString(@"Isracard", @"Isracard"),NSLocalizedString(@"Diners", @"Diners"),NSLocalizedString(@"Leumi card", @"Leumi card"), nil];
    arrcardtypes = [[NSMutableArray alloc]init];
    //[arrcardtypes addObject:@"לבחור חברת כרטיסי"];
    [arrcardtypes addObject:@"סוג כרטיס"];
    [arrcardtypes addObject:@"מסטר קארד"]; //master
    [arrcardtypes addObject:@"אמריקן אקספרס"]; //american express
    [arrcardtypes addObject:@"ויזה"]; // visa
    [arrcardtypes addObject:@"ישראכרט"];//isra
    [arrcardtypes addObject:@"דיינרס"]; //dinner
    [arrcardtypes addObject:@"לאומי קארד"]; //liumi
    
    arrTmp = [arrcardtypes mutableCopy];
    
    arrYears = [DateTimeUtil getYears:[NSDate date]];
    [arrYears insertObject:@"בחר שנה" atIndex:0];
    NSDictionary *dicM = [DateTimeUtil getMonths];
    arrMonths = [[NSMutableArray alloc]init];
    [arrMonths addObject:@"בחר חודש"];
    [arrMonths addObjectsFromArray:[dicM objectForKey:@"arrM"]];
    //arrMonths = [dicM objectForKey:@"arrM"];
    arrNum = [[NSMutableArray alloc]init];
    [arrNum addObject:@"0"];
    [arrNum addObjectsFromArray:[dicM objectForKey:@"arrNum"]];
    
    
    arrCardObj = [[NSMutableArray alloc]init];
    
    CardData *cardObj = [[CardData alloc]initWithDictionary:[NSDictionary dictionaryWithObjects:@[@"",@"",@"",@"",@"",@"",orderTotal,@""] forKeys:@[@"ownid",@"ownname",@"cardno",@"cardtype",@"expmon",@"expyr",@"ordamt",@"cvv"]]];
    [arrCardObj addObject:cardObj];
    
    float orderAmt = [orderTotal floatValue];
    restCashAmt = orderAmt;
    lblchk.text = [NSString stringWithFormat:@"סמן לתשלום יתרה במזומן-%@%.f",SHEKEL,restCashAmt];
    //[NSString stringWithFormat:@"%@%.f-סמן לתשלום יתרה במזומן",SHEKEL,orderAmt];
    
    arrOrdTot = [[NSMutableArray alloc] init];
    [arrOrdTot addObject:orderTotal];
    
    UITapGestureRecognizer *gest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKey)];
    [self.view addGestureRecognizer:gest];
    
    // Register notification when the keyboard will be show
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scrollKeyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scrollKeyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [tblcards reloadData];
    NSIndexPath *idxpath = [NSIndexPath indexPathForRow:0 inSection:0];
    CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
    cell.txtsumtopay.text = [NSString stringWithFormat:@"%.f",orderAmt]; //At start order amount
    
    [self showBackButton:YES];
    [self.navigationController.navigationBar setTintColor:[UIColor redColor]];
    
    
    NSArray *list = [factoryCoreService fetchFactoryList];
    if(!list || list.count<=0)
    {
        [self getfactoryList];
    }
    
}

-(void)customiseFactoryUI
{
    factoryContext = [FactoryInfoContex sharedManager];
    
    [self.lblFE setText:NSLocalizedString(@"factoryListTitle", nil)];
    [self.lblPC setText:NSLocalizedString(@"company_pay", nil)];
    [self.lblST setText:NSLocalizedString(@"setting_time_title", nil)];
    
    [self.dropDownFE setTitle:NSLocalizedString(@"factoryList_Placeholder", nil) forState:UIControlStateNormal];
    [self.dropDownST setTitle:NSLocalizedString(@"setting_time_placeholder", nil) forState:UIControlStateNormal];
    
    self.isCheckedFE = NO;
    self.isCheckedPC = NO;
    self.isCheckedST = NO;
    
    
    [self.dropDownFE setHidden:YES];
    [self.dropDownST setHidden:YES];
    
    [self setSettingTime];
    
    factoryCoreService = [FactoryCoreService sharedInstance:self];
    
    [self toggleTimeandCompanyviewby:true];
}


-(void) chkboxFE:(NSInteger)isCheckedFE
{
    if (isCheckedFE == 1) {
        _isCheckFE =YES;
    }
    else
    {
        _isCheckPC=YES;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return noOfCards;
}
// The data to return for the row and component (column) that's being passed in
//- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    return arrcardtypes[row];
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CardCell *cell = (CardCell*)[tableView dequeueReusableCellWithIdentifier:@"cardcell"];
    CardData *cardObj = [arrCardObj objectAtIndex:indexPath.row];
    
    cell.backgroundColor = [UIColor colorWithRed:226.0/255.0 green:218.0/255.0 blue:206.0/255.0 alpha:1];
    cell.btncardtype.tag = indexPath.row;
    cell.txtownid.tag = 151;
    cell.txtcardcvv.tag = 152;
    cell.txtcardno.tag = 153;
    
    cell.btnmonth.tag = indexPath.row;
    cell.btnyear.tag = indexPath.row;
    // [cell.btncardtype addTarget:self action:@selector(showCardTypes:) forControlEvents:UIControlEventTouchUpInside];
    // [cell.btnmonth addTarget:self action:@selector(showMPicker:) forControlEvents:UIControlEventTouchUpInside];
    // [cell.btnyear addTarget:self action:@selector(showYPicker:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.txtsumtopay.tag = indexPath.row;
    cell.txtsumtopay.text = [arrOrdTot objectAtIndex:indexPath.row];
    cell.cardCellDelegate = self;
    cell.currentIndexPath = indexPath;
       [cell populateData:cardObj];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) updateWithAttributeString:(UILabel *) lbl color:(UIColor *) color text:(NSString *)text secondText:(NSString *)newtext
{
    if(!text || [text isEmpty])
        return;
    
    NSString *str = lbl.text;
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableAttributedString *attributedText2=[[NSMutableAttributedString alloc] initWithString:str] ;
    
    //now set attributes
    [attributedText setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:lbl.font,NSFontAttributeName,nil] range:NSMakeRange(0, [str length])];
    [attributedText2 setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:lbl.font,NSFontAttributeName,nil] range:NSMakeRange(0, [str length])];
    NSRange range = [str rangeOfString:text];
    NSRange range2=[str rangeOfString:newtext];
    [attributedText setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName,nil] range:range];
    [attributedText2 setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName,nil] range:range2];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:lbl.text];
    [string addAttribute:NSForegroundColorAttributeName value:color range:range];
    [string addAttribute:NSForegroundColorAttributeName value:color range:range2];
    lbl.attributedText = string;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)showToolbar
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Next", @"Next") style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)], nil];
    
    
    
    [numberToolbar sizeToFit];
    //self.txtPhone.inputAccessoryView = numberToolbar;
    for (int i=0; i < noOfCards; i++) {
        
        NSIndexPath *idxpath = [NSIndexPath indexPathForRow:i inSection:0];
        CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
        
        cell.txtownid.inputAccessoryView = numberToolbar;
        cell.txtcardno.inputAccessoryView = numberToolbar;
        cell.txtcardcvv.inputAccessoryView = numberToolbar;
        cell.txtsumtopay.inputAccessoryView = numberToolbar;
    }
    
}
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
}

- (IBAction)splitcards:(id)sender {
    
    float remAmt = [self validateAmt];
    restCashAmt = remAmt;
    float orderAmt = [orderTotal floatValue];
    NSLog(@"remAmt: %.02f",remAmt);
    if (remAmt > 0 && (restCashAmt != orderAmt)) {
        
        //validate fields
        if(![self validateFields:TRUE])
        {
            NSString *strerr = @"יש למלא את הפרטים החסרים";//NSLocalizedString(@"All fields are mandatory", @"All fields are mandatory");
            [[[[iToast makeText:strerr] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            return;
        }
        
        noOfCards = noOfCards+1;
        //CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
        //cell.txtsumtopay.text = [NSString stringWithFormat:@"%.02f", remAmt];
        //lblchk.text = [NSString stringWithFormat:@"%@-%@%.02f",NSLocalizedString(@"mark to pay the rest with cash", @"mark to pay the rest with cash"),NSLocalizedString(@"shekel", @"shekel"),remAmt];
        //[cell setNeedsDisplay];
        [arrOrdTot addObject:[NSString stringWithFormat:@"%.f", remAmt]];
        CardData *cardObj = [[CardData alloc] init];
        cardObj.ordamt = [NSString stringWithFormat:@"%.f", remAmt];
        
        
        [arrCardObj addObject:cardObj];
        
        //[tblcards reloadData];
        NSIndexPath *idxpath = [NSIndexPath indexPathForRow:noOfCards-1 inSection:0];
        
        
        [tblcards beginUpdates];
        [tblcards insertRowsAtIndexPaths:@[idxpath] withRowAnimation:UITableViewRowAnimationRight];
        [tblcards endUpdates];
        [tblcards scrollToRowAtIndexPath:idxpath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }else{
        NSString *str = @"נא לערוך את הסכום על מנת לפצל לכרטיס נוסף";
        [[[[iToast makeText:str] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
    }
}

//added by Sarika [08/08/2016]
-(IBAction)sendData:(id)sender {
    NSMutableDictionary *mainDict = [NSMutableDictionary dictionary];
    NSMutableArray *cardArr = [[NSMutableArray alloc]init];

//gauri added
    if ( _isCheckFE && _isCheckPC) {
    
        NSMutableDictionary *dict = [dicOrder objectForKey:@"ordering"];
        [dict setObject:factoryContext.factoryid forKey:@"factoryId"];
        [dict setObject:factoryContext.paidByFactory forKey:@"paidByFactory"];
        
        [dict setObject:factoryContext.deliveryOrderTime forKey:@"orderDeliveryTime"];
        

        
        [dicOrder setObject:dict forKey:@"ordering"];
      
        [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
        [[ConnectionsManager sharedManager] submitOrder:dicOrder withdelegate:self] ;

    }
    else
    // API call
      if ([self validateFields:NO])
    {
        
        //Validate for amount
        float remAmount = [self validateAmt];
        if(remAmount != 0 && !restpay)
        {
            NSString *strerr = [NSString stringWithFormat:@"סך ההזמנה %@ יש לסמן לתשלום היתרה במזומן או לערוך את השדה המחיר לסגירת סכום העסקה המלא",orderTotal];
            [[[[iToast makeText:strerr] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            return ;
        }
        
        
        int lastCard = 0;
        if(restpay)
        {
            float remAmount = [self validateAmt];
            if(remAmount == 0)
                lastCard = 1;
        }
        
        for (int i=0; i < noOfCards - lastCard; i++) {
            
            CardData *cardObj = [arrCardObj objectAtIndex:i];
            // NSIndexPath *idxpath = [NSIndexPath indexPathForRow:i inSection:0];
            
            // CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
            
            NSString *mon =@"";//= [arrNum objectAtIndex:[arrMonths indexOfObject:cardObj.expmon]];
            if ([arrMonths containsObject:cardObj.expmon]) {
                mon = [arrNum objectAtIndex:[arrMonths indexOfObject:cardObj.expmon]];
            }
            NSString *expdate = [NSString stringWithFormat:@"%@%@",mon,cardObj.expyr];
            
            NSDictionary *carddic = [[NSDictionary alloc]initWithObjects:@[cardObj.ownname,cardObj.ownid,@"",expdate,cardObj.cvv,[self encodeCardNo:cardObj.cardno],cardObj.cardtype,cardObj.ordamt] forKeys:@[@"name",@"identityCard",@"phone",@"expiredDate",@"cvv",@"cardNUmber",@"idTypeCard",@"amount"]];
            [cardArr addObject:carddic];
            
        }
        
        float remAmt = [self validateAmt];
        if(lastCard == 1)
        {
            remAmt = [self remainingAmoutSkipLastCard];
        }
        
        NSString *restamt = [NSString stringWithFormat:@"%.f",remAmt];
        //[mainDict setObject:@"" forKey:@"orderId"];
        [mainDict setObject:cardArr forKey:@"cardData"];
        [mainDict setObject:restamt forKey:@"payRestAmtCash"];
        
        [dicOrder setObject:mainDict forKey:@"paydetails"];
        
        
        NSMutableDictionary *dict = [dicOrder objectForKey:@"ordering"];
        [dict setObject:factoryContext.factoryid forKey:@"factoryId"];
        [dict setObject:factoryContext.paidByFactory forKey:@"paidByFactory"];
        
        [dict setObject:factoryContext.deliveryOrderTime forKey:@"orderDeliveryTime"];
        
        [dicOrder setObject:dict forKey:@"ordering"];
        
        //[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
        //[[ConnectionsManager sharedManager] submitOrderPayment:mainDict withdelegate:self];
        [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
        [[ConnectionsManager sharedManager] submitOrder:dicOrder withdelegate:self] ;
    }else{
        float remAmt = [self validateAmt];
        if (!editInfo && restpay && arrCardObj.count == 1 && remAmt == 0)
        {
            CardData *cardObj = [arrCardObj objectAtIndex:0];
            cardObj.ordamt = 0;
            
            NSString *restamt = [NSString stringWithFormat:@"%.f",[orderTotal floatValue]];
            [mainDict setObject:cardArr forKey:@"cardData"];
            [mainDict setObject:restamt forKey:@"payRestAmtCash"];
            
            [dicOrder setObject:mainDict forKey:@"paydetails"];
            
            [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
            [[ConnectionsManager sharedManager] submitOrder:dicOrder withdelegate:self] ;
        }
        else{
            
            NSString *strerr;// = @"יש למלא את הפרטים החסרים";//NSLocalizedString(@"All fields are mandatory", @"All fields are mandatory");
            if (remAmt < 0) {
                strerr = @"סכום לתשלום גדול מסך ההזמנה";
            }else{
                strerr = @"יש למלא את הפרטים החסרים";
            }
            [[[[iToast makeText:strerr] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            
        }
    }
}



-(NSString*)encodeCardNo:(NSString*)cardno
{
    NSString *str = [cardno stringByReplacingOccurrencesOfString:@"1" withString:@"A"];
    str = [str stringByReplacingOccurrencesOfString:@"2" withString:@"B"];
    str = [str stringByReplacingOccurrencesOfString:@"3" withString:@"C"];
    str = [str stringByReplacingOccurrencesOfString:@"4" withString:@"D"];
    NSLog(@"%@", str);
    return str;
}
- (IBAction)checkRestPay:(id)sender {
    
    // NSIndexPath *idxpath = [NSIndexPath indexPathForRow:0 inSection:0];
    //  CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
    
    if (restpay) {
        restpay = NO;
        // restCashAmt = 0;
        // cell.txtsumtopay.text = orderTotal;
        //  lblchk.text = [NSString stringWithFormat:@"%@%.02f-סמן לתשלום יתרה במזומן",SHEKEL,restCashAmt];
        [btnchk setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
    }else{
        restpay = YES;
        
        
        float  rAmt = [self validateAmt];
        if(noOfCards == 1 && rAmt == 0)
        {
            restCashAmt = [orderTotal floatValue];
        }
        else {
            restCashAmt = [self remainingAmoutSkipLastCard];
        }
        /* if (![self validateFields]) {
         float orderAmt = [orderTotal floatValue];
         restCashAmt = orderAmt;
         
         cell.txtsumtopay.text = @"0";
         lblchk.text = [NSString stringWithFormat:@"%@%.02f-סמן לתשלום יתרה במזומן",SHEKEL,orderAmt];
         }
         */
        [btnchk setBackgroundImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
        lblchk.text = [NSString stringWithFormat:@"סמן לתשלום יתרה במזומן-%@%.f",SHEKEL,restCashAmt];
        //[NSString stringWithFormat:@"%@%.f-סמן לתשלום יתרה במזומן",SHEKEL,restCashAmt];
        
    }
}

-(int) cardTypeFromText:(NSString *) text
{
    int i = 0;
    for (NSString *txt in arrcardtypes)
    {
        
        if([text isEqualToString:txt])
            return i;
        i++;
    }
    
    return 0;
}

-(float)remainingAmoutSkipLastCard
{
    float orderAmt = [orderTotal floatValue];
    float total=0;
    
    for (int i=0; i < noOfCards-1; i++) {
        CardData *cardObj = [arrCardObj objectAtIndex:i];
        total = total + [cardObj.ordamt floatValue];
    }
    
    return orderAmt - total;
    
}

-(BOOL)validateFields:(BOOL) forSplit
{
    BOOL isValid = NO;
    editInfo = NO;
    int cnt = 0;
    CreditCardValidation *cc = [[CreditCardValidation alloc]init];
    float orderAmt = [orderTotal floatValue];
    
    int lastCard = 0;
    if(restpay && !forSplit)
    {
        float remAmount = [self validateAmt];
        if(remAmount == 0)
            lastCard = 1;
    }
    
    
    for (int i=0; i < noOfCards - lastCard; i++) {
        
        CardData *cardObj = [arrCardObj objectAtIndex:i];
        NSIndexPath *idxpath = [NSIndexPath indexPathForRow:i inSection:0];
        
        CardCell *cell = (CardCell*)[tblcards cellForRowAtIndexPath:idxpath];
        
        [self setErr:cell.txtname setflag:NO];
        [self setErr:cell.txtownid setflag:NO];
        [self setErrDD:cell.btncardtype setflag:NO];
        [self setErr:cell.txtcardno setflag:NO];
        [self setErr:cell.txtcardcvv setflag:NO];
        [self setErrDD:cell.btnmonth setflag:NO];
        [self setErrDD:cell.btnyear setflag:NO];
        
        if (cardObj.ownname.length <= 0) {
            [self setErr:cell.txtname setflag:YES];
            cnt++;
        }else{
            if(![self isValidOwnerName:cardObj.ownname])
            {
                [self setErr:cell.txtname setflag:YES];
                cnt++;
            }
            else
                editInfo = YES;
        }
        if (cardObj.ownid.length <= 0){
            [self setErr:cell.txtownid setflag:YES];
            cnt++;
        }else{
            
            if(![self isValidOwnerId:cardObj.ownid])
            {
                [self setErr:cell.txtownid setflag:YES];
                cnt++;
            }
            else
                editInfo = YES;
        }
        if (cardObj.cardtype.length <= 0){
            [self setErrDD:cell.btncardtype setflag:YES];
            cnt++;
        }else{
            editInfo = YES;
        }
        if (cardObj.cardno.length <= 0){
            [self setErr:cell.txtcardno setflag:YES];
            //NSString *strerr = @"מספר כרטיס שגוי";//NSLocalizedString(@"All fields are mandatory", @"All fields are mandatory");
            //[[[[iToast makeText:strerr] setGravity:iToastGravityBottom] setDuration:iToastDurationLong] show];
            cnt++;
        }
        
        if (cardObj.cvv.length <= 0){
            [self setErr:cell.txtcardcvv setflag:YES];
            cnt++;
        }else{
            editInfo = YES;
        }
        
        
        if (cardObj.expmon.length <= 0 || [cardObj.expmon isEqualToString:@"חוֹדֶשׁ"]){
            [self setErrDD:cell.btnmonth setflag:YES];
            cnt++;
        }else{
            editInfo = YES;
        }
        if (cardObj.expyr.length <= 0 || [cardObj.expyr isEqualToString:@"שָׁנָה"]){
            [self setErrDD:cell.btnyear setflag:YES];
            cnt++;
        }else{
            editInfo = YES;
        }
        if (![self validateExp:cell.btnmonth.titleLabel.text yr:cell.btnyear.titleLabel.text]) {
            //[self setErrDD:cell.btnmonth setflag:YES];
            //[self setErrDD:cell.btnyear setflag:YES];
            cnt++;
            // NSString *err = @"אנא בחר תוקף חוקי";
            // [[[[iToast makeText:err] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            // return FALSE;
        }
        if ([self validateAmt] < 0) {
            [self setErr:cell.txtsumtopay setflag:YES];
            cnt++;
        }
        
        if(cardObj.cardtype.length >0 && cardObj.cardno.length > 0)
        {
            
            if (![cc validateCard:cardObj.cardno withType:[self cardTypeFromText:cardObj.cardtype]])
            {
                // NSString *strerr = [NSString stringWithFormat:@"מספר כרטיס %@ לא חוקי",cardObj.cardtype];
                //  [[[[iToast makeText:strerr] setGravity:iToastGravityBottom] setDuration:iToastDurationLong] show];
                
                [self setErr:cell.txtcardno setflag:YES];
                cnt++;
            }else{
                editInfo = YES;
            }
        }
        if (cardObj.ordamt == 0) {
            [self setErr:cell.txtsumtopay setflag:YES];
            cnt++;
        }
        
    }
    //now if not for split then check price is proper else alert user
    
    
    
    if (cnt == 0) {
        isValid = YES;
    }
    return isValid;
}

-(void)setErr:(UITextField*)txtfld setflag:(BOOL)flg
{
    
    if (flg) {
        txtfld.layer.borderWidth = 0.5;
        txtfld.layer.borderColor = [UIColor redColor].CGColor;
    }else{
        txtfld.layer.borderWidth = 0.0;
        //txtfld.layer.borderColor = [UIColor clearColor].CGColor;
    }
}
-(void)setErrDD:(UIButton*)btn setflag:(BOOL)flg
{
    
    if (flg) {
        btn.layer.borderWidth = 0.5;
        btn.layer.borderColor = [UIColor redColor].CGColor;
    }else{
        btn.layer.borderWidth = 0.0;
        //btn.layer.borderColor = [UIColor clearColor].CGColor;
    }
}

-(void)hideKey
{
    [self.view endEditing:YES];
    [self toggleBaseFactoryView:NO];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField.tag == 151)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 9;
    }
    else if(textField.tag == 152)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 5;
    }
    else if(textField.tag == 153)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 17;
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    for (int i=0; i < noOfCards; i++) {
        
        NSIndexPath *idxpath = [NSIndexPath indexPathForRow:i inSection:0];
        CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
        if (textField==cell.txtcardno || textField==cell.txtsumtopay || textField==cell.txtownid)
        {
            [self  showToolbar];
            break;
        }
    }
    return YES;
}
// To be link with your TextField event "Editing Did Begin"
//  memoryze the current TextField
- (IBAction)textFieldDidBeginEditing:(UITextField *)textField
{
    actifText = textField;
    [self toggleBaseFactoryView:YES];
}

// To be link with your TextField event "Editing Did End"
//  release current TextField
- (IBAction)textFieldDidEndEditing:(UITextField *)textField
{
    for (int i=0; i < noOfCards; i++) {
        
        CardData *cardObj = [arrCardObj objectAtIndex:i];
        NSIndexPath *idxpath = [NSIndexPath indexPathForRow:i inSection:0];
        CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
        
        if (textField == cell.txtsumtopay) {
            [arrOrdTot replaceObjectAtIndex:i withObject:cell.txtsumtopay.text];
            cardObj.ordamt = cell.txtsumtopay.text;
            [self updateRemainingAmount: (noOfCards-1 == i)];
            
            if ([self validateAmt] < 0) {
                NSString *strerr = @"סכום לתשלום גדול מסך ההזמנה";//NSLocalizedString(@"Invalid amount", @"Invalid amount");
                [[[[iToast makeText:strerr] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
                btnchk.enabled = NO;
            }else{
                btnchk.enabled = YES;
            }
            break;
        }else if (textField == cell.txtownid){
            cardObj.ownid = cell.txtownid.text;
            break;
        }else if (textField == cell.txtname){
            cardObj.ownname = cell.txtname.text;
            break;
        }
        else if (textField == cell.txtcardcvv){
            cardObj.cvv = cell.txtcardcvv.text;
            break;
        }
        else if (textField == cell.txtcardno){
            cardObj.cardno = cell.txtcardno.text;
            [self validateCardNumber:cardObj];
            break;
        }
        
        
    }
    
    //[NSString stringWithFormat:@"%@%.f-סמן לתשלום יתרה במזומן",SHEKEL,remAmt];
    [self toggleBaseFactoryView:NO];
    actifText = nil;
}

-(void) updateRemainingAmount:(BOOL) lastCard
{
    float remAmt = 0;
    /*if(!lastCard)
     {
     remAmt = [self remainingAmoutSkipLastCard];
     //update last card
     
     NSIndexPath *idxpath = [NSIndexPath indexPathForRow:(noOfCards - 1) inSection:0];
     CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
     if(cell)
     {
     CardData *cardObj = [arrCardObj objectAtIndex:(noOfCards - 1)];
     cardObj.ordamt = [NSString stringWithFormat:@"%f",remAmt];
     cell.txtsumtopay.text = [NSString stringWithFormat:@"%f",remAmt];
     
     }
     }
     else*/
    
    remAmt = [self validateAmt];
    
    restCashAmt = remAmt;
    
    //lblchk.text = [NSString stringWithFormat:@"%@-%@%.02f",NSLocalizedString(@"PAY_REST_BY_CASH", @""),SHEKEL,remAmt];
    NSString *val = [NSString stringWithFormat:@"%.f",fabsf(restCashAmt)];
    if (restCashAmt < 0) {
        lblchk.text = [NSString stringWithFormat:@"סמן לתשלום יתרה במזומן-%@-%@",val,SHEKEL];
    }else{
        lblchk.text = [NSString stringWithFormat:@"סמן לתשלום יתרה במזומן-%@%@",SHEKEL,val];
    }
    
}

-(void) validateCardNumber:(CardData *)cardObj
{
    
    if(cardObj.cardtype.length >0 && cardObj.cardno.length > 0)
    {
        CreditCardValidation *cc = [[CreditCardValidation alloc] init];
        if (![cc validateCard:cardObj.cardno withType:[self cardTypeFromText:cardObj.cardtype]])
        {
            NSString *strerr = [NSString stringWithFormat:@"מספר כרטיס %@ לא חוקי",cardObj.cardtype];
            [[[[iToast makeText:strerr] setGravity:iToastGravityBottom] setDuration:iToastDurationLong] show];
        }
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    for (int i=0; i < noOfCards; i++) {
        
        NSIndexPath *idxpath = [NSIndexPath indexPathForRow:i inSection:0];
        CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
        if (textField == cell.txtname) {
            
            [cell.txtownid becomeFirstResponder];
        }
        
    }
    //[textField resignFirstResponder];
    return YES;
}
-(void) keyboardWillShow:(NSNotification *)note
{
    // Get the keyboard size
    BOOL flagkey=NO;
    for (int i=0; i < noOfCards; i++) {
        
        NSIndexPath *idxpath = [NSIndexPath indexPathForRow:i inSection:0];
        CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
        if (actifText == cell.txtcardcvv) {
            flagkey=YES;
        }
    }
    // if (flagkey) {
    
    [botview setHidden:YES];
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = tblcards.frame;
    
    // Start animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the Table view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height -= keyboardBounds.size.height;
    else
        frame.size.height -= keyboardBounds.size.width;
    
    // Apply new size of table view
    tblcards.frame = frame;
    
    // Scroll the table view to see the TextField just above the keyboard
    if (actifText)
    {
        CGRect textFieldRect = [tblcards convertRect:actifText.bounds fromView:actifText];
        [tblcards scrollRectToVisible:textFieldRect animated:NO];
    }
    
    [UIView commitAnimations];
    //}
}

-(void) keyboardWillHide:(NSNotification *)note
{
    // Get the keyboard size
    [botview setHidden:NO];
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = tblcards.frame;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Increase size of the Table view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height += keyboardBounds.size.height;
    else
        frame.size.height += keyboardBounds.size.width;
    
    // Apply new size of table view
    tblcards.frame = frame;
    
    [UIView commitAnimations];
}
-(void)hideKeyBoard
{
    
}

-(float)validateAmt
{
    float orderAmt = [orderTotal floatValue];
    float total=0;
    
    for (int i=0; i < noOfCards; i++) {
        //  NSIndexPath *idxpath = [NSIndexPath indexPathForRow:i inSection:0];
        //CardCell *cell = [tblcards cellForRowAtIndexPath:idxpath];
        CardData *cardObj = [arrCardObj objectAtIndex:i];
        total = total + [cardObj.ordamt floatValue];
    }
    //total = total + restCashAmt;
    
    return orderAmt - total;
    //    if (total <= orderAmt) {
    //        return YES;
    //    }
    //    return NO;
}

-(BOOL)validateExp:(NSString*)mon yr:(NSString*)yr
{
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"MMYY"];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSString *currdtstr= [NSString stringWithFormat:@"%ld%ld", (long)month, (long)year];
    NSDate *currdt = [df dateFromString:currdtstr];
    NSString *mystr = [NSString stringWithFormat:@"%@%@",mon,yr];
    NSDate *seldt = [df dateFromString:mystr];
    
    if ([currdt compare:seldt] == NSOrderedDescending) {
        return NO;
    }
    return YES;
}

-(void)success:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    
    //Success
    
    //Manoj
    if([response.method isEqualToString:mGetFactorylist])
    {
        NSArray *responseList;
        if([response.respondeBody isKindOfClass:[NSArray class]])
        {
            responseList = response.respondeBody;
            if(responseList.count)
            {
                NSMutableArray *dummyList = [NSMutableArray array];
                for(NSDictionary *dict in responseList)
                {
                    FactoryListData *factoryData = [[FactoryListData alloc] initWithDictionary:dict];
                    
                    //Save/Update data..
                    [factoryCoreService saveFactoryInDB:factoryData withError:nil];
                    
                    [dummyList addObject:factoryData];
                }
                
                factoryList = dummyList;
            }
        }
    }
    
    if([response.method isEqualToString:mSubmitOrder])
    {
        NSDictionary *responseList;
        if([response.respondeBody isKindOfClass:[NSDictionary class]])
        {
            responseList = response.respondeBody;
            orderId = [responseList objectForKey:@"orderId"];
        }
        /*
         ההזמנה בסך XX התקבלה ונשלחה לעסק בהצלחה ותטופל בהקדם.
         תודה על הזמנתך.
         */
        
        
        //Manoj
        factoryContext.factoryid = @"";
        factoryContext.deliveryOrderTime = @"";
        factoryContext.paidByFactory = @"";
        
        
        NSString * ConfirmMessage;//= [NSString stringWithFormat:@"\n %@ \n%@",cnfMsg,NSLocalizedString(@"Confirm your order", @"Confirm your order")];
        int lastCard = 0;
        if(restpay)
        {
            float remAmount = [self validateAmt];
            if(remAmount == 0)
                lastCard = 1;
        }
        if (noOfCards - lastCard > 0) {
            ConfirmMessage= [NSString stringWithFormat:@"%@ %@ %@ %@ %@",orderTotal,NSLocalizedString(@"cardmsg1", @"cardmsg1"),NSLocalizedString(@"cardmsg2", @"cardmsg2"),NSLocalizedString(@"cardmsg3", @"cardmsg3"),NSLocalizedString(@"cardmsg4", @"cardmsg4")];
            ;
        }else{
            ConfirmMessage= [NSString stringWithFormat:@"\n %@ \n%@",cnfMsg,NSLocalizedString(@"Confirm your order", @"Confirm your order")];
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:ConfirmMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        // save order to core db
        NSNumber *numOrderId = [NSNumber numberWithInt:[orderId intValue]];
        NSNumber *numOrderSum = [NSNumber numberWithInt:[orderTotal intValue]];
        NSString *orderObj = [JSONUtil dicToJSON:dicOrder];
        NSString *cartObj = [JSONUtil dicToJSON:cartDic];
        
        //save order to core db
        
        //Additional Extras
        NSMutableArray *aditionalExtrasList = [[NSMutableArray alloc] init];
        for (WSAdditionalExtrasResponse *item in self.selectedBussiness.aditionalExtrasList)
        {
            if(item.selected)
            {
                [aditionalExtrasList addObject:[item getDictionaryFromObject]];
            }
            
        }
        
        NSDictionary *orderDic = [[NSDictionary alloc] initWithObjects:@[numOrderId,[NSDate date],numOrderSum,logo,orderObj,cartObj] forKeys:@[@"orderId",@"orderDate",@"orderSum",@"logoURL",@"orderObj",@"cartData"]];
        
        WSOrderData *orderdata = [[WSOrderData alloc] initWithDictionary:orderDic];
        ServiceManager *serviceManager = [ServiceManager sharedInstance:self];
        [serviceManager saveMyOrdersInDB:orderdata withError:nil];
        
        if (self.selectedBussiness != nil) {
            [[AppGlobalData sharedManager] clearBusinessCart:self.selectedBussiness.id];
        }
        
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)scrollKeyboardWillShow:(NSNotification*)note
{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    CGRect kbRect = [note.userInfo [UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0);
    tblcards.contentInset = contentInsets;
    tblcards.scrollIndicatorInsets = contentInsets;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    // set views with new info
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    if(actifText)
    {
        CGRect fieldFrame =tblcards.frame;
        fieldFrame.origin = [tblcards convertPoint:fieldFrame.origin toView:self.view];
        fieldFrame.origin.y += fieldFrame.size.height;
        if (!CGRectContainsPoint(aRect, fieldFrame.origin) )
        {
            [tblcards scrollRectToVisible:actifText.frame animated:NO];
        }
    }
    // commit animations
    [UIView commitAnimations];
    
}

-(void)scrollKeyboardWillHide:(NSNotification*)note
{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    tblcards.contentInset = UIEdgeInsetsZero;
    [tblcards setContentInset:UIEdgeInsetsZero];
    [tblcards setScrollIndicatorInsets:UIEdgeInsetsZero];
    [UIView commitAnimations];
}
-(void) scrollKeyboardWillShow:(NSNotification *)note withField:(UIView *) activeField
{
    // get keyboard size and loctaion
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    CGRect kbRect = [note.userInfo [UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0);
    tblcards.contentInset = contentInsets;
    tblcards.scrollIndicatorInsets = contentInsets;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    CGRect aRect = tblcards.frame;
    aRect.size.height -= kbRect.size.height;
    if(activeField)
    {
        CGRect fieldFrame =activeField.frame;
        fieldFrame.origin.y += fieldFrame.size.height;
        if (!CGRectContainsPoint(aRect, fieldFrame.origin) )
        {
            fieldFrame.origin.y -=fieldFrame.size.height;
            [tblcards scrollRectToVisible:fieldFrame animated:NO];
        }
    }
    // commit animations
    [UIView commitAnimations];
}

/**
 Card Cell Delegate
 */

-(void) cardCell:(CardCell *)cardCell toggleScroll:(BOOL)flag
{
    [self.tblcards setScrollEnabled:flag];
    [self.tblcards layoutIfNeeded];
}

/**
 Card owner and name validation
 */

-(BOOL) isValidOwnerName:(NSString *) ownerName{
    
    if([NSString isEmpty:ownerName])
        return FALSE;
    
    if ([ownerName componentsSeparatedByString:@" "].count < 2)
        return FALSE;
    
    return TRUE;
}

-(BOOL) isValidOwnerId:(NSString *) str
{
    
    if([NSString isEmpty:str])
        return FALSE;
    else
    {
        str = [str stringWithLength:9 prependStr:@"0"];
        return [self validateOwnerId:str];
        
    }
    
    return TRUE;
    
}
-(BOOL) validateOwnerId:(NSString *) str
{
    int i,k;
    float totalSum = 0;
    for (i = 0; i < str.length; i++) {
        NSString *tmp = [str substringWithRange:NSMakeRange(i, 1)];
        k = [tmp intValue];
        if (i % 2 == 0) {
            int input = (k);
            float sum = 0;
            while (input != 0) {
                int lastdigit = input % 10;
                sum += lastdigit;
                input /= 10;
            }
            totalSum = totalSum + sum;
        } else {
            int input = (k * 2);
            float sum = 0;
            while (input != 0) {
                int lastdigit = input % 10;
                sum += lastdigit;
                input /= 10;
            }
            totalSum = totalSum + sum;
        }
    }
    totalSum = totalSum / 10;
    return fmodf(totalSum, 1.0) == 0;
    
}


-(void)getfactoryList
{
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
    [[ConnectionsManager sharedManager] getFactoryList:nil withdelegate:self];
}


//Manoj
-(void)setSettingTime
{
    NSMutableArray *settingArray = [NSMutableArray array];
    NSDate * cDate = [NSDate date];
    NSInteger currentTime = [DateTimeUtil currentTimeOfDayFromDate:cDate];
    NSInteger remainingTime = currentTime % 1800;
    currentTime = currentTime + (1800 - remainingTime);
    NSInteger currentHour = currentTime / 3600;
    NSInteger currentMin = (currentTime % 3600) / 60;
    NSInteger startHour = 12;
    NSInteger startMin = 0;
    
    if(currentHour < 9 || currentHour > 18)
    {
        arrSettingTime = [NSMutableArray array];
        return;
    }
    
    if(currentHour > 11)
    {
        startHour =  currentHour + 1;
        startMin = currentMin;
    }
    NSString *minStr = @"";
    
    while(startHour < 18 || (startHour == 18 && currentMin == 0))
    {
        
        if(currentMin == 0)
            minStr = [NSString stringWithFormat:@"0%ld",(long)currentMin];
        else
            minStr = [NSString stringWithFormat:@"%ld",(long)currentMin];
        NSString *str = [NSString stringWithFormat:@"%ld:%@",(long)startHour,minStr];
        
        [settingArray addObject:str];
        
        if(currentMin == 30)
        {
            startHour = startHour + 1;
            currentMin = 0;
        }
        else{
            currentMin = 30;
        }
        
    }
    
    arrSettingTime = [[NSMutableArray alloc] initWithArray:settingArray];
}

- (IBAction)showFEPicker:(id)sender
{
    NSArray *list = [factoryCoreService fetchFactoryList];
    NSArray *final = [NSArray array];
    if(list.count)
    {
        NSMutableArray *dummy = [NSMutableArray array];
        
        for(Fcatorycd *fcCD in list)
        {
            FactoryListData *ls = [[FactoryListData alloc] init];
            [ls populateObjectFromCorData:fcCD];
            [dummy addObject:ls];
        }
        
        final = dummy;
    }
    
    [FTPopOverMenu showForSender:sender
                        withMenu:final
     
                       doneBlock:^(NSInteger selectedIndex) {
                           if(selectedIndex >= 0)
                           {
                               
                               FactoryListData *factoryData;
                               factoryData = [final objectAtIndex:selectedIndex];
                               [self.dropDownFE setTitle:factoryData.name forState:UIControlStateNormal];
                               
                               factoryContext.factoryid = factoryData.id;
                               
                               [self toggleTimeandCompanyviewby:false];
                           }
                           
                           
                       } dismissBlock:^{
                           
                       }];
}

- (IBAction)showSTPicker:(id)sender
{
    [FTPopOverMenu showForSender:sender
                        withMenu:arrSettingTime
     
                       doneBlock:^(NSInteger selectedIndex) {
                           if(selectedIndex >= 0)
                           {
                               
                               
                               factoryContext.deliveryOrderTime = [arrSettingTime objectAtIndex:selectedIndex];
                               
                               [self.dropDownST setTitle:[arrSettingTime objectAtIndex:selectedIndex] forState:UIControlStateNormal];
                           }
                           
                           
                       } dismissBlock:^{
                           
                       }];
}

- (IBAction)onClickFECheckbox:(id)sender
{
//    if(self.factoryCellDelegate && [self.factoryCellDelegate respondsToSelector:@selector(chkboxFE:)])
//    {
//        [self.factoryCellDelegate chkboxFE:1 ];
//    }
    
    [self chkboxFE:1];
    
    if(!self.isCheckedFE)
    {
        [self.btnFECheckBox setBackgroundImage:[UIImage imageNamed:@"checkbox_green@1x"] forState:UIControlStateNormal];
    }
    else
    {
        [self.btnFECheckBox setBackgroundImage:[UIImage imageNamed:@"Unchecked Checkbox"] forState:UIControlStateNormal];
        factoryContext.factoryid = @"";
        [self toggleTimeandCompanyviewby:true];
        [self.dropDownST setHidden:true];
    }
    
    [self.dropDownFE setHidden:self.isCheckedFE];
    if(!factoryContext || factoryContext.factoryid.length <= 0)
    {
        [self.dropDownFE setTitle:NSLocalizedString(@"factoryList_Placeholder", nil) forState:UIControlStateNormal];
    }
    self.isCheckedFE = !self.isCheckedFE;
}

- (IBAction)onClickPCCheckbox:(id)sender
{
//    if(self.factoryCellDelegate && [self.factoryCellDelegate respondsToSelector:@selector(chkboxFE:)])
//    {
//
//    }
    
    [self chkboxFE:2];
    
    
    if(!self.isCheckedPC)
    {
        [self.btnPCCheckBox setBackgroundImage:[UIImage imageNamed:@"checkbox_green@1x"] forState:UIControlStateNormal];
        factoryContext.paidByFactory = @"1";
    }
    else
    {
        [self.btnPCCheckBox setBackgroundImage:[UIImage imageNamed:@"Unchecked Checkbox"] forState:UIControlStateNormal];
        factoryContext.paidByFactory = @"0";
    }
    
    self.isCheckedPC = !self.isCheckedPC;
}

- (IBAction)onClickSTCheckbox:(id)sender
{
    if(!self.isCheckedST)
    {
        [self.btnSTCheckBox setBackgroundImage:[UIImage imageNamed:@"checkbox_red@1x"] forState:UIControlStateNormal];
    }
    else
    {
        [self.btnSTCheckBox setBackgroundImage:[UIImage imageNamed:@"Unchecked Checkbox"] forState:UIControlStateNormal];
        factoryContext.deliveryOrderTime = @"";
    }
    
    if(!factoryContext || factoryContext.deliveryOrderTime.length <= 0)
    {
        [self.dropDownST setTitle:NSLocalizedString(@"setting_time_placeholder", nil) forState:UIControlStateNormal];
    }
    
    [self.dropDownST setHidden:self.isCheckedST];
    self.isCheckedST = !self.isCheckedST;
}

-(void)toggleTimeandCompanyviewby:(BOOL)hide
{
    NSInteger currentTime = [DateTimeUtil currentTimeOfDayFromDate:[NSDate date]];
    NSInteger endTime = [DateTimeUtil convertIntoSeconds:17 m:0 s:0];
    NSInteger startTime = [DateTimeUtil convertIntoSeconds:9 m:0 s:0];
    
    BOOL hideLateDelivery = hide || NO;
    
    if(currentTime < startTime || currentTime > endTime)
    {
        factoryContext.deliveryOrderTime = @"";
        hideLateDelivery = TRUE;
    }
    
    float height = 40.0f;
    if(hide)
    {
        factoryContext.paidByFactory = @"0";
        factoryContext.deliveryOrderTime = @"";
        height = 0.0f;
    }
    
    if(hideLateDelivery)
    {
        
        self.constraint_ST_Height.constant = 0.0f;
    }
    else{
        self.constraint_ST_Height.constant = height;
    }
    self.constraint_PC_Height.constant = height;
    
    
    self.constraint_baseFactoryViewHeight.constant = self.constraint_FE_Height.constant + self.constraint_PC_Height.constant + self.constraint_ST_Height.constant;
    
    currentBaseFactoryHeight = self.constraint_baseFactoryViewHeight.constant;

    
    [UIView animateWithDuration:2
                     animations:^{
                         [self.view setNeedsLayout];
                     }];
    
    
}

//Manoj
-(void)toggleBaseFactoryView:(BOOL)hide
{

    if(hide)
    {
        currentBaseFactoryHeight = self.constraint_baseFactoryViewHeight.constant;
        self.constraint_baseFactoryViewHeight.constant = 0.0f;
        
        self.dropDownFE.hidden = YES;
        self.dropDownST.hidden = YES;
        
    }
    else
    {
        if(currentBaseFactoryHeight >0){
            self.constraint_baseFactoryViewHeight.constant = currentBaseFactoryHeight;
        }
        
        [self.dropDownST setHidden:!_isCheckedST];
        [self.dropDownFE setHidden:!_isCheckFE];
        
    }
    
    [UIView animateWithDuration:2
                     animations:^{
                         [self.view setNeedsLayout];
                     }];
}

@end
