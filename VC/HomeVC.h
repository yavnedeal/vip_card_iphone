//
//  HomeVC.h
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "WSFindBussinessListResponse.h"
#import "WSCategoryFilterDetailResponse.h"
#import "GalleryVC.h"

@interface HomeVC : BaseViewController
{
	NSArray *recipePhotos;
	

	
}
@property (strong, nonatomic) IBOutlet UIButton *btnOpeningHours;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
@property (strong, nonatomic) IBOutlet UIButton *btnWanz;
@property (strong, nonatomic) IBOutlet UIImageView *imgPercentage;
@property (strong, nonatomic) IBOutlet UIImageView *imgProductImage;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property(strong, nonatomic)NSArray *array_Home;
@property (weak, nonatomic) IBOutlet UIImageView *img_Menu;

@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_topfacebook;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone;
@property (strong, nonatomic) IBOutlet UIButton *btnWebsite;
@property (strong, nonatomic) IBOutlet UIButton *btnFacebook;
@property (strong, nonatomic) IBOutlet UIView *veiwWebsite;
@property (strong, nonatomic) IBOutlet UIView *viewFacebook;
@property (strong, nonatomic) IBOutlet UIView *viewConatinView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_FacebookViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_WebsiteViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_top;
@property(strong,nonatomic)WSFindBussinessListResponse *bussinessResponse;
@property(strong,nonatomic)WSCategoryFilterDetailResponse *categoryResponse;
- (IBAction)onClick_OpeningHour:(id)sender;
- (IBAction)onClick_Phone:(id)sender;
- (IBAction)onClick_PhoneNumber:(id)sender;
//- (IBAction)onClick_Menu:(id)sender;
- (IBAction)actionMenu:(id)sender;
- (IBAction)onClick_Wize:(id)sender;
- (IBAction)onClick_Facebook:(id)sender;
- (IBAction)onClick_site:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@end
