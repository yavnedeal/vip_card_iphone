//
//  CommonCollectionViewController.h
//  VIPCard
//
//  Created by SanC on 18/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "BaseViewController.h"
#import "DetailedViewController.h"
#import "ShabatView.h"


@interface CommonCollectionViewController : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
	BOOL flag;
	AppDelegate *appDelegate;
	ShabatView *shabbatView;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
//locationPopupWindow
@property (strong, nonatomic) IBOutlet UIView *viewPopupWindow;
@property (strong, nonatomic) IBOutlet UIButton *btnCategoryList;
@property (strong, nonatomic) IBOutlet UIButton *btnFindBussinessList;
@property (nonatomic, strong) NSArray *array_images;
@property (nonatomic,assign)BOOL flagForColor,indicatorFlag;


- (IBAction)touchFindBussinessList:(id)sender;
- (IBAction)touchCategoryList:(id)sender;

@end
