//
//  SideMenuViewController.h
//  VIPCard
//
//  Created by SanC on 22/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "VIPCardTableViewCell.h"
#import "BaseViewController.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"


@interface SideMenuViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
