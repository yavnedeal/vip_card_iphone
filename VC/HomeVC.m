//
//  HomeVC.m
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "HomeVC.h"
#import "GalleryCell.h"
#import "WSAddressResponse.h"
#import "ImageCollectionViewCell.h"
#import "WSCategoryMealResponse.h"
#import "CategoryForMealHolderVC.h"


@interface HomeVC ()<ServerResponseDelegate>

@end

@implementation HomeVC
@synthesize bussinessResponse,categoryResponse;

- (void)viewDidLoad {
	[super viewDidLoad];
	[self showBackButton:YES];
	// Do any additional setup after loading the view.
	//NSLog(@"bussiness response=%@",bussinessResponse);
	if (bussinessResponse!=nil)
 {
	 self.lblPhone.text=bussinessResponse.phone;
	 [self.imgProductImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", mFetchImages, bussinessResponse.logo]] placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
	 self.imgPercentage.image=[UIImage imageNamed:[NSString stringWithFormat:@"imgDiscount_%ldPer",(long)[bussinessResponse.vipDiscount integerValue ]]];
     //@gauri for hide viewwebsite when there is no responce//
     if([bussinessResponse.site isEqualToString:@""])
     {
         self.veiwWebsite.hidden=YES;
         _con_topfacebook.constant=_con_topfacebook.constant-30;
     }else
     {
          [self.btnWebsite setTitle: bussinessResponse.site forState: UIControlStateNormal];
     }
     
     if([NSString isEmpty:bussinessResponse.facebook])
         self.viewFacebook.hidden=YES;
     else
     {
       self.viewFacebook.hidden= NO;
	   [self.btnFacebook setTitle:bussinessResponse.facebook forState: UIControlStateNormal];
     }
     
	 NSArray *array_Address=bussinessResponse.addressList;
	 WSAddressResponse  *response=[array_Address objectAtIndex:0];
	 self.lblAddress.text=[NSString stringWithFormat: @"%@ %@, %@",response.street,response.NumberStreet,response.town];
	 if ((bussinessResponse.homeGalleries.count>0))
		 self.array_Home=bussinessResponse.homeGalleries;
	 else
		 self.collectionView.hidden=YES;

	 //Menu button hide and show
	 self.btnMenu.hidden = YES;
     // modified by Sarika 12th Sept 2016
	 if([bussinessResponse.idTypeBusiness integerValue] == 7 || [bussinessResponse.idTypeBusiness integerValue] == 8)
		 self.btnMenu.hidden = NO;
	}
	else
	{

		self.lblPhone.text=categoryResponse.phone;
		[self.imgProductImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", mFetchImages, categoryResponse.logo]] placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
		self.imgPercentage.image=[UIImage imageNamed:[NSString stringWithFormat:@"imgDiscount_%ldPer",(long)[categoryResponse.vipDiscount integerValue ]]];
        
        if([NSString isEmpty:categoryResponse.site])
        {
            self.veiwWebsite.hidden=YES;
            _con_topfacebook.constant=_con_topfacebook.constant-30;
        }else
        {
            self.veiwWebsite.hidden= NO;
            [self.btnWebsite setTitle: categoryResponse.site forState: UIControlStateNormal];
        }
        
        if([NSString isEmpty:categoryResponse.facebook])
        {
            self.viewFacebook.hidden=YES;
        }
        else
        {
            self.viewFacebook.hidden = NO;
		  [self.btnFacebook setTitle:categoryResponse.facebook forState: UIControlStateNormal];
        }
        
		NSArray *array_Address=categoryResponse.address;
		WSAddressResponse  *response=[array_Address objectAtIndex:0];
		self.lblAddress.text=[NSString stringWithFormat: @"%@ %@, %@",response.street,response.NumberStreet,response.town];
		if ((categoryResponse.homeGalleries.count>0))
        {
            self.collectionView.hidden=NO;
			self.array_Home=categoryResponse.gridGalleries;
        }
		else
        {
			self.collectionView.hidden=YES;
        }

		//Menu button hide and show
		self.btnMenu.hidden = YES;
		if([categoryResponse.idTypeBusiness integerValue] == 7)
			self.btnMenu.hidden = NO;
	}

	[_btnOpeningHours setTitle:NSLocalizedString(@"opening_HourMainTitle",@"opening_HourMainTitle") forState:UIControlStateNormal];
	NSDictionary * linkAttributes = @{NSForegroundColorAttributeName:_btnOpeningHours.titleLabel.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)};
	NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:_btnOpeningHours.titleLabel.text attributes:linkAttributes];
	[_btnOpeningHours.titleLabel setAttributedText:attributedString];
    
    
    
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return self.array_Home.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *identifier = @"ImageCollectionViewCell";
	ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
	[cell populatedData:[self.array_Home objectAtIndex:indexPath.row]];
    
    //cell.layer.borderWidth=1.0f;
    cell.layer.borderColor=[UIColor grayColor].CGColor;
    
    if(indexPath.item == [self.array_Home count] -1 ||
        [self.array_Home count] == 1)
    {
        cell.seperatorLineView.hidden = YES;
    }
    else
    {
        cell.seperatorLineView.hidden = NO;
    }
        
    
	return cell;
}
//@Gauri method added//
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	GalleryVC *galleryVC = [self getVCWithSB_ID:kGalleryVC];
	galleryVC.array_getData=[self.array_Home mutableCopy];
	galleryVC.indePath=indexPath.row;
	galleryVC.path=indexPath;
	[self.navigationController pushViewController:galleryVC animated:YES];

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}

- (IBAction)onClick_OpeningHour:(id)sender {
	if (bussinessResponse.openingHoursList!=nil && bussinessResponse.openingHoursList.count>0)
    {
		[self showPopupwithmessage:1 toShowValue:bussinessResponse.openingHoursList];
	}

    else
    [[[[iToast makeText:@"לא מוגדר שעות עבודה"] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];

}

- (IBAction)onClick_Phone:(id)sender {
	NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",bussinessResponse.phone]];
	if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
		[[UIApplication sharedApplication] openURL:phoneUrl];
	} else
	{
		UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
		[calert show];
	}
}

- (IBAction)onClick_PhoneNumber:(id)sender {
	NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",bussinessResponse.mobile]];
	if (![bussinessResponse.mobile isEqualToString:@""])
 {
	 if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
		 [[UIApplication sharedApplication] openURL:phoneUrl];
	 } else
	 {
		 UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
		 [calert show];
	 }

	}
}



- (IBAction)actionMenu:(id)sender
{
    [self getCatforMealByBussinessID:bussinessResponse.id];
}

- (IBAction)onClick_Wize:(id)sender {
	//NSLog(@"bussiness response=%@",bussinessResponse);
	NSArray *data=bussinessResponse.addressList;
	WSAddressResponse *addressResponse=[data objectAtIndex:0];
	NSString *str_nameStreet=addressResponse.street;
	NSString *str_numberStreet=addressResponse.NumberStreet;
	NSString *str_town=addressResponse.town;
	NSString *str_concate=[NSString stringWithFormat:@"%@ %@ %@",str_nameStreet,str_numberStreet,str_town];
	[self navigateToLatitude:0.0 longitude:0.0 address:str_concate];

}
//@Gauri added
- (IBAction)onClick_Facebook:(id)sender
{
    //if empty return
    if([NSString isEmpty:bussinessResponse.facebook])
        return ;
	// check whether facebook is (likely to be) installed or not
	if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
		// Safe to launch the facebook app
		NSString *combined = [NSString stringWithFormat:@"%@%@",@"fb://profile/",bussinessResponse.facebookPage];
		NSURL *fbAppURL = [NSURL URLWithString:[combined stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
		[self openAppUrl:fbAppURL];

	}
	else{
		NSString *combined = [NSString stringWithFormat:@"%@%@",@"https://www.facebook.com/",bussinessResponse.facebookPage];
		NSURL *fbAppURL = [NSURL URLWithString:[combined stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
		[self openAppUrl:fbAppURL];

	}
}

-(BOOL) openAppUrl:(NSURL *) url
{
    if([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
        return TRUE;
    }
    
    return NO;
}



//@Gauri added action//
- (IBAction)onClick_site:(id)sender
{
    
    NSURL *siteUrl;
    NSString *stringUrl = [NSString  stringWithFormat:@"%@",bussinessResponse.site];
    if (![stringUrl containsString:@"http"])
    {

        siteUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",bussinessResponse.site]];
    }
    else
    {
        siteUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",bussinessResponse.site]];
    }
    [[UIApplication sharedApplication] openURL:siteUrl];

}

-(void)getCatforMealByBussinessID:(NSString *)bussinessID
{
    [[ConnectionsManager sharedManager] getCatMealforBussinesswithID:bussinessID delegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:YES];
}

-(void)success:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    
    if([response.method isEqualToString:@"fetchCatMealForBusiness"])
    {
		//  NSLog(@"%@", response.respondeBody);
        NSArray *responseList = response.respondeBody;
        if(responseList.count)
        {
            NSMutableArray *temp = [NSMutableArray array];
            for(NSDictionary *dic in responseList)
            {
                WSCategoryMealResponse *bussinessRespons = [[WSCategoryMealResponse alloc] initWithDictionary:dic];
                [temp addObject:bussinessRespons];
            }
            
            NSArray *bussineesListTemp = temp;            
            CategoryForMealHolderVC *categoryMealHolderVC = [self getVCWithSB_ID:kCategoryForMealHolder_SB_ID];
            categoryMealHolderVC.baseviewTitle = self.title;
            categoryMealHolderVC.categoryList = bussineesListTemp;
            [self.navigationController pushViewController:categoryMealHolderVC animated:YES];
            
        }
    }
}

-(void)failure:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    
    if([response.method isEqualToString:@"fetchCatMealForBusiness"])
    {
        NSLog(@"%@", response.respondeBody);
    }
}


@end
