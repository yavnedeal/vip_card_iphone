//
//  MainTableViewCell.m
//  VIPCard
//
//  Created by fis on 20/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "MainTableViewCell.h"
#import "WSConstants.h"
#import "AppGlobalConstant.h"
#import "UIImageView+WebCache.h"
#import "SHCStrikethroughLabel.h"
#import "AppColorConstants.h"


@interface MainTableViewCell()
{
	NSString *customSaleTypeVal;
	NSString *customSaletypeVal2;
}

@end

@implementation MainTableViewCell

- (void)awakeFromNib {
	// Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];

	// Configure the view for the selected state
}

#pragma mark-UserDefineMethods
-(void)populateData:(WSCouponsResponse *)couponseResponse andState:(BOOL)aStatus
{


	[self.nameBussiness setHidden:aStatus];
	[self.name setHidden:aStatus];
	[self.desc setHidden:aStatus];
	[self.discPrice setHidden:aStatus];
	[self.mainPrice setHidden:aStatus];
	[self.imgView setHidden:aStatus];
	self.mainPrice.text = @"";
	self.discPrice.text = @"";
	NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mFetchImages,couponseResponse.image]];
	[self.imgView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
	self.name.text = couponseResponse.name;
	self.nameBussiness.text = couponseResponse.nameBusiness;
    [self.nameBussiness sizeToFit];
	self.desc.text = couponseResponse.des;
    
	self.desc.textAlignment = NSTextAlignmentCenter;
	//self.nameBussiness.textAlignment = UITextAlignmentCenter;
	self.name.textAlignment = NSTextAlignmentCenter;
	self.name.font = helveticaFont(25);
	if([couponseResponse.price2 integerValue] >0)
		self.discPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL,couponseResponse.price2];
	if([couponseResponse.price1 integerValue] >0)
		self.mainPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL,couponseResponse.price1];
	self.discPrice.textColor = COLOR_MAIN_PRICE;
	[self calculatesaleType:couponseResponse];

	[self updateLblSize];

	
    // Populate coupon Text field in salesType
   /* if(![NSString isEmpty:couponseResponse.textPrice])
    {
        self.lbl_SalesType.text = couponseResponse.textPrice;
        self.constraintSaleTypeHEight.constant = 21.f;
    }
    else {
        self.lbl_SalesType.text = @"";
        self.constraintSaleTypeHEight.constant = 0.f;
    }*/
    
	//[self updateWithAttributeString:self.lbl_SalesType color:COLOR_MAIN_PRICE text:customSaleTypeVal secondText:customSaletypeVal2];
}

-(void) updateWithAttributeString:(UILabel *) lbl color:(UIColor *) color text:(NSString *)text secondText:(NSString *)newtext
{
	if(!text || [text isEmpty])
		return;

	NSString *str = lbl.text;
	NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:str];
	NSMutableAttributedString *attributedText2=[[NSMutableAttributedString alloc] initWithString:str] ;

	//now set attributes
	[attributedText setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:lbl.font,NSFontAttributeName,nil] range:NSMakeRange(0, [str length])];
	[attributedText2 setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:lbl.font,NSFontAttributeName,nil] range:NSMakeRange(0, [str length])];
	NSRange range = [str rangeOfString:text];
	NSRange range2=[str rangeOfString:newtext];
	[attributedText setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName,nil] range:range];
	[attributedText2 setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName,nil] range:range2];
	NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:lbl.text];
	[string addAttribute:NSForegroundColorAttributeName value:color range:range];
	[string addAttribute:NSForegroundColorAttributeName value:color range:range2];
	lbl.attributedText = string;
}


-(void)calculatesaleType:(WSCouponsResponse *)couponseResponse
{
	customSaleTypeVal = @"";
	customSaletypeVal2=@"";
	self.lbl_SalesType.attributedText = nil;
    
    if([couponseResponse.idTypeSeals integerValue] == 1)
    {
        
        if([couponseResponse.price1 integerValue] >0)
        {
            self.mainPrice.hidden = NO;
            self.mainPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL,couponseResponse.price1];
        }
        if([couponseResponse.price2 integerValue] >0)
            self.discPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL,couponseResponse.price2];
        
    }
    else if([couponseResponse.idTypeSeals integerValue] == 10)
    {
        self.mainPrice.hidden = YES;
        self.discPrice.hidden = NO;
        self.discPrice.text = [couponseResponse dealPriceFormatedValue];
    }
    else {
        self.mainPrice.hidden = YES;
        self.discPrice.hidden = NO;
        self.discPrice.text = [couponseResponse dealPriceFormatedValue];
    }
    
    /*
	switch ([couponseResponse.idTypeSeals integerValue])
 {
     case 1:
		 if([couponseResponse.price1 integerValue] >0)
			 self.mainPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL,couponseResponse.price1];
		 if([couponseResponse.price2 integerValue] >0)
			 self.discPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL,couponseResponse.price2];
			break;

	 case 2:

		 self.mainPrice.hidden=YES;
		 self.discPrice.text=[NSString stringWithFormat:@"%@ %@%@ %@",NSLocalizedString(@"dSeco", @"dSeco"),couponseResponse.discount,@"%",NSLocalizedString(@"dDisc", @"dDisc")];
 
			break;
	 case 3:
		 self.mainPrice.hidden=YES;
         
		 self.discPrice.text=[NSString stringWithFormat:@"%@ %@%@ %@",NSLocalizedString(@"dSeco", @"dSeco"), SHEKEL, couponseResponse.discount,NSLocalizedString(@"dDisc", @"dDisc")];
			break;

	 case 4:
		 self.mainPrice.hidden=YES;
		 self.discPrice.text=[NSString stringWithFormat:@"%@ %@ %@ %@",NSLocalizedString(@"dBuy", @"dBuy"), couponseResponse.quantity1, NSLocalizedString(@"dGet", @"dGet"),couponseResponse.quantity2];
		 break;

	 case 5:
	 {
		 self.mainPrice.hidden=YES;
		 self.discPrice.text=[NSString stringWithFormat:@"%@ %@ %@ %@%@",NSLocalizedString(@"dBuy", @"dBuy"), couponseResponse.quantity1, NSLocalizedString(@"dB", @"dB"),SHEKEL,couponseResponse.price1];
	 }


		 break;

	 case 6:
		 self.mainPrice.hidden=YES;
		 self.discPrice.text=[NSString stringWithFormat:@"%@ %@%@ %@ %@%@",NSLocalizedString(@"dBuy2", @"dBuy2"),SHEKEL,couponseResponse.price1 , NSLocalizedString(@"dPayOnly", @"dPayOnly"),SHEKEL,couponseResponse.price2];


		 break;

	 case 7:
		 self.mainPrice.hidden=YES;
		 self.discPrice.text=[NSString stringWithFormat:@"%@%@ %@ %@",couponseResponse.discount,@"%",NSLocalizedString(@"dDisc", @"dDisc"),couponseResponse.textPrice];

	 case 8:
		 self.mainPrice.hidden=YES;
         
		 self.discPrice.text=[NSString stringWithFormat:@"%@+%@ %@ ",couponseResponse.quantity1,couponseResponse.quantity2,couponseResponse.textPrice];
		 break;

	 case 9:
		 self.mainPrice.hidden=YES;
		 self.discPrice.text=[NSString stringWithFormat:@"%@%@ %@ %@",SHEKEL,couponseResponse.discount,NSLocalizedString(@"dDisc", @"dDisc"),couponseResponse.textPrice];
		 break;


  default:
			break;
	}
     */
}

-(void) updateLblSize{
	CGRect f = self.name.frame;
	[self.name sizeToFit];
	f.size.height = self.name.frame.size.height;

	self.constaintLblNameHeight.constant = f.size.height;

	//Desc
	f = self.desc.frame;
	[self.desc sizeToFit];
	f.size.height = self.desc.frame.size.height;
	self.constaintLblDescHeight.constant = f.size.height;
    
    //Price
    [self.discPrice sizeToFit];
    f.size.height = CGRectGetHeight(self.discPrice.frame);
    self.constraintDiscPriceHeight.constant = f.size.height;
    
}


-(void)populateDataBussinessList:(WSCouponsResponse *)dealResponse andState:(BOOL)aStatus
{
	[self.nameBussiness setHidden:aStatus];
	[self.name setHidden:aStatus];
	[self.desc setHidden:aStatus];
	[self.discPrice setHidden:aStatus];
	[self.mainPrice setHidden:aStatus];
	[self.imgView setHidden:aStatus];
	self.mainPrice.text = @"";
	self.discPrice.text = @"";
	NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mFetchImages,dealResponse.image]];
	[self.imgView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
	self.name.text = dealResponse.name;
	self.nameBussiness.text = dealResponse.nameBusiness;
    [self.nameBussiness sizeToFit];
	self.desc.text = dealResponse.des;

	self.desc.textAlignment = NSTextAlignmentCenter;
	self.name.textAlignment = NSTextAlignmentCenter;
	self.name.font = helveticaFont(25);

	if([dealResponse.price2 integerValue] >0)
		self.discPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL,dealResponse.price2];
	if([dealResponse.price1 integerValue] >0)
		self.mainPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL,dealResponse.price1];
	self.discPrice.textColor = COLOR_MAIN_PRICE;


	[self calculatesaleTypeForBusiness:dealResponse];

	[self updateLblSize];

}


-(void)calculatesaleTypeForBusiness:(WSCouponsResponse *)dealResponse
{
	customSaleTypeVal = @"";
	customSaletypeVal2=@"";
	self.lbl_SalesType.attributedText = nil;

	if([dealResponse.idTypeSeals integerValue] == 1)
	{

		if([dealResponse.price1 integerValue] >0)
		{
			self.mainPrice.hidden = NO;
			self.mainPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL,dealResponse.price1];
		}
		if([dealResponse.price2 integerValue] >0)
			self.discPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL,dealResponse.price2];

	}
    else if([dealResponse.idTypeSeals integerValue] == 10)
    {
        self.mainPrice.hidden = YES;
        self.discPrice.hidden = NO;
        self.discPrice.text = [dealResponse dealPriceFormatedValue];
        if ([NSString isEmpty:dealResponse.textPrice]) {
            self.discPrice.textAlignment = NSTextAlignmentRight;
        }else{
            self.discPrice.textAlignment = NSTextAlignmentLeft;
        }
    }
	else {
		self.mainPrice.hidden = YES;
        self.discPrice.hidden = NO;
		self.discPrice.text = [dealResponse dealPriceFormatedValue];
        
	}
}
@end
