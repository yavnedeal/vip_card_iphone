//
//  BussinessList.h
//  VIPCard
//
//  Created by SanC on 12/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "BussinessListCell.h"


@interface BussinessList : BaseViewController
@property(nonatomic,strong)NSArray *array_deals;
@property(nonatomic,strong)NSArray *array_Coupans;
@property(nonatomic,strong)NSArray *array_List;//common for both array;
@property(nonatomic,retain)UITableView *tableview;
@property (assign) BOOL isCoupns;

@property (assign) BOOL isSearch;
@property(nonatomic,strong) NSString *str_module;




@end
