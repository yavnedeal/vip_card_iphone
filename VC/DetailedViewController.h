//
//  DetailedViewController.h
//  VIPCard
//
//  Created by SanC on 18/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"

@interface DetailedViewController : BaseViewController
@property (weak, nonatomic) NSString *imgName,*labelDesc;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end
