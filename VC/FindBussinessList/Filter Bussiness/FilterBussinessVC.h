//
//  contactShowViewController.h
//  MyStore
//
//  Created by SanC on 11/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface FilterBussinessVC : BaseViewController
{
	NSDictionary *dict_data;
	NSArray *array_SectionTitles;
	 NSArray *array_IndexTitles;
	__weak IBOutlet UISearchBar *searchBar;
}
@property (weak, nonatomic) IBOutlet UITableView *tbl_Contact;

@end
