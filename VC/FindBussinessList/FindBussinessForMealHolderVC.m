//
//  FindBussinessForMealHolderVC.m
//  VIPCard
//
//  Created by SanC on 13/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

//idTypeBusiness


#import "YSLContainerViewController.h"
#import "HomeVC.h"
#import "AboutBussinessVC.h"
#import "BussinessList.h"
#import "FindBussinessForMealHolderVC.h"
#import "CommonCollectionViewController.h"
#import "Obj_FindControllerList.h"



@interface FindBussinessForMealHolderVC ()<YSLContainerViewControllerDelegate>
{
	NSMutableArray *viewControllers;
}
@end

@implementation FindBussinessForMealHolderVC
@synthesize findBussinessResponse = _findBussinessResponse;
@synthesize findcategoryResponse=_findcategoryResponse;

- (void)viewDidLoad {
    [super viewDidLoad];
	viewControllers = [NSMutableArray array];
	[self showBackButton:YES];
	//NSLog(@"%@",_findBussinessResponse);
	if (_findBussinessResponse!=nil)
	 [self loadData];
	if (_findcategoryResponse!=nil)
		[self loadDataFromCategoryFilter];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)loadData
{

	// HomeVC *vc = [[HomeVC alloc] init];
    NSMutableArray *viewArray = [[NSMutableArray alloc] init];
    
	// AboutBussinessVC *aboutViewController = [[AboutBussinessVC alloc] init];
	// BussinessList *bussinessListViewController=[[BussinessList alloc] init];
	 viewControllers = [NSMutableArray array];
	if (_findBussinessResponse.gridGalleries.count>0)
	{
        CommonCollectionViewController *commonCollectionViewController=[self getVCWithSB_ID:kCommonCollectionVC_SB_ID];
		commonCollectionViewController.title = NSLocalizedString(@"t_gallery_b", @"t_gallery_b");

        commonCollectionViewController.array_images=[_findBussinessResponse.gridGalleries mutableCopy];
        commonCollectionViewController.flagForColor=true;
        
        NSMutableDictionary *keyDic = [[NSMutableDictionary alloc] init];
        [keyDic setObject:commonCollectionViewController forKey:@"GALLERY"];
        [viewArray addObject:keyDic];
        
        [viewControllers addObject:commonCollectionViewController];
	}
	 if(![_findBussinessResponse.about isEqualToString:@""])
	 {
		AboutBussinessVC *aboutViewController = [self getVCWithSB_ID:kAboutBussiness];
		aboutViewController.title = NSLocalizedString(@"t_about_b",@"t_about_b");

		aboutViewController.bussinessDiscription = [_findBussinessResponse.about mutableCopy];
		aboutViewController.bussinessTitle = [_findBussinessResponse.name mutableCopy];
		//		aboutViewController.currentIndex = 0;
         
         NSMutableDictionary *keyDic = [[NSMutableDictionary alloc] init];
         [keyDic setObject:aboutViewController forKey:@"ABOUT"];
         [viewArray addObject:keyDic];

         
		[viewControllers addObject:aboutViewController];

	 }
	 if(_findBussinessResponse.dealsList.count>0)
	 {

		BussinessList *bussinessListViewController = [self getVCWithSB_ID:kBussinessList];
		 bussinessListViewController.title = NSLocalizedString(@"t_deals_b",@"t_deals_b");
		bussinessListViewController.array_deals=[_findBussinessResponse.dealsList mutableCopy];
         
         NSMutableDictionary *keyDic = [[NSMutableDictionary alloc] init];
         [keyDic setObject:bussinessListViewController forKey:@"DEALS"];
         [viewArray addObject:keyDic];

         
		[viewControllers addObject:bussinessListViewController];

	 }
	 if (_findBussinessResponse.couponesList.count>0)
	 {

		BussinessList *bussinessListViewController = [self getVCWithSB_ID:kBussinessList];
		bussinessListViewController.title = NSLocalizedString(@"t_coupune_b",@"t_coupune_b");
		bussinessListViewController.array_Coupans=[_findBussinessResponse.couponesList mutableCopy];
         
         NSMutableDictionary *keyDic = [[NSMutableDictionary alloc] init];
         [keyDic setObject:bussinessListViewController forKey:@"COUPONS"];
         [viewArray addObject:keyDic];

         
		[viewControllers addObject:bussinessListViewController];
	 }
	 if(_findBussinessResponse.description && _findBussinessResponse.description != nil)
	 {
		HomeVC *vc=[self getVCWithSB_ID:kHomeVC];
		 vc.title = NSLocalizedString(@"t_home_b",@"t_home_b");
		vc.bussinessResponse=_findBussinessResponse;
         
         NSMutableDictionary *keyDic = [[NSMutableDictionary alloc] init];
         [keyDic setObject:vc forKey:@"HOME"];
         [viewArray addObject:keyDic];

         
		[viewControllers addObject:vc];
	 }

	 float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
	 float navigationHeight = self.navigationController.navigationBar.frame.size.height;

	 YSLContainerViewController *containerVC = [[YSLContainerViewController alloc]initWithControllers:viewControllers
	 topBarHeight:statusHeight + navigationHeight
	 parentViewController:self];
    containerVC.flag=true;
	 containerVC.delegate = self;
	 containerVC.menuItemFont = [UIFont fontWithName:@"Arial" size:16];


	[self.view addSubview:containerVC.view];
}



-(void)loadDataFromCategoryFilter
{
	//HomeVC *vc = [[HomeVC alloc] init];
	NSMutableArray *viewArray = [[NSMutableArray alloc] init];
	//AboutBussinessVC *aboutViewController = [[AboutBussinessVC alloc] init];
	
	viewControllers = [NSMutableArray array];
	if (_findcategoryResponse.gridGalleries.count>0)
	{
		CommonCollectionViewController *commonCollectionViewController=[self getVCWithSB_ID:kCommonCollectionVC_SB_ID];
		commonCollectionViewController.title = NSLocalizedString(@"t_gallery_b", @"t_gallery_b");

		commonCollectionViewController.array_images=[_findcategoryResponse.gridGalleries mutableCopy];
		commonCollectionViewController.flagForColor=true;

		NSMutableDictionary *keyDic = [[NSMutableDictionary alloc] init];
		[keyDic setObject:commonCollectionViewController forKey:@"GALLERY"];
		[viewArray addObject:keyDic];

		[viewControllers addObject:commonCollectionViewController];
	}
	if(![_findcategoryResponse.about isEqualToString:@""])
	{
		AboutBussinessVC *aboutViewController = [self getVCWithSB_ID:kAboutBussiness];
		aboutViewController.title = NSLocalizedString(@"t_about_b",@"t_about_b");

		aboutViewController.bussinessDiscription = [_findcategoryResponse.about mutableCopy];
		aboutViewController.bussinessTitle = [_findcategoryResponse.name mutableCopy];

		NSMutableDictionary *keyDic = [[NSMutableDictionary alloc] init];
		[keyDic setObject:aboutViewController forKey:@"ABOUT"];
		[viewArray addObject:keyDic];


		[viewControllers addObject:aboutViewController];

	}
	if(_findcategoryResponse.deals.count>0)
	{
        BussinessList *bussinessListViewController = nil;
		bussinessListViewController = [self getVCWithSB_ID:kBussinessList];
		bussinessListViewController.title = NSLocalizedString(@"t_deals_b",@"t_deals_b");
		bussinessListViewController.array_deals=[_findcategoryResponse.deals mutableCopy];

		NSMutableDictionary *keyDic = [[NSMutableDictionary alloc] init];
		[keyDic setObject:bussinessListViewController forKey:@"DEALS"];
		[viewArray addObject:keyDic];
		[viewControllers addObject:bussinessListViewController];
	}
	if (_findcategoryResponse.coupones.count>0)
	{
        BussinessList *bussinessListViewController = nil;
		bussinessListViewController = [self getVCWithSB_ID:kBussinessList];
		bussinessListViewController.title = NSLocalizedString(@"t_coupune_b",@"t_coupune_b");
		bussinessListViewController.array_Coupans=[_findcategoryResponse.coupones mutableCopy];

		NSMutableDictionary *keyDic = [[NSMutableDictionary alloc] init];
		[keyDic setObject:bussinessListViewController forKey:@"COUPONS"];
		[viewArray addObject:keyDic];


		[viewControllers addObject:bussinessListViewController];
	}
	if(_findcategoryResponse.description && _findcategoryResponse.description != nil)
	{
		HomeVC *vc = [self getVCWithSB_ID:kHomeVC];
		vc.title = NSLocalizedString(@"t_home_b",@"t_home_b");
		vc.categoryResponse=_findcategoryResponse;

		NSMutableDictionary *keyDic = [[NSMutableDictionary alloc] init];
		[keyDic setObject:vc forKey:@"HOME"];
		[viewArray addObject:keyDic];


		[viewControllers addObject:vc];
	}

	float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
	float navigationHeight = self.navigationController.navigationBar.frame.size.height;

	YSLContainerViewController *containerVC = [[YSLContainerViewController alloc]initWithControllers:viewControllers
																						topBarHeight:statusHeight + navigationHeight
																				parentViewController:self];
	containerVC.flag=true;
	containerVC.delegate = self;
	containerVC.menuItemFont = [UIFont fontWithName:@"Arial" size:16];


	[self.view addSubview:containerVC.view];

}
-(void)checkViewController
{

}
- (void)containerViewItemIndex:(NSInteger)index currentController:(UIViewController *)controller;

{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
