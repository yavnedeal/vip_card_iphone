//
//  FindBussinessList.m
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
#import "SWRevealViewController.h"
#import "WSAddressResponse.h"
#import "FindBussinessList.h"
#import "FindBussinessListCell.h"
#import "FieldCategoryDetailCell.h"
#import "WSCouponsResponse.h"
#import "WSFindBussinessListResponse.h"

#import "CategoryForMealListVC.h"
#import "FindBussinessForMealHolderVC.h"
#import "CustomImgView.h"



@interface FindBussinessList () <ServerResponseDelegate,SWRevealViewControllerDelegate>
{
	NSMutableArray *bussinessArray,*arrayCopyBussiness;
	WSCouponsResponse *wsCouponsRes;
	NSMutableArray *viewControllers;
}
@end

@implementation FindBussinessList

- (void)viewDidLoad {
	[super viewDidLoad];
	[self showBackButton:YES];
	bussinessArray = [[NSMutableArray alloc] init];
	arrayCopyBussiness=[[NSMutableArray alloc]init];
	arrayCopyBussiness = bussinessArray;
    [self customSetup];

	[self getDealOfDay];
    UIColor *color = [UIColor darkGrayColor];
    self.txtFieldSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"hint_search", @"hint_search") attributes:@{NSForegroundColorAttributeName: color}];
	[btn_SideSearchBar setTitle:NSLocalizedString(@"title_activity_BussinessList", @"title_activity_BussinessList") forState:UIControlStateNormal];
   

    
    
}
-(void)viewWillAppear:(BOOL)animated
{
	self.navigationController.navigationItem.hidesBackButton = YES;

	//[self showBackButton:YES];
	//[self getDealOfDay];
    [super viewWillAppear:animated];
    
}

-(void)getDealOfDay
{
	[[ConnectionsManager sharedManager] getFindBussinessList_withdelegate:self];
	[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return  [bussinessArray count] ;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	// To "clear" the footer view
	return [UIView new] ;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	FindBussinessListCell *cell = (FindBussinessListCell*) [self tableView:tableView cellForRowAtIndexPath:indexPath];

	WSFindBussinessListResponse *bussinessResponse = [bussinessArray objectAtIndex:indexPath.row];
	if(bussinessResponse.idTypeBusiness.integerValue >= 6)
	{
		CGSize size = [self getStringSize:cell.lblProductDiscription andString:bussinessResponse.des];
		if (size.height > 20)
			return 200 + (size.height-20);
		else
			return 200;
	}
	else if(bussinessResponse.idTypeBusiness.integerValue >= 2 && bussinessResponse.idTypeBusiness.integerValue <= 5)
	{
		CGSize size = [self getStringSize:cell.lblProductDiscription andString:bussinessResponse.des];
		if (size.height > 20)
			return 96 + (size.height-20);
		else
			return 96;
	}
	else if(bussinessResponse.idTypeBusiness.integerValue == 1)
	{
		CGSize size = [self getStringSize:cell.lblProductDiscription andString:bussinessResponse.des];
		if (size.height > 90)
			return 96 + size.height;
		else
			return 96;
	}

	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

	WSFindBussinessListResponse *bussinessResponse = [bussinessArray objectAtIndex:indexPath.row];

	if(bussinessResponse.idTypeBusiness.integerValue >= 6)
	{
		FindBussinessListCell *cell = (FindBussinessListCell *)[tableView dequeueReusableCellWithIdentifier:@"findbussiness"];
		cell.btnCall.tag=indexPath.row;

		[self populatedFindBussinessDataList:cell toCategory:bussinessResponse type:@"Big"];
		return cell;
	}
	else if(bussinessResponse.idTypeBusiness.integerValue >= 2 & bussinessResponse.idTypeBusiness.integerValue<= 5)
	{
		FindBussinessListCell *cell = (FindBussinessListCell *)[tableView dequeueReusableCellWithIdentifier:@"findbussiness_small" ];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.btnCall.tag=indexPath.row;

		[self populatedFindBussinessDataList:cell toCategory:bussinessResponse type:@"Middle"];
		return cell;
	}
	else if(bussinessResponse.idTypeBusiness.integerValue == 1)
	{
		FieldCategoryDetailCell *cell = (FieldCategoryDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"FieldCategoryDetailSmallCell" ];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		//[self populatedFindBussinessDataList:cell toCategory:bussinessResponse];
		cell.btn_Call.tag=indexPath.row;
        [self populateFieldSmallCell:cell data:bussinessResponse];

		return cell;
	}
	//[self populatedFindBussinessDataList:cell toIndexPath:indexPath];

	return  0;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WSFindBussinessListResponse *bussinessResponse = [bussinessArray objectAtIndex:indexPath.row];
    FindBussinessForMealHolderVC *categoryMealHolderVC = [self getVCWithSB_ID:kFindBussinessHolderVC];
    categoryMealHolderVC.findBussinessResponse = bussinessResponse;
    [self.navigationController pushViewController:categoryMealHolderVC animated:YES];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
	NSString *searchString = self.txtFieldSearch.text;
	if([string isEqualToString:@""])
		searchString = [searchString substringToIndex:[searchString length] - 1];
	else
		searchString = [searchString stringByAppendingString:string];

	[self searchCouponsby:searchString];
	return YES;

}
#pragma mark-OtherMethods
- (IBAction)onClick_SideMenu:(id)sender
{
}




- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btn_SideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	if (![textField.text isEqualToString:@""])
	 [self searchCouponsby:self.txtFieldSearch.text];
	return YES;
}


-(void)searchCouponsby:(NSString *)aStr
{
	NSPredicate *cityPredicate = [NSPredicate predicateWithFormat:@"(SELF.name contains[cd] %@) OR (SELF.des contains[cd] %@)",aStr, aStr];
	NSArray *filterArray = [arrayCopyBussiness filteredArrayUsingPredicate:cityPredicate];

	bussinessArray=[filterArray mutableCopy];
	

	if([aStr isEqualToString:@""])
		bussinessArray = arrayCopyBussiness;
	
	[self.findBussinessList reloadData];
}

-(void) populateFieldSmallCell:(FieldCategoryDetailCell *) cell data:(WSFindBussinessListResponse*)bussinessResponse

{
    NSString *combined = [NSString stringWithFormat:@"%@%@", mFetchImages, bussinessResponse.image];
    NSURL *finalURL=[NSURL URLWithString:combined];
    [cell.img_View sd_setImageWithURL:finalURL placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE] ];
    
    cell.img_View.layer.cornerRadius = cell.img_View.bounds.size.width / 2;
    cell.img_View.clipsToBounds = YES;
    
    cell.lbl_Name.text = [NSString stringWithFormat:@"%@",bussinessResponse.name];
    cell.lbl_Phone.text = bussinessResponse.phone;
	[cell.btn_Call addTarget:self action:@selector(touchCall:) forControlEvents:UIControlEventTouchUpInside];
	[cell.btn_ImgPopUp addTarget:self action:@selector(onClick_BtnPopUp:) forControlEvents:UIControlEventTouchUpInside];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
}


-(void)populatedFindBussinessDataList:(FindBussinessListCell*)cell toCategory:(WSFindBussinessListResponse*)bussinessResponse type:(NSString *)str_type
{
	NSString *combined ;
	if ([str_type isEqualToString:@"Big"])
	 combined= [NSString stringWithFormat:@"%@%@", mFetchImages, bussinessResponse.image];
	else
		combined= [NSString stringWithFormat:@"%@%@", mFetchImages, bussinessResponse.logo];

	NSURL *finalURL=[NSURL URLWithString:combined];
	//cell.imgProduct.image =[UIImage imageNamed:bussinessResponse.image];

	[cell.imgProduct sd_setImageWithURL:finalURL placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE] ];
    
	//NSData *imageData = [NSData dataWithContentsOfURL:finalURL];
	//cell.imgProduct.image=[cell.imgProduct.image vImageScaledImageWithSize:CGSizeMake(self.view.frame.size.width,200)];


	cell.lblProductTitle.text = [NSString stringWithFormat:@"%@",bussinessResponse.name];
	cell.lblProductDiscription.text = [NSString stringWithFormat:@"%@",bussinessResponse.des];
	CGSize size = [self getStringSize:cell.lblProductDiscription andString:bussinessResponse.des];

	if(size.height > 20)
		cell.con_lblDiscriptionHeight.constant = size.height;
	else
		cell.con_lblDiscriptionHeight.constant = 20;


	WSAddressResponse *addressResponse = [bussinessResponse.addressList objectAtIndex:0];
	NSString *address = [NSString stringWithFormat:@"%@ %@ , %@",addressResponse.street,addressResponse.NumberStreet,addressResponse.town];
	cell.lblAddress.text = address;



	[cell.btnCall addTarget:self action:@selector(touchCall:) forControlEvents:UIControlEventTouchUpInside];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(IBAction)touchCall:(id)sender
{
	UIButton *btnCall = (UIButton*)sender;
	WSFindBussinessListResponse *bussinessResponse = [bussinessArray objectAtIndex:btnCall.tag];
	NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",bussinessResponse.phone]];

	if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
		[[UIApplication sharedApplication] openURL:phoneUrl];
	} else
	{
		UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
		[calert show];
	}
}


-(IBAction)onClick_BtnPopUp:(id)sender
{
	UIButton *btnCall = (UIButton*)sender;
	WSCategoryFilterDetailResponse *bussinessResponse = [bussinessArray objectAtIndex:btnCall.tag];

	NSString *combined = [NSString stringWithFormat:@"%@%@", mFetchImages, bussinessResponse.logo];
	NSURL *finalURL=[NSURL URLWithString:combined];

	CustomImgView *imgView=[[CustomImgView alloc] initWithImage:Place_HOLDER_IMAGEBLUE imgUrl:finalURL];
	[imgView showInView:nil];

	
}
- (void) success:(WSBaseResponse *)response
{
	[DejalActivityView removeView];

	if([response.method isEqualToString:mFetchFindBusinessList])
	{
		//Fetch Coupons response
		if([response.respondeBody isKindOfClass:[NSArray class]])
		{
			NSArray *responseList = response.respondeBody;
			if(responseList.count)
			{
				NSMutableArray *temp = [NSMutableArray array];
				for(NSDictionary *dic in responseList)
				{
					WSFindBussinessListResponse *dealsResponse = [[WSFindBussinessListResponse alloc] initWithDictionary:dic];
					[temp addObject:dealsResponse];
				}
				//TODO Array
				bussinessArray = [[NSMutableArray alloc]initWithArray:temp];
				arrayCopyBussiness=bussinessArray;
				[_findBussinessList reloadData];

			}
		}
	}
}

- (void) failure:(WSBaseResponse *)response
{
	[DejalActivityView removeView];
	if([response.method isEqualToString:mGetDealOfDay])
	{

	}
}

#pragma  YSL Delegate
/*
- (void)containerViewItemIndex:(NSInteger)index currentController:(UIViewController *)controller
{
    //do nothing
}
*/


@end
