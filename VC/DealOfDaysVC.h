//
//  DetailedViewController.h
//  VIPCard
//
//  Created by SanC on 18/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"

@interface DealOfDaysVC : BaseViewController<UITextFieldDelegate,UIScrollViewDelegate>
{
    AppDelegate *appDelegate;
}
@property (strong ,nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (weak, nonatomic) IBOutlet UIButton *btnPrev;
- (IBAction)touchPrev:(id)sender;
- (IBAction)touchNext:(id)sender;
-(void)barcodeimagegenerator;
@property(nonatomic,retain)UIImageView *imgbarcode;
@property(nonatomic,retain)UILabel *lblBarCodeValue;
-(IBAction)touchToCopoun:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consTop_collectionView;


@end
