//
//  SideMenuViewController.m
//  VIPCard
//
//  Created by SanC on 22/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "SideMenuViewController.h"
#import"SWRevealViewController.h"
#import "MainScreenViewController.h"
#import "CommonCollectionViewController.h"
#import "BaseViewController.h"
#import "FindBussinessList.h"
#import "FieldCategoryFilterVC.h"
#import "BussinessListVC.h"



@interface SideMenuViewController ()

{
	NSMutableArray *arr;
	NSArray *imagesArray;
}

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBackButton:YES];
    // Do any additional setup after loading the view.
	//arr=@[@"מבצעים",@"קופונים",@"אוכל ומשלוחים",@"מוצר היום",@"מצא עסקים"];
    arr = [[NSMutableArray alloc] init];
    [arr addObject:@"מבצעים"];
    [arr addObject:@"קופונים"];
    [arr addObject:@"אוכל ומשלוחים"];
    [arr addObject:@"מוצר היום"];
    [arr addObject:@"מצא עסקים"];
    [arr addObject:@"מבצעים ברשתות"];
    //[arr addObject:@""];ההזמנות שלי
	[arr addObject:@"ההזמנות שלי"];

	//,@"def_img"
	imagesArray=@[@"menu_icon_sale",@"menu_coupon",@"menu_meal",@"menu_icon_day",@"menu_business1",@"menu_marketing_chains",@"menu_my_offer"];
	_tableView.backgroundColor = COLOR_GREEN_ALL;
	// Do any additional setup after loading the view.
    
               
    }
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [arr count];
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // To "clear" the footer view
    return [UIView new] ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	VIPCardTableViewCell *cell = (VIPCardTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"VIPCardTableViewCell"];

	if (cell==nil)
	{
		NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"VIPCardTableViewCell" owner:self options:nil];
		cell=[nib objectAtIndex:0];
	}
    cell.backgroundColor = COLOR_GREEN_ALL;
    
	cell.priceLabel.text = [arr objectAtIndex:indexPath.row];
    cell.priceLabel.textColor = [UIColor whiteColor];
	//cell.descriptionLabel.text = [arr objectAtIndex:indexPath.row];
	cell.imageView.image = [UIImage imageNamed:[imagesArray objectAtIndex:indexPath.row]];
    cell.imageView.backgroundColor = [UIColor whiteColor];
    cell.imageView.layer.cornerRadius = 0.0;
    cell.imageView.layer.masksToBounds = YES;
	//[[cell imageView]setImage:[UIImage imageNamed:[imagesArray objectAtIndex:indexPath.item]]];
	return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

	//[tableView deselectRowAtIndexPath:indexPath animated:NO];
	if (indexPath.row==4)
    {
		/*
		BaseViewController *baseViewController=[[BaseViewController alloc] init];
		SWRevealViewController *revealViewController = self.revealViewController;
		[revealViewController rightRevealToggleAnimated:YES];
		[baseViewController showFindBussinessPopUp];

		 */
        
        
		[self showFindBussinessPopUp];
        
        

	}
    else if (indexPath.row==5)
    {
       
        BussinessListVC *dealVC = [self getVCWithSB_ID:kBussinessList_VC];
        dealVC.isMarketPlace=YES;
        // [self.navigationController pushViewController:dealVC animated:YES];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.index =300;
        SWRevealViewController *vc = self.revealViewController;
        if(!vc)
        {
          vc = [self.storyboard instantiateViewControllerWithIdentifier:kRevealVC_SB_ID];
          [vc setFrontViewController:dealVC];
          [self.navigationController pushViewController:vc animated:YES];
        }
        else if( ![self.revealViewController.frontViewController isKindOfClass:[BussinessListVC class]])
        {
             [vc pushFrontViewController:dealVC animated:NO];
        }
       
        
        
        if(self.revealViewController)
            [self.revealViewController revealToggleAnimated:NO];
        
    }
    else if (indexPath.row==6)
    {
        //show my orders
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyOrdersVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }
	else
	{
       // SWRevealViewController *vc = self.revealViewController;
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSInteger oldIndex = appDelegate.index;
        appDelegate.index =indexPath.row;

		//SWRevealViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kRevealVC_SB_ID];
        if(!self.revealViewController)
        {
            SWRevealViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kRevealVC_SB_ID];
            // self.revealViewController setFrontViewController:
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if( ![self.revealViewController.frontViewController isKindOfClass:[MainScreenViewController class]]
           || (oldIndex != appDelegate.index))
        {
            MainScreenViewController *mainVC = [self getVCWithSB_ID:kMainScreenVC_SB_ID];
            [self.revealViewController pushFrontViewController:mainVC animated:NO];
        }
        
        
        if(self.revealViewController)
           [self.revealViewController revealToggleAnimated:NO];
        
		
	}

}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    MainScreenViewController *mv=[[MainScreenViewController alloc]init];
            UINavigationController *navigate=(UINavigationController*) self.revealViewController.frontViewController;
            [navigate setViewControllers:@[mv] animated:NO];
            
            [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
            
            
        }


-(void)findBussiness
{

}
-(void)filterCategory
{

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
