 //
//  ShoppingCartSummaryVC.m
//  VIPCard
//
//  Created by Andrei on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#define vHeight 60
#define lPadding 3
#define extraMealPriceWidth 60.0f

#import "WSMealsResponse.h"
#import "ShoppingCartSummaryVC.h"
#import "OrderingDetailsObj.h"
#import "AddressFormVC.h"
#import "AppGlobalConstant.h"
#import "WSConstants.h"
#import "DealOfTheDayCell.h"
#import "AppGlobalData.h"
#import "WSBussiness.h"
#import "DODcell.h"
#import "WSAdditionalExtrasResponse.h"
#import "AppDelegate.h"
#import "WSBZDeliveryTime.h"
#import "WSDeliveryTimeResponse.h"
#import "DateTimeUtil.h"

@interface ShoppingCartSummaryVC ()<DODCellDelegate>
{
    AppGlobalData *appGlobalData;
    NSMutableArray *orderType;
	NSMutableArray *listOfCartItems;
    UILabel *labelForSize;
    AppDelegate *appdelegate;
    WSBZDeliveryTime *bzdeliverytime;
    NSArray *timeArr;
}
@property (nonatomic, strong) NSString * selectedString;


@property (nonatomic, strong) NSString *strOrderNote;
@property (nonatomic, strong) NSString *strDeliveryName;

@end

@implementation ShoppingCartSummaryVC
//@synthesize listOfCartItems;
@synthesize isFromCart = _isFromCart;
@synthesize listOfCartItems = listOfCartItems;
@synthesize myOrder,isFromMyOrder,cartArr,myOrdDiscount;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBackButton:YES];
    [self.navigationController.navigationBar setTintColor:[UIColor redColor]];
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSFontAttributeName: helveticaMediumFont(18),
                                                                       
                                                                       }];
	freeCondition=false;
    
    self.strOrderNote = @"";
    
	self.title=NSLocalizedString(@"Order summary", @"Order summary");
    
    labelForSize = [[UILabel alloc] init];
    labelForSize.frame = CGRectMake(0, 0, _tableView.frame.size.width - 20, 20);
    labelForSize.numberOfLines = 0;
    

   orderType = [[NSMutableArray alloc] init];
    [orderType addObject:[NSString stringWithFormat:@"%@",NSLocalizedString(@"order_type_delivery", @"order_type_delivery")]];
    [orderType addObject:[NSString stringWithFormat:@"%@",NSLocalizedString(@"order_type_takeaway", @"order_type_takeaway")]];
    
//    btn_submit
    
    [_btnAddCart setTitle:NSLocalizedString(@"btn_submit", @"btn_submit") forState:UIControlStateNormal];
     _selectedString = [orderType objectAtIndex:0];
    
    
    appGlobalData = [AppGlobalData sharedManager];
    // Do any additional setup after loading the view.
    
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    timeArr = self.selectedBussiness.deliveryTimeList;
    NSLog(@"timeArr:%@",timeArr);
    /*
    if (timeArr == nil) {
        [self getDeliveryTime];
    }*/
    
}

-(CGFloat) stringHeight:(NSString *) str withFont:(UIFont *) font withFrame:(CGRect) frame
 {
    
    labelForSize.numberOfLines = 0;
     labelForSize.frame = frame;
    [labelForSize setText:str];
    [labelForSize setFont:font];
    [labelForSize sizeToFit];
    
    return labelForSize.frame.size.height + 2;
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	//----------------------------------

    if(self.selectedBussiness)
	{
        if (isFromMyOrder)
        {
            
            self.listOfCartItems = cartArr;
        }
		else if(_isFromCart)
		{
			self.listOfCartItems = [appGlobalData getBussinessCart:self.businessId];
		}
        
		[self.tableView reloadData];

	}
	else{
		WSBussiness *business = [appGlobalData getBusinessDataByKey:self.businessId];
		if(business == nil)
		{
			[self fetchOnlyBussiness:self.businessId];//call api
		}
		else{
			self.selectedBussiness = business;
			if(_isFromCart)
			{
				self.listOfCartItems = [appGlobalData getBussinessCart:self.businessId];
			}
			[self.tableView reloadData];
		}
	}

    // validate order time
    //NSLog(@"openingHoursList: %@",self.selectedBussiness.openingHoursList);
    //WSBaseData *data1 = [self.selectedBussiness.openingHoursList objectAtIndex:0];
    //[appdelegate validateTime:openHour time:closeHour];

	//----------------------------------------



	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWasShown:)
												 name:UIKeyboardWillShowNotification
											   object:nil];

	// Register notification when the keyboard will be hide
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWasHidden:)
												 name:UIKeyboardWillHideNotification
											   object:nil];
}

-(void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];

	[[NSNotificationCenter defaultCenter] removeObserver:self
											  name:UIKeyboardWillShowNotification
											   object:nil];

	// Register notification when the keyboard will be hide
	[[NSNotificationCenter defaultCenter] removeObserver:self
											  name:UIKeyboardWillHideNotification
											   object:nil];
}

#pragma Handle Keyboard Events
- (void)keyboardWasShown:(NSNotification *)aNotification
{
	NSDictionary* info = [aNotification userInfo];
	CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
	_tableView.contentInset = contentInsets;
	_tableView.scrollIndicatorInsets = contentInsets;

	[_tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)keyboardWasHidden:(NSNotification *)aNotification
{
	[UIView animateWithDuration:.3 animations:^(void)
	 {
		 _tableView.contentInset = UIEdgeInsetsZero;
		 _tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
	 }];
}

#pragma mark-API call
-(void)fetchOnlyBussiness:(NSString*)bussinessId
{
	[[ConnectionsManager sharedManager] fetchOnlyBussiness_withdelegate:bussinessId delegate:self];
	[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}


#pragma mark-API Response
- (void) success:(WSBaseResponse *)response
{
	//TODO save data in self.listOfCartItems
	[DejalActivityView removeView];

	if([response.method isEqualToString:@"fetchBusiness"])
	{
		self.selectedBussiness = [[WSBussiness alloc] initWithDictionary:response.respondeBody];
		[appGlobalData addBusinessData:[[WSBussiness alloc] initWithDictionary:response.respondeBody] withKey:self.businessId];
		if(_isFromCart)
		   self.listOfCartItems = [appGlobalData getBussinessCart:self.businessId];

        
        
		[self.tableView reloadData];
	}
    else if ([response.method isEqualToString:@"getBusinessDeliveryTime"])
    {
        WSDeliveryTimeResponse *result = [[WSDeliveryTimeResponse alloc] initWithDictionary:response.respondeBody];
        self.selectedBussiness.deliveryTimeList = result.deliveryTimeList;
        timeArr = self.selectedBussiness.deliveryTimeList;
        openHour = bzdeliverytime.OpenHour;
        closeHour = bzdeliverytime.CloseHour;
        
        [self submitAddress];
    }
}

- (void) failure:(WSBaseResponse *)response
{
	[DejalActivityView removeView];
	if([response.method isEqualToString:mGetDealOfDay])
	{

	}
}

-(void)showPickerView
{
    [MMPickerView showPickerViewInView:self.view
                           withStrings:orderType
                           withOptions:@{MMbackgroundColor: [UIColor whiteColor],
                                         MMtextColor: [UIColor blackColor],
                                         MMtoolbarColor: [UIColor whiteColor],
                                         MMbuttonColor: [UIColor blueColor],
                                         MMfont: [UIFont systemFontOfSize:18],
                                         MMvalueY: @3,
                                         MMselectedObject:_selectedString,
                                         MMtextAlignment:@1}
                            completion:^(NSString *selectedString) {
                                 _selectedString = selectedString;
                                [_tableView reloadData];
                            }];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat ) extraMealCellHeight:(NSArray*)extraMeals
{
    CGFloat h = 0.f;
    CGRect frame = CGRectMake(0, 0, _tableView.frame.size.width - extraMealPriceWidth - 5, 20);
    
    for(WSExtraForMealsResponse *meals in extraMeals)
    {
        if(![meals hasSelectedData])
            continue;
        NSString *descrStr=@"";
        if (isFromMyOrder) {
            descrStr = meals.selectedExtraMealsResponse.Description;
        }else{
            descrStr = meals.description;
        }
        h = h + [self stringHeight:descrStr withFont:helveticaBoldFont(18) withFrame:frame] + lPadding;
        NSString *mealStr = meals.selectedExtraMealsResponse.Name;
        if([meals.maelOrExtra boolValue])
        {
            mealStr = [self getExtraMealsFromList:meals.selectedExtraMealsResponseList ];
            
        }
        h = h + [self stringHeight:mealStr withFont:helveticaFont(16)  withFrame:frame] + lPadding;
    }
    
    return h;
}

-(UIView*)createLabel:(CGRect)frame toCount:(NSArray*)extraMeals toCell:(DODItemDetailsCell*)cell
{
    float currPosition = 0;
    UIView *view =[[UIView alloc] init];
    
    CGSize cellSize = _tableView.bounds.size;
    CGFloat prizeLblWidth = extraMealPriceWidth + 0.0f;
    
    for(WSExtraForMealsResponse *meals in extraMeals)
    {
        
        if(![meals hasSelectedData])
            continue;
        //label setting
            //UILabel *subTitleLabel = [[UILabel alloc] init];
             UILabel *subTitleLabel = [[UILabel alloc] init];
            subTitleLabel.adjustsFontSizeToFitWidth = YES;
            subTitleLabel.minimumScaleFactor = 10.0f/12.0f;
            subTitleLabel.clipsToBounds = YES;
            subTitleLabel.textColor = [UIColor blackColor];
            subTitleLabel.font=helveticaFont(16);
            [view addSubview:subTitleLabel];
        
            //title
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = 0;
        titleLabel.adjustsFontSizeToFitWidth = YES;
        titleLabel.minimumScaleFactor = 10.0f/12.0f;
        titleLabel.clipsToBounds = YES;
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.font=helveticaBoldFont(18);
        [view addSubview:titleLabel];
        
        //extra Price
        UILabel *extramealprice=[[UILabel alloc] init];
        extramealprice.adjustsFontSizeToFitWidth = YES;
        extramealprice.minimumScaleFactor = 10.0f/12.0f;
        extramealprice.clipsToBounds = YES;
        extramealprice.textColor = [UIColor blackColor];
        extramealprice.textAlignment = NSTextAlignmentLeft;
        extramealprice.font=helveticaBoldFont(18);

        [view addSubview:extramealprice];
        
        //extra price SHEKEL
        UILabel *extramealpriceShekel=[[UILabel alloc] init];
        extramealpriceShekel.adjustsFontSizeToFitWidth = YES;
        //extramealpriceShekel.minimumScaleFactor = 10.0f/12.0f;
        extramealpriceShekel.textColor = [UIColor blackColor];
        extramealpriceShekel.textAlignment = NSTextAlignmentRight;
        extramealpriceShekel.font=helveticaBoldFont(18);

        [view addSubview:extramealprice];
        [view addSubview:extramealpriceShekel];
         NSString *priceStr=@"0";
		int total = 0;
		if(meals.selectedExtraMealsResponse)
		{
            if (isFromMyOrder) {
                total = [meals.selectedExtraMealsResponse.price intValue] + [meals.extraForMealPrice intValue];
            }else{
                total = [meals.selectedExtraMealsResponse.price intValue] + [meals.price intValue];
            }
		}
		else if(meals.selectedExtraMealsResponseList
				&& [meals.selectedExtraMealsResponseList isKindOfClass:[NSArray class]])
		{
			total = 0;
			for (WSExtraForMealsResponse *r in meals.selectedExtraMealsResponseList)
			{
                if (isFromMyOrder) {
                    total=total + [r.price intValue] + [meals.extraForMealPrice intValue];
                }else{
                    total=total + [r.price intValue] + [meals.price intValue];
                }
			}


		}
		priceStr = [NSString stringWithFormat:@"%d",total];

        if(total > 0)
        {
        [extramealprice setText: [NSString stringWithFormat:@"%@",priceStr ]];
        [extramealpriceShekel setText: [NSString stringWithFormat:@"%@",SHEKEL ]];
        }
        else {
            [extramealprice setText: @""];
            [extramealpriceShekel setText: @""];
            
        }


        
        titleLabel.frame = CGRectMake(prizeLblWidth,currPosition, cellSize.width - prizeLblWidth - 5, 25);
        if (isFromMyOrder) {
            [titleLabel setText:meals.selectedExtraMealsResponse.Description];
        }else{
            [titleLabel setText:meals.description];
        }
        CGRect tmpFrame = titleLabel.frame;
        [titleLabel sizeToFit];
        tmpFrame.size.height = CGRectGetHeight(titleLabel.frame);
        titleLabel.frame = tmpFrame;
        titleLabel.textAlignment = NSTextAlignmentRight;
        
        currPosition = CGRectGetMaxY(titleLabel.frame) + lPadding;
        
        subTitleLabel.frame = CGRectMake(prizeLblWidth, currPosition ,cellSize.width - prizeLblWidth - 5, 25);
        
        subTitleLabel.numberOfLines = 0;
        
        if([meals.maelOrExtra boolValue])
        {
            NSString *mealStr = [self getExtraMealsFromList:meals.selectedExtraMealsResponseList];
            [subTitleLabel setText:mealStr];
        }
        else
        {
            [subTitleLabel setText:meals.selectedExtraMealsResponse.Name];
        }
        tmpFrame = subTitleLabel.frame;
        [subTitleLabel sizeToFit];
        tmpFrame.size.height = CGRectGetHeight(subTitleLabel.frame);
        subTitleLabel.frame = tmpFrame;
        
        subTitleLabel.textAlignment = NSTextAlignmentRight;
        
        
        extramealprice.frame=CGRectMake(29, CGRectGetMinY(titleLabel.frame), prizeLblWidth - 10, 25);
        extramealpriceShekel.frame=CGRectMake(19, CGRectGetMinY(titleLabel.frame)+2,11,19);
        
        currPosition =  CGRectGetMaxY(subTitleLabel.frame) + lPadding;
        
    }
    
    NSInteger height = currPosition; //[extraMeals count] * vHeight; //60 for VIew height
    //view.backgroundColor = [UIColor redColor];
    view.frame = CGRectMake(0, frame.size.height + frame.origin.y + 20, _tableView.frame.size.width, height);
    
    view.tag = 121;
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // To "clear" the footer view
    return [UIView new] ;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_isFromCart )
    {
        CGRect frame = CGRectMake(0, 0, 0, 21);
        frame.size.width = CGRectGetWidth(_tableView.frame) - 185;
        
        if (indexPath.row == [self.listOfCartItems count]) {
//            if (self.selectedBussiness.aditionalExtrasList==4) {
//                return 178+85;
//            }
            return 178;
        }
        else if (indexPath.row > [self.listOfCartItems count])
        {
            NSInteger myIndex = indexPath.row - [self.listOfCartItems count] - 1;
            WSAdditionalExtrasResponse *wsdata=[self.selectedBussiness.aditionalExtrasList objectAtIndex:myIndex];
            
            CGFloat sizeTitleDes =[self stringHeight:wsdata.name withFont:helveticaBoldFont(16) withFrame:frame];
            if(sizeTitleDes > 28)
                return 35 + sizeTitleDes - 28;
            return 35;
        }
        else{
            
            WSMealsResponse *wsMeansRes = [self.listOfCartItems objectAtIndex:indexPath.row];
            
            //DODItemDetailsCell *cell = (DODItemDetailsCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
            
            float height = 0.0;
            //height = cell.baseHolderView.frame.size.height;
            height = height + [self extraMealCellHeight:wsMeansRes.extraForMealsList];//cell.baseHolderView.frame.size.height;
            height = height+132;
//            cell.lblTitleDescription
//            cell.lblSubTitleDescription

           
            
            
            CGFloat sizeTitleDes =[self stringHeight:wsMeansRes.name withFont:helveticaBoldFont(16) withFrame:frame];//[self getStringSize:cell.lblTitleDescription andString:cell.lblTitleDescription.text];

            CGFloat sizeTitleSubDes = [self stringHeight:wsMeansRes.description withFont:helveticaFont(16) withFrame:frame];//[self getStringSize:cell.lblSubTitleDescription andString:cell.lblSubTitleDescription.text];
        
            if(sizeTitleDes > 21)
            {
                height += sizeTitleDes - 11 ;
            }

            if(sizeTitleSubDes > 90)
            {
                height += sizeTitleSubDes - 80;
            }
           // else
          //  {
           //     height = height + 0;
           // }
            
            return height;
        }
    }
    else{

		if (indexPath.row == 0) {

			CGFloat height = 132;

			//            cell.lblTitleDescription
			//            cell.lblSubTitleDescription

			CGRect frame = CGRectMake(0, 0, 0, 21);
			frame.size.width = CGRectGetWidth(_tableView.frame) - 185;


			CGFloat sizeTitleDes =[self stringHeight:_wsCouponsResponse.name withFont:helveticaBoldFont(16) withFrame:frame];//[self getStringSize:cell.lblTitleDescription andString:cell.lblTitleDescription.text];

			CGFloat sizeTitleSubDes = [self stringHeight:_wsCouponsResponse.des withFont:helveticaFont(16) withFrame:frame];//[self getStringSize:cell.lblSubTitleDescription andString:cell.lblSubTitleDescription.text];

			if(sizeTitleDes > 21)
			{
				height += sizeTitleDes - 11 ;
			}

			if(sizeTitleSubDes > 90)
			{
				height += sizeTitleSubDes - 80;
			}

			return height;

			//return 132; //Minus item discription height
		}
		else{
			return 178 ; //Minus total cell height
		}

		/*
		CGRect frame = CGRectMake(0, 0, 0, 21);
		frame.size.width = CGRectGetWidth(_tableView.frame) - 185;

		if (indexPath.row == [self.listOfCartItems count]) {
			//            if (self.selectedBussiness.aditionalExtrasList==4) {
			//                return 178+85;
			//            }
			return 178;
		}

		else{

			WSMealsResponse *wsMeansRes = [self.listOfCartItems objectAtIndex:indexPath.row];

			//DODItemDetailsCell *cell = (DODItemDetailsCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];

			float height = 0.0;
			//height = cell.baseHolderView.frame.size.height;
			height = height + [self extraMealCellHeight:wsMeansRes.extraForMealsList];//cell.baseHolderView.frame.size.height;
			height = height+132;
			




			CGFloat sizeTitleDes =[self stringHeight:wsMeansRes.name withFont:helveticaBoldFont(16) withFrame:frame];//[self getStringSize:cell.lblTitleDescription andString:cell.lblTitleDescription.text];

			CGFloat sizeTitleSubDes = [self stringHeight:wsMeansRes.description withFont:helveticaFont(16) withFrame:frame];//[self getStringSize:cell.lblSubTitleDescription andString:cell.lblSubTitleDescription.text];

			if(sizeTitleDes > 21)
			{
				height += sizeTitleDes - 11 ;
			}

			if(sizeTitleSubDes > 90)
			{
				height += sizeTitleSubDes - 80;
			}
			return height;


    }*/

	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_isFromCart)
    {

		return (self.listOfCartItems && self.listOfCartItems.count > 0) ?[self.listOfCartItems count] + 1 + [self.selectedBussiness.aditionalExtrasList count]: 0;
    }
    else
    {
		return (self.selectedBussiness)?2:0;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}             // called when 'return' key pressed. return NO to ignore.

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    return YES;
}



- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag == 251)
    {
        self.strOrderNote = textField.text;
    }
    
}



-(double)getTotalPrice
{
  //  double total = 0;
    double mainTotal = 0;
    for(WSMealsResponse *wsMealsRes in self.listOfCartItems)
     {
         for(WSExtraForMealsResponse *meals in wsMealsRes.extraForMealsList)
         {
			 if(![meals hasSelectedData])
				 continue;
             int subtotal=0;
     
             if([meals.selectedExtraMealsResponseList isKindOfClass:[NSArray class]])
             {
                 for (WSExtraForMealsResponse *r in meals.selectedExtraMealsResponseList)
                 {
                     if (isFromMyOrder) {
                         subtotal = subtotal+[r.price intValue] + [meals.extraForMealPrice intValue];
                     }else{
                         subtotal = subtotal+[r.price intValue] + [meals.price intValue];
                     }
                 }
                 
             }
             else if([meals.selectedExtraMealsResponse isKindOfClass:[WSExtraMealsResponse class]])
             {
                 if (isFromMyOrder) {
                     subtotal = [meals.extraForMealPrice intValue] + [meals.selectedExtraMealsResponse.price intValue];
                 }else{
                     subtotal = [meals.price intValue] + [meals.selectedExtraMealsResponse.price intValue];
                 }
             }
              mainTotal += subtotal;
        }
         mainTotal += [wsMealsRes.price integerValue];
     }
    
    
     /*  if(mainTotal > [self.selectedBussiness.minDelivery intValue])
       {
           total += mainTotal;
 
       }
       else
         total += mainTotal+[self.selectedBussiness.delivery intValue];*/
    //End
   // return total;
    //now here add addional items
    if([self.selectedBussiness.aditionalExtrasList count] > 0)
    {
        for (WSAdditionalExtrasResponse *item in self.selectedBussiness.aditionalExtrasList)
        {
           if(item.selected)
             mainTotal += [item.price intValue];
        }
    }
   
   
    return mainTotal;
   
}


-(BOOL) isValidForDiscount{
    
    //Now check if any one item not belong to cat 64
    BOOL isValid = TRUE;
    
    for(WSMealsResponse *wsMealsRes in self.listOfCartItems)
    {
        if(wsMealsRes.idCategoryForBusiness != nil && [wsMealsRes.idCategoryForBusiness intValue] == 64)
        {
            isValid = FALSE;
            break;
        }
    }
    
    //@Sarika
    if (isFromMyOrder) {
        if ([myOrdDiscount intValue] <= 0) {
            isValid = FALSE;
        }
    }
    
    return self.isFromCart && isValid && !self.isFromDealCouponScreen;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{


    if(_isFromCart)
    {
    
        if (indexPath.row == [self.listOfCartItems count]) {
        
            
            DODTotalCell *dodTotalCell = [tableView dequeueReusableCellWithIdentifier:@"dodtotalcell"];

            dodTotalCell.selectionStyle = UITableViewCellSelectionStyleNone;
            dodTotalCell.txtMessage.delegate=self;
            dodTotalCell.txtMessage.tag = 251;
			dodTotalCell.img_BckOfTextfield.layer.borderWidth=1.0f;
            dodTotalCell.txtMessage.text = self.strOrderNote;
			//dodTotalCell.img_BckOfTextfield.layer.borderColor= COLOR_LIGHT_GRAY].CGColor;
            NSInteger mainTotal = 0;
            
            
            mainTotal = [self getTotalPrice];

			NSInteger disct = 0;
            
            
			if([self isValidForDiscount] && self.selectedBussiness.vipDiscount && [self.selectedBussiness.vipDiscount integerValue] > 0)
            {
                disct = ((mainTotal * [self.selectedBussiness.vipDiscount integerValue]) / 100);
                NSLog(@"discount a: %ld",disct);
                dodTotalCell.con_vipdiscount_ht.constant=21;
                dodTotalCell.con_vipdiscount_shekel_ht.constant=17;
                dodTotalCell.lblDiscount.text = [NSString stringWithFormat:@" %@ %ld",NSLocalizedString(@"lbl_discount", @"lbl_discount"),(long)disct];
                dodTotalCell.lbl_discountShekel.text=[NSString stringWithFormat:@"%@",SHEKEL];
            }else{
                dodTotalCell.con_vipdiscount_ht.constant=0;
                dodTotalCell.con_vipdiscount_shekel_ht.constant=0;
                dodTotalCell.lblDiscount.text = @"";
                dodTotalCell.lbl_discountShekel.text=@"";
            }
            dodTotalCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            dodTotalCell.lblMainTotal.text =  [NSString stringWithFormat:@" %@ %ld",NSLocalizedString(@"sub_total_cart_price", @"sub_total_cart_price"),(long)mainTotal];
            dodTotalCell.lbl_mainTotalShekel.text=[NSString stringWithFormat:@"%@",SHEKEL];
            
            
            if(mainTotal > [self.selectedBussiness.minDelivery intValue])
            {
               dodTotalCell.lblTotalAfterDiscount.text =  [NSString stringWithFormat:@" %@ %ld",NSLocalizedString(@"total_order_summery_price", @"total_order_summery_price"),(long) mainTotal - disct];
                dodTotalCell.lbl_totalAfterDiscountShekel.text=  [NSString stringWithFormat:@" %@",SHEKEL];

            }
            else
                dodTotalCell.lblTotalAfterDiscount.text =  [NSString stringWithFormat:@" %@ %ld",NSLocalizedString(@"total_order_summery_price", @"total_order_summery_price"),(long) mainTotal - disct + [self.selectedBussiness.delivery intValue]];
            dodTotalCell.lbl_totalAfterDiscountShekel.text=  [NSString stringWithFormat:@" %@",SHEKEL];
            
           
            //End

            
            

            
            [dodTotalCell.btnDeliveryType addTarget:self action:@selector(orderTypeAction:) forControlEvents:UIControlEventTouchUpInside];
            
            if([_selectedString isEqualToString:NSLocalizedString(@"order_type_delivery", @"order_type_delivery")])
            {
                
                 if(mainTotal < [self.selectedBussiness.minDelivery intValue])
                {
                    dodTotalCell.lblDeliveryName.text = [NSString stringWithFormat:@"%@",self.selectedBussiness.delivery];
                     dodTotalCell.lblDeliveryNameShekel.text = [NSString stringWithFormat:@"%@",SHEKEL];
                    freeCondition=false;
                   
                }
                else
                {
                    dodTotalCell.lblDeliveryName.text = NSLocalizedString(@"lbl_free_delivery", @"lbl_free_delivery");
                    dodTotalCell.lblDeliveryNameShekel.text=@"";
					freeCondition=true;

                }
                dodTotalCell.lblOrderType.text = _selectedString;
                dodTotalCell.lblDeliveryName.textColor = [UIColor blackColor];
            }
            else
            {
                dodTotalCell.lblOrderType.text = _selectedString;
                dodTotalCell.lblDeliveryName.text = @"";
				dodTotalCell.lblDeliveryNameShekel.text=@"";

//				//working shilpa
				long total=(long) mainTotal-disct;
				dodTotalCell.lblTotalAfterDiscount.text= [NSString stringWithFormat:@"%ld",total] ;
            }
            path=indexPath;
            
            self.strDeliveryName = dodTotalCell.lblDiscount.text;
            
            
            return  dodTotalCell;
            
        }
        else if (indexPath.row > [self.listOfCartItems count])
        {
            NSInteger myIndex = indexPath.row - [self.listOfCartItems count] - 1;
           WSAdditionalExtrasResponse *wsdata=[self.selectedBussiness.aditionalExtrasList objectAtIndex:myIndex];
             DODcell *dodCell = [tableView dequeueReusableCellWithIdentifier:@"dodcell"];
            if(wsdata.price && [wsdata.price intValue] > 0)
            {
            dodCell.lbl_Price.text= [NSString stringWithFormat:@"%@",wsdata.price];
            dodCell.lbl_priceShekel.text= [NSString stringWithFormat:@"%@",SHEKEL];
            }
            else {
                dodCell.lbl_Price.text= @"";
                dodCell.lbl_priceShekel.text= @"";
            }
            dodCell.lbl_Name.text=[NSString stringWithFormat:@"%@",wsdata.name];
            [dodCell updateCheckBox:wsdata.selected];
            dodCell.cellData = wsdata;
            dodCell.cellDelegate = self;
            dodCell.cellIndex = myIndex;
            [dodCell.lbl_Name sizeToFit];
            CGFloat h = dodCell.lbl_Name.frame.size.height;
            if( h > 28)
                dodCell.constaintLblNameHeight.constant = h;
            else
                dodCell.constaintLblNameHeight.constant = 21.f;
            
            return  dodCell;
        }
        else
        {

            WSMealsResponse *wsMeansRes = [self.listOfCartItems objectAtIndex:indexPath.row];
        
            DODItemDetailsCell *dodItemDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"doddetailscell"];
            dodItemDetailsCell.selectionStyle = UITableViewCellSelectionStyleNone;
            if(wsMeansRes.price && [wsMeansRes.price intValue] > 0)
            {
            dodItemDetailsCell.lblPrice.text = [NSString stringWithFormat:@"%@",wsMeansRes.price];
            dodItemDetailsCell.lblShekel.text= [NSString stringWithFormat:@"%@",SHEKEL];
            }
            else {
                dodItemDetailsCell.lblPrice.text = @"";
                dodItemDetailsCell.lblShekel.text= @"";
            }
			dodItemDetailsCell.lblTitleDescription.font = helveticaBoldFont(16);
            dodItemDetailsCell.lblTitleDescription.text = [NSString stringWithFormat:@"%@",wsMeansRes.name];
			dodItemDetailsCell.lblSubTitleDescription.font = helveticaFont(16);
            dodItemDetailsCell.lblSubTitleDescription.text = [NSString stringWithFormat:@"%@",wsMeansRes.description];
            
            if(wsMeansRes.image.length > 0){
                NSString *imgStr = [NSString stringWithFormat:@"%@%@",mFetchImages,wsMeansRes.image];
                [ dodItemDetailsCell.imgProduct sd_setImageWithURL:[NSURL URLWithString:imgStr] placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
                //dodItemDetailsCell.imgProduct.image = [UIImage imageNamed:wsMeansRes.image];
            }
            else
                dodItemDetailsCell.imgProduct.image = [UIImage imageNamed:PLACE_HOLDER_IMAGE];
            
            dodItemDetailsCell.btnDelete.tag = indexPath.row;
            dodItemDetailsCell.btnDelete.tintColor=[UIColor whiteColor];
            [dodItemDetailsCell.btnDelete addTarget:self action:@selector(deleteItem:) forControlEvents:UIControlEventTouchUpInside];
            [dodItemDetailsCell.btnDelete  setTitle:NSLocalizedString(@"button_delete", @"button_delete") forState:UIControlStateNormal];

			[dodItemDetailsCell.lblTitleDescription sizeToFit];

            
			CGSize sizeTitleDes = dodItemDetailsCell.lblTitleDescription.frame.size;
			//[self getStringSize:dodItemDetailsCell.lblTitleDescription andString:dodItemDetailsCell.lblTitleDescription.text];
			[dodItemDetailsCell.lblSubTitleDescription sizeToFit];
			CGSize sizeTitleSubDes = dodItemDetailsCell.lblSubTitleDescription.frame.size;
			//[self getStringSize:dodItemDetailsCell.lblSubTitleDescription andString:dodItemDetailsCell.lblSubTitleDescription.text];
            
            
            if(sizeTitleDes.height > 21)
            {
                dodItemDetailsCell.con_titleDescription.constant =  sizeTitleDes.height;
            }
			else
			{
				dodItemDetailsCell.con_titleDescription.constant = 21;
			}

            if(sizeTitleSubDes.height > 90)
            {
                dodItemDetailsCell.con_subTitleDescription.constant =  sizeTitleSubDes.height;
            }
			else
			{
				dodItemDetailsCell.con_subTitleDescription.constant = 90;
			}

            if(dodItemDetailsCell.baseHolderView)
            {
                [dodItemDetailsCell.baseHolderView removeFromSuperview];
                dodItemDetailsCell.baseHolderView = nil;
            }
			
            
            if((sizeTitleSubDes.height + sizeTitleDes.height) > 111)
            {
                dodItemDetailsCell.baseHolderView = [self createLabel:dodItemDetailsCell.lblSubTitleDescription.frame toCount:wsMeansRes.extraForMealsList toCell:dodItemDetailsCell];
            }
            else
            {
                dodItemDetailsCell.baseHolderView = [self createLabel:dodItemDetailsCell.btnDelete.frame toCount:wsMeansRes.extraForMealsList toCell:dodItemDetailsCell];

            }
            
            [dodItemDetailsCell addSubview:dodItemDetailsCell.baseHolderView];
            
            // [self createLabel:dodItemDetails.btnDelete.frame toCount:wsMeansRes.extraForMealsList];
            
           // [self stringHeight:self.view.frame toData:wsMeansRes.extraForMealsList toCell:dodItemDetailsCell];
            
            //set lable height - multi line by - RR
            
            
            return dodItemDetailsCell;



        }
        
    }
	else  //-------------BELOW SECTION FOR SINGLE ITEM CAME FROM DEAL/COUPON PAGE-----------------------
    {
        if (indexPath.row == 0) {


//
			DODItemDetailsCell *dodItemDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"doddetailscell"];
			dodItemDetailsCell.selectionStyle = UITableViewCellSelectionStyleNone;

			dodItemDetailsCell.lblPrice.text = [NSString stringWithFormat:@"%@",_wsCouponsResponse.price2];
			dodItemDetailsCell.lblShekel.text= [NSString stringWithFormat:@"%@",SHEKEL];
			dodItemDetailsCell.lblTitleDescription.font = helveticaBoldFont(16);
			dodItemDetailsCell.lblTitleDescription.text = [NSString stringWithFormat:@"%@",_wsCouponsResponse.name];
			dodItemDetailsCell.lblSubTitleDescription.font = helveticaFont(16);
			dodItemDetailsCell.lblSubTitleDescription.text = [NSString stringWithFormat:@"%@",_wsCouponsResponse.des];

			if(_wsCouponsResponse.image.length > 0){
				NSString *imgStr = [NSString stringWithFormat:@"%@%@",mFetchImages,_wsCouponsResponse.image];
				[ dodItemDetailsCell.imgProduct sd_setImageWithURL:[NSURL URLWithString:imgStr] placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
				//dodItemDetailsCell.imgProduct.image = [UIImage imageNamed:wsMeansRes.image];
			}
			else
				dodItemDetailsCell.imgProduct.image = [UIImage imageNamed:PLACE_HOLDER_IMAGE];

			dodItemDetailsCell.btnDelete.tag = indexPath.row;
			dodItemDetailsCell.btnDelete.tintColor=[UIColor whiteColor];
			[dodItemDetailsCell.btnDelete addTarget:self action:@selector(deleteItem:) forControlEvents:UIControlEventTouchUpInside];
			[dodItemDetailsCell.btnDelete  setTitle:NSLocalizedString(@"button_delete", @"button_delete") forState:UIControlStateNormal];


			[dodItemDetailsCell.lblTitleDescription sizeToFit];


			CGSize sizeTitleDes = dodItemDetailsCell.lblTitleDescription.frame.size;
			//[self getStringSize:dodItemDetailsCell.lblTitleDescription andString:dodItemDetailsCell.lblTitleDescription.text];
			[dodItemDetailsCell.lblSubTitleDescription sizeToFit];
			CGSize sizeTitleSubDes = dodItemDetailsCell.lblSubTitleDescription.frame.size;


			//CGSize sizeTitleDes = [self getStringSize:dodItemDetailsCell.lblTitleDescription andString:dodItemDetailsCell.lblTitleDescription.text];

			//CGSize sizeTitleSubDes = [self getStringSize:dodItemDetailsCell.lblSubTitleDescription andString:dodItemDetailsCell.lblSubTitleDescription.text];


			if(sizeTitleDes.height > 21)
			{
				dodItemDetailsCell.con_titleDescription.constant =  sizeTitleDes.height;
			}

			if(sizeTitleSubDes.height > 90)
			{
				dodItemDetailsCell.con_subTitleDescription.constant =  sizeTitleSubDes.height;
			}

			if(dodItemDetailsCell.baseHolderView)
			{
				[dodItemDetailsCell.baseHolderView removeFromSuperview];
				dodItemDetailsCell.baseHolderView = nil;
			}
			return dodItemDetailsCell;

			//[dodItemDetailsCell addSubview:dodItemDetailsCell.baseHolderView];


			//-----------------------old code


//            DODItemDetailsCell *dodItemDetails = [tableView dequeueReusableCellWithIdentifier:@"doddetailscell"];
//
//
//
//
//            dodItemDetails.selectionStyle = UITableViewCellSelectionStyleNone;
//            dodItemDetails.lblPrice.text = [NSString stringWithFormat:@"%@",_wsCouponsResponse.price2];
//            dodItemDetails.lblTitleDescription.text = [NSString stringWithFormat:@"%@",_wsCouponsResponse.name];
//            dodItemDetails.lblSubTitleDescription.text = [NSString stringWithFormat:@"%@",_wsCouponsResponse.des];
//            dodItemDetails.imgProduct.image = [UIImage imageNamed:_wsCouponsResponse.image];
//            
//            dodItemDetails.viewItemDetailsHeightConstraints.constant = 0;
//            dodItemDetails.viewDetail.hidden = YES;
//            
//            [dodItemDetails.btnDelete addTarget:self action:@selector(deleteItem:) forControlEvents:UIControlEventTouchUpInside];
//            
//            return dodItemDetails;

        }
        else
        {
           /* DODTotalCell *dodTotalCell = [tableView dequeueReusableCellWithIdentifier:@"dodtotalcell"];
            dodTotalCell.selectionStyle = UITableViewCellSelectionStyleNone;
            dodTotalCell.lblTotalAfterDiscount.text =  [NSString stringWithFormat:@"%@",_wsCouponsResponse.price2];
            dodTotalCell.viewMailTotalHeigthConstraints.constant = 0;
            dodTotalCell.viewMainTotal.hidden = YES;
            return  dodTotalCell;*/
			DODTotalCell *dodTotalCell = [tableView dequeueReusableCellWithIdentifier:@"dodtotalcell"];

			dodTotalCell.selectionStyle = UITableViewCellSelectionStyleNone;
			dodTotalCell.txtMessage.delegate=self;
			dodTotalCell.txtMessage.tag = 251;
			dodTotalCell.img_BckOfTextfield.layer.borderWidth=1.0f;
			dodTotalCell.txtMessage.text = self.strOrderNote;
			//dodTotalCell.img_BckOfTextfield.layer.borderColor= COLOR_LIGHT_GRAY].CGColor;
			NSInteger mainTotal = 0;


			mainTotal = [self.wsCouponsResponse.price2 integerValue];

			NSInteger disct = 0;
			if([self isValidForDiscount] && self.selectedBussiness.vipDiscount && [self.selectedBussiness.vipDiscount integerValue] > 0)
            {	disct = ((mainTotal * [self.selectedBussiness.vipDiscount integerValue]) / 100);
                NSLog(@"discount b: %ld",disct);
                dodTotalCell.con_vipdiscount_ht.constant=21;
                dodTotalCell.con_vipdiscount_shekel_ht.constant=17;
            }else{
                dodTotalCell.con_vipdiscount_ht.constant=0;
                dodTotalCell.con_vipdiscount_shekel_ht.constant=0;
            }
			dodTotalCell.selectionStyle = UITableViewCellSelectionStyleNone;


			dodTotalCell.lblMainTotal.text =  [NSString stringWithFormat:@" %@ %ld",NSLocalizedString(@"sub_total_cart_price", @"sub_total_cart_price"),(long)mainTotal];
			dodTotalCell.lbl_mainTotalShekel.text=[NSString stringWithFormat:@"%@",SHEKEL];
			dodTotalCell.lblDiscount.text = [NSString stringWithFormat:@" %@ %ld",NSLocalizedString(@"lbl_discount", @"lbl_discount"),(long)disct];
			dodTotalCell.lbl_discountShekel.text=[NSString stringWithFormat:@"%@",SHEKEL];

			if(mainTotal > [self.selectedBussiness.minDelivery intValue])
			{
				dodTotalCell.lblTotalAfterDiscount.text =  [NSString stringWithFormat:@" %@ %ld",NSLocalizedString(@"total_order_summery_price", @"total_order_summery_price"),(long) mainTotal - disct];
				dodTotalCell.lbl_totalAfterDiscountShekel.text=  [NSString stringWithFormat:@" %@",SHEKEL];

			}
			else
            {
				dodTotalCell.lblTotalAfterDiscount.text =  [NSString stringWithFormat:@" %@ %ld",NSLocalizedString(@"total_order_summery_price", @"total_order_summery_price"),(long) mainTotal - disct + [self.selectedBussiness.delivery intValue]];
			dodTotalCell.lbl_totalAfterDiscountShekel.text=  [NSString stringWithFormat:@" %@",SHEKEL];
            }


			//End


			


			[dodTotalCell.btnDeliveryType addTarget:self action:@selector(orderTypeAction:) forControlEvents:UIControlEventTouchUpInside];

			if([_selectedString isEqualToString:NSLocalizedString(@"order_type_delivery", @"order_type_delivery")])
			{
				
				if(mainTotal < [self.selectedBussiness.minDelivery intValue])
				{
					dodTotalCell.lblDeliveryName.text = [NSString stringWithFormat:@"%@",self.selectedBussiness.delivery];
					dodTotalCell.lblDeliveryNameShekel.text = [NSString stringWithFormat:@"%@",SHEKEL];
                    freeCondition = false;

				}
				else
				{
					dodTotalCell.lblDeliveryName.text = NSLocalizedString(@"lbl_free_delivery", @"lbl_free_delivery");
                    dodTotalCell.lblDeliveryNameShekel.text = @"";
					
                    freeCondition = true;


				}
				dodTotalCell.lblOrderType.text = _selectedString;
				dodTotalCell.lblDeliveryName.textColor = [UIColor blackColor];
			}
			else
			{
				dodTotalCell.lblOrderType.text = _selectedString;
				dodTotalCell.lblDeliveryName.text = @"";
                dodTotalCell.lblDeliveryNameShekel.text = @"";

			}
			path=indexPath;

			self.strDeliveryName = dodTotalCell.lblDiscount.text;


			return  dodTotalCell;


        }
    }

    //DODTotalCell *dodTotalCell = [tableView dequeueReusableCellWithIdentifier:@"DODTotalCell" forIndexPath:indexPath];

}

#pragma -Handle Check/Uncheck of Additional
-(void) dodCell:(DODcell *)dodCell onSelectionChange:(WSAdditionalExtrasResponse *)aData
{
    //reload cell;
    NSIndexPath *totalRowIndex = [NSIndexPath indexPathForRow:[self.listOfCartItems count] inSection:0];
    
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:totalRowIndex, nil] withRowAnimation:UITableViewRowAnimationNone];


}

-(IBAction)orderTypeAction:(id)sender
{
    [self showPickerView];
//    lbl_free_delivery
}

-(IBAction)deleteItem:(id)sender
{
    UIButton *btnTag = (UIButton*)sender;
    
    [appGlobalData removeFromBusinessCart:self.businessId cartItem:[self.listOfCartItems objectAtIndex:btnTag.tag]];
    self.listOfCartItems = [appGlobalData getBussinessCart:self.businessId];
    //[self.listOfCartItems removeObjectAtIndex:btnTag.tag];
    if([self.listOfCartItems count] == 0)
        [self.navigationController popViewControllerAnimated:YES];
    [_tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //nothing
}



-(IBAction)AddAddress:(id)sender
{
    
    //added by Sarika [02-08-2016]
    // validate order time
    //NSLog(@"openingHoursList: %@",mealResponse);
    
    NSLog(@"timeArr:%@",timeArr);
    if (timeArr == nil) {
        [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
        [self getDeliveryTime];
    }else{
        [self submitAddress];
    }
    //WSBaseData *data1 = [self.selectedBussiness.openingHoursList objectAtIndex:0];
}
-(void)submitAddress{

    BOOL isValidTime = YES;
    if (timeArr != nil) {
        
        for (WSBZDeliveryTime *bzDelTimeObj in timeArr) {
        
            NSLog(@"openhour, closehour:%@, %@",bzDelTimeObj.OpenHour,bzDelTimeObj.CloseHour);
            isValidTime = [DateTimeUtil validateTime:bzDelTimeObj.OpenHour time:bzDelTimeObj.CloseHour];
            if (isValidTime) {
                break;
            }
        }
    }
    
       if (!isValidTime) {
        [[[[iToast makeText:NSLocalizedString(@"Business not open for delivery now, please come back later", nil)] setGravity:iToastGravityBottom] setDuration:iToastDurationShort] show];
        
    }else{
    OrderingDetailsObj *orderingDetailsObj = [[OrderingDetailsObj alloc] init];
    AddressFormVC *addressVC = [self getVCWithSB_ID:kAddressFormVC];

    if(_isFromCart)
    {
        //[appGlobalData.listOfCartItems count]
        //DODTotalCell *cell = (DODTotalCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self.listOfCartItems count] inSection:0]];
        /*
         @property (strong, nonatomic) IBOutlet UIView *viewMainTotal;
         @property (strong, nonatomic) IBOutlet UILabel *lblMainTotal;
         @property (strong, nonatomic) IBOutlet UILabel *lblDiscount;
         @property (strong, nonatomic) IBOutlet UIButton *btnDeliveryType;
         @property (strong, nonatomic) IBOutlet UILabel *lblDeliveryName;
         @property (strong, nonatomic) IBOutlet UILabel *lblTotalAfterDiscount;
         @property (strong, nonatomic) IBOutlet UITextField *txtMessage;
         @property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewMailTotalHeigthConstraints;
         */
        
        long total = [self getTotalPrice];
        
        orderingDetailsObj.priceBeforeDiscount = [NSString stringWithFormat:@"%ld",total];




        float disc = 0.0;
        
        if([self isValidForDiscount])
           disc = ((total * [self.selectedBussiness.vipDiscount intValue]) / 100);


        orderingDetailsObj.totalAmount = [NSString stringWithFormat:@"%ld", (long)(total - disc)];

		if(![_selectedString isEqualToString:NSLocalizedString(@"order_type_delivery", @"order_type_delivery")] || freeCondition)
		{
			long total_new=(long)(total - disc);
			orderingDetailsObj.totalAfterDiscount=[NSString stringWithFormat:@"%ld",total_new];
            orderingDetailsObj.TakeAwayOrDelivery = @"2";
            
		}
		else
		{
			long total_new=(long)(total - disc)+[self.selectedBussiness.delivery intValue];

			orderingDetailsObj.totalAfterDiscount=[NSString stringWithFormat:@"%ld", total_new];
            
            orderingDetailsObj.TakeAwayOrDelivery = @"1";

		}


        orderingDetailsObj.priceDiscount = [NSString stringWithFormat:@"%ld",(long)disc];
        //TODO Delivery Charges
        
        orderingDetailsObj.CommentOnOrder =self.strOrderNote ;//cell.txtMessage.text;
        //orderingDetailsObj.TakeAwayOrDelivery = self.strDeliveryName;//cell.lblDeliveryName.text;
        addressVC.selectedOrderObj = orderingDetailsObj;
        addressVC.wsCouponsResponse  = nil;
    }
    else
    {
        addressVC.wsCouponsResponse = self.wsCouponsResponse;
        addressVC.selectedOrderObj = nil;
        
        
    }
        
    if(![_selectedString isEqualToString:NSLocalizedString(@"order_type_delivery", @"order_type_delivery")])
        {
            addressVC.takeawayOrDeliveryFlag = @"2";
        }
    else{
        addressVC.takeawayOrDeliveryFlag = @"1";
    }
    addressVC.orderComment = self.strOrderNote;
	addressVC.selectedBussiness = self.selectedBussiness;
    addressVC.isFromCart = _isFromCart;
    if (isFromMyOrder) {
        addressVC.myOrder = myOrder;
        addressVC.isFromMyOrder = isFromMyOrder;
    }
    [self.navigationController pushViewController:addressVC animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//-(UIView*)createLabel:(CGRect)frame toCount:(NSArray*)extraMeals toCell:(DODItemDetailsCell*)cell

/*
 Run loop to get the string from selected extra meals
*/

-(NSString *)getExtraMealsFromList:(NSArray *)extraMeals
{
    NSMutableString *mealStr = [NSMutableString string];
    
    for (WSExtraMealsResponse *selecExtraMeals in extraMeals)
    {
        if(mealStr.length > 0)
            [mealStr appendString:[NSString stringWithFormat:@", %@" ,selecExtraMeals.Name]];
        else
            [mealStr appendString:[NSString stringWithFormat:@"%@" ,selecExtraMeals.Name]];
    }
    
    return mealStr;
}

-(NSString *)getExtraMeals:(WSExtraForMealsResponse *)extraMeals
{
    if(extraMeals.selectedExtraMealsResponse)
    {
        return extraMeals.selectedExtraMealsResponse.Name;
    }
    return nil;
}

-(CGRect)stringHeight:(CGRect)frame toData:(NSArray*)extraMeals toCell:(DODItemDetailsCell*)cell
{

    
    return frame;
}

//added by Sarika 03/08/2016
-(void)getDeliveryTime
{
    [[ConnectionsManager sharedManager] getBusinessDeliveryTime_withdelegate:self.businessId delegate:self];
}

@end
