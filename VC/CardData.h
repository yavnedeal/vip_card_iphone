//
//  CardData.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 22/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface CardData : WSBaseData


@property (strong , nonatomic) NSString *ownname,*ownid,*cardno,*cardtype,*expmon,*expyr,*ordamt,*cvv;
@end
