//
//  SlideShowVC.h
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Base/BaseViewController.h"

@interface SlideShowVC : BaseViewController
@property (strong, nonatomic) IBOutlet UICollectionView *galleryCollectionView;
@end
