//
//  FactoryListData.m
//  VIPCard
//
//  Created by Manoj Kumar on 11/5/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "FactoryListData.h"

@implementation FactoryListData
@synthesize id, name;


-(id)initWithDictionary:(NSDictionary *)dict
{
    self.id = [dict objectForKey:@"id"];
    self.name = [dict objectForKey:@"name"];
    
    
    return self;
}

-(id)populateCoreDataFromObject:(id)coreData
{
    Fcatorycd *factCD = (Fcatorycd*)coreData;
    factCD.factoryID = self.id;
    factCD.name = self.name;
    
    return factCD;
}
-(void)populateObjectFromCorData:(id)coreData
{
    Fcatorycd *factCD = (Fcatorycd*)coreData;
    self.id = factCD.factoryID;
    self.name = factCD.name;
}

@end
