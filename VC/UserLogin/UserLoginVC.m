
//
//  UserLoginVC.m
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "UserLoginVC.h"



@interface UserLoginVC ()<ServerResponseDelegate>
{
    BOOL flag;
   
}
@end

@implementation UserLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBackButton:YES];
    [self setupUI];
    
    

}
-(IBAction)ActionBack:(id)sender

{
        [self dismissViewControllerAnimated:YES completion:^{
        }];

    }

-(void)memberloginAPI
{
    NSString *str;
    [[ConnectionsManager sharedManager] getloginwithmember_withdelegate:str delegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
    
}

-(void)setupUI
{
   self.lbl_Dont_have_card.text = NSLocalizedString(@"TITLE_DONT_HAVE_CARD", @"TITLE_DONT_HAVE_CARD");
   self.lbl_Have_Card.text  = NSLocalizedString(@"TITLE_HAVE_CARD", @"TITLE_HAVE_CARD");
   self.lbl_Please_SignIn.text  = NSLocalizedString(@"TITLE_PLEASE_SIGN_IN", @"TITLE_PLEASE_SIGN_IN");
   self.lbl_Titcker_Card.text  = NSLocalizedString(@"TITLE_TICKET", @"TITLE_TICKET");
    
    [_btn_Login setTitle:NSLocalizedString(@"BUTTON_TITLE_LOGIN", @"BUTTON_TITLE_LOGIN") forState:UIControlStateNormal];
    [_btnCheckOut setTitle:NSLocalizedString(@"TITLE_CHECKOUT", @"TITLE_CHECKOUT") forState:UIControlStateNormal];
    
    [_btn_Login buttonCornerRadius:_btn_Login toRadius:12];
    [_btnCheckOut buttonCornerRadius:_btnCheckOut toRadius:12];
    [_txtTicketNumber makeBorderTextView:_txtTicketNumber toColor:COLOR_TEXT_FIELD_BORDER toWidth:2];
   self.constraints_HaveCardHeight.constant = 0;
   self.viewHaveYouCard.hidden = YES;
}

- (IBAction)btn_login:(id)sender
 {
    
     [self memberloginAPI];

 }

-(IBAction)action_haveCard:(id)sender
{
    if(!_btnHaveCard.isSelected)
    {
       self.constraints_HaveCardHeight.constant = 142;
       self.viewHaveYouCard.hidden = NO;
       self.img_Have_Card.image = [UIImage imageNamed:@"checkbox_clicked"];
       self.btnHaveCard.selected = YES;
    }
    else
    {
       self.constraints_HaveCardHeight.constant = 0;
        self.viewHaveYouCard.hidden = YES;
       self.img_Have_Card.image = [UIImage imageNamed:@"checkbox"];
       self.btnHaveCard.selected = NO;
    }
}
-(IBAction)action_dontHaveCard:(id)sender
{
    if(!_btnDontHaveCard.isSelected)
    {
        
       self.img_Dont_have_card.image = [UIImage imageNamed:@"checkbox_clicked"];
       self.btnDontHaveCard.selected = YES;
    }
    else
    {
       self.img_Dont_have_card.image = [UIImage imageNamed:@"checkbox"];
       self.btnDontHaveCard.selected = NO;
    }
    
    UIViewController *dealVC = [self getVCWithSB_ID:kUserRegistrationFilterVC];
    [self.navigationController pushViewController:dealVC animated:YES];
    self.img_Dont_have_card.image = [UIImage imageNamed:@"checkbox"];
    self.btnDontHaveCard.selected = NO;


}

-(IBAction)action_SignIN:(id)sender
{
    
}
-(IBAction)action_SignUp:(id)sender
{
    
}

//API respon
#pragma mark-APIResponse

- (void) success:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    if([response.method isEqualToString:@""])
    {
    }
}


- (void) failure:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    if([response.method isEqualToString:@""])
    {
    }
}


@end
