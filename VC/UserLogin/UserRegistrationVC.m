//
//  UserRegistrationVC.m
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
#import "IQKeyboardManager.h"
#import "UserRegistrationVC.h"

@interface UserRegistrationVC ()<ServerResponseDelegate>
{
}
@end

@implementation UserRegistrationVC

- (void)viewDidLoad {
    [self showBackButton:YES];
    [self setupUI];
    [super viewDidLoad];
    self.dic_data=[[NSMutableDictionary alloc] init];
    
//    if ([[IQKeyboardManager sharedManager] isEnabled])
//    {
//        [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
//        [[IQKeyboardManager sharedManager] setEnable:YES];
//    }
}
//@Gauri Added

-(void)showToolbar
{
     UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
      numberToolbar.barStyle = UIBarStyleBlackTranslucent;
      numberToolbar.items = [NSArray arrayWithObjects: [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(nextBecomeResponder)], [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)], nil];
    
    
    
      [numberToolbar sizeToFit];
       self.textPhone.inputAccessoryView = numberToolbar;
}

-(void)nextBecomeResponder
{
    [self.textEmail becomeFirstResponder];
}

-(void)doneWithNumberPad
{
    //NSString *numberFromTheKeyboard = self.textPhone.text;
    [self.textEmail becomeFirstResponder];
    [self.textPhone resignFirstResponder];
}

-(void)registrationAPI:(NSDictionary*)dictdata

{
    [[ConnectionsManager sharedManager] getregistration_withdelegate:dictdata delegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}

-(void)setupUI
{
//    "PLACEHOLDER_REQUIRED_FIELDS"     =    "(שדה חובה)‎";
//    "TITLE_EMAIL"     =   ":דוא'ל‎*";
//    "TITLE_PASSWORD"     =   "*סיסמה:‎";
//    "TITLE_PASSWORD_AUTH"     =   "*אימות סיסמה";
//    "TITLE_LASTNAME"     =   "*שם משפחה:‎‎‎";
//    "TITLE_PHONE"     =   "*פלאפון:";
//    "TITLE_T_LIDH"     =   "*ת.לידה:‎‎";
//    "TITLE_FIRSTNAME"     =   "*שם פרטי:‎‎";
//    "BUTTON_TITLE_COMPLETE_REG"   = "סיים הרשמה‎";
    
    
    self.titleEmail.text = NSLocalizedString(@"TITLE_EMAIL", @"TITLE_EMAIL");
    
    self.titleFirstName.text = NSLocalizedString(@"TITLE_FIRSTNAME", @"TITLE_FIRSTNAME");
    
    self.titleLastName.text = NSLocalizedString(@"TITLE_LASTNAME", @"TITLE_LASTNAME");
    
    self.titlePassword.text = NSLocalizedString(@"TITLE_PASSWORD", @"TITLE_PASSWORD");
    
    self.titlePassword_Auth.text = NSLocalizedString(@"TITLE_PASSWORD_AUTH", @"TITLE_PASSWORD_AUTH");
    
    self.titlePhone.text = NSLocalizedString(@"TITLE_PHONE", @"TITLE_PHONE");
    
    self.titleT_LIDH.text = NSLocalizedString(@"TITLE_T_LIDH", @"TITLE_T_LIDH");
    
    self.textEmail.placeholder = NSLocalizedString(@"PLACEHOLDER_REQUIRED_FIELDS", @"PLACEHOLDER_REQUIRED_FIELDS");
    
    self.textFirstName.placeholder = NSLocalizedString(@"PLACEHOLDER_REQUIRED_FIELDS", @"PLACEHOLDER_REQUIRED_FIELDS");
    
    self.textLastName.placeholder = NSLocalizedString(@"PLACEHOLDER_REQUIRED_FIELDS", @"PLACEHOLDER_REQUIRED_FIELDS");
    
    self.textPassword.placeholder = NSLocalizedString(@"PLACEHOLDER_REQUIRED_FIELDS", @"PLACEHOLDER_REQUIRED_FIELDS");
    
    self.textPassword_Auth.placeholder = NSLocalizedString(@"PLACEHOLDER_REQUIRED_FIELDS", @"PLACEHOLDER_REQUIRED_FIELDS");
    
    self.textPhone.placeholder = NSLocalizedString(@"PLACEHOLDER_REQUIRED_FIELDS", @"PLACEHOLDER_REQUIRED_FIELDS");
    
    self.textT_LIDH.placeholder = NSLocalizedString(@"PLACEHOLDER_REQUIRED_FIELDS", @"PLACEHOLDER_REQUIRED_FIELDS");
    
    
    [self.textEmail makeBorderTextView:self.textEmail toColor:COLOR_TEXT_FIELD_GRAY_BORDER toWidth:2];
    [self.textFirstName makeBorderTextView:self.textFirstName toColor:COLOR_TEXT_FIELD_GRAY_BORDER toWidth:2];
    [self.textLastName makeBorderTextView:self.textLastName toColor:COLOR_TEXT_FIELD_GRAY_BORDER toWidth:2];
    [self.textPassword makeBorderTextView:self.textPassword toColor:COLOR_TEXT_FIELD_GRAY_BORDER toWidth:2];
    [self.textPassword_Auth makeBorderTextView:self.textPassword_Auth toColor:COLOR_TEXT_FIELD_GRAY_BORDER toWidth:2];
    _textPassword.secureTextEntry = true;
    _textPassword_Auth.secureTextEntry = true;

    if([_textPassword.text isEqualToString:_textPassword_Auth.text]) {
        // passwords are equal
    }
    [self.textPhone makeBorderTextView:self.textPhone toColor:COLOR_TEXT_FIELD_GRAY_BORDER toWidth:2];
    [_textPhone setKeyboardType:UIKeyboardTypeNumberPad];

    [self.textT_LIDH makeBorderTextView:self.textT_LIDH toColor:COLOR_TEXT_FIELD_GRAY_BORDER toWidth:2];
    //@Gauri Added//
         UIDatePicker *datePicker = [[UIDatePicker alloc] init];
         datePicker.datePickerMode = UIDatePickerModeDate;
         [datePicker addTarget:self action:@selector(updateTextField:)
         forControlEvents:UIControlEventValueChanged];
        [self.textT_LIDH setInputView:datePicker];
    
    [self.textT_LIDH addPaddingRight:self.textT_LIDH];
    [self.textEmail addPaddingRight:self.textEmail];
    [self.textFirstName addPaddingRight:self.textFirstName];
    [self.textLastName addPaddingRight:self.textLastName];
    [self.textPassword addPaddingRight:self.textPassword];
    [self.textPassword_Auth addPaddingRight:self.textPassword_Auth];
    [self.textPhone addPaddingRight:self.textPhone];
 
    [self.btnCheckOut setTitle:NSLocalizedString(@"BUTTON_TITLE_COMPLETE_REG", @"BUTTON_TITLE_COMPLETE_REG") forState:UIControlStateNormal];
    
    self.check.text = NSLocalizedString(@"check", @"check");
    [self.titlCheck setText:NSLocalizedString(@"title_check", @"title_check")];
    
    [self.btnCheckOut buttonCornerRadius:self.btnCheckOut toRadius:10];
}
-(void)updateTextField:(id)sender
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d/M/Y"];
    _textT_LIDH.text = [formatter stringFromDate:[sender date]];
}


-(IBAction)actionCheckOut:(id)sender
{
    if ([self checkValidation])
    {
        [self registrationAPI:self.dic_data];
    }
}

-(IBAction)actionCheck:(id)sender
{
    
    if(!self.btnCheck.isSelected)
    {
        self.imgCheck.image = [UIImage imageNamed:@"checkbox_clicked"];
        self.btnCheck.selected = YES;
    }
    else
    {
        self.imgCheck.image = [UIImage imageNamed:@"checkbox"];
        self.btnCheck.selected = NO;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    //[textField makeBorderTextView:textField toColor:COLOR_TEXT_FIELD_RED_BORDER toWidth:1];
    [textField setBackground:[UIImage imageNamed:@"orange_btn"]];
    if (textField==self.textPhone)   //@Gauri Added
    {
        [self  showToolbar];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //[textField makeBorderTextView:textField toColor:COLOR_TEXT_FIELD_GRAY_BORDER toWidth:1];
   /* if (textField==self.textPhone)
     {
        [textField setInputAccessoryView:<#(UIView * _Nullable)#>]
    
    }*/
     [textField setBackground:[UIImage imageNamed:@"grey_btn"]];
   
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
   

  return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

-(BOOL)checkValidation
{
    NSString *errorMessage;
    errorMessage = @"";
    
    
//    "err_reg_email_none"   =  "הזן דוא\"ל";
//    "err_reg_email_x"   =  "דוא\"ל לא תקין";
//    "err_reg_password"   =  "הזן סיסמה באורך 14 -\  6 תויים";
//    "err_reg_password_valide"   =  "סיסמה לא מאומתת";
//    "err_reg_first_name"   =  "הזן שם פרטי";
//    "err_reg_last_name"   =  "הזן שם משפחה";
//    "err_reg_phone"   =  "הזן מספר פלאפון תקין";
//    "err_reg_date_brith"   =  "בחר תאריך לידה";
//    "err_reg_regulation"   =  "אשר את התקנון";
//    "err_dialog_choice"   =  "בחירה לא לנכונה";
//    "err_dialog_none_choice"   =  "לא נבחר עסק";
//    "err_dialog_none_card"   =  "כרטיס זה לא קיים / פעיל";
//    "err_no_insert"   =  "לא הצליח להוסיף את המשתמש";
//    "insert"   =  "הוספת המשתמש בוצע\nברוך הבא";
//    "err_hour_opening"   =  "לא מוגדר שעות עבודה";

    
    if(!self.textEmail.text || [self.textEmail.text isEqualToString:@""])
    {
        errorMessage = NSLocalizedString(@"err_reg_email_none", @"err_reg_email_none");
    }
    else if(!self.textPassword.text || [self.textPassword.text isEqualToString:@""])
    {
        errorMessage = NSLocalizedString(@"err_reg_password", @"err_reg_password");
    }
    else if(!self.textPassword_Auth.text || [self.textPassword_Auth.text isEqualToString:@""])
    {
        errorMessage = NSLocalizedString(@"err_reg_password_valide", @"err_reg_password_valide");
    }
    else if(!self.textFirstName.text || [self.textFirstName.text isEqualToString:@""])
    {
        errorMessage = NSLocalizedString(@"err_reg_first_name", @"err_reg_first_name");
    }
    else if(!self.textLastName.text || [self.textLastName.text isEqualToString:@""])
    {
        errorMessage = NSLocalizedString(@"err_reg_last_name", @"err_reg_last_name");
    }
    else if(!self.textPhone.text || [self.textPhone.text isEqualToString:@""])
    {
        errorMessage = NSLocalizedString(@"err_reg_phone", @"err_reg_phone");
    }
    else if(!self.textT_LIDH.text || [self.textT_LIDH.text isEqualToString:@""])
    {
        errorMessage = NSLocalizedString(@"", @"");
    }
    
    if(errorMessage.length > 0)
    {
        UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:@"error" message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [errorAlertView  show];
        
        return NO;
    }
    else
    {
        [self.dic_data setObject:self.textEmail.text forKey:@"email"];
        [self.dic_data setObject:self.textPassword.text forKey:@"password"];
        [self.dic_data setObject:self.textFirstName.text forKey:@"fname"];
        [self.dic_data setObject:self.textLastName.text forKey:@"lname"];
        [self.dic_data setObject:self.textPhone.text forKey:@"phone"];
        [self.dic_data setObject:self.textT_LIDH.text forKey:@"dateOfBirth"];
        
        return YES;
    }

    return YES;
}
- (void) success:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    if([response.method isEqualToString:@"register"])
    {
        UIViewController *dealVC = [self getVCWithSB_ID:kCommonCollectionVC_SB_ID];
        [self.navigationController pushViewController:dealVC animated:YES];
        
   }
}


- (void) failure:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    if([response.method isEqualToString:@"register"])
    {
    }
}

@end

