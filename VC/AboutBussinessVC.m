//
//  AboutBussinessVC.m
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "AboutBussinessVC.h"
#import "MarketPlaceHolderVC.h"

@interface AboutBussinessVC ()

@end

@implementation AboutBussinessVC
@synthesize currentIndex,bussinessTitle,bussinessDiscription,flagForTerms,mLimitations;

- (void)viewDidLoad {
    [super viewDidLoad];
     [self showBackButton:YES];
    // Do any additional setup after loading the view.
    if (flagForTerms==YES)
     {
         [self showBackButton:YES];
        lbl_Title.text=bussinessTitle;
         txtView_AboutUs.text=mLimitations;
         txtView_AboutUs.editable=NO;
         
    }
    else
    {
	lbl_Title.text=bussinessTitle;
	txtView_AboutUs.text=bussinessDiscription;
    txtView_AboutUs.editable=NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)ActionBack:(id)sender
{
  
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
