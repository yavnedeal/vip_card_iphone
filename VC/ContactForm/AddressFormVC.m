//
//  ContactFormVC.m
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "AddressFormVC.h"
#import "WSTownsResponse.h"
#import "WSStreetsResponse.h"
#import "DEMOCustomAutoCompleteCell.h"
#import "CityAutoCompleteObject.h"
#import "StreetAutoCompleteObject.h"
#import "NIDropDown.h"
#import "WSMealsResponse.h"
#import "DateTimeUtil.h"
#import "WSAdditionalExtrasResponse.h"
#import "MyOrdersVC.h"
#import "BillingFormVC.h"
#import "JSONUtil.h"

#define TXT_AUTO_CITY 14
#define TXT_AUTO_STREET 15
#define TXT_AUTO_AREA 16


@interface AddressFormVC () <ServerResponseDelegate, NIDropDownDelegate>
{
	NSMutableArray *townsArray;
	NSMutableArray *streetsArray;
	StreetAutoCompleteObject *streetSelected;
	CityAutoCompleteObject *citySelected;
	NSString *selectedArea,*totalAmt;
	NSMutableArray *arraySelectedArea;
   
    NSMutableDictionary *mainDict,*coreDic;
	NIDropDown *dropDown;

	AppGlobalData *appGlobalData;
}
@end

@implementation AddressFormVC
@synthesize selectedOrderObj,isFromCart,myOrder,isFromMyOrder;

- (void) viewDidLayoutSubviews
{
	//CGRect contentRect = CGRectZero;
	//for (UIView *view in self.scroll_View.subviews) {
	//	contentRect = CGRectUnion(contentRect, view.frame);
	//}
	//self.scroll_View.contentSize = contentRect.size;
	self.scroll_View.contentSize=CGSizeMake(self.scroll_View.frame.size.width,700);
}


- (void)viewDidLoad {

	//[self.view layoutIfNeeded];
	[self showBackButton:YES];
	[super viewDidLoad];

    NSLog(@"selectedBussiness: %@",_selectedBussiness);
    
	appGlobalData = [AppGlobalData sharedManager];

	_lblTitleForAddress.text = NSLocalizedString(@"lbl_shipping_address", @"lbl_shipping_address");
	_lblTitleForPersonal.text = NSLocalizedString(@"lbl_how_to_reach", @"lbl_how_to_reach");


	[self.btnPhoneCode setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

	arraySelectedArea = [[NSMutableArray alloc] init];
	[arraySelectedArea addObject:@"050"];[arraySelectedArea addObject:@"052"];[arraySelectedArea addObject:@"053"];
	[arraySelectedArea addObject:@"054"];[arraySelectedArea addObject:@"055"];[arraySelectedArea addObject:@"056"];
	[arraySelectedArea addObject:@"057"];[arraySelectedArea addObject:@"058"];[arraySelectedArea addObject:@"059"];

    self.txtCity.autoCompleteTableAppearsAsKeyboardAccessory = YES;
    self.txtStreet.autoCompleteTableAppearsAsKeyboardAccessory = YES;
    
	[_btnSave setTitle:NSLocalizedString(@"btn_submit", @"btn_submit") forState:UIControlStateNormal];

	townsArray = [[NSMutableArray alloc] init];
	streetsArray = [[NSMutableArray alloc] init];
	[self setupUI];

	//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShowWithNotification:) name:UIKeyboardDidShowNotification object:nil];

	//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHideWithNotification:) name:UIKeyboardDidHideNotification object:nil];

	// Do any additional setup after loading the view.
    
    if (isFromMyOrder) {
        [self autoPopulateFields];
    }
}
-(void)autoPopulateFields
{
    NSDictionary *order =[JSONUtil JSONStrToDic:myOrder.orderObj];
    NSDictionary *addressDic = [order objectForKey:@"address"];
    
    NSString *phone = [addressDic objectForKey:@"Phone"];
    NSArray *phnArr;
    NSString *code, *phoneNum=@"";
    if ([phone rangeOfString:@"-"].location==NSNotFound) {
        phoneNum = phone;
        code=@"";
    }else{
        phnArr = [phone componentsSeparatedByString:@"-"];
        code = [phnArr objectAtIndex:0];
        if ([phnArr count]>1) {
            phoneNum = [phnArr objectAtIndex:1];
        }
    }
    
    
    //self.txt_pin.text = [addressDic objectForKey:@""]
    self.txtApartment.text = [NSString stringWithFormat:@"%@",[addressDic objectForKey:@"NumberApartment"]];
    //self.txtCity.text = [addressDic objectForKey:@""];
    self.txtDrivingDirection.text = [addressDic objectForKey:@"DrivingDirections"];
    self.txtEmail.text = [addressDic objectForKey:@"Email_id"];
    self.txtFirstName.text = [addressDic objectForKey:@"fullName"];
    self.txtPhone.text = phoneNum;
    [self.btnPhoneCode setTitle:code forState:UIControlStateNormal];
    self.txtFloor.text = [NSString stringWithFormat:@"%@",[addressDic objectForKey:@"Floor"]];
    self.txtNumberStreet.text = [NSString stringWithFormat:@"%@",[addressDic objectForKey:@"NumberStreet"]];
    self.txtCity.text = [self getCityName:[NSString stringWithFormat:@"%@",[addressDic objectForKey:@"IdTown"]]];
    self.txtStreet.text = [self getStreetName:[NSString stringWithFormat:@"%@",[addressDic objectForKey:@"IdStreet"]]];
}
// @Gauri Added
-(void)showToolbar
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Next", @"Next") style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)], nil];
    
    
    
    [numberToolbar sizeToFit];
    self.txtPhone.inputAccessoryView = numberToolbar;
    self.txtApartment.inputAccessoryView = numberToolbar;
    self.txtFloor.inputAccessoryView = numberToolbar;
    self.txtNumberStreet.inputAccessoryView = numberToolbar;
}


-(void)doneWithNumberPad
{
    //NSString *numberFromTheKeyboard = self.textPhone.text;
    if ( [self.txtNumberStreet resignFirstResponder])
    {
        [self.txtFloor becomeFirstResponder];
    }
    else
    if ( [self.txtFloor resignFirstResponder])
    {
        [self.txtApartment becomeFirstResponder];
    }
    else
    if ([self.txtApartment resignFirstResponder])
    {
        [self.txtDrivingDirection becomeFirstResponder];
    }
    else
    {
        [self.txtEmail becomeFirstResponder];
    }
}
-(void)setupUI
{
	self.txtCity.autoCompleteDelegate = self;
	self.txtStreet.autoCompleteDelegate = self;
	self.txtSelectArea.autoCompleteDelegate = self;

	self.txtCity.autoCompleteDataSource = self;
	self.txtStreet.autoCompleteDataSource = self;
	self.txtSelectArea.autoCompleteDataSource = self;


	self.txtCity.fieldTag = TXT_AUTO_CITY;
	self.txtStreet.fieldTag  = TXT_AUTO_STREET;
	self.txtSelectArea.fieldTag = TXT_AUTO_AREA;


	[self.txtCity setAutoCompleteTableBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];
    [self.txtCity setAutoCompleteFontSize:18];
	[self.txtStreet setAutoCompleteTableBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];
    [self.txtStreet setAutoCompleteFontSize:18];
	[self.txtSelectArea setAutoCompleteTableBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];


	if ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] != NSOrderedAscending) {
		[self.txtCity registerAutoCompleteCellClass:[DEMOCustomAutoCompleteCell class]
							 forCellReuseIdentifier:@"CustomCellId"];
		[self.txtSelectArea registerAutoCompleteCellClass:[DEMOCustomAutoCompleteCell class]
								   forCellReuseIdentifier:@"CustomCellId"];
		[self.txtStreet registerAutoCompleteCellClass:[DEMOCustomAutoCompleteCell class]
							   forCellReuseIdentifier:@"CustomCellId"];
	}
	else{
		//Turn off bold effects on iOS 5.0 as they are not supported and will result in an exception
		self.txtCity.applyBoldEffectToAutoCompleteSuggestions = NO;
		self.txtSelectArea.applyBoldEffectToAutoCompleteSuggestions = NO;
		self.txtStreet.applyBoldEffectToAutoCompleteSuggestions = NO;
	}
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	[self getStreet];
	[self getTowns];
	[self.view layoutIfNeeded];
	[self setupFormWithBorder];
}

-(void)getStreet
{
	[[ConnectionsManager sharedManager] getstreets_withdelegate:self];
	[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}

-(void)getTowns
{
	[[ConnectionsManager sharedManager] gettowns_withdelegate:self];
	[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}

-(BOOL)checkValidation
{
	NSString *strMessage = @"";

	/* "err_city"   =  "י להזין עיר/ישוב";
	 "err_street"   =  "יש להזין רחוב";
	 "err_house"   =  "יש להזין מס\'";
	 "err_floor"   =  "Please enter valid Floor no";
	 "err_apartment"   =  " Please enter valid apartment no";
	 "err_first_name"   =  "יש להזין שם";
	 "err_last_name"   =  "יש להזין שם משפחה";
	 "err_email"   =  "Please enter valid Email";
	 "err_phone"   =  "יש להזין מס\' נייד";
	 "err_select_area"   =  "בחר קידומת";*/

    // @Sarika optional fields for takeaway
    if ([self.takeawayOrDeliveryFlag intValue]==1) {
        
    
	if(_txtCity.text.length <= 0 || [_txtCity.text isEqualToString:@""])
	{
		strMessage = NSLocalizedString(@"err_city", @"enter_city");
	}
	else if(_txtStreet.text.length <= 0 || [_txtStreet.text isEqualToString:@""])
	{
		strMessage = NSLocalizedString(@"err_street", @"err_street");
	}
	else if(_txtNumberStreet.text.length <= 0 || [_txtNumberStreet.text isEqualToString:@""])
	{
		strMessage = NSLocalizedString(@"err_house", @"");
	}
    }
//	    else if(_txtFloor.text.length <= 0 || [_txtFloor.text isEqualToString:@""])
//	    {
//	        strMessage = NSLocalizedString(@"err_floor", @"");
//	    }
//	    else if(_txtApartment.text.length <= 0 || [_txtApartment.text isEqualToString:@""])
//	    {
//	        strMessage = NSLocalizedString(@"err_apartment", @"");
//	    }
//	    else if(_txtDrivingDirection.text.length <= 0 || [_txtDrivingDirection.text isEqualToString:@""])
//	    {
//	        strMessage = NSLocalizedString(@"Please enter note.", @"Please enter note.");
//	    }
	else if(_txtFirstName.text.length <= 0 || [_txtFirstName.text isEqualToString:@""])
	{
		strMessage = NSLocalizedString(@"err_first_name", @"");
	}
//	else if(_txtLastName.text.length <= 0 || [_txtLastName.text isEqualToString:@""])
//	{
//		strMessage = NSLocalizedString(@"err_last_name", @"");
//	}
	else if(_txtPhone.text.length <= 0 || [_txtPhone.text isEqualToString:@""])
	{
    
		strMessage = NSLocalizedString(@"err_phone", @"");
	}
    else if(_txtPhone.text.length!=7)
    {
        
        strMessage = NSLocalizedString(@"phone err", @"phone err");
    }
	//else if(_txtSelectArea.text.length <= 0 || [_txtSelectArea.text isEqualToString:@""])
	else if(self.btnPhoneCode.titleLabel.text.length <=0 || [self.btnPhoneCode.titleLabel.text isEqualToString:@""])
	{
		strMessage = NSLocalizedString(@"err_select_area", @"");
	}
//	    else if(_txtEmail.text.length <= 0 || [_txtEmail.text isEqualToString:@""])
//	    {
//	        strMessage = NSLocalizedString(@"err_email", @"");
//	    }

	if(strMessage.length > 0)
	{
		[[[[iToast makeText:strMessage] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
		return NO;
	}
    else
    	return YES;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(IBAction)buttonSaveAction:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)setupFormWithBorder
{
	UIColor *color = [UIColor lightGrayColor];
	_mainContainView.layer.borderColor = color.CGColor;
	_mainContainView.layer.borderWidth = 1.0;
	_mainContainView.layer.masksToBounds = YES;
	_viewContactView.layer.borderColor = color.CGColor;
	_viewContactView.layer.borderWidth = 1.0;
	_viewContactView.layer.masksToBounds = YES;

	[_viewAddressStreet.layer addSublayer:[self getLayerFrame:_viewAddressStreet.frame toPosition:BOTTOM]];
	[_viewAddressStreet.layer addSublayer:[self getLayerFrame:_viewAddressStreet.frame toPosition:TOP]];
	[_viewUserDetails.layer addSublayer:[self getLayerFrame:_viewUserDetails.frame toPosition:BOTTOM]];
	[_viewPhoneArea.layer addSublayer:[self getLayerFrame:_viewPhoneArea.frame toPosition:BOTTOM]];

	[_txtFloor.layer addSublayer:[self getLayerFrame:_txtFloor.frame toPosition:LEFT]];
	[_txtFloor.layer addSublayer:[self getLayerFrame:_txtFloor.frame toPosition:RIGHT]];

}

-(void)success:(WSBaseResponse *)response
{
	[DejalActivityView removeView];

	//Success
	if([response.method isEqualToString:mFetchTowns])
	{
		if([response.respondeBody isKindOfClass:[NSArray class]])
		{
			NSArray *responseList = response.respondeBody;
			if(responseList.count)
			{
				townsArray = [[NSMutableArray alloc] init];

				for(NSMutableDictionary *dic in responseList)
				{
					WSTownsResponse *townsResponse = [[WSTownsResponse alloc] initWithDictionary:dic];
					[townsArray addObject:townsResponse];
				}
                if (isFromMyOrder) {
                    [self autoPopulateFields];
                }
			}
		}
	}
	else if([response.method isEqualToString:mFetchStreets])
	{
		if([response.respondeBody isKindOfClass:[NSArray class]])
		{
			NSArray *responseList = response.respondeBody;
			if(responseList.count)
			{
				streetsArray = [[NSMutableArray alloc] init];
				for(NSMutableDictionary *dic in responseList)
				{
					WSStreetsResponse *streetsResponse = [[WSStreetsResponse alloc] initWithDictionary:dic];
					[streetsArray addObject:streetsResponse];
				}

                if (isFromMyOrder) {
                    [self autoPopulateFields];
                }
			}
		}
	}

	else if ([response.method isEqualToString:mSubmitOrder])
	{
		//Success submit order

		/*
		 {
		 "method": "saveOrder",
		 "result": {
		 "status": "success",
		 "error_code": 0
		 },
		 "responseBody": {
		 "orderId": "12"
		 }
		 }
		 */
        NSString *totalAmount = @"";
        if(self.selectedOrderObj)
        {

            totalAmount = self.selectedOrderObj.totalAfterDiscount;
        }
        else if(self.wsCouponsResponse){
            totalAmount = self.wsCouponsResponse.price2;
        }
        
           NSString *temp = [NSString stringWithFormat:@"הזמנתך בסך %@ש\"ח",totalAmount];
        
        /*
        NSString * ConfirmMessage= [NSString stringWithFormat:@"\n %@ \n%@",temp,NSLocalizedString(@"Confirm your order", @"Confirm your order")];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:ConfirmMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];*/
        
		//Clear Business Cart here
		if(self.selectedOrderObj)
		{
			[[AppGlobalData sharedManager] clearBusinessCart:self.selectedBussiness.id];
		}

		//[self.navigationController popToRootViewControllerAnimated:YES];
        
       
        
	}
}

-(void)failure:(WSBaseResponse *)response
{

	[DejalActivityView removeView];



	if([response.method isEqualToString:mFetchStreets])
	{

	}
	else if([response.method isEqualToString:mFetchTowns])
	{

	}

	else if ([response.method isEqualToString:mSubmitOrder])
	{
		//Success submit order
	}
}

//Autocomplete

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
 possibleCompletionsForString:(NSString *)string
			completionHandler:(void (^)(NSArray *))handler
{
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
	dispatch_async(queue, ^{
		if(self.simulateLatency){
			CGFloat seconds = arc4random_uniform(4)+arc4random_uniform(4); //normal distribution
			NSLog(@"sleeping fetch of completions for %f", seconds);
			sleep(seconds);
		}

		NSArray *completions;
		// if(self.testWithAutoCompleteObjectsInsteadOfStrings){
		if(textField.tag == TXT_AUTO_CITY)
		{
			completions = [self allCityObjects];
		}else if(textField.tag == TXT_AUTO_STREET)
		{
			completions = [self allStreetObjects];
		}
		else  if(textField.tag == TXT_AUTO_AREA)
		{
			completions = [self allSelectedArea];
		}
		//        } else {
		//            completions = [self allCountries];
		//        }
		handler(completions);
	});
}


- (NSArray *)allCityObjects
{
	NSMutableArray *mutableAutos = [NSMutableArray new];
	if(townsArray){
		for(WSTownsResponse *city in townsArray){
			CityAutoCompleteObject *cityAuto = [[CityAutoCompleteObject alloc] initWithCityName:city.name andId:city.id];
			[mutableAutos addObject:cityAuto];
		}
	}
	return [NSArray arrayWithArray:mutableAutos];
}

- (NSArray *)allStreetObjects
{
	NSMutableArray *mutableAutos = [NSMutableArray new];
	if(streetsArray){
		for(WSStreetsResponse *street in streetsArray){
			StreetAutoCompleteObject *streetAuto = [[StreetAutoCompleteObject alloc] initWithStreetName:street.name andId:street.id];
			[mutableAutos addObject:streetAuto];
		}
	}
	return [NSArray arrayWithArray:mutableAutos];
}

- (NSArray *)allSelectedArea
{
	NSMutableArray *mutableAutos = [NSMutableArray new];
	if(arraySelectedArea){
		/*for(WSStreetsResponse *street in streetsArray){
		 StreetAutoCompleteObject *streetAuto = [[StreetAutoCompleteObject alloc] initWithStreetName:street.name andId:street.id];
		 [mutableAutos addObject:streetAuto];
		 }*/
		mutableAutos = arraySelectedArea;
	}
	return [NSArray arrayWithArray:mutableAutos];
}

#pragma mark - MLPAutoCompleteTextField Delegate
- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
		  shouldConfigureCell:(UITableViewCell *)cell
	   withAutoCompleteString:(NSString *)autocompleteString
		 withAttributedString:(NSAttributedString *)boldedString
		forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
			forRowAtIndexPath:(NSIndexPath *)indexPath;
{
	//This is your chance to customize an autocomplete tableview cell before it appears in the autocomplete tableview
	NSString *filename = [autocompleteString stringByAppendingString:@".png"];
	filename = [filename stringByReplacingOccurrencesOfString:@" " withString:@"-"];
	filename = [filename stringByReplacingOccurrencesOfString:@"&" withString:@"and"];
	[cell.imageView setImage:[UIImage imageNamed:filename]];

	return YES;
}


- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
	   withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
			forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	if(selectedObject)
	{
		if([selectedObject isKindOfClass:[CityAutoCompleteObject class]])
		{
			CityAutoCompleteObject *temp = (CityAutoCompleteObject *) selectedObject;

			NSArray *cities = [self allCityObjects];
			if([cities indexOfObject:temp])
			{
				NSLog(@"yes it already exists");
			}
			else
			{
				NSLog(@"Nahi baba its custom one");
			}

			//  [self loadStreetsByCity:temp.cityId];
			citySelected = selectedObject;
		}
		else  if([selectedObject isKindOfClass:[StreetAutoCompleteObject class]])
		{
			streetSelected = selectedObject;
		}
		else
		{
			selectedArea = [NSString stringWithFormat:@"%@",selectedObject];
		}
	}
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	self.txtActiveField = textField;
  
    if (textField==self.txtPhone || textField==self.txtFloor || textField==self.txtNumberStreet || textField==self.txtApartment)
    {
        [self  showToolbar];
    }

	return YES;
}

-(void) textFieldDidBeginEditing:(UITextField *)textField
{
	self.txtActiveField = textField;

	//  textField.inputAccessoryView = self.textFieldAccessoryView;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
	if(textField.tag == TXT_AUTO_CITY || textField.tag == TXT_AUTO_STREET || textField.tag == TXT_AUTO_AREA)
	{
		// [textField resignFirstResponder];

		[self checkStringExixtsinObject];
	}
	if (textField.tag==TXT_AUTO_CITY)
	{
        _txtStreet.autocorrectionType = UITextAutocorrectionTypeNo;

		[self.txtStreet becomeFirstResponder];
		return YES;
	}
	else if (textField.tag==TXT_AUTO_STREET)
	{
		[self.txtNumberStreet becomeFirstResponder];
		return YES;
	}
	else if (textField.tag==self.txtNumberStreet.tag)
	{
		[self.txtFloor becomeFirstResponder];
		return YES;
	}
	else if (textField.tag==self.txtFloor.tag)
	{
		[self.txtApartment becomeFirstResponder];
		return YES;
	}
	else if (textField.tag==self.txtApartment.tag)
	{
		[self.txtDrivingDirection becomeFirstResponder];
		return YES;
	}
	else if (textField.tag==self.txtDrivingDirection.tag)
	{
		[self.txtFirstName becomeFirstResponder];
		return YES;
	}
//	else if (textField.tag==self.txtFirstName.tag)
//	{
//		[self.txtLastName becomeFirstResponder];
//		return YES;
//	}
	else if (textField.tag==self.txtFirstName.tag)
	{
		[self.txt_pin becomeFirstResponder];
		return YES;
	}
	else if (textField.tag==self.txt_pin.tag)
	{
		[self.txtPhone becomeFirstResponder];
		return YES;
	}
	else if (textField.tag==self.txtPhone.tag)
	{
		[self.txtEmail becomeFirstResponder];
		return YES;
	}
	else if (textField.tag==self.txtEmail.tag)
	{
		[self.txtEmail resignFirstResponder];
		return YES;
	}
	return NO;
}
-(NSString*)getCityName:(NSString*)cityId
{
    NSString *cityname=@"";
    if(townsArray)
    {
        for(WSTownsResponse *city in townsArray)
        {
            if([city.id isEqualToString:cityId])
            {
                
                 cityname = city.name;
                
                break;
            }
        }
    }
    return cityname;
}
-(NSString*)getStreetName:(NSString*)streetId
{
    NSString *streetname=@"";
    if(streetsArray)
    {
        for(WSStreetsResponse *street in streetsArray)
        {
            if([street.id isEqualToString:streetId])
            {
                
                streetname = street.name;
                
                break;
            }
        }
    }
    return streetname;
}
- (void)checkStringExixtsinObject
{
	BOOL exists;
	exists = NO;
	//Москва
	if([self.txtActiveField isEqual:self.txtCity])
	{
		if([self.txtCity.text length] > 0)
		{
			if(townsArray)
			{
				for(WSTownsResponse *city in townsArray)
				{
					if([city.name isEqualToString:self.txtCity.text])
					{
						if(!citySelected)
						{
							citySelected = [[CityAutoCompleteObject alloc] init];
						}
						citySelected.cityId = city.id;
						citySelected.cityName = city.name;
						// [self loadStreetsByCity:citySelected.cityId];
						exists = YES;

						break;
					}
				}
			}
		}
	}
	else if ([self.txtActiveField isEqual:self.txtStreet])
	{
		if([self.txtStreet.text length] > 0)
		{
			if(streetsArray)
			{
				for(WSStreetsResponse *street in streetsArray)
				{
					if([street.name isEqualToString:self.txtStreet.text])
					{
						if(!streetSelected)
						{
							streetSelected = [[StreetAutoCompleteObject alloc] init];
						}

						streetSelected.streetId = street.id;
						streetSelected.streetName = street.name;

						exists = YES;
						break;
					}
				}
			}
		}
	}
	else if ([self.txtActiveField isEqual:self.txtSelectArea])
	{
		if([self.txtSelectArea.text length] > 0)
		{
			if(arraySelectedArea)
			{
				for(int i = 0; i< arraySelectedArea.count ; i++)
				{
					if([[arraySelectedArea objectAtIndex:i] isEqualToString:_txtSelectArea.text])
					{
						selectedArea = [arraySelectedArea objectAtIndex:i];
						exists = YES;
						break;
					}
				}
			}
		}
	}/*
	  //
	  "str_street_not_exists"     =   "Пожалуйста, выберите улицу";
	  "str_city_not_exists"       =   "Пожалуйста, выберите город";
	  */
	if([self.txtActiveField isEqual:self.txtCity] || [self.txtActiveField isEqual:self.txtStreet] || [self.txtActiveField isEqual:self.txtSelectArea])
	{
		if(!exists)
		{
			if([self.txtActiveField isEqual:self.txtStreet]){
				self.txtStreet.text = @"";
				[self.txtStreet closeAutoCompleteTableView];
				//  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:LocaleMSG(@"str_street_not_exists") delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
				// [alert show];
			}
			else if([self.txtActiveField isEqual:self.txtCity])
			{
				self.txtCity.text = @"";
				[self.txtCity closeAutoCompleteTableView];
				//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:LocaleMSG(@"str_city_not_exists") delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
				//   [alert show];
			}
			else
			{
				self.txtSelectArea.text = @"";
				[self.txtSelectArea closeAutoCompleteTableView];
			}
		}
	}

	[self.txtActiveField resignFirstResponder];
}

- (IBAction)onClickPhoneCodeButton:(id)sender
{
	CGFloat f = 150;
	dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arraySelectedArea :nil :@"up"];
	dropDown.delegate = self;
}

- (IBAction)onClickSubmitOrder:(id)sender {

    [self.txtActiveField resignFirstResponder];
	if([self checkValidation])
    {
		[self submitOrder];
    }
}

- (void) niDropDownDelegateMethod:(NIDropDown *)sender andTitle:(NSString *)selected
{
	[self.btnPhoneCode setTitle:selected forState:UIControlStateNormal];
	[self rel];
}

-(void)rel{
	dropDown = nil;
}

-(NSMutableDictionary*)detailOrder
{
	//NSMutableArray *detailsOrder = [NSMutableArray array];
	NSMutableDictionary *detailsDic = [NSMutableDictionary dictionary];
	NSMutableArray *mealsArray = [NSMutableArray array];

	NSMutableArray *listOfCartItems = [appGlobalData getBussinessCart:self.selectedBussiness.id];
	
	for(WSMealsResponse *wsMealsRes in listOfCartItems)
	{
		NSMutableDictionary *mealsDic = [NSMutableDictionary dictionary];

		[mealsDic setObject:SAFE_DEF(wsMealsRes.id, @"") forKey:@"Id"];
		[mealsDic setObject:SAFE_DEF(wsMealsRes.name, @"") forKey:@"name"];
		[mealsDic setObject:SAFE_DEF(wsMealsRes.idCategoryForBusiness, @"") forKey:@"idCategoryForBusiness"];
		[mealsDic setObject:SAFE_DEF(wsMealsRes.image, @"") forKey:@"Image"];
		[mealsDic setObject:SAFE_DEF(wsMealsRes.description, @"") forKey:@"Description"];
		[mealsDic setObject:SAFE_DEF(wsMealsRes.price, @"") forKey:@"price"];

		NSMutableArray *extraMealsArray = [NSMutableArray array];

		for(WSExtraForMealsResponse *extraForMeals in wsMealsRes.extraForMealsList)
		{
			if(![extraForMeals hasSelectedData])
				continue;

			NSMutableDictionary *extraForMealsDic = [NSMutableDictionary dictionary];
            NSInteger mealPrice = (![NSString isEmpty:extraForMeals.price]) ? [extraForMeals.price integerValue] : 0;
			if([extraForMeals isMultiChoice])
			{

				//NSMutableArray *tempextraMealsArray = [NSMutableArray array];
				NSArray *selectedExtraForMealsList = extraForMeals.selectedExtraMealsResponseList;
				if(selectedExtraForMealsList && selectedExtraForMealsList.count > 0)
				{
					for(WSExtraMealsResponse *mealResponse in selectedExtraForMealsList)
					{
						NSMutableDictionary *mealDict = [NSMutableDictionary dictionary];
						/*
						 "id": "21600",
						 "Name": "לאפה של מוניר",
						 "price": "4000",
						 "IdTypeExtra": "31000000"
						 */
                        NSInteger price = (![NSString isEmpty:mealResponse.price])? [mealResponse.price integerValue] : 0;
                        price = mealPrice + price;
                        NSString *extraPrice = [NSString stringWithFormat:@"%ld",price];

						[mealDict setObject:SAFE_DEF(mealResponse.id, @"") forKey:@"id"];
						[mealDict setObject:SAFE_DEF(mealResponse.Name, @"") forKey:@"Name"];
                        [mealDict setObject:SAFE_DEF(extraForMeals.description, @"") forKey:@"Description"];
						[mealDict setObject:SAFE_DEF(extraPrice, @"") forKey:@"price"];
						[mealDict setObject:SAFE_DEF(mealResponse.IdTypeExtra, @"") forKey:@"IdTypeExtra"];
                        //Meal fields
                        [mealDict setObject:SAFE_DEF(extraForMeals.price, @"") forKey:@"extraForMealPrice"];
						[extraMealsArray addObject:mealDict];
					}
					//[extraMealsArray addObject:tempextraMealsArray];
					//Adding array
					//[mealDict setObject:tempMealsResponseList forKey:@"extraMeals"];
				}

			}
			else
			{
				WSExtraMealsResponse *extraMealResponse = extraForMeals.selectedExtraMealsResponse;
                NSInteger price = (![NSString isEmpty:extraMealResponse.price])? [extraMealResponse.price integerValue] : 0;
                price = mealPrice + price;
                NSString *extraPrice = [NSString stringWithFormat:@"%ld",price];
				[extraForMealsDic setObject:SAFE_DEF(extraMealResponse.id, @"") forKey:@"id"];
				[extraForMealsDic setObject:SAFE_DEF(extraMealResponse.Name, @"") forKey:@"Name"];
                [extraForMealsDic setObject:SAFE_DEF(extraForMeals.description, @"") forKey:@"Description"];
				[extraForMealsDic setObject:SAFE_DEF(extraPrice, @"") forKey:@"price"];
				[extraForMealsDic setObject:SAFE_DEF(extraMealResponse.IdTypeExtra, @"") forKey:@"IdTypeExtra"];
                [extraForMealsDic setObject:SAFE_DEF(extraForMeals.price, @"") forKey:@"extraForMealPrice"];
				[extraMealsArray addObject:extraForMealsDic];
			}


		}

		[mealsDic setObject:extraMealsArray forKey:@"extraMeals"];
		[mealsArray addObject:mealsDic];
	}
	[detailsDic setObject:mealsArray forKey:@"Meals"];
	//[detailsOrder addObject:detailsDic];

	//    NSMutableDictionary *detailsDic_ = [NSMutableDictionary dictionary];
	//    [detailsDic_ setObject:detailsOrder forKey:@"DetailsOrder"];
	return detailsDic;

}

-(NSMutableDictionary*)detailOrderForCore
{
    if(self.wsCouponsResponse)
    {
        //NSMutableArray *detailsOrder = [NSMutableArray array];
        NSMutableDictionary *detailsDic = [NSMutableDictionary dictionary];
        NSMutableArray *mealsArray = [NSMutableArray array];
        NSMutableDictionary *mealsDic = [NSMutableDictionary dictionary];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.id, @"") forKey:@"Id"];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.name, @"") forKey:@"name"];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.idCategor, @"") forKey:@"idCategoryForBusiness"];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.image, @"") forKey:@"Image"];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.des, @"") forKey:@"Description"];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.price2, @"") forKey:@"price"];
        
        NSMutableArray *extraMealsArray = [NSMutableArray array];
        [mealsDic setObject:extraMealsArray forKey:@"extraMeals"];
        [mealsArray addObject:mealsDic];
        
        [detailsDic setObject:mealsArray forKey:@"Meals"];
        return detailsDic;
    }else{
    //NSMutableArray *detailsOrder = [NSMutableArray array];
    NSMutableDictionary *detailsDic = [NSMutableDictionary dictionary];
    NSMutableArray *mealsArray = [NSMutableArray array];
    
    NSMutableArray *listOfCartItems = [appGlobalData getBussinessCart:self.selectedBussiness.id];
    
    for(WSMealsResponse *wsMealsRes in listOfCartItems)
    {
        NSMutableDictionary *mealsDic = [NSMutableDictionary dictionary];
        
        [mealsDic setObject:SAFE_DEF(wsMealsRes.id, @"") forKey:@"id"];
        [mealsDic setObject:SAFE_DEF(wsMealsRes.name, @"") forKey:@"name"];
        [mealsDic setObject:SAFE_DEF(wsMealsRes.idCategoryForBusiness, @"") forKey:@"idCategoryForBusiness"];
        [mealsDic setObject:SAFE_DEF(wsMealsRes.image, @"") forKey:@"image"];
        [mealsDic setObject:SAFE_DEF(wsMealsRes.description, @"") forKey:@"description"];
        [mealsDic setObject:SAFE_DEF(wsMealsRes.price, @"") forKey:@"price"];
        
        NSMutableArray *extraMealsArray = [NSMutableArray array];
        
        for(WSExtraForMealsResponse *extraForMeals in wsMealsRes.extraForMealsList)
        {
            if(![extraForMeals hasSelectedData])
                continue;
            
            NSMutableDictionary *extraForMealsDic = [NSMutableDictionary dictionary];
            
            if([extraForMeals isMultiChoice])
            {
                
                //NSMutableArray *tempextraMealsArray = [NSMutableArray array];
                NSArray *selectedExtraForMealsList = extraForMeals.selectedExtraMealsResponseList;
                if(selectedExtraForMealsList && selectedExtraForMealsList.count > 0)
                {
                    for(WSExtraMealsResponse *mealResponse in selectedExtraForMealsList)
                    {
                        NSMutableDictionary *mealDict = [NSMutableDictionary dictionary];
                        /*
                         "id": "21600",
                         "Name": "לאפה של מוניר",
                         "price": "4000",
                         "IdTypeExtra": "31000000"
                         */
                        
                       
                        
                        [mealDict setObject:SAFE_DEF(mealResponse.id, @"") forKey:@"id"];
                        [mealDict setObject:SAFE_DEF(mealResponse.Name, @"") forKey:@"Name"];
                        [mealDict setObject:SAFE_DEF(extraForMeals.description, @"") forKey:@"Description"];
                        [mealDict setObject:SAFE_DEF(mealResponse.price, @"") forKey:@"price"];
                        [mealDict setObject:SAFE_DEF(mealResponse.IdTypeExtra, @"") forKey:@"IdTypeExtra"];
                        //[mealDict setObject:mealResponse.selected forKey:@"Selected"];
                        [mealDict setObject:SAFE_DEF(extraForMeals.price, @"") forKey:@"extraForMealPrice"];
                        [extraMealsArray addObject:mealDict];
                    }
                    //[extraMealsArray addObject:tempextraMealsArray];
                    //Adding array
                    //[mealDict setObject:tempMealsResponseList forKey:@"extraMeals"];
                }
                
            }
            else
            {
                WSExtraMealsResponse *extraMealResponse = extraForMeals.selectedExtraMealsResponse;
                [extraForMealsDic setObject:SAFE_DEF(extraMealResponse.id, @"") forKey:@"id"];
                [extraForMealsDic setObject:SAFE_DEF(extraMealResponse.Name, @"") forKey:@"Name"];
                [extraForMealsDic setObject:SAFE_DEF(extraMealResponse.Description, @"") forKey:@"Description"];
                [extraForMealsDic setObject:SAFE_DEF(extraMealResponse.price, @"") forKey:@"price"];
                [extraForMealsDic setObject:SAFE_DEF(extraMealResponse.IdTypeExtra, @"") forKey:@"IdTypeExtra"];
                //[extraForMealsDic setObject:extraMealResponse.selected forKey:@"Selected"];
                [extraForMealsDic setObject:SAFE_DEF(extraForMeals.price, @"") forKey:@"extraForMealPrice"];
                [extraMealsArray addObject:extraForMealsDic];
            }
            
            
        }
        
        [mealsDic setObject:extraMealsArray forKey:@"extraForMealsList"];
        [mealsDic setObject:[[mainDict objectForKey:@"ordering"] objectForKey:@"aditionalExtras"] forKey:@"aditionalExtras"];
        [mealsArray addObject:mealsDic];
    }
    [detailsDic setObject:mealsArray forKey:@"Meals"];
    //[detailsOrder addObject:detailsDic];
    
    //    NSMutableDictionary *detailsDic_ = [NSMutableDictionary dictionary];
    //    [detailsDic_ setObject:detailsOrder forKey:@"DetailsOrder"];
    return detailsDic;
    }
    
}

//SanC - Added support for Coupon/Deal Orders

-(NSMutableDictionary*)detailDealOrder
{
    //NSMutableArray *detailsOrder = [NSMutableArray array];
    NSMutableDictionary *detailsDic = [NSMutableDictionary dictionary];
    NSMutableArray *mealsArray = [NSMutableArray array];
         NSMutableDictionary *mealsDic = [NSMutableDictionary dictionary];
         [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.id, @"") forKey:@"Id"];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.name, @"") forKey:@"name"];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.idCategor, @"") forKey:@"idCategoryForBusiness"];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.image, @"") forKey:@"Image"];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.des, @"") forKey:@"Description"];
        [mealsDic setObject:SAFE_DEF(self.wsCouponsResponse.price2, @"") forKey:@"price"];
        
        NSMutableArray *extraMealsArray = [NSMutableArray array];
        [mealsDic setObject:extraMealsArray forKey:@"extraMeals"];
        [mealsArray addObject:mealsDic];
    
    [detailsDic setObject:mealsArray forKey:@"Meals"];
     return detailsDic;
    
}

-(void)submitOrder
{
	//Submit Order
    
	mainDict = [NSMutableDictionary dictionary];

	NSMutableDictionary *addressDataDict = [NSMutableDictionary dictionary];
	//[addressDataDict setObject:SAFE(self.txtNumberStreet.text)  forKey:@"NumberStreet"];
	//[addressDataDict setObject:SAFE_DEF(self.txtFloor.text, @"") forKey:@"Floor"];
	//[addressDataDict setObject:SAFE_DEF(self.txtApartment.text, @"") forKey:@"NumberApartment"];

	//[addressDataDict setObject:SAFE_DEF(streetSelected.streetId, @"") forKey:@"IdStreet"];
	//[addressDataDict setObject:SAFE_DEF(citySelected.cityId, @"") forKey:@"IdTown"];
    
    [addressDataDict setObject:SAFE([NSNumber numberWithInt:[streetSelected.streetId intValue]]) forKey:@"IdStreet"];
    [addressDataDict setObject:SAFE([NSNumber numberWithInt:[citySelected.cityId intValue]]) forKey:@"IdTown"];
    [addressDataDict setObject:SAFE([NSNumber numberWithInt:[self.txtNumberStreet.text intValue]])  forKey:@"NumberStreet"];
    [addressDataDict setObject:SAFE([NSNumber numberWithInt:[self.txtFloor.text intValue]]) forKey:@"Floor"];
    [addressDataDict setObject:SAFE([NSNumber numberWithInt:[self.txtApartment.text intValue]]) forKey:@"NumberApartment"];

    
	[addressDataDict setObject:SAFE(self.txtDrivingDirection.text) forKey:@"DrivingDirections"];

	[addressDataDict setObject:SAFE(self.txtFirstName.text) forKey:@"fullName"];
	//[addressDataDict setObject:SAFE(self.txtLastName.text) forKey:@"Last_name"];
	[addressDataDict setObject:SAFE(self.txtEmail.text) forKey:@"Email_id"];
    NSString *phoneCode = @"";
    if(self.btnPhoneCode.titleLabel && ![NSString isEmpty:self.btnPhoneCode.titleLabel.text])
    {
        phoneCode = [NSString stringWithFormat:@"%@-",SAFE(self.btnPhoneCode.titleLabel.text)];
    }
    [addressDataDict setObject:[NSString stringWithFormat:@"%@%@", phoneCode, SAFE(self.txtPhone.text)] forKey:@"Phone"];
	//[addressDataDict setObject:SAFE(self.txtPhone.text) forKey:@"Phone"];
    
	//Address
	[mainDict setObject:addressDataDict forKey:@"address"];



	//At a time only
	WSBussiness *bussiness = self.selectedBussiness;
    NSDictionary *dicBz = [bussiness WSBusinessToDic:bussiness];
    

	NSMutableDictionary *tempOrderingDict = [NSMutableDictionary dictionary];
	//[tempOrderingDict setObject:@"" forKey:@"IdUser"];
     [tempOrderingDict setObject:SAFE([NSNumber numberWithInt:[@"0" intValue]]) forKey:@"IdStreet"];
	//NSString *phoneCode = @"";
	if(self.btnPhoneCode.titleLabel && ![NSString isEmpty:self.btnPhoneCode.titleLabel.text])
	{
		phoneCode = [NSString stringWithFormat:@"%@-",self.btnPhoneCode.titleLabel.text];
	}
	[tempOrderingDict setObject:[NSString stringWithFormat:@"%@%@", phoneCode, self.txtPhone.text] forKey:@"Phone"];
	[tempOrderingDict setObject:SAFE_DEF(bussiness.id, @"") forKey:@"IdBusiness"];
    NSInteger priceBeforeDisc = 0;
    if(self.wsCouponsResponse) //SanC - To Support Deal/Coupon
    {
        [tempOrderingDict setObject:SAFE_DEF(self.orderComment, @"") forKey:@"CommentOnOrder"];
        //Price fields
		NSString *disc = @"0";
		NSInteger total = [self.wsCouponsResponse.price2 integerValue];
        /* NO Discount for Coupon/Deal
		if(self.selectedBussiness.vipDiscount && [self.selectedBussiness.vipDiscount integerValue] > 0)
		{
			NSInteger discVal = total * [self.selectedBussiness.vipDiscount integerValue]/100;
			total = total - discVal;
			disc = [NSString stringWithFormat:@"%ld",(long)discVal];
		}
         */
        priceBeforeDisc = total;
        [tempOrderingDict setObject:disc forKey:@"PriceDiscount"];
        [tempOrderingDict setObject:SAFE_DEF(self.wsCouponsResponse.price2, @"") forKey:@"PriceBefureDiscount"];
		NSString *totalAmount = [NSString stringWithFormat:@"%ld",(long)total];
		[tempOrderingDict setObject:SAFE_DEF(totalAmount, @"")  forKey:@"totalPrice"];
        
    }
    else {
        
        [tempOrderingDict setObject:SAFE_DEF(self.selectedOrderObj.totalAmount, @"")  forKey:@"totalPrice"];
        [tempOrderingDict setObject:SAFE_DEF(self.selectedOrderObj.CommentOnOrder, @"") forKey:@"CommentOnOrder"];
        if(![NSString isEmpty:self.selectedOrderObj.priceBeforeDiscount])
          priceBeforeDisc = [self.selectedOrderObj.priceBeforeDiscount integerValue];
        //Price fields
        [tempOrderingDict setObject:SAFE_DEF(self.selectedOrderObj.priceDiscount, @"") forKey:@"PriceDiscount"];
        [tempOrderingDict setObject:SAFE_DEF(self.selectedOrderObj.priceBeforeDiscount, @"") forKey:@"PriceBefureDiscount"];
        
        
    }
    //min delivery
    [tempOrderingDict setObject:SAFE_DEF(bussiness.minDelivery, @"") forKey:@"minDelivery"];
    
    [tempOrderingDict setObject:SAFE_DEF(bussiness.idAddress, @"") forKey:@"IdAddress"];
    [tempOrderingDict setObject:SAFE_DEF(self.txtDrivingDirection.text, @"") forKey:@"CommentsShipping"];
    [tempOrderingDict setObject:SAFE([NSNumber numberWithInt:[@"0" intValue]]) forKey:@"IdPayment"];
    [tempOrderingDict setObject:[DateTimeUtil stringFromDateTime:[NSDate date] withFormat:SERVER_FULL_DATE_FORMAT] forKey:@"DateOrder"];
    if(priceBeforeDisc < [bussiness.minDelivery integerValue])
    {
      [tempOrderingDict setObject:SAFE_DEF(bussiness.delivery, @"0") forKey:@"DeliveryPrice"];
    }
    else{
      [tempOrderingDict setObject: @"0" forKey:@"DeliveryPrice"];
    }
    [tempOrderingDict setObject:SAFE_DEF(bussiness.delivery, @"") forKey:@"BussinessDeliveryPrice"];
    //for discount
    [tempOrderingDict setObject:SAFE_DEF(self.takeawayOrDeliveryFlag, @"") forKey:@"TakeAwayOrDelivery"];
    

    //SanC - To Support Deal/Coupon
    if(self.wsCouponsResponse)
    {
        [tempOrderingDict setObject:[self detailDealOrder] forKey:@"DetailsOrder"];
    }
    else
    {
        [tempOrderingDict setObject:[self detailOrder] forKey:@"DetailsOrder"];
    }
//Additional Extras
	NSMutableArray *aditionalExtrasList = [[NSMutableArray alloc] init];
    for (WSAdditionalExtrasResponse *item in self.selectedBussiness.aditionalExtrasList)
    {
        if(item.selected)
        {
            [aditionalExtrasList addObject:[item getDictionaryFromObject]];
         }
        
    }
    /*
    NSMutableArray *mealResList = [NSMutableArray array];
    NSMutableDictionary *detailOrderDic = [[NSMutableDictionary alloc] init];
    NSMutableArray *listOfCartItems = [appGlobalData getBussinessCart:self.selectedBussiness.id];
    
    for(WSMealsResponse *wsMealsRes in listOfCartItems)
    {
        NSMutableDictionary *mealDict = [NSMutableDictionary dictionary];
        
        [mealDict setObject:SAFE_DEF(wsMealsRes.id, @"") forKey:@"Id"];
        [mealDict setObject:SAFE_DEF(wsMealsRes.name, @"") forKey:@"name"];
        [mealDict setObject:SAFE_DEF(wsMealsRes.idCategoryForBusiness, @"") forKey:@"idCategoryForBusiness"];
        [mealDict setObject:SAFE_DEF(wsMealsRes.image, @"") forKey:@"Image"];
        [mealDict setObject:SAFE_DEF(wsMealsRes.description, @"") forKey:@"Description"];
        [mealDict setObject:SAFE_DEF(wsMealsRes.price, @"") forKey:@"price"];
        
        NSArray *selectedExtraMealsList = wsMealsRes.extraForMealsList;
        NSMutableArray *tempMealsResponseList = [NSMutableArray array];
        
        for(WSExtraForMealsResponse *extraForMeals in selectedExtraMealsList)
        {
            
            NSMutableDictionary *extraformealDic = [NSMutableDictionary dictionary];
            if([extraForMeals.maelOrExtra boolValue])
            {
                NSArray *selectedExtraForMealsList = extraForMeals.selectedExtraMealsResponseList;
                if(selectedExtraMealsList.count)
                {
                    for(WSExtraMealsResponse *mealResponse in selectedExtraForMealsList)
                    {
                        NSMutableDictionary *mealDict = [NSMutableDictionary dictionary];
                       
                        
                        [extraformealDic setObject:SAFE_DEF(mealResponse.id, @"") forKey:@"id"];
                        [extraformealDic setObject:SAFE_DEF(mealResponse.Name, @"") forKey:@"Name"];
                        [extraformealDic setObject:SAFE_DEF(mealResponse.price, @"") forKey:@"price"];
                        [extraformealDic setObject:SAFE_DEF(mealResponse.IdTypeExtra, @"") forKey:@"IdTypeExtra"];
                        
                        [tempMealsResponseList addObject:extraformealDic];
                    }
                    
                    //Adding array
                    //[mealDict setObject:tempMealsResponseList forKey:@"extraMeals"];
                }
            }
            else
            {
                //Single object
                
                WSExtraMealsResponse *extraMealResponse = extraForMeals.selectedExtraMealsResponse;
                [extraformealDic setObject:SAFE_DEF(extraMealResponse.id, @"") forKey:@"id"];
                [extraformealDic setObject:SAFE_DEF(extraMealResponse.Name, @"") forKey:@"Name"];
                [extraformealDic setObject:SAFE_DEF(extraMealResponse.price, @"") forKey:@"price"];
                [extraformealDic setObject:SAFE_DEF(extraMealResponse.IdTypeExtra, @"") forKey:@"IdTypeExtra"];
                
                [tempMealsResponseList addObject:extraformealDic];
                
                //NSArray *singleObj = [[NSArray alloc] initWithObjects:extraMealResponse, nil];
                //[mealDict setObject:singleObj forKey:@"extraMeals"];
            }
        }
        
        [mealResList addObject:mealDict];
        
        NSLog(@"Ordering Object : %@", mainDict);
    }
    
    [tempOrderingDict setObject:mealResList forKey:@"Meals"];
    */
    
    NSMutableDictionary *orderingDic = [NSMutableDictionary dictionary];
    [orderingDic setObject:tempOrderingDict forKey:@"DetailsOrder"];
    
 	//[tempOrderingDict setObject:bussiness.aditionalExtrasList forKey:@"aditionalExtras"];
    
    
    [tempOrderingDict setObject:aditionalExtrasList forKey:@"aditionalExtras"];
    [tempOrderingDict setObject:self.selectedBussiness.sendEmail forKey:@"sendEmail"];
    [tempOrderingDict setObject:self.selectedBussiness.sendFax forKey:@"sendFax"];
    [tempOrderingDict setObject:self.selectedBussiness.sendSms forKey:@"sendSms"];
	[mainDict setObject:tempOrderingDict forKey:@"ordering"];

    coreDic = [NSMutableDictionary dictionary];
    
    [coreDic setObject:[self detailOrderForCore] forKey:@"cartObj"];
    [coreDic setObject:dicBz forKey:@"selBZ"];
    [coreDic setObject:[NSNumber numberWithBool:isFromCart] forKey:@"isFromCart"];
    
    NSLog(@"Order Desc : %@",mainDict);
    NSLog(@"mainDict: %@",[self dicToJSON:mainDict]);
    totalAmt = [[mainDict objectForKey:@"ordering"] objectForKey:@"totalPrice"];

	//[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
	//[[ConnectionsManager sharedManager] submitOrder:mainDict withdelegate:self];
    
    
    
    //added by Sarika [08/08/2016]
    
    NSString *totalAmount = @"";
    if(self.selectedOrderObj)
    {
        
        totalAmount = self.selectedOrderObj.totalAfterDiscount;
    }
    else if(self.wsCouponsResponse){
        totalAmount = self.wsCouponsResponse.price2;
    }
    
    NSString *temp = [NSString stringWithFormat:@"הזמנתך בסך %@ש\"ח",totalAmount];
    
    NSString *orderId = @"";
    NSString *logo = [NSString stringWithFormat:@"%@%@",mFetchImagesMealsDeals,self.selectedBussiness.logo];
    
    
    BillingFormVC *billformVC = [self getVCWithSB_ID:@"SB_ID_BillingForm"];
    billformVC.orderTotal = totalAmount;
    //billformVC.orderId = orderId;
    billformVC.cnfMsg = temp;
    billformVC.sendSms = _selectedBussiness.sendSms;
    billformVC.sendFax = _selectedBussiness.sendFax;
    billformVC.sendEmail = _selectedBussiness.sendEmail;
    billformVC.logo = logo;
    billformVC.dicOrder = mainDict;
    billformVC.cartDic = coreDic;
    billformVC.isFromMyOrder=isFromMyOrder;
    billformVC.myOrder=myOrder;
    billformVC.selectedBussiness = self.selectedBussiness;
    [self.navigationController pushViewController:billformVC animated:YES];

}

-(NSString*)dicToJSON:(NSDictionary*)dic
{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return myString;
}
-(NSDictionary*)JSONStrToDic:(NSString*)jsonStr
{
    NSError * err;
    NSDictionary *response = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
    return response;
}
/*
     NSMutableArray *mealResList = [NSMutableArray array];
     NSMutableDictionary *detailOrderDic = [[NSMutableDictionary alloc] init];
 
     for(WSMealsResponse *wsMealsRes in appGlobalData.listOfCartItems)
     {
         NSMutableDictionary *mealDict = [NSMutableDictionary dictionary];
         / *
          "Id": "9300",
          "name": "בנדורה אקספרס ",
          "idCategoryForBusiness": "19",
          "Image": "93.ajpg",
          "Description": "סלט ירקות חתוך דק מתובל בשמן זית ולימון, נתחי שווארמה פרגית, בצל שרוף, טחינה של סעיד, סומק ופטרוזיליה",
          "price": "42",
         // * /

        [mealDict setObject:SAFE_DEF(wsMealsRes.id, @"") forKey:@"Id"];
        [mealDict setObject:SAFE_DEF(wsMealsRes.name, @"") forKey:@"name"];
        [mealDict setObject:SAFE_DEF(wsMealsRes.idCategoryForBusiness, @"") forKey:@"idCategoryForBusiness"];
        [mealDict setObject:SAFE_DEF(wsMealsRes.image, @"") forKey:@"Image"];
        [mealDict setObject:SAFE_DEF(wsMealsRes.description, @"") forKey:@"Description"];
        [mealDict setObject:SAFE_DEF(wsMealsRes.price, @"") forKey:@"price"];

        NSArray *selectedExtraMealsList = wsMealsRes.extraForMealsList;
        NSMutableArray *tempMealsResponseList = [NSMutableArray array];

        for(WSExtraForMealsResponse *extraForMeals in selectedExtraMealsList)
        {

            NSMutableDictionary *extraformealDic = [NSMutableDictionary dictionary];
            if([extraForMeals.maelOrExtra boolValue])
            {
                NSArray *selectedExtraForMealsList = extraForMeals.selectedExtraMealsResponseList;
                if(selectedExtraMealsList.count)
                {
                    for(WSExtraMealsResponse *mealResponse in selectedExtraForMealsList)
                    {
                        NSMutableDictionary *mealDict = [NSMutableDictionary dictionary];
                        / *
                         "id": "21600",
                         "Name": "לאפה של מוניר",
                         "price": "4000",
                         "IdTypeExtra": "31000000"
                         * /

                        [extraformealDic setObject:SAFE_DEF(mealResponse.id, @"") forKey:@"id"];
                        [extraformealDic setObject:SAFE_DEF(mealResponse.Name, @"") forKey:@"Name"];
                        [extraformealDic setObject:SAFE_DEF(mealResponse.price, @"") forKey:@"price"];
                        [extraformealDic setObject:SAFE_DEF(mealResponse.IdTypeExtra, @"") forKey:@"IdTypeExtra"];

                        [tempMealsResponseList addObject:extraformealDic];
                    }

                    //Adding array
                    //[mealDict setObject:tempMealsResponseList forKey:@"extraMeals"];
                }
            }
            else
            {
                //Single object

                WSExtraMealsResponse *extraMealResponse = extraForMeals.selectedExtraMealsResponse;
                [extraformealDic setObject:SAFE_DEF(extraMealResponse.id, @"") forKey:@"id"];
                [extraformealDic setObject:SAFE_DEF(extraMealResponse.Name, @"") forKey:@"Name"];
                [extraformealDic setObject:SAFE_DEF(extraMealResponse.price, @"") forKey:@"price"];
                [extraformealDic setObject:SAFE_DEF(extraMealResponse.IdTypeExtra, @"") forKey:@"IdTypeExtra"];

                [tempMealsResponseList addObject:extraformealDic];

                //NSArray *singleObj = [[NSArray alloc] initWithObjects:extraMealResponse, nil];
                //[mealDict setObject:singleObj forKey:@"extraMeals"];
            }
        }

        [mealResList addObject:mealDict];

        NSLog(@"Ordering Object : %@", mainDict);
    }

    [tempOrderingDict setObject:mealResList forKey:@"Meals"];


    NSMutableDictionary *orderingDic = [NSMutableDictionary dictionary];
    [orderingDic setObject:tempOrderingDict forKey:@"DetailsOrder"];
*/

@end