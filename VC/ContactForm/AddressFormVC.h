//
//  ContactFormVC.h
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "MLPAutoCompleteTextField.h"
#import "OrderingDetailsObj.h"
#import "WSCouponsResponse.h"
#import "ServiceManager.h"
#import "MyOrders.h"

@class MLPAutoCompleteTextField;
@interface AddressFormVC : BaseViewController <MLPAutoCompleteTextFieldDelegate, MLPAutoCompleteTextFieldDataSource,UITextFieldDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_View;
@property (nonatomic, strong) OrderingDetailsObj *selectedOrderObj;

@property (strong, nonatomic) IBOutlet UIView *mainContainView; //Main View 4
@property (strong, nonatomic) IBOutlet UIView *viewAddressStreet; // Top bottom
@property (strong, nonatomic) IBOutlet UIView *viewContactView; //Bottom Main 4
@property (strong, nonatomic) IBOutlet UIView *viewUserDetails; //bottom
@property (strong, nonatomic) IBOutlet UIView *viewPhoneArea;//bottom

@property(retain,nonatomic) UITextField *txtActiveField;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleForAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleForPersonal;

@property (strong, nonatomic) IBOutlet MLPAutoCompleteTextField *txtCity;
@property (strong, nonatomic) IBOutlet MLPAutoCompleteTextField *txtStreet;
@property (assign) BOOL simulateLatency;

@property (strong, nonatomic) IBOutlet UITextField *txtNumberStreet;
@property (strong, nonatomic) IBOutlet UITextField *txtFloor;
@property (strong, nonatomic) IBOutlet UITextField *txtApartment;
@property (strong, nonatomic) IBOutlet UITextField *txtDrivingDirection;

@property (strong, nonatomic) IBOutlet UITextField *txtFirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;
@property (strong, nonatomic) IBOutlet MLPAutoCompleteTextField *txtSelectArea;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txt_pin;

@property (strong, nonatomic) IBOutlet UIButton *btnSave;
@property (strong, nonatomic) IBOutlet UIButton *btnPhoneCode;

@property (strong,nonatomic) WSBussiness *selectedBussiness;

@property (strong,nonatomic) NSString *takeawayOrDeliveryFlag;

@property (strong ,nonatomic) WSCouponsResponse *wsCouponsResponse;
@property(strong,nonatomic) NSString *orderComment;
@property (assign ,nonatomic) BOOL isFromCart,isFromMyOrder;
@property(strong,nonatomic) MyOrders *myOrder;

- (IBAction)onClickPhoneCodeButton:(id)sender;
- (IBAction)onClickSubmitOrder:(id)sender;
-(NSMutableDictionary*)detailOrderForCore;

@end
