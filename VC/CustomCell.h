//
//  CustomCell.h
//  VIPCard
//
//  Created by SanC on 18/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionViewCell.h"

@interface CustomCell : BaseCollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end
