//
//  MainViewController.h
//  VIPCard
//
//  Created by fis on 20/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
