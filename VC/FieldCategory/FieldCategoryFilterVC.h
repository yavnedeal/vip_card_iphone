//
//  fieldCategoryFilterVC.h
//  VIPCard
//
//  Created by SanC on 20/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "IZValueSelectorView.h"


@interface FieldCategoryFilterVC : BaseViewController<ServerResponseDelegate,IZValueSelectorViewDataSource,IZValueSelectorViewDelegate>
{
	NSDictionary *dict_data,*mainCopy;
	NSArray *array_SectionTitles,*array_Copytitle;
    NSMutableArray *array_IndexTitle1,*array_IndexTitle1Copy;
	NSArray *array_IndexTitles;
	__weak IBOutlet UITableView *tbl_View;
	__weak IBOutlet UISearchBar *searchBar;
	NSMutableArray *array_category;
    __weak IBOutlet UIScrollView *scroll_View;
	NSString *str_tableTitle;
	NSArray *array_sectionNameFilter;
}
@property (weak, nonatomic) IBOutlet UITextField *txtFieldSearch;
@property (nonatomic) BOOL loadingMoreTableViewData;
@property (weak, nonatomic) IBOutlet IZValueSelectorView *selectorVertical;



@end
