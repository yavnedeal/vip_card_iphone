//
//  fieldCategoryFilterVC.m
//  VIPCard
//
//  Created by SanC on 20/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "FieldCategoryFilterVC.h"
#import "WSCategoryFilterResponse.h"
#import "WSCategoryFilterDetailResponse.h"
#import "FieldCategoryDetailVC.h"




@interface FieldCategoryFilterVC () <ServerResponseDelegate>
{
	NSMutableArray *filter;

}
@property (weak, nonatomic) IBOutlet UIButton *btnRevealMenu;
@end

@implementation FieldCategoryFilterVC
@synthesize selectorVertical = _selectorVertical;


- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	[self showBackButton:YES];
    [self customSetup];
	
	array_IndexTitle1=[[NSMutableArray alloc] initWithArray: @[NSLocalizedString(@"A","A"),NSLocalizedString(@"B","B"),NSLocalizedString(@"C","C"),NSLocalizedString(@"D","D"),NSLocalizedString(@"E","E"),NSLocalizedString(@"F","F"),NSLocalizedString(@"G","G"),NSLocalizedString(@"H","H"),NSLocalizedString(@"I","I"),NSLocalizedString(@"J","J"),NSLocalizedString(@"K","K"),NSLocalizedString(@"L","L"),NSLocalizedString(@"M","M"),NSLocalizedString(@"N","N"),NSLocalizedString(@"O","O"),NSLocalizedString(@"P","P"),NSLocalizedString(@"Q","Q"),NSLocalizedString(@"R","R"),NSLocalizedString(@"S","S"),NSLocalizedString(@"T","T"),NSLocalizedString(@"U","U"),NSLocalizedString(@"V","V")]];
	
	searchBar.barTintColor = [UIColor whiteColor];
	searchBar.backgroundColor=[UIColor whiteColor];
	[searchBar setImage:[[UIImage imageNamed: @"Search_icone"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
	///call service
	[self performSelector:@selector(getCategoryFilter) withObject:nil afterDelay:0];
	self.selectorVertical.dataSource = self;
	self.selectorVertical.delegate = self;
	self.selectorVertical.shouldBeTransparent = YES;
	self.selectorVertical.horizontalScrolling = NO;
	tbl_View.hidden=YES;
	self.selectorVertical.hidden=YES;
    UIColor *color = [UIColor darkGrayColor];
    self.txtFieldSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"חפש" attributes:@{NSForegroundColorAttributeName: color}];

}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnRevealMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:YES];

}

#pragma mark-UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

	return [array_IndexTitle1 count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if(!array_category || array_category.count == 0)
		return 0;
	// Return the number of rows in the section.
	NSString *sectionTitle = [array_SectionTitles objectAtIndex:section];
	NSArray *sectionData = [dict_data objectForKey:sectionTitle];
	return [sectionData count];
}


- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
	return array_IndexTitle1[section];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
	// Configure the cell...
	NSString *sectionTitle = [array_SectionTitles objectAtIndex:indexPath.section];
	NSArray *sectionData = [dict_data objectForKey:sectionTitle];
	WSCategoryFilterResponse *str_data=[sectionData objectAtIndex:indexPath.row];
	cell.backgroundColor=[UIColor clearColor];
	tableView.backgroundColor=[UIColor clearColor];
	tableView.separatorColor=[UIColor clearColor];
	cell.textLabel.text = str_data.name;
	cell.textLabel.textAlignment = NSTextAlignmentRight;
	cell.detailTextLabel.textAlignment = NSTextAlignmentRight;
	self.selectorVertical.hidden=NO;
	tbl_View.hidden=NO;
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	if ([tableView.dataSource tableView:tableView numberOfRowsInSection:section] == 0) {
		return 0;
	} else {
		// whatever height you'd want for a real section header
		return 18;
	}
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
	/* Create custom view to display section header... */
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width-25, 18)];
	[label setFont:helveticaBoldFont(18)];
	label.textAlignment = NSTextAlignmentRight;
	label.textColor=[UIColor redColor];
	NSString *string =array_IndexTitle1[section];
	/* Section header is in 0th index... */
	[label setText:string];
	[view addSubview:label];
	[view setBackgroundColor:[UIColor clearColor]]; //your background color...
	return view;
}

#pragma mark-UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *sectionTitle = [array_SectionTitles objectAtIndex:indexPath.section];
	NSArray *sectionData = [dict_data objectForKey:sectionTitle];
	WSCategoryFilterResponse *str_data=[sectionData objectAtIndex:indexPath.row];
	str_tableTitle=str_data.name;
	[self getCategoryDetail:str_data.id :@"המתן לטעינה..."];
}
#pragma mark-API Calling
//mFetchFilterList
-(void)getCategoryFilter
{
	[[ConnectionsManager sharedManager] getFildCategoryFilterList_withdelegate:self];
	[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];

}

-(void)getCategoryDetail:(NSString *)filterId :(NSString *)str_category
{
	[[ConnectionsManager sharedManager] getFieldCategoryDetail_withdelegate:filterId delegate:self];
	[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}

#pragma mark-UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
	NSString *searchString = self.txtFieldSearch.text;

	if([string isEqualToString:@""])
		searchString = [searchString substringToIndex:[searchString length] - 1];
	else
		searchString = [searchString stringByAppendingString:string];

	[self searchCouponsby:searchString];
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	if (![textField.text isEqualToString:@""])
	 [self searchCouponsby:self.txtFieldSearch.text];
	return YES;
}

#pragma mark-UserDefineMethods

-(void)searchCouponsby:(NSString *)aStr
{

	NSMutableArray *tmp = [NSMutableArray array];
	for(NSString *section in array_SectionTitles)
	{
		NSArray *fil = [mainCopy objectForKey:section];
		NSPredicate *cityPredicate = [NSPredicate predicateWithFormat:@"(SELF.name contains[cd] %@)",aStr];
		NSArray *filterArray = [fil filteredArrayUsingPredicate:cityPredicate];
		NSLog(@"Fileter Data : %@",filterArray);

		NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
		[dic setObject:filterArray forKey:section];
		[tmp addObject:dic];
	}

	NSMutableDictionary *dict_final=[[NSMutableDictionary alloc] init];
	for (NSDictionary *dictionary in tmp){
		// looping through the array
		[dict_final addEntriesFromDictionary:dictionary];
	}

	if([aStr isEqualToString:@""])
	{
		array_SectionTitles = array_Copytitle;
		dict_data = mainCopy;
	}
	else
	{
		dict_data=dict_final;
	}

	[tbl_View reloadData];
}

-(void)sortingForCategory
{
	NSMutableArray *array_Response=[[NSMutableArray alloc] init];
	for (int i=0; i<array_category.count; i++)
	{
		WSCategoryFilterResponse *wscategoryResponse=[array_category objectAtIndex:i];
        NSString *firstChar = [wscategoryResponse.name substringWithRange:NSMakeRange(0, 1)];
        
        if(![array_IndexTitle1 containsObject:firstChar])
        {
            [array_IndexTitle1 addObject:firstChar];
        }
		[array_Response addObject:wscategoryResponse];
	}
    [array_IndexTitle1 sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
	NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name"
																 ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
	NSArray *sortedArray = [array_Response sortedArrayUsingDescriptors:sortDescriptors];
	filter = [NSMutableArray array];
    /**
     Now try to check if we have all the items under array_IndexTitle1;
     */
    array_IndexTitle1Copy=[[NSMutableArray alloc] init];
    array_sectionNameFilter=array_IndexTitle1;
	[array_IndexTitle1 sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    for (int i=0; i<3; i++)
    {
        [array_IndexTitle1Copy addObjectsFromArray:array_IndexTitle1];
    }
    ///
    
	for(NSString *ch in array_IndexTitle1)
	{
		NSPredicate *pred =[NSPredicate predicateWithFormat:@"SELF.name beginswith[c] %@", ch];
		NSArray *filterData  = [sortedArray filteredArrayUsingPredicate:pred];
		NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
		[dic setObject:filterData forKey:ch];
		[filter addObject:dic];
	}

	NSMutableDictionary *dict_final=[[NSMutableDictionary alloc] init];
	for (NSDictionary *dictionary in filter){
		// looping through the array
		[dict_final addEntriesFromDictionary:dictionary];
	}
	dict_data=dict_final;
	mainCopy = dict_final;
	array_IndexTitles=array_IndexTitle1;
	array_SectionTitles=[[dict_data allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	array_Copytitle=array_SectionTitles;
	[tbl_View reloadData];
}


#pragma mark- IZValueSelector dataSource
- (NSInteger)numberOfRowsInSelector:(IZValueSelectorView *)valueSelector {
    return (array_IndexTitle1Copy)?array_IndexTitle1Copy.count:0;
}

//ONLY ONE OF THESE WILL GET CALLED (DEPENDING ON the horizontalScrolling property Value)
- (CGFloat)rowHeightInSelector:(IZValueSelectorView *)valueSelector {
	return 25.0;
}

- (CGFloat)rowWidthInSelector:(IZValueSelectorView *)valueSelector {
	return 40.0;
}

- (UIView *)selector:(IZValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index
{
	return [self selector:valueSelector viewForRowAtIndex:index selected:NO];
}

- (UIView *)selector:(IZValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index selected:(BOOL)selected {
	UILabel * label = nil;
	label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.selectorVertical.frame.size.width, 25)];
	[label setFont:helveticaBoldFont(16)];

	label.text = [NSString stringWithFormat:@"%@",[array_IndexTitle1Copy objectAtIndex:index]];

	label.textAlignment =  NSTextAlignmentCenter;
	if (selected) {
		label.textColor = [UIColor blackColor];

	} else {
		label.textColor = [UIColor blackColor];
	}
	return label;
}

- (CGRect)rectForSelectionInSelector:(IZValueSelectorView *)valueSelector {
	//Just return a rect in which you want the selector image to appear
	//Use the IZValueSelector coordinates
	//Basically the x will be 0
	//y will be the origin of your image
	//width and height will be the same as in your selector image
	return CGRectMake(0.0,+self.selectorVertical.frame.size.height/2-35 ,40.0,25.0);
}

#pragma IZValueSelector delegate
- (void)selector:(IZValueSelectorView *)valueSelector didSelectRowAtIndex:(NSInteger)index {
	if(array_IndexTitle1.count <= index)
		index = index % array_IndexTitle1.count;
	
	if ([tbl_View numberOfSections] > index && index > -1)
	{
		NSString *sectionTitle = [array_SectionTitles objectAtIndex:index];
		NSArray *sectionData = [dict_data objectForKey:sectionTitle];
		if(sectionData.count > 0)
		//for safety, should always be YES
			[tbl_View scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]
							atScrollPosition:UITableViewScrollPositionTop
									animated:YES];
	}
}


//logic to scroll index bar infinity

-(void) izValueSelectorView:(IZValueSelectorView *)valueSelector scrollViewDidScrollInUsingClass:(UIScrollView *)scrollView
{

	CGSize contentSize = scrollView.contentSize;
	CGFloat firstHalf = contentSize.height / 3;
	CGPoint currentOffset = scrollView.contentOffset;

	if(currentOffset.y >= firstHalf * 2)
	{
		currentOffset.y -= firstHalf;
		scrollView.contentOffset = currentOffset ;
	}
	else if(currentOffset.y < firstHalf)
	{
		currentOffset.y += firstHalf;
		scrollView.contentOffset = currentOffset;
	}
}


#pragma mark-APIResponse
- (void) success:(WSBaseResponse *)response
{
	[DejalActivityView removeView];
	if([response.method isEqualToString:mFetcategoryFilters])
	{
		//Fetch Coupons response
		if([response.respondeBody isKindOfClass:[NSArray class]])
		{
			NSArray *responseList = response.respondeBody;
			if(responseList.count)
			{
				NSMutableArray *temp = [NSMutableArray array];
				for(NSDictionary *dic in responseList)
				{
					WSCategoryFilterResponse *categoryResponse=[[WSCategoryFilterResponse alloc] initWithDictionary:dic];
					[temp addObject:categoryResponse];
					NSLog(@"category=%@", categoryResponse);
				}
				//TODO Array
				array_category = [[NSMutableArray alloc]initWithArray:temp];
				NSLog(@"array_category=%@",array_category);


			}
		}
		
		[self sortingForCategory];
        [self.selectorVertical reloadData];
        [self.selectorVertical selectRowAtIndex:array_IndexTitle1.count animated:NO ]; //FIRST time
	}
	else if([response.method isEqualToString:mFetchcategoryFilterDetail] )
	{
		//Fetch Coupons response
		if([response.respondeBody isKindOfClass:[NSArray class]])
		{
			NSArray *responseList = response.respondeBody;
			if(responseList.count)
			{
				NSMutableArray *temp = [NSMutableArray array];
				for(NSDictionary *dic in responseList)
				{
					WSCategoryFilterDetailResponse *categoryDetailResponse=[[WSCategoryFilterDetailResponse alloc] initWithDictionary:dic];
					[temp addObject:categoryDetailResponse];

					NSLog(@"dict=%@",dic);
				}
				NSLog(@"dict=%@",temp);
				FieldCategoryDetailVC *fieldCategoryVc=[self getVCWithSB_ID:kFieldCategoryDetailVC];
				fieldCategoryVc.array_categoryDetailList=[temp mutableCopy];
				fieldCategoryVc.str_Title=str_tableTitle;
				[self.navigationController pushViewController:fieldCategoryVc animated:YES];



		}
	}
		
}
}

- (void) failure:(WSBaseResponse *)response
{
	[DejalActivityView removeView];
	if([response.method isEqualToString:mGetDealOfDay])
	{

	}
}

@end
