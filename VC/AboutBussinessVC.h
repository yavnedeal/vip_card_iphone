//
//  AboutBussinessVC.h
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Base/BaseViewController.h"

@interface AboutBussinessVC : BaseViewController
{
	
	__weak IBOutlet UILabel *lbl_Title;
	__weak IBOutlet UITextView *txtView_AboutUs;
}

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic,strong) NSString *bussinessDiscription;
@property (nonatomic,strong) NSString *bussinessTitle;
@property(nonatomic,assign)BOOL flagForTerms;
@property(nonatomic,strong)NSString *mLimitations;
@end
