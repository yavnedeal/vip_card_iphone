

//
//  MainScreenViewController.m
//  VIPCard
//
//  Created by fis on 20/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "MainScreenViewController.h"
#import "YSLContainerViewController.h"
#import "ViewController.h"
#import "CustomTableViewController.h"
#import "DetailedViewController.h"
#import "SWRevealViewController.h"
#import "CommonCollectionViewController.h"
#import "ViPListViewController.h"
#import "ScrollViewController.h"
#import "ScrollViewImageViewController.h"

#import "WSFetchCouponsResponse.h"
#import "WSDealsResponse.h"
#import "WSBussinessObjResponse.h"
#import "BussinessListVC.h"

@interface MainScreenViewController ()<YSLContainerViewControllerDelegate, ServerResponseDelegate, UITextFieldDelegate>
{
	NSMutableArray *myArray;
	NSMutableArray *viewControllers;
    
    NSUInteger currentIndex;
}

@property (nonatomic) IBOutlet UIButton* revealButtonItem;

@property (nonatomic,assign)  NSInteger currentScreenTypeIndex;

@end

@implementation MainScreenViewController
@synthesize index,firstArray,str_ViewControllers;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBackButton:YES];
    [self.btnCancel addTarget:self action:@selector(onClickCancelButton) forControlEvents:UIControlEventTouchUpInside];
    
	[self customSetup];
	[self viewControllerCount];
    UIColor *color = [UIColor darkGrayColor];
    self.txtFldSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"חפש" attributes:@{NSForegroundColorAttributeName: color}];
    
    // Do any additional setup after loading the view.
    
        
        
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self setupOnWillDisplay];
    [super viewWillAppear:animated];

}

-(void) setupOnWillDisplay
{
    [self showBackButton:YES];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    switch (appDelegate.index)
    {
        case 0:
            [self.btnCancel setTitle:NSLocalizedString(@"title_activity_Coupan", @"title_activity_Coupan") forState:UIControlStateNormal];
            
            break;
        case 1:
            [self.btnCancel setTitle:NSLocalizedString(@"title_activity_Deals", @"title_activity_Deals") forState:UIControlStateNormal];
            
            break;
        case 2:
            [self.btnCancel setTitle:NSLocalizedString(@"title_activity_business", @"title_activity_business") forState:UIControlStateNormal];
            
            break;
            
            
        default:
            break;
    }
    self.txtFldSearch.placeholder = NSLocalizedString(@"hint_search", @"hint_search");

}


-(void) refreshViewOnChange
{

    [self viewControllerCount];
    [self setupOnWillDisplay];
    
}


- (void)customSetup
{
	SWRevealViewController *revealViewController = self.revealViewController;
	if ( revealViewController )
	{
		[self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
		[self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
	}
}

#pragma mark -- YSLContainerViewControllerDelegate
- (void)containerViewItemIndex:(NSInteger)indexY currentController:(UIViewController *)controller
{
    currentIndex = indexY;
	[controller viewWillAppear:YES];
}

-(void)viewControllerCount
{
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.currentScreenTypeIndex = appDelegate.index;
	switch (appDelegate.index)
	{
  case 0:
		{
            [self getDeals];
		}
			break;
		case 1:

		{
            [self getCoupons];
		}
			break;
		case 2:
		{
            [self getBussinessList];
		}
			break;
		case 3:
		{
			UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kDealOfTheDay];
			[self.navigationController pushViewController:vc animated:YES];
		}
			break;

		case 4:
		{
			//int count = 6;
			viewControllers= [[NSMutableArray alloc]init];
			/*for (int i=0; i<=count; i++)
			{
				ViewController *vc = [self getVCWithSB_ID:kFilterBussiness];
				vc.title = @"FilterBussinessVC";
				[viewControllers addObject:vc];

			}*/
		}
			break;
		case 5:
		{

			int count = 6;
			viewControllers= [[NSMutableArray alloc]init];
			for (int i=0; i<=count; i++)
			{
				ViewController *vc = [self getVCWithSB_ID:kCommonCollectionVC_SB_ID];
				vc.title = @"CommonCollectionViewController";
				[viewControllers addObject:vc];

			}

		}
			break;
		default:
			break;
    }
}

//API Call
-(void)getCoupons
{
    [[ConnectionsManager sharedManager] fetchCoupons_withdelegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}

-(void)getDeals
{
    [[ConnectionsManager sharedManager] getdeals_withdelegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}

-(void)getBussinessList
{
    [[ConnectionsManager sharedManager] getBussinessList_withdelegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:YES];
}

- (IBAction)scrollViewButton:(id)sender
{
	[self performSegueWithIdentifier:@"scrollDetailSegue" sender:nil];
}

#pragma Mark ServerResponseDelegate
-(void)success:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    
    //Success
    if([response.method isEqualToString:mFetchBussinessList])
    {
        if([response.respondeBody isKindOfClass:[NSArray class]])
        {
            NSArray *responseList = response.respondeBody;
            if(responseList.count)
            {
                NSMutableArray *temp = [NSMutableArray array];
                for(NSDictionary *dic in responseList)
                {
                    WSBussinessObjResponse *bussinessResponse = [[WSBussinessObjResponse alloc] initWithDictionary:dic];
                    [temp addObject:bussinessResponse];
                }
                
                NSArray *bussineesListTemp = temp;


				NSSortDescriptor *ageDescriptor = [[NSSortDescriptor alloc] initWithKey:@"id.intValue" ascending:YES];
				NSArray *sortDescriptors = @[ageDescriptor];
				NSArray *sortedArray = [bussineesListTemp sortedArrayUsingDescriptors:sortDescriptors];
                viewControllers= [[NSMutableArray alloc]init];
                for (WSBussinessObjResponse *bussinessResponse in sortedArray)
                {
                    BussinessListVC *vc = [self getVCWithSB_ID:kBussinessList_VC];
                    if(bussinessResponse.name && bussinessResponse.name != nil)
                    {
                        vc.title = bussinessResponse.name;
                        vc.bussinessObjResponse = bussinessResponse;
                        vc.isMarketPlace = FALSE;
                        if([bussinessResponse.bussinessList count])
                            [viewControllers addObject:vc];
                    }
                }
                float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
                float navigationHeight = self.navigationController.navigationBar.frame.size.height;
                
                viewControllers=[[[viewControllers reverseObjectEnumerator] allObjects] mutableCopy];

                YSLContainerViewController *containerVC = [[YSLContainerViewController alloc]initWithControllers:viewControllers
                                                                                                    topBarHeight:statusHeight + navigationHeight
                                                                                            parentViewController:self];
                containerVC.view.frame = CGRectMake(0, 40, containerVC.view.frame.size.width, containerVC.view.frame.size.height);
                containerVC.delegate = self;
                containerVC.menuItemFont = [UIFont fontWithName:@"Arial" size:16];
				containerVC.menuItemBckColor=[UIColor redColor];
                
                [self.view addSubview:containerVC.view];
            }
        }
    }
    
    

    
    if([response.method isEqualToString:mFetchCoupons])
    {
        //Fetch Coupons response
        if([response.respondeBody isKindOfClass:[NSArray class]])
        {
            NSArray *responseList = response.respondeBody;
            if(responseList.count)
            {
                NSMutableArray *temp = [NSMutableArray array];
                for(NSDictionary *dic in responseList)
                {
                    WSFetchCouponsResponse *dealsResponse = [[WSFetchCouponsResponse alloc] initWithDictionary:dic];
                    [temp addObject:dealsResponse];
                }
                
                NSArray *dealsList = temp;
                viewControllers= [[NSMutableArray alloc]init];
                for (WSDealsResponse *dealsResponse in dealsList)
                {
                    ViPListViewController *vc = [self getVCWithSB_ID:kVIPListVC_SB_ID];
                    vc.title = dealsResponse.name;
                    vc.isCoupns = NO;
                    vc.isSearch = NO;
					vc.str_module=@"Coupon";
                    vc.dealsResponseObj = dealsResponse;
                    if([dealsResponse.couponsList count])
                        [viewControllers addObject:vc];
                }
                NSArray* reversed = [[viewControllers reverseObjectEnumerator] allObjects];
                viewControllers = [reversed mutableCopy];
              /*  float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
                float navigationHeight = self.navigationController.navigationBar.frame.size.height;
                
                YSLContainerViewController *containerVC = [[YSLContainerViewController alloc]initWithControllers:viewControllers
                                                                                                    topBarHeight:statusHeight + navigationHeight
                                                                                            parentViewController:self];
                containerVC.delegate = self;
                containerVC.menuItemFont = [UIFont fontWithName:@"Arial" size:16];
                
                [self.view addSubview:containerVC.view];
                
                [self.view bringSubviewToFront:self.baseView];*/
                
                float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
                float navigationHeight = self.navigationController.navigationBar.frame.size.height;
                
                YSLContainerViewController *containerVC = [[YSLContainerViewController alloc]initWithControllers:viewControllers
                                                                                                    topBarHeight:statusHeight + navigationHeight
                                                                                            parentViewController:self];
                containerVC.view.frame = CGRectMake(0, 40, containerVC.view.frame.size.width, containerVC.view.frame.size.height);
                containerVC.delegate = self;
                containerVC.isScrollShow = NO;
                containerVC.menuItemFont = [UIFont fontWithName:@"Arial" size:16];
                
                [self.view addSubview:containerVC.view];
            }
        }
    }
    
    else if ([response.method isEqualToString:mFetchDeals])
    {
        if([response.respondeBody isKindOfClass:[NSArray class]])
        {
            NSArray *responseList = response.respondeBody;
            if(responseList.count) 
            {
                NSMutableArray *temp = [NSMutableArray array];
                for(NSDictionary *dic in responseList)
                {
                    WSDealsResponse *dealsResponse = [[WSDealsResponse alloc] initWithDictionary:dic];
                    [temp addObject:dealsResponse];
                }
                
                NSArray *dealsList = temp;
                viewControllers= [[NSMutableArray alloc]init];
                for (WSDealsResponse *dealsResponse in dealsList)
                {
                    ViPListViewController *vc = [self getVCWithSB_ID:kVIPListVC_SB_ID];
                    vc.title = dealsResponse.name;
                    vc.isCoupns = YES;
                    vc.isSearch = NO;
					vc.str_module=@"Deal";

                    vc.dealsResponseObj = dealsResponse;
                     if([dealsResponse.couponsList count])
                         [viewControllers addObject:vc];
                }
                NSArray* reversed = [[viewControllers reverseObjectEnumerator] allObjects];
                viewControllers = [reversed mutableCopy];

             /*   float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
                float navigationHeight = self.navigationController.navigationBar.frame.size.height;
                
                YSLContainerViewController *containerVC = [[YSLContainerViewController alloc]initWithControllers:viewControllers
                                                                                                    topBarHeight:statusHeight + navigationHeight
                                                                                            parentViewController:self];
                containerVC.delegate = self;
                containerVC.menuItemFont = [UIFont fontWithName:@"Arial" size:16];
                
                [self.view addSubview:containerVC.view];
                
                [self.view bringSubviewToFront:self.baseView];*/
                
                float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
                float navigationHeight = self.navigationController.navigationBar.frame.size.height;
                
                YSLContainerViewController *containerVC = [[YSLContainerViewController alloc]initWithControllers:viewControllers
                                                                                                    topBarHeight:statusHeight + navigationHeight
                                                                                            parentViewController:self];
                containerVC.view.frame = CGRectMake(0, 40, containerVC.view.frame.size.width, containerVC.view.frame.size.height);
                containerVC.delegate = self;
                containerVC.isScrollShow = NO;
                containerVC.menuItemFont = [UIFont fontWithName:@"Arial" size:16];
                
                [self.view addSubview:containerVC.view];

            }
        }
        //Fetch Market responce
        
    }
}


-(void)failure:(WSBaseResponse *)response
{
[DejalActivityView removeView];
    //Failure
    if([response.method isEqualToString:mFetchCoupons])
    {
        //Fetch Coupons error response
    }
    
    else if ([response.method isEqualToString:mFetchDeals])
    {
        //Fetch Deals error response
    }
}

-(void)searchCouponsby:(NSString *)aStr
{
    
    if(self.currentScreenTypeIndex == 2)
    {
          NSPredicate *businessPredicate = [NSPredicate predicateWithFormat:@"(SELF.name contains[cd] %@) OR (SELF.des contains[cd] %@)",aStr, aStr];
        if(![NSString isEmpty:aStr])
        {
            for (BussinessListVC *vc in viewControllers)
            {
                NSArray *filteredList = [vc.bussinessObjResponse.bussinessList filteredArrayUsingPredicate:businessPredicate];
                vc.filteredList = filteredList;
                vc.isSearch = YES;
                [vc reloadScreenData];
            }
            
        }
        else {
            for (BussinessListVC *vc in viewControllers)
            {
                vc.filteredList = nil;
                vc.isSearch = NO;
                [vc reloadScreenData];
            }
            
        }
        
        
        
        //return
    }
    

   // aStr = @"בימבה מכונית";
    NSPredicate *cityPredicate = [NSPredicate predicateWithFormat:@"(SELF.name contains[cd] %@) OR (SELF.des contains[cd] %@)",aStr, aStr];
//    המוצר התווסף לסל
    ViPListViewController *vc = [viewControllers objectAtIndex:currentIndex];
    if([vc isKindOfClass:[ViPListViewController class]])
    {
        NSArray *filterArray = [vc.dealsResponseObj.couponsList filteredArrayUsingPredicate:cityPredicate];
        vc.filteredCouponsList = filterArray;
        vc.isSearch = YES;
        if([aStr isEqualToString:@""])
            vc.filteredCouponsList = vc.dealsResponseObj.couponsList;
        [vc viewWillAppear:YES];
    }
    
   // NSArray *filteredArray = [self.dealsResponseObj.couponsList filteredArrayUsingPredicate:cityPredicate];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self.view endEditing:YES];
    [self searchCouponsby:self.txtFldSearch.text];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *searchString = self.txtFldSearch.text;
    
    if([string isEqualToString:@""])
        searchString = [searchString substringToIndex:[searchString length] - 1];
    else
        searchString = [searchString stringByAppendingString:string];
    
    [self searchCouponsby:searchString];
    return YES;
}

-(void)onClickCancelButton
{
    ViPListViewController *vc = [viewControllers objectAtIndex:currentIndex];
    if([vc isKindOfClass:[ViPListViewController class]])
    {
        vc.filteredCouponsList = vc.dealsResponseObj.couponsList;
        vc.isSearch = NO;
        [vc viewWillAppear:YES];
        [self.txtFldSearch setText:@""];
    }
}

@end
