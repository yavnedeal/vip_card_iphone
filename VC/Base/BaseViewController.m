//
//  BaseViewController.m
//  VIPCard
//
//  Created by SanC on 17/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//




#import "BaseViewController.h"
#import "AppColorConstants.h"
#import "UIImageView+WebCache.h"
#import "PopViewController.h"
#import "UserLoginVC.h"
#import "AppDelegate.h"
#import "BussinessPopup.h"
#import "FindBussinessList.h"
#import "FieldCategoryFilterVC.h"
#import "WSCouponsResponse.h"
#import "CategoryForMealHolderVC.h"
#import "CategoryForMealDetailVC.h"

@interface BaseViewController ()<PopupViewDelegate,BussinessPopupDelegate>
{
    PopViewController *popVC;
    NSInteger selectedindex;
    NSArray *array;
}
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES ;
  //  UIGestureRecognizer *gesture = [[UIGestureRecognizer alloc] initWithTarget:self action:@selector(hideBaseView:)];
    // Do any additional setup after loading the view.
}

-(void) hideBaseView:(UIGestureRecognizer *) rec{
    //Override in child controllers
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear: YES];
	[self.navigationController.navigationBar setHidden:NO];
	[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
	[[UINavigationBar appearance] setTitleTextAttributes: @{NSFontAttributeName: helveticaMediumFont(18),

															}];


}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
	UIImage *originalImage = sourceImage;
	// scaling set to 2.0 makes the image 1/2 the size.
	UIImage *scaledImage =[UIImage imageWithCGImage:[originalImage CGImage]
						scale:(originalImage.scale * 2.0)
				  orientation:(originalImage.imageOrientation)];
	return scaledImage;
}

-(CGSize)getStringSize:(UILabel *)aLabel andString:(NSString *)aStr
{
    CGSize expectedLabelSize;
    if(!aStr ||[aStr isEqualToString:@""])
        return expectedLabelSize;
    NSMutableString *myString = [[NSMutableString alloc] initWithString:aStr];

	CGSize maximumLabelSize = CGSizeMake(aLabel.frame.size.width,9999);

	expectedLabelSize = [myString sizeWithFont:aLabel.font
									constrainedToSize:maximumLabelSize
										lineBreakMode:aLabel.lineBreakMode];

	//adjust the label the the new height.
	CGRect newFrame = aLabel.frame;
	newFrame.size.height = expectedLabelSize.height;
	aLabel.frame = newFrame;
	return expectedLabelSize;
}



-(void)showPopupwithmessage:(NSInteger)popupIndex toShowValue:(id)value
{
//-(void)showPopupwithmessage:(BOOL)isTimerPopup toShowValue:(id)value

    popVC = [[PopViewController alloc] initWithNibName:[self loadNibFileforPopViewController:popupIndex] bundle:nil];
    popVC.value = value;
    popVC.delegate = self;
    [popVC showInView:[[UIApplication sharedApplication].delegate window] withMessage:@"" animated:NO];
   // [popVC.btnSignIn addTarget:self action:@selector(actionSignIN:) forControlEvents:UIControlEventTouchUpInside];
  //  [popVC.btnSignUp addTarget:self action:@selector(actionSignup:) forControlEvents:UIControlEventTouchUpInside];

}


-(void)showFindBussinessPopUp
{
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	BussinessPopup * pp2 = [BussinessPopup bussinessPopup:CGRectMake(0, 0,appDelegate.window.frame.size.width,appDelegate.window.frame.size.height )];
	pp2.bussDelegate = self;
   [[[UIApplication sharedApplication].delegate window]  addSubview:pp2];
}

-(void) businessPopup:(BussinessPopup *)bussPopup withButtonIndex:(NSInteger)butonIndex
{
	if(butonIndex == 1)
	{
		[self onClick_FindBussiness];
	}
	else  {
		[self onClick_FilterBussinessCategory];
	}

	if(self.revealViewController)
	{
		[self.revealViewController revealToggleAnimated:NO];
	}
}

-(void)onClick_FindBussiness
{
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	FindBussinessList *findBussinessList = [storyboard instantiateViewControllerWithIdentifier:kFindBussinessList];
    SWRevealViewController *vc = self.revealViewController;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.index = 250;
    if(!vc){
	  vc = [storyboard instantiateViewControllerWithIdentifier:kRevealVC_SB_ID];
      [vc setFrontViewController:findBussinessList animated:NO];
      [self.navigationController pushViewController:vc animated:YES];
    }
    else {
        if(![self.revealViewController.frontViewController isKindOfClass:[FindBussinessList class]])
        {
            [self.revealViewController pushFrontViewController:findBussinessList animated:NO];
        }
    }
	
	

}

-(void)onClick_FilterBussinessCategory
{
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	FieldCategoryFilterVC *fieldCategoryFilterVC = [storyboard instantiateViewControllerWithIdentifier:kFieldCategoryFilterVC];
		AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
		appDelegate.index = 250;
        SWRevealViewController *vc = self.revealViewController;
        if(!vc){
            vc = [storyboard instantiateViewControllerWithIdentifier:kRevealVC_SB_ID];
            [vc setFrontViewController:fieldCategoryFilterVC animated:NO];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else {
            if(![self.revealViewController.frontViewController isKindOfClass:[FieldCategoryFilterVC class]])
            {
                [self.revealViewController pushFrontViewController:fieldCategoryFilterVC animated:NO];
            }
        }


}



- (IBAction)actionSignIN:(id)sender
{
   // [self.delegate closeButton];
   // [self removeAnimate];
    
    // _popupBaseView.hidden = YES;
}
- (IBAction)actionSignup:(id)sender
{
   // [self.delegate closeButton];
   // [self removeAnimate];
    
    // _popupBaseView.hidden = YES;
}
-(NSString *)loadNibFileforPopViewController:(NSInteger)isTimerPopup
{
    NSString *fileName;
    
    if(isTimerPopup == TIME)
    {
        fileName = @"TImePopViewController";
    }
    else if(isTimerPopup == LOGIN)
    {
        if(IDIOM == IPAD)
        {
            fileName = @"PopUpViewController";
        }
        else
        {
            fileName = @"PopUpViewController";
        }
    }
    else if(isTimerPopup == BARCODE)
    {
        if(IDIOM == IPAD)
        {
            fileName = @"newpopViewController";
        }
        else
        {
            fileName = @"newpopViewController";
        }
    }
    return fileName;
}


-(void)showBackButton:(BOOL)isShow
{
	if (isShow)
	{
		UIButton *img = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
		img.imageEdgeInsets = UIEdgeInsetsMake(2, -10, 2, 10);
		[img setImage:[UIImage imageNamed:@"backArrow.png"] forState:UIControlStateNormal];
		[img addTarget:self action:@selector(ActionBack:) forControlEvents:UIControlEventTouchUpInside];
		UIBarButtonItem *one = [[UIBarButtonItem alloc]initWithCustomView:img];

		self.navigationItem.leftBarButtonItem = one;
	}
}

-(IBAction)ActionBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(CALayer*)getLayerFrame:(CGRect)toFrame toPosition:(NSInteger)position
{
   // if([position isEqualToString:@"TOP"])
    if(TOP == position)
    {
        CALayer *topBorderMain = [CALayer layer];
        topBorderMain.frame = CGRectMake(0.0f, 1.0f, self.view.frame.size.width, 1.0f);
        topBorderMain.backgroundColor = [UIColor grayColor].CGColor;
        return topBorderMain;
    }
    else if(BOTTOM == position)
    {
        CALayer *topBorderMain = [CALayer layer];
        topBorderMain.frame = CGRectMake(0.0f, toFrame.size.height - 1 ,self.view.frame.size.width, 1.0f);
        topBorderMain.backgroundColor = [UIColor grayColor].CGColor;
        return topBorderMain;
    }
    else if(LEFT == position)
    {
        CALayer *topBorderMain = [CALayer layer];
        topBorderMain.frame = CGRectMake(1.0f, 0 ,1,toFrame.size.height);
        topBorderMain.backgroundColor = [UIColor grayColor].CGColor;
        return topBorderMain;
    }
    else
    {
        CALayer *topBorderMain = [CALayer layer];
        topBorderMain.frame = CGRectMake(toFrame.size.width - 1,0,1, toFrame.size.height);
        topBorderMain.backgroundColor = [UIColor grayColor].CGColor;
        return topBorderMain;
    }

    
}

-(void)actionToCall:(NSString*)number
{
    NSString *phNo = number;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [calert show];
    }
}

-(id )getVCWithSB_ID:(NSString *)storyBoardName
{
  
	UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:storyBoardName];
	return vc;
}
-(NSURL*)baseImageURL:(NSString*)imgName
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mFetchImages,imgName]];
    return url;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) navigateToLatitude:(double)latitude
				  longitude:(double)longitude address:(NSString*)str_Address
{

//Gauri added 31/5/2016


    NSString *urlStr = @"waze://";
    
    if(str_Address && ![str_Address isEmpty])
    {
        urlStr =
        [NSString stringWithFormat:@"waze://?q=%@",
         [str_Address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    else
    {
        urlStr =
        [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes&z=10",
         latitude, longitude];
    }
    
    
    if ([[UIApplication sharedApplication]
         canOpenURL:[NSURL URLWithString:@"waze://"]])
         {
        // Waze is installed. Launch Waze and start navigation
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
        
    } else {
        
        // Waze is not installed. Launch AppStore to install Waze app
        [[UIApplication sharedApplication] openURL:[NSURL
                                                    URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
    }
}


- (IBAction)submitte_btn:(id)sender
{
    UIButton *btnCopoun = (UIButton*)sender;
    selectedindex = btnCopoun.tag;
    WSCouponsResponse *wsCoiponsRes = [array objectAtIndex:btnCopoun.tag];
    wsCoiponsRes = wsCoiponsRes;
  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (wsCoiponsRes.idCategor.integerValue == 64)
        {
            
            CategoryForMealHolderVC *vc = [storyBoard instantiateViewControllerWithIdentifier:kCategoryForMealHolder_SB_ID];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            CategoryForMealDetailVC *vc1 = [storyBoard instantiateViewControllerWithIdentifier:kCategoryForMealDetailVC_SB_ID];
            
            [self.navigationController presentViewController:vc1 animated:YES completion:nil];
                }
    

}

-(void)showAlertview:(NSString*)alertMsg
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:alertMsg delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    [alert show];
}
-(BOOL)islogin
{

    NSLog(@"User Login Id : %@",[[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_LOGIN_USERID]);
    if ([[NSUserDefaults standardUserDefaults] valueForKey:KEY_USER_LOGIN_USERID]!=nil)
     {
         return YES;
    }
    return NO;
}



@end
