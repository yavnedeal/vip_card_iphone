//
//  BaseTableViewCell.h
//  VIPCard
//
//  Created by SanC on 17/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BaseTableViewCell : UITableViewCell

-(CGSize)getStringSize:(UILabel *)aLabel andString:(NSString *)aStr;
@end
