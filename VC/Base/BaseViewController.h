//
//  BaseViewController.h
//  VIPCard
//
//  Created by SanC on 17/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//


typedef enum
{
    TOP = 1,
    BOTTOM = 2,
    LEFT = 3,
    RIGHT = 4
} BORDERLAYER;


typedef enum
{
    TIME = 1,
    LOGIN = 2,
    BARCODE = 3
} POPUP_NAME;


#import "UITextField+TextField.h"
#import "UIButton+Button.h"
#import "UIImageView+ImageView.h"
#import "UILabel+Lable.h"

#import "SHCStrikethroughLabel.h"
#import <UIKit/UIKit.h>
#import "NSString+CommonForApp.h"
#import "AppGlobalConstant.h"
#import "KeyConstant.h"
#import "ConnectionsManager.h"
#import "DejalActivityView.h"
#import "DODItemDetailsCell.h"
#import "DODTotalCell.h"
#import "AppGlobalData.h"
#import "iToast.h"
#import "UITableView+EmptyFooter.h"
#import "DateTimeUtil.h"

#import "AppColorConstants.h"
#import "UIImageView+WebCache.h"


#define IS_IPHONE_4 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)480) < DBL_EPSILON)
#define IS_IPHONE_5 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)568) < DBL_EPSILON)
#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)


@interface BaseViewController : UIViewController<UITextFieldDelegate>
-(id)getVCWithSB_ID:(NSString *)storyBoardName;

-(CALayer*)getLayerFrame:(CGRect)toFrame toPosition:(NSInteger)position;
-(CGSize)getStringSize:(UILabel *)aLabel andString:(NSString *)aStr;
-(void)showBackButton:(BOOL)isShow;;
-(void)actionToCall:(NSString*)number;
-(NSURL*)baseImageURL:(NSString*)imgName;


//- (void)showInView:(UIView *)aView withMessage:(NSString *)msg animated:(BOOL)animated;
-(void)showFindBussinessPopUp;

-(void)showPopupwithmessage:(NSInteger)popupName toShowValue:(id)value;
- (void) navigateToLatitude:(double)latitude
				  longitude:(double)longitude address:(NSString*)str_Address;
-(void)onClick_FindBussiness;
-(void)onClick_FilterBussinessCategory;
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width;

@property (nonatomic,assign) BOOL isFromDealCouponScreen; //SanC





- (IBAction)submitte_btn:(id)sender;

-(void)showAlertview:(NSString*)alertMsg;
-(BOOL)islogin;
@property (weak, nonatomic) IBOutlet UIView *onClick;

@end
