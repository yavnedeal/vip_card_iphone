//
//  GalleryVC.h
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Base/BaseViewController.h"

@interface GalleryVC : BaseViewController
@property (strong, nonatomic) IBOutlet UICollectionView *galleryCollectionView;
@property(nonatomic,strong)NSArray *array_getData;
@property(nonatomic, assign) NSInteger indePath;
@property(nonatomic,assign)NSIndexPath *path;
@end
