//
//  ShoppingCartSummaryVC.h
//  VIPCard
//
//  Created by Andrei on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Base/BaseViewController.h"
#import "WSCouponsResponse.h"

#import "MMPickerView.h"
#import "MyOrders.h"

@interface ShoppingCartSummaryVC : BaseViewController<ServerResponseDelegate,UITextFieldDelegate>
{
    NSIndexPath *path;
	bool freeCondition;
    NSString *openHour, *closeHour;
}

@property (strong ,nonatomic) WSCouponsResponse *wsCouponsResponse;

//@property (nonatomic, strong) NSArray *listOfCartItems;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbarOutlet;
@property (strong ,nonatomic) IBOutlet UITableView *tableView;
@property (strong ,nonatomic) IBOutlet UIButton *btnAddCart;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (assign ,nonatomic) BOOL isFromCart,isFromMyOrder;
@property (strong, nonatomic) MyOrders *myOrder;
@property (nonatomic, strong) NSMutableArray *listOfCartItems,*cartArr;

@property (strong,nonatomic) WSBussiness *selectedBussiness;
@property (strong,nonatomic) NSString *businessId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_bottomViewHeight;
@property (strong,nonatomic) NSString *myOrdDiscount;

@end
