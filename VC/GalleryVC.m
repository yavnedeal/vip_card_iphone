 //
//  GalleryVC.m
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "GalleryVC.h"
#import "GalleryCell.h"

@interface GalleryVC ()

@end

@implementation GalleryVC
@synthesize array_getData,indePath,path;


- (void)viewDidLoad {
	[super viewDidLoad];
	[self showBackButton:YES];
	// Do any additional setup after loading the view.
//	[self.view layoutIfNeeded];
//	[self.galleryCollectionView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:NO];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}
- (void)viewDidLayoutSubviews {
	[self.galleryCollectionView scrollToItemAtIndexPath:path
									   atScrollPosition:UICollectionViewScrollPositionRight animated:NO];
}

//-(IBAction)ActionBack:(id)sender
//{
//	[self.navigationController popViewControllerAnimated:NO];
//
//	}
#pragma mark-UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return array_getData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *identifier = @"gallery";
	GalleryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
	[cell populatedData:[array_getData objectAtIndex:indexPath.row]];
	return cell;
}


/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
