//
//  CatForMenuDataCell.h
//  VIPCard
//
//  Created by SanC on 09/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSExtraMealsResponse.h"

@interface CatForMenuDataCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckBox;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

-(void)populateData:(WSExtraMealsResponse *)extraMealObj Forprice:(int)price;

-(void) toggleCheckBox:(BOOL) flag;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constaintCheckBoxWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constaintLblNameHeight;

@end
