//
//  CatMealDetailCell.m
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "CatMealDetailCell.h"
#import "AppGlobalConstant.h"
#import "AppGlobalConstant.h"
#import "WSConstants.h"
#import "UIImageView+WebCache.h"

@implementation CatMealDetailCell

/*
*id,*name,*idCategoryForBusiness,*image,*description
*/

-(void)populateData:(WSMealsResponse *)aData
{
    [self.lblName setText:aData.name];
    [self.lblDesc setText:aData.description];
    [self.lblPrice setText:[NSString stringWithFormat:@"%@%@",SHEKEL,aData.price]];
    
    if(aData.image.length > 0)
    {
        NSURL *urlString = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mFetchImages,aData.image]];
        [self.mealImg sd_setImageWithURL:urlString placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
     //   [self.mealImg setImage:[UIImage imageNamed:aData.image]];
    }
    else
        [self.mealImg setImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
}

@end
