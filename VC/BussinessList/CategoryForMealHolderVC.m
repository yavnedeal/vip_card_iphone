
//
//  CategoryForMealHolderVC.m
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
#import "ShoppingCartSummaryVC.h"
#import "CategoryForMealHolderVC.h"
#import "CategoryForMealListVC.h"
#import "YSLContainerViewController.h"
#import "WSMealsResponse.h"
#import "WSExtraForMealsResponse.h"
#import  "WSBussiness.h"
#import "WSCouponsResponse.h"
#import "CategoryForMealDetailVC.h"

@interface CategoryForMealHolderVC () <YSLContainerViewControllerDelegate>
{
    
     AppGlobalData *appGlobalData;
    NSMutableArray *viewControllers;
    NSInteger currentIndex;
    
}

@property (nonatomic,strong) NSString *selectedBussinedId;
@end

@implementation CategoryForMealHolderVC
@synthesize categoryList,baseviewTitle,listOfmealItems;


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

    self.title = baseviewTitle;
	self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName: helveticaMediumFont(22)};

    [_lblNumberOfItems setHidden:NO];
	  if(self.categoryList && self.categoryList.count > 0)
	  {
		  WSCategoryMealResponse *bussinessResponse  = [self.categoryList objectAtIndex:0];
		  self.selectedBussinedId = bussinessResponse.idBusiness;
          _lblNumberOfItems.text = [NSString stringWithFormat:@"%lu",(unsigned long)[[appGlobalData getBussinessCart:self.selectedBussinedId] count]];
	  }
	else
		_lblNumberOfItems.text = @"0";

    
	[super viewWillAppear:animated];

}

-(void)viewDidLoad
{
    [self showBackButton:YES];
    self.cartTitle.text = NSLocalizedString(@"lbl_cart", @"lbl_cart");
    [super viewDidLoad];
    [self loadData];
    [self.view bringSubviewToFront:_cartContainView];
	 appGlobalData = [AppGlobalData sharedManager];
	
	
}

#pragma mark -- YSLContainerViewControllerDelegate
- (void)containerViewItemIndex:(NSInteger)indexY currentController:(UIViewController *)controller
{
    currentIndex = indexY;

    [controller viewWillAppear:YES];
   
}

-(void)loadData
{
    NSArray *bussineesListTemp = self.categoryList;
	//NSLog(@"bussinessListTemp%@",self.categoryList);
    viewControllers= [[NSMutableArray alloc]init];
    CategoryForMealListVC *vc;
    NSMutableArray *tempResponse = [[NSMutableArray alloc] init];
    NSInteger index= 0;
   
    
    for (WSCategoryMealResponse *bussinessResponse in bussineesListTemp)
    {

        if(![NSString isEmpty:bussinessResponse.name])
        {
			vc = [self getVCWithSB_ID:kCategoryForMealListVC];
            vc.title = bussinessResponse.name;
            vc.categoryMealResponse = bussinessResponse;
            vc.currentIndex = index;
            vc.businessId = self.selectedBussiness.id;//self.selectedBussinedId;
            vc.phone = self.selectedBussiness.orderphone;
            [tempResponse addObject:bussinessResponse];
           // vc.dealCouponResponse = self.dealCouponResponse;
             vc.listOfSubBussinessItems = [[NSMutableArray alloc] initWithArray:bussineesListTemp];
           // vc.isFromDealCouponScreen = self.isFromDealCouponScreen;//SanC
           [viewControllers addObject:vc];
            index ++;
        }
    }
    float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    
    viewControllers=[[[viewControllers reverseObjectEnumerator] allObjects] mutableCopy];

    YSLContainerViewController *containerVC = [[YSLContainerViewController alloc]initWithControllers:viewControllers
                                                                                        topBarHeight:statusHeight + navigationHeight
                                                                                parentViewController:self];
    containerVC.delegate = self;
    containerVC.menuItemFont = [UIFont fontWithName:@"Arial" size:16];
    
    [self.view addSubview:containerVC.view];
}

-(IBAction)cartButtonAction:(id)sender
{
	//BUSINESS ID SHOULD NOT BE NIL ON THIS SCREEN
	if (!self.selectedBussinedId ||  [[appGlobalData getBussinessCart:self.selectedBussinedId] count] == 0)
	{
		[[[[iToast makeText:NSLocalizedString(@"NoItemInCart", @"NoItemInCart")] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
		return;
	}

    ShoppingCartSummaryVC *summaryVC = [self.storyboard instantiateViewControllerWithIdentifier:kShoppingCartSummaryVC_SB_ID];

    summaryVC.isFromCart = YES;
	summaryVC.businessId = self.selectedBussinedId;
	summaryVC.selectedBussiness = self.selectedBussiness;
    summaryVC.isFromDealCouponScreen = self.isFromDealCouponScreen; //SanC
    [self.navigationController.navigationBar setBarTintColor:[UIColor redColor]];
    [self.navigationController pushViewController:summaryVC animated:YES];

}


- (IBAction)onClickAddtoCartButton:(id)sender
{
    CategoryForMealListVC *vc = [viewControllers objectAtIndex:currentIndex];
    BOOL isValid = [vc isValidData];
    if(isValid)
    {
        NSLog(@"is valid");
    }
    else
    {
        NSLog(@"invalid data");
    }
}
@end
