//
//  CategoryForMealListVC.h
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "WSCategoryMealResponse.h"
@class WSCouponsResponse;


@interface CategoryForMealListVC : BaseViewController <UITableViewDataSource, UITableViewDelegate, ServerResponseDelegate>


@property (weak, nonatomic) IBOutlet UITableView *mealTableView;
@property (nonatomic, strong) WSCategoryMealResponse *categoryMealResponse;
@property (nonatomic, strong) NSMutableArray *listOfSubBussinessItems;

@property (nonatomic, strong) NSString *businessId,*phone;

@property (nonatomic, assign) NSInteger currentIndex;

@property (strong, nonatomic) WSCouponsResponse *dealCouponResponse;

-(BOOL)isValidData;

@end
