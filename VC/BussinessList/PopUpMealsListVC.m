//
//  PopUpMealsListVC.m
//  VIPCard
//
//  Created by Vishal Kolhe on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
#define CheckedImage    @"checkbox_clicked"
#define UnCheckImage    @"checkbox"


#import "PopUpMealsListVC.h"
#import "CatForMenuDataCell.h"

@interface PopUpMealsListVC ()
{
    NSArray *listOfMeals;
    
    NSMutableSet *listOfItems;
    BOOL maelOrExtra;
	int price;
}
@end

@implementation PopUpMealsListVC
@synthesize extraForMealsObj;

-(void)viewDidLoad
{
    [super viewDidLoad];

	[self showBackButton:YES];
	[super viewDidLoad];



	listOfMeals = extraForMealsObj.extraMealsList;
	maelOrExtra = [extraForMealsObj.maelOrExtra boolValue];

    if(!maelOrExtra)
        _btnSubmit.hidden = YES;
	else
		_btnSubmit.hidden=NO;
    
    [self showBackButton:YES];
    listOfItems = [NSMutableSet set];
    



    
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(onClickCancel)];
//    [self.navigationItem setLeftBarButtonItem:backButton];

}
-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];



}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

	CGFloat height = 42;

	float h = height * [listOfMeals count];
	if(h > 410.0)
	{
		self.tableView.frame = CGRectMake(0, 50, self.tableView.frame.size.width, 410);
	}
	else
	{
		self.tableView.frame = CGRectMake(0, 50, self.tableView.frame.size.width, h);
	}
	NSLog(@"h:%f", h);

	if(maelOrExtra)
	{
		if(extraForMealsObj.selectedExtraMealsResponseList != nil)
		{
			for (id item in extraForMealsObj.selectedExtraMealsResponseList) {

				[listOfItems addObject:item];
			}

		}

	}
	else {
		if (extraForMealsObj.selectedExtraMealsResponse != nil)
		{
			[listOfItems addObject:extraForMealsObj.selectedExtraMealsResponse];
		}
	}

	[self.tableView reloadData];
}

-(IBAction)ActionBack:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:^{

	}];
}
-(void)onClickCancel
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listOfMeals.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // To "clear" the footer view
    return [UIView new] ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CatForMenuDataCell";
	tableView.separatorColor=[UIColor clearColor];
    CatForMenuDataCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell.imgCheckBox setHidden:!maelOrExtra];
    
    WSExtraMealsResponse *extraMeals = [listOfMeals objectAtIndex:indexPath.row];
    
    if(maelOrExtra)
    {
        if([listOfItems containsObject:extraMeals])
        {
            [cell.imgCheckBox setImage:[UIImage imageNamed:CheckedImage]];

        }
        else
        {
            [cell.imgCheckBox setImage:[UIImage imageNamed:UnCheckImage]];
        }
    }
    	[cell populateData:extraMeals Forprice:[extraForMealsObj.price intValue]];
	
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    WSExtraMealsResponse *mealResponse = [listOfMeals objectAtIndex:indexPath.row];
   ;
//    int pric=[mealResponse.price intValue]+[extraForMealsObj.price intValue];
//    mealResponse.price= [NSString stringWithFormat:@"%d",pric];
;
    [self submitData:mealResponse];
}

- (IBAction)onClickSubmitButton:(id)sender
{
    if(listOfItems.allObjects.count)
    {
        [self.delegate selectedListofItems:listOfItems.allObjects];
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}

-(void)submitData:(WSExtraMealsResponse *)aData
{
    if(maelOrExtra)
    {
        if([listOfItems containsObject:aData])
        {
            [listOfItems removeObject:aData];
        }
        else
        {
            [listOfItems addObject:aData];
        }
        
        [self.tableView reloadData];
    }
    else
    {
        [self.delegate selectedItem:aData];
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}

@end
