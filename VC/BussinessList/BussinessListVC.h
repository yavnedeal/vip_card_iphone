//
//  BussinessListVC.h
//  VIPCard
//
//  Created by Vishal Kolhe on 27/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "WSBussinessObjResponse.h"
#import "SWRevealViewController.h"

@interface BussinessListVC : BaseViewController
{
    NSDictionary *dict_data,*mainCopy;
    NSArray *array_SectionTitles,*array_Copytitle;
    NSMutableArray *array_IndexTitle1,*array_IndexTitle1Copy;
    NSArray *array_IndexTitles;
    NSMutableArray *array_category;
    NSString *str_tableTitle;


    

}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) WSBussinessObjResponse *bussinessObjResponse;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_searchbarheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_searchbar;
@property (nonatomic, assign)BOOL isMarketPlace;
@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet UITextField *txtFldSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel, *sidebarbutton;
@property (weak, nonatomic) IBOutlet UIButton *searchbtn;
-(void)onClickCancelButton;
-(void)searchCouponsby:(NSString *)aStr;
@property (assign) BOOL isSearch;
@property (nonatomic, strong) NSArray *filteredList;
- (IBAction)onClickCancelButton:(id)sender;

-(void) reloadScreenData;

@end
