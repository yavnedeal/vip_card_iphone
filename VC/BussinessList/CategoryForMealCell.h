//
//  CategoryForMealCell.h
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "WSMealsResponse.h"

@interface CategoryForMealCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mealImg;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

-(void)populateData:(WSMealsResponse *)aData;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_lblNameConstraints;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_lblDecConstraints;

@end
