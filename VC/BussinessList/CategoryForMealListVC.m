//
//  CategoryForMealListVC.m
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//


#define CELL_HEIGHT 101

#import "CategoryForMealListVC.h"
#import "CategoryForMealCell.h"
#import "WSMealsResponse.h"
#import "CategoryForMealDetailVC.h"
#import <sys/utsname.h>
#import "WSCouponsResponse.h"

@interface CategoryForMealListVC ()<UIAlertViewDelegate>
{
    NSMutableArray *viewControllers;
    NSArray *mealList;
    AppDelegate *appdelobj;
}

@end

@implementation CategoryForMealListVC
@synthesize categoryMealResponse,listOfSubBussinessItems,currentIndex,phone;



-(void)viewDidLoad
{
    [super viewDidLoad];
        [self showBackButton:YES];
    [self.mealTableView setDataSource:self];
    [self.mealTableView setDelegate:self];
    mealList = self.categoryMealResponse.mealsList;
    
    appdelobj = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    //added by Sarika [04-08-2016]
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(markBzOrderCall) name:@"callClickNotification" object:nil];
}

-(void)  viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor redColor]];
    
    if (![NSString isEmpty:phone])
    {
        
        UIImage* image3 = [UIImage imageNamed:@"Call_icon"];
        CGRect frameimg = CGRectMake(200,5, 26,26);
        
        UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
        [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
        [someButton addTarget:self action:@selector(makecall)
             forControlEvents:UIControlEventTouchUpInside];
        [someButton setShowsTouchWhenHighlighted:YES];
        
        UIBarButtonItem *callbtn =[[UIBarButtonItem alloc] initWithCustomView:someButton];
        
        
        self.navigationController.navigationBar.topItem.rightBarButtonItem =callbtn;
    }
    
}



-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
    
    
    [self.mealTableView reloadData];

    
    [self updateCurrentIndexPosition];
    
    
    
	
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //NSLog(@"index: %ld",buttonIndex);
}
-(void)makecall
{
    //NSString *phNo = @"+919800000000";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    //[self markBzOrderCall];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
        appdelobj.callflag = YES;
        //[self markBzOrderCall];
        
    } else
    {
        [[[[iToast makeText:NSLocalizedString(@"Call facility not available", @"")] setGravity:iToastGravityBottom] setDuration:iToastDurationShort] show];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"callClickNotification" object:nil];
}
// added by Sarika 03/08/2016
-(void)markBzOrderCall
{
    NSString *deviceOS = [[UIDevice currentDevice] systemVersion];
    NSString *deviceName = [self deviceName];
    
    NSString *deviceInfo = [NSString stringWithFormat:@"%@, OS: %@",deviceOS,deviceName];
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjects:@[self.businessId,deviceInfo] forKeys:@[@"idBusiness",@"deviceInfo"]];
    [[ConnectionsManager sharedManager] markBusinessCallOrder_withdelegate:dic delegate:self];
    NSLog(@"markBzOrderCall....");
}
-(NSString*)deviceName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

-(void) updateCurrentIndexPosition {
    
    if(self.listOfSubBussinessItems && self.currentIndex < [self.listOfSubBussinessItems count] && self.currentIndex >=0 )
    {
        WSCategoryMealResponse *categoryMeals = [self.listOfSubBussinessItems objectAtIndex:self.currentIndex];
        
        if([categoryMeals.mealsList count] > 0){
            
            NSIndexPath *ip = [NSIndexPath indexPathForRow:0 inSection:self.currentIndex];
            [_mealTableView scrollToRowAtIndexPath:ip
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:NO];
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:YES];
	[self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    WSCategoryMealResponse *categoryMeals = [self.listOfSubBussinessItems objectAtIndex:section];

    return [categoryMeals.mealsList count];
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _mealTableView.frame.size.width, 50)];
    headerView.backgroundColor = [UIColor lightGrayColor];
    WSCategoryMealResponse *categoryMeals = [self.listOfSubBussinessItems objectAtIndex:section];
    UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0,0, _mealTableView.frame.size.width-10, 30)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.text = [NSString stringWithFormat:@"%@",categoryMeals.name];
    label.textAlignment = NSTextAlignmentRight;
    label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    
   // [label.layer addSublayer:bottomBorder];
    
    [headerView addSubview:label];
    
    return headerView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return (self.listOfSubBussinessItems)?[self.listOfSubBussinessItems count]:0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // To "clear" the footer view
    return [UIView new] ;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CategoryForMealCell";
    CategoryForMealCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
    WSCategoryMealResponse *categoryMeals = [self.listOfSubBussinessItems objectAtIndex:indexPath.section];
    WSMealsResponse *mealObj = [categoryMeals.mealsList objectAtIndex:indexPath.row];
    
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0,0, _mealTableView.frame.size.width,1);
    topBorder.backgroundColor = [UIColor redColor].CGColor;
    [cell.layer addSublayer:topBorder];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0, 30, _mealTableView.frame.size.width,1);
    bottomBorder.backgroundColor = [UIColor redColor].CGColor;

    
    
    [cell populateData:mealObj];
   
   
    
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WSCategoryMealResponse *categoryMeals = [self.listOfSubBussinessItems objectAtIndex:indexPath.section];
    WSMealsResponse *mealObj = [categoryMeals.mealsList objectAtIndex:indexPath.row];
    
    [self moveToDetailPage:mealObj withBussinessId:categoryMealResponse.idBusiness];
    
}

-(void) moveToDetailPage:(WSMealsResponse *) mealObj withBussinessId:(NSString *) idBusiness
{
    CategoryForMealDetailVC *catMealDetailVC = [self getVCWithSB_ID:kCategoryForMealDetailVC_SB_ID];
    //Fixed issue : when same item added to cart twice
    catMealDetailVC.mealResponse = [mealObj copyObject];
    catMealDetailVC.businessId = idBusiness;
    
    [self.navigationController pushViewController:catMealDetailVC animated:YES];
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //CategoryForMealCell *cell = (CategoryForMealCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    WSCategoryMealResponse *categoryMeals = [self.listOfSubBussinessItems objectAtIndex:indexPath.section];
    WSMealsResponse *mealObj = [categoryMeals.mealsList objectAtIndex:indexPath.row];
    
    CGFloat w = CGRectGetWidth(tableView.frame);
    
    CGSize toName = [mealObj.name sizeWithFont:helveticaFont(17.0) constrainedToSize:CGSizeMake(w - 170, NSIntegerMax) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize toDes = [mealObj.description sizeWithFont:helveticaLightFont1(16.0) constrainedToSize:CGSizeMake(w - 145, NSIntegerMax) lineBreakMode:NSLineBreakByWordWrapping];

    //CGSize toDes = [self getStringSize:cell.lblDesc andString:cell.lblDesc.text];
   // CGSize toName = [self getStringSize:cell.lblName andString:cell.lblName.text];
    
    float hName;
    float hDes;
    
    
    if(toName.height > 21)
        hName = toName.height;
    else
        hName = 21;
    
    if(toDes.height > 20)
        hDes = toDes.height;
    else
        hDes = 20;
    
    
    if ((hDes + hName) > 80)
    {
        return (hDes + hName) + 20;
    }
    
    return CELL_HEIGHT;
}




-(BOOL)isValidData
{
    
    return YES;
}

//ServerResponseDelegate
-(void)success:(WSBaseResponse *)response
{
    if ([response.method isEqualToString:mMarkBusinessCallOrder])
    {
        NSLog(@"response: %@",response);
    }
}

-(void)failure:(WSBaseResponse *)response
{
    if([response.method isEqualToString:mFetchCatMealsForBussiness])
    {
        
    }
}

@end
