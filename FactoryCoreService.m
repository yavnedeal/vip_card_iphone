//
//  FactoryCoreService.m
//  VIPCard
//
//  Created by Manoj Kumar on 11/8/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "FactoryCoreService.h"
#import "Fcatorycd.h"

@implementation FactoryCoreService

static FactoryCoreService *_sharedInstance;

+(FactoryCoreService *) sharedInstance:(id)vc
{
    if(_sharedInstance != nil)
        return _sharedInstance;
    
    _sharedInstance = [[FactoryCoreService alloc] initWithDelegate:vc];
    return _sharedInstance;
}

-(BOOL)saveFactoryInDB:(FactoryListData *) myOrder withError:(CustomError **) error
{
    BOOL result = YES;
    
    Fcatorycd *oldobj;
    
    Fcatorycd *obj = [self fetchFactoryBy:myOrder.id];
    if(obj && obj != nil)
    {
        oldobj = obj;
    }
    else
    {
        oldobj = (Fcatorycd *) [NSEntityDescription insertNewObjectForEntityForName:kTableFactoryCD  inManagedObjectContext:moContext];
    }
    
    oldobj = [myOrder populateCoreDataFromObject:oldobj];
    
    if (![moContext save:error])
    {
        result = NO;
    }
    result = YES;
    
    return result;
}

-(NSArray*)fetchFactoryList
{
    id result = [[CoreDataBase sharedInstanse] getListFromDB:kTableFactoryCD withPredicate:nil];
    NSArray *arr = (NSArray*)result;
    
    return arr;
}

-(void)deleteFactoryby:(NSString *)factoryId
{
    NSFetchRequest *data = [[NSFetchRequest alloc] init];
    [data setEntity:[NSEntityDescription entityForName:kTableFactoryCD inManagedObjectContext:moContext]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(factoryID = %@)", factoryId];
    [data setPredicate:predicate];
    NSError *error = nil;
    NSArray *contextArr = [moContext executeFetchRequest:data error:&error];
    //error handling goes here
    for (NSManagedObject *object in contextArr) {
        [moContext deleteObject:object];
    }
    
    NSError *saveError = nil;
    [moContext save:&saveError];
}

-(Fcatorycd *)fetchFactoryBy:(NSString *)factoryID
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(factoryID = %@)", factoryID];
    id result = [[CoreDataBase sharedInstanse] getListFromDB:kTableFactoryCD withPredicate:predicate];
    NSArray *arr = (NSArray*)result;
    if(arr.count)
    {
        return [arr firstObject];
    }
    return nil;
}

@end
