//
//  Fcatorycd+CoreDataProperties.h
//  VIPCard
//
//  Created by Syntel-Amargoal1 on 11/9/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Fcatorycd.h"

NS_ASSUME_NONNULL_BEGIN

@interface Fcatorycd (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *factoryID;
@property (nullable, nonatomic, retain) NSString *name;

@end

NS_ASSUME_NONNULL_END
