//
//  FactoryInfoContex.m
//  VIPCard
//
//  Created by Manoj Kumar on 11/9/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "FactoryInfoContex.h"
#import "DateTimeUtil.h"
@implementation FactoryInfoContex
@synthesize paidByFactory, factoryid, deliveryOrderTime;

+ (id)sharedManager {
    static FactoryInfoContex *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        
        self.paidByFactory = @"";
        self.factoryid = @"";
        self.deliveryOrderTime = @"";
        
        self.lateOrderStartDate = [DateTimeUtil getDateForHours:12];
        self.lateOrderEndDate = [DateTimeUtil getDateForHours:18];
    }
    return self;
}

@end
