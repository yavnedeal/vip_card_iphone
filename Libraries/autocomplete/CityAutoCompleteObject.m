//
//  DEMOCustomAutoCompleteObject.m
//  MLPAutoCompleteDemo
//
//  Created by Eddy Borja on 4/19/13.
//  Copyright (c) 2013 Mainloop. All rights reserved.
//

#import "CityAutoCompleteObject.h"

@interface CityAutoCompleteObject ()

@end

@implementation CityAutoCompleteObject


- (id)initWithCityName:(NSString *)name andId:(NSString *)cityId
{
    self = [super init];
    if (self) {
        [self setCityName:name];
        [self setCityId:cityId];
    }
    return self;
}

#pragma mark - MLPAutoCompletionObject Protocl

- (NSString *)autocompleteString
{
    return self.cityName;
}

@end
