//
//  StreetAutoCompleteObject.m
//  rosinter
//
//  Created by Yogesh Bhamre on 09/10/14.
//  Copyright (c) 2014 Enovate. All rights reserved.
//

#import "StreetAutoCompleteObject.h"

@implementation StreetAutoCompleteObject
- (id)initWithStreetName:(NSString *)name andId:(NSString *)streetId
{
    self = [super init];
    if (self) {
        [self setStreetName:name];
        [self setStreetId:streetId];
    }
    return self;
}

#pragma mark - MLPAutoCompletionObject Protocl

- (NSString *)autocompleteString
{
    return self.streetName;
}
@end
