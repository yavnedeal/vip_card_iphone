//
//  FactoryCoreService.h
//  VIPCard
//
//  Created by Manoj Kumar on 11/8/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseCoreService.h"
#import "FactoryListData.h"

@interface FactoryCoreService : BaseCoreService
+(FactoryCoreService *) sharedInstance:(id)vc;
-(BOOL)saveFactoryInDB:(FactoryListData *) myOrder withError:(CustomError **) error;
-(NSArray*)fetchFactoryList;
-(void)deleteFactoryby:(NSString *)factoryId;
@end
