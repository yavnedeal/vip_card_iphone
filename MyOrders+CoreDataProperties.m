//
//  MyOrders+CoreDataProperties.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 17/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MyOrders+CoreDataProperties.h"

@implementation MyOrders (CoreDataProperties)

@dynamic orderId;
@dynamic orderDate;
@dynamic orderSum;
@dynamic logoURL;
@dynamic orderObj;
@dynamic cartData;

@end
