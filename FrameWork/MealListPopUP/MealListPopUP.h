//
//  MealListPopUP.h
//  VIPCard
//
//  Created by SanC on 09/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSExtraForMealsResponse.h"
#import "CatForMenuDataCell.h"

@protocol MealListPopUPDelegate <NSObject>

-(void)selectedItem:(id)item;
-(void)selectedListofItems:(NSArray *)aList;

@end

@interface MealListPopUP : UIView
{
	NSArray *listOfMeals;
	NSMutableSet *listOfItems;
	BOOL maelOrExtra;
	int price;
	UITapGestureRecognizer *gesture;
	__weak IBOutlet UIButton *btn_Submit;
	UIButton *btnSubmit;
	__weak IBOutlet UIView *internalView;

}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) id<MealListPopUPDelegate> delegate;
@property (nonatomic, strong) WSExtraForMealsResponse *extraForMealsObj;

- (IBAction)onClick_SubmitData:(id)sender;
-(void)refreshMealList;


@end
