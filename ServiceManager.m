//
//  ServiceManager.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 17/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "ServiceManager.h"

@implementation ServiceManager

static ServiceManager *_sharedInstance;

+(ServiceManager *) sharedInstance:(id)vc
{
    if(_sharedInstance != nil)
        return _sharedInstance;
    
    _sharedInstance = [[ServiceManager alloc] initWithDelegate:vc];
    return _sharedInstance;
    
}

-(BOOL)saveMyOrdersInDB:(WSOrderData *) myOrder withError:(CustomError **) error
{
    //Perform Offline
    BOOL result = YES;
    
    
    MyOrders *myOrderObj;
    
        myOrderObj = (MyOrders *) [NSEntityDescription insertNewObjectForEntityForName:kTableMyOrders  inManagedObjectContext:moContext];
    
        
        myOrderObj = [myOrder populateCoreDataFromObject:myOrderObj];
        
        if (![moContext save:error])
        {
            result = NO;
        }
        result = YES;
    
    return result;
}
-(NSArray*)fetchMyOrders
{
    id result = [[CoreDataBase sharedInstanse] getListFromDB:kTableMyOrders withPredicate:nil];
    NSArray *arr = (NSArray*)result;
    
    return arr;
}
-(void)deleteOrder:(NSString *)orderId
{
    NSFetchRequest *data = [[NSFetchRequest alloc] init];
    [data setEntity:[NSEntityDescription entityForName:kTableMyOrders inManagedObjectContext:moContext]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(orderId = %@)", orderId];
    [data setPredicate:predicate];
    NSError *error = nil;
    NSArray *contextArr = [moContext executeFetchRequest:data error:&error];
    //error handling goes here
    for (NSManagedObject *object in contextArr) {
        [moContext deleteObject:object];
    }
    
    NSError *saveError = nil;
    [moContext save:&saveError];
}

@end
