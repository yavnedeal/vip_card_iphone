//
//  ServiceManager.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 17/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseCoreService.h"
#import "WSOrderData.h"

@interface ServiceManager : BaseCoreService
{
    
}
+(ServiceManager *) sharedInstance:(id)vc;
-(BOOL)saveMyOrdersInDB:(WSOrderData *) myOrder withError:(CustomError **) error;
-(NSArray*)fetchMyOrders;
-(void)deleteOrder:(NSString *)orderId;
@end
