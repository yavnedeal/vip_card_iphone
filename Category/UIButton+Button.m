//
//  UIButton+Button.m
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "UIButton+Button.h"

@implementation UIButton (Button)

-(UIButton*)buttonCornerRadius:(UIButton*)button toRadius:(NSInteger)radius
{
    UIButton *tempButton = button;
    
    tempButton.layer.cornerRadius = radius;
    tempButton.layer.masksToBounds = YES;
    
    return tempButton;
}

-(UIButton*)buttonBorder:(UIButton*)button toBorderSize:(NSInteger)borderSize toBorderColor:(UIColor*)borderColor
{
    UIButton *tempButton = button;
    
    tempButton.layer.borderColor = borderColor.CGColor;
    tempButton.layer.borderWidth = borderSize;    
    
    return tempButton;
}

@end
