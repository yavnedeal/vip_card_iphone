//
//  UITableView+EmptyFooter.m
//  VIPCard
//
//  Created by Andrei on 31/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "UITableView+EmptyFooter.h"

@implementation UITableView (EmptyFooter)
-(void) customizeTableView
{
    self.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if(IS_OS_7_OR_LATER)
    {
        self.separatorInset = UIEdgeInsetsZero;
    }
    self.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    self.separatorColor = [UIColor grayColor];
}
@end
