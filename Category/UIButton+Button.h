//
//  UIButton+Button.h
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Button)

-(UIButton*)buttonCornerRadius:(UIButton*)button toRadius:(NSInteger)radius;
-(UIButton*)buttonBorder:(UIButton*)button toBorderSize:(NSInteger)borderSize toBorderColor:(UIColor*)borderColor;
@end
