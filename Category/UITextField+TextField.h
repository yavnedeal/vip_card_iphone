//
//  UITextField+TextField.h
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (TextField)

-(UITextField*)makeBorderTextView:(UITextField*)textField toColor:(UIColor*)color toWidth:(NSInteger)width;
-(UITextField*)addPaddingRight:(UITextField*)textField;
-(UITextField*)addPaddingLeft:(UITextField*)textField;
@end
