//
//  NSString+LocalizeString.m
//  Gold Chain
//
//  Created by Andrei Boulgakov on 04/11/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "AppGlobalConstant.h"
#import "NSString+LocalizeString.h"

@implementation NSString (LocalizeString)

+ (NSString*)languageSelectedStringForKey:(NSString *)key
{
   /* NSString *path;
    if([NSString isHebrewLang:[NSString getCurrentSavedLanguage]])  //[[NSString getCurrentSavedLanguage] isEqualToString:HEBREW_LANGUAGE]) {
    {
        path = [[NSBundle mainBundle] pathForResource:@"he" ofType:@"lproj"];
    }
    else if([[NSString getCurrentSavedLanguage] isEqualToString:RUSSIAN_LANGUAGE]) {
        
        path = [[NSBundle mainBundle] pathForResource:@"ru" ofType:@"lproj"];
    }
    else {
        
        path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
    }*/
    
    //NSBundle *languageBundle = [NSBundle bundleWithPath:path];
    //NSString *str = [languageBundle localizedStringForKey:key value:@"" table:nil];
    
    return key;
}

+ (NSString *)getCurrentSavedLanguage {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *languageStr = [userDefaults objectForKey:APPLICATION_DEFAULT_LANGUAGE];
    
    if(languageStr && languageStr !=nil) {
        
        return languageStr;
    }
    
    return nil;
}

+(BOOL)isHebrewLang:(NSString *)aLang
{
    if([aLang rangeOfString:HEBREW_LANGUAGE].location != NSNotFound)
    {
        return YES;
    }
    return NO;
}



@end