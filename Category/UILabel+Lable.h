//
//  UILabel+Lable.h
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Lable)
-(UILabel*)setBottomBorder:(UILabel*)lable;
@end
