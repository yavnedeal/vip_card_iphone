//
//  UIImageView+ImageView.h
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (ImageView)
-(UIImageView*)makeBorderImageView:(UIImageView*)imageView toColor:(UIColor*)color toWidth:(NSInteger)width;
-(UIImageView*)imageViewCornerRadius:(UIImageView*)imageView toRadius:(NSInteger)radius;
@end
