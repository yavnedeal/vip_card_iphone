//
//  ChoiceListForMeals.m
//  VIPCard
//
//  Created by SanC on 10/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "ChoiceListForMeals.h"
#import "WSExtraForMealsResponse.h"

@interface ChoiceListForMeals ()

@end

@implementation ChoiceListForMeals
@synthesize array_choice,str_choice;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	NSLog(@"array_choice%@",array_choice);
	NSLog(@"array_choice%@",str_choice);
	array_ChoiceName = [self.array_choice filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isChoice = %@", str_choice]];
	array_ChoiceUniqeName=[array_ChoiceName valueForKeyPath:@"@distinctUnionOfObjects.nameChoice"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return array_ChoiceUniqeName.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	// To "clear" the footer view
	return [UIView new] ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"] ;

		cell.selectionStyle = UITableViewCellSelectionStyleNone;

	}
	WSExtraForMealsResponse *response=[array_ChoiceUniqeName objectAtIndex:indexPath.row];
	cell.textLabel.text=response.nameChoice;
	return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}



@end
