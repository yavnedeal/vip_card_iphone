//
//  ImageCollectionViewCell.h
//  VIPCard
//
//  Created by SanC on 13/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "WSConstants.h"
#import "AppGlobalConstant.h"

@interface ImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
-(void)populatedData:(NSString*)data;
@property (strong, nonatomic) IBOutlet UIView *seperatorLineView;

@end
