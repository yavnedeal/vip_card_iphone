//
//  MenuCellTableViewCell.h
//  VIPCard
//
//  Created by Andrei on 31/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgMenu;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@end
