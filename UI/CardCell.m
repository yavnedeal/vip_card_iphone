//
//  CardCell.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 02/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "CardCell.h"
#import "NSString+CommonForApp.h"
#import "DateTimeUtil.h"
#import "iToast.h"
#import "FTPopOverMenu.h"



/*
 
 //Manoj
 "factoryListTitle" = "אביﬠובד מפﬠל";
 "factoryList_Placeholder"   =   "בהרמפּﬠל";
 
 "company_pay"   =   "תשלום ﬠ\"י החברה";
 "company_pay_sub_title" =   "(הזמנה תשלח לﬠסק לאחר אישור המבּﬠל)";
 
 "setting_time_title"  =   "סמך להזמנה ﬠתידיח";
 "setting_time_placeholder" =    "בחר שﬠה";
 
 
 [btnchk setBackgroundImage:[UIImage imageNamed:@"uncheck"] forState:UIControlStateNormal];
 }else{
 restpay = YES;
 restCashAmt = [self remainingAmoutSkipLastCard];
 if (![self validateFields]) {
 float orderAmt = [orderTotal floatValue];
 restCashAmt = orderAmt;
 
 cell.txtsumtopay.text = @"0";
 lblchk.text = [NSString stringWithFormat:@"%@%.02f-סמן לתשלום יתרה במזומן",SHEKEL,orderAmt];
 }
 */
//[btnchk setBackgroundImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];


@implementation CardCell
@synthesize lblcardno,lblcardtype,lblexpdate,lblname,lblownid,lblsumtopay,lblcurency,pickcardtype,pickview,MPickView,MPicker,YPicker,YPickView,lblcardcvv;

static NSMutableArray *arrcardtypes;
static NSMutableArray *arrYears;
static NSMutableArray *arrMonths;
static NSMutableArray *arrNum;


- (void)awakeFromNib {
    // Initialization code
    
    [super awakeFromNib];
    
    UIColor *color = [UIColor redColor]; // select needed color
    NSString *string = @"*"; // the string to colorize
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:string attributes:attrs];
    
    
    
    lblname.text =  [NSString stringWithFormat:@"* %@", NSLocalizedString(@"Full name of the card holder", nil)];
    lblownid.text = [NSString stringWithFormat:@"* %@", NSLocalizedString(@"ID of the card owner", nil)];
    lblcardtype.text = [NSString stringWithFormat:@"* %@", NSLocalizedString(@"Card type", nil)];
    lblcardno.text = [NSString stringWithFormat:@"* %@", NSLocalizedString(@"Ticket Number", nil)];
    lblexpdate.text = [NSString stringWithFormat:@"* %@", NSLocalizedString(@"In effect until", nil)];
    lblsumtopay.text = [NSString stringWithFormat:@"* %@", NSLocalizedString(@"Billing Amount", nil)];
    lblcardcvv.text = [NSString stringWithFormat:@"* %@", NSLocalizedString(@"CVV", nil)];
    //lblownid.text = NSLocalizedString(@"Click here for more card payment split", nil);
    lblcurency.text = NSLocalizedString(@"shekel", nil);
    
    
    
    
    
    [self updateWithAttributeString:lblname color:[UIColor redColor] text:NSLocalizedString(@"Full name of the card holder", nil) secondText:@"*"];
    [self updateWithAttributeString:lblownid color:[UIColor redColor] text:NSLocalizedString(@"ID of the card owner", nil) secondText:@"*"];
    [self updateWithAttributeString:lblcardtype color:[UIColor redColor] text:NSLocalizedString(@"Card type", nil) secondText:@"*"];
    [self updateWithAttributeString:lblcardno color:[UIColor redColor] text:NSLocalizedString(@"Ticket Number", nil) secondText:@"*"];
    [self updateWithAttributeString:lblexpdate color:[UIColor redColor] text:NSLocalizedString(@"In effect until", nil) secondText:@"*"];
    [self updateWithAttributeString:lblsumtopay color:[UIColor redColor] text:NSLocalizedString(@"Billing Amount", nil) secondText:@"*"];
    
    pickview.layer.cornerRadius = 0.2;
    
    MPickView.layer.cornerRadius = 0.2;
    YPickView.layer.cornerRadius = 0.2;
    /**
     
     */
    
    arrcardtypes = [[NSMutableArray alloc]init];
    //[arrcardtypes addObject:@"לבחור חברת כרטיסי"];
    [arrcardtypes addObject:@"סוג כרטיס"];
    [arrcardtypes addObject:@"מסטר קארד"];
    [arrcardtypes addObject:@"אמריקן אקספרס"];
    [arrcardtypes addObject:@"ויזה"];
    [arrcardtypes addObject:@"ישראכרט"];
    [arrcardtypes addObject:@"דיינרס"];
    [arrcardtypes addObject:@"לאומי קארד"];
    
    arrYears = [DateTimeUtil getYears:[NSDate date]];
    [arrYears insertObject:@"בחר שנה" atIndex:0];
    NSDictionary *dicM = [DateTimeUtil getMonths];
    arrMonths = [[NSMutableArray alloc]init];
    [arrMonths addObject:@"בחר חודש"];
    [arrMonths addObjectsFromArray:[dicM objectForKey:@"arrM"]];
    arrNum = [[NSMutableArray alloc]init];
    [arrNum addObject:@"0"];
    [arrNum addObjectsFromArray:[dicM objectForKey:@"arrNum"]];
    
    UITapGestureRecognizer *tapGesutre= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onCellTap:)];
    tapGesutre.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tapGesutre];
    
    //Add gesture
    [self addPickerGesture];
    
    
    
    
    
    //    self.constraint_PC_Height.constant = 0.0f;
    //    self.constraint_ST_Height.constant = 0.0f;
    
    //[self.btnSTCheckBox setHidden:YES];
    
    

    
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return true;
}

-(void) addPickerGesture{
    UITapGestureRecognizer* gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized2:)];
    gestureRecognizer.cancelsTouchesInView = NO;
    gestureRecognizer.delegate = self;
    [self.MPicker addGestureRecognizer:gestureRecognizer];
    [self.MPicker setShowsSelectionIndicator:YES];
    
    gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
    gestureRecognizer.cancelsTouchesInView = NO;
    gestureRecognizer.delegate = self;
    [self.YPicker addGestureRecognizer:gestureRecognizer];
    [self.YPicker setShowsSelectionIndicator:YES];
}

- (void)pickerViewTapGestureRecognized:(UITapGestureRecognizer*)gestureRecognizer
{
    CGPoint touchPoint = [gestureRecognizer locationInView:gestureRecognizer.view.superview];
    CGRect frame = self.YPicker.frame;
    CGRect selectorFrame = CGRectInset( frame, 0.0, self.YPicker.bounds.size.height * 0.85 / 2.0 );
    if( CGRectContainsPoint( selectorFrame, touchPoint) )
    {
        NSInteger selectedIndex = [self.YPicker selectedRowInComponent:0];
        if(selectedIndex > 0)
        {
            [self pickerView:self.MPicker didSelectRow:selectedIndex inComponent:0];
            [self hideAllPickerView];
        }
    }
    
}

- (void)pickerViewTapGestureRecognized2:(UITapGestureRecognizer*)gestureRecognizer
{
    CGPoint touchPoint = [gestureRecognizer locationInView:gestureRecognizer.view.superview];
    CGRect frame = self.MPicker.frame;
    CGRect selectorFrame = CGRectInset( frame, 0.0, self.MPicker.bounds.size.height * 0.85 / 2.0 );
    if( CGRectContainsPoint( selectorFrame, touchPoint) )
    {
        NSInteger selectedIndex =   [self.MPicker selectedRowInComponent:0];
        if(selectedIndex > 0)
        {
            [self pickerView:self.MPicker didSelectRow:selectedIndex inComponent:0];
            [self hideAllPickerView];
        }
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



-(void) updateWithAttributeString:(UILabel *) lbl color:(UIColor *) color text:(NSString *)text secondText:(NSString *)newtext
{
    if(!text || [text isEmpty])
        return;
    
    NSString *str = lbl.text;
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableAttributedString *attributedText2=[[NSMutableAttributedString alloc] initWithString:str] ;
    
    //now set attributes
    [attributedText setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:lbl.font,NSFontAttributeName,nil] range:NSMakeRange(0, [str length])];
    [attributedText2 setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:lbl.font,NSFontAttributeName,nil] range:NSMakeRange(0, [str length])];
    NSRange range = [str rangeOfString:text];
    NSRange range2=[str rangeOfString:newtext];
    [attributedText setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName,nil] range:range];
    [attributedText2 setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:color,NSForegroundColorAttributeName,nil] range:range2];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:lbl.text];
    //[string addAttribute:NSForegroundColorAttributeName value:color range:range];
    [string addAttribute:NSForegroundColorAttributeName value:color range:range2];
    lbl.attributedText = string;
}
-(void)populateData:(CardData *)aData
{
    self.cellData = aData;
    self.txtownid.text = aData.ownid;
    self.txtname.text = aData.ownname;
    self.txtcardno.text = aData.cardno;
    self.txtsumtopay.text = aData.ordamt;
    self.txtcardcvv.text = aData.cvv;
    if (aData.cardtype.length > 0) {
        [self.btncardtype setTitle:aData.cardtype forState:UIControlStateNormal];
    }else{
        [self.btncardtype setTitle:[arrcardtypes objectAtIndex:0] forState:UIControlStateNormal];
    }
    if (aData.expmon.length > 0) {
        [self.btnmonth setTitle:aData.expmon forState:UIControlStateNormal];
    }else{
        [self.btnmonth setTitle:[arrMonths objectAtIndex:0] forState:UIControlStateNormal];
    }
    if (aData.expyr.length > 0) {
        [self.btnyear setTitle:[arrYears objectAtIndex:0] forState:UIControlStateNormal];
    }
    
    //Factory Screen based on Row Index
    
    
}

/*-(void) handleFactoryScreen{
    if(self.currentIndexPath == nil)
        return;
    
    NSInteger row = self.currentIndexPath.row;
    
    if (row == 0) {
        self.constraint_FE_Height.constant = 0;
        self.constraint_PC_Height.constant = 0;
        self.constraint_ST_Height.constant = 0;
        self.constraint_FE_YPOS.constant = 0;
        self.constraint_PC_Ypos.constant = 0;
        self.constraint_ST_Ypos.constant = 0;
        
    }
    else {
        
        self.constraint_FE_Height.constant = 0;
        self.constraint_PC_Height.constant = 0;
        self.constraint_ST_Height.constant = 0;
        self.constraint_FE_YPOS.constant = 0;
        self.constraint_PC_Ypos.constant = 0;
        self.constraint_ST_Ypos.constant = 0;
    }
    
    
}*/

-(void) onCellTap:(UIGestureRecognizer *) gesture{
    [self hideAllPickerView];
    [self endEditing:YES];
}



- (IBAction)showYearPicker:(UIButton *)sender {
    /*
     [self hideAllPickerView];
     [self.YPicker reloadAllComponents];
     [self.YPickView setHidden:NO];
     [self performDeletegate:FALSE];
     */
    [FTPopOverMenu showForSender:sender
                        withMenu:arrYears
     
                       doneBlock:^(NSInteger selectedIndex) {
                           if(selectedIndex > 0)
                           {
                               CardData *cardObj = self.cellData;
                               cardObj.expyr = [arrYears objectAtIndex:selectedIndex];
                               [self.btnyear setTitle:[arrYears objectAtIndex:selectedIndex] forState:UIControlStateNormal];
                               [self validateCardExpiry];
                           }
                           
                           
                       } dismissBlock:^{
                           
                       }];
}

- (IBAction)showMonthPicker:(UIButton *)sender {
    /* [self hideAllPickerView];
     [self.MPicker reloadAllComponents];
     [self.MPickView setHidden:NO];
     [self performDeletegate:FALSE];*/
    [FTPopOverMenu showForSender:sender
                        withMenu:arrMonths
     
                       doneBlock:^(NSInteger selectedIndex) {
                           if(selectedIndex > 0)
                           {
                               CardData *cardObj = self.cellData;
                               cardObj.expmon = [arrMonths objectAtIndex:selectedIndex];
                               [self.btnmonth setTitle:[arrMonths objectAtIndex:selectedIndex] forState:UIControlStateNormal];
                               [self validateCardExpiry];
                           }
                           
                           
                       } dismissBlock:^{
                           
                       }];
}

- (IBAction)showCardTypeView:(UIButton *)sender {
    [self hideAllPickerView];
    /*
     [self.pickcardtype reloadAllComponents];
     [self.pickview setHidden:NO];
     [self performDeletegate:FALSE];
     */
    
    [FTPopOverMenu showForSender:sender
                        withMenu:arrcardtypes
     
                       doneBlock:^(NSInteger selectedIndex) {
                           if(selectedIndex > 0)
                           {
                               CardData *cardObj = self.cellData;
                               cardObj.cardtype = [arrcardtypes objectAtIndex:selectedIndex];
                               [self.btncardtype setTitle:[arrcardtypes objectAtIndex:selectedIndex] forState:UIControlStateNormal];
                           }
                           
                           
                       } dismissBlock:^{
                           
                       }];
}



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (pickerView  == YPicker )
    {
        return arrYears.count;
    }
    else if(pickerView == MPicker)
    {
        return arrMonths.count;
    }
    else if(pickerView == pickcardtype)
    {
        return arrcardtypes.count;
    }
    
    return 0;
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view
{
    UILabel *view1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 30)];
    // [view1 setBackgroundColor:[UIColor lightGrayColor]];
    NSString * aText = @"";
    if (pickerView  == YPicker )
    {
        aText =  [arrYears objectAtIndex:row];
    }
    else if(pickerView == MPicker)
    {
        aText =  [arrMonths objectAtIndex:row];
    }
    else if(pickerView == pickcardtype)
    {
        aText =  [arrcardtypes objectAtIndex:row];
    }
    
    view1.text = aText;
    view1.textAlignment = NSTextAlignmentCenter;
    view1.font = [UIFont systemFontOfSize:17];
    view1.textColor = [UIColor blackColor];
    return view1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    //[selsender setTitle:[arrTmp objectAtIndex:row] forState:UIControlStateNormal];
    
    
    CardData *cardObj = self.cellData;
    if (pickerView == YPicker) {
        if(row == 0)
        {
            [pickerView selectRow:(row+1) inComponent:component animated:NO];
            return;
        }
        cardObj.expyr = [arrYears objectAtIndex:row];
        [self.btnyear setTitle:[arrYears objectAtIndex:row] forState:UIControlStateNormal];
    }
    if (pickerView == self.MPicker) {
        if(row == 0)
        {
            [pickerView selectRow:(row+1) inComponent:component animated:NO];
            return;
        }
        cardObj.expmon = [arrMonths objectAtIndex:row];
        [self.btnmonth setTitle:[arrMonths objectAtIndex:row] forState:UIControlStateNormal];
    }
    if (pickerView == self.pickcardtype) {
        cardObj.cardtype = [arrcardtypes objectAtIndex:row];
        [self.btncardtype setTitle:[arrcardtypes objectAtIndex:row] forState:UIControlStateNormal];
    }
    else{
        
    }
    if(pickerView == self.pickcardtype)
        [self hideAllPickerView] ;
}

-(void) validateCardExpiry{
    if (self.btnmonth.titleLabel.text.length > 0 && ![self.btnmonth.titleLabel.text isEqualToString:@"MM"] && ![self.btnmonth.titleLabel.text isEqualToString:@"בחר חודש"] && ![self.btnmonth.titleLabel.text isEqualToString:@"חוֹדֶשׁ"] && ![self.btnyear.titleLabel.text isEqualToString:@"שָׁנָה"] && ![self.btnyear.titleLabel.text isEqualToString:@"בחר שנה"])
    {
        
        NSDateFormatter *df = [[NSDateFormatter alloc]init];
        [df setDateFormat:@"MMYYYY"];
        
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        
        [components setDay:1];
        
        
        NSDate *currdt = [[NSCalendar currentCalendar] dateFromComponents:components];
        NSString *mon = [arrNum objectAtIndex:[arrMonths indexOfObject:self.btnmonth.titleLabel.text]];
        NSString *year = self.btnyear.titleLabel.text;
        
        [components setMonth:[mon integerValue]];
        [components setYear:[year integerValue]];
        
        NSDate *seldt = [[NSCalendar currentCalendar] dateFromComponents:components];
        
        if ([currdt compare:seldt] == NSOrderedDescending) {
            [[[[iToast makeText:@"אנא בחר תוקף חוקי"] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
            //[selsender setTitle:@"" forState:UIControlStateNormal];
        }
    }
}
-(void) performDeletegate:(BOOL) flag{
    
    if(self.cardCellDelegate && [self.cardCellDelegate respondsToSelector:@selector(cardCell:toggleScroll:)])
    {
        [self.cardCellDelegate cardCell:self toggleScroll:flag];
    }
}

-(void) hideAllPickerView{
    [MPickView setHidden:YES];
    [YPickView setHidden:YES];
    [pickview setHidden:YES];
    [self performDeletegate:TRUE];
    
}


@end
