//
//  MarketDealDetailVC.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 27/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "CatTopProductDetail.h"
#import "AppDelegate.h"

@interface MarketDealDetailVC : BaseViewController
{
    AppDelegate *appDelegate;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionview;
@property (weak, nonatomic) CatTopProductDetail *cartprod;
@property (nonatomic, strong) NSArray *listOfmarketItems;
@property (nonatomic) NSInteger selIndex;

@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (weak, nonatomic) IBOutlet UIButton *btnPrev;
- (IBAction)touchPrev:(id)sender;
- (IBAction)touchNext:(id)sender;
- (IBAction)showinfo:(id)sender;

@end
