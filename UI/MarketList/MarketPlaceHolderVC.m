//
//  MarketPlaceHolderVC.m
//  VIPCard
//
//  Created by Vishal Kolhe on 23/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "MarketPlaceHolderVC.h"
#import "AppGlobalData.h"
#import "YSLContainerViewController.h"
#import "MarketDealsResponce.h"
#import "MarketProductVC.h"
#import "CatTopProduct.h"
#import "AboutBussinessVC.h"

@interface MarketPlaceHolderVC () <YSLContainerViewControllerDelegate,UISearchDisplayDelegate,UISearchBarDelegate>
{
    
   
    NSMutableArray *viewControllers;
    NSInteger currentIndex;
   
}
@property (weak, nonatomic) IBOutlet UIView *topSearchBarView;
@end

@implementation MarketPlaceHolderVC
@synthesize categoryList,baseviewTitle,listOfmarketItems,expiredDate,limitation1,currentIndex;



-(void)viewWillAppear:(BOOL)animated
{
    [self showBackButton:YES];
    [self getCatTopProduct:_marketPlaceId];
    listOfmarketItems=[[NSMutableArray alloc]init];
    if(![_marketPlaceId isEqualToString:@"-1"])
    {
       
        self.lbl_Date.text=[NSString stringWithFormat:@"בתוקף עד-%@",[self dateformat]];
        self.lbl_Terms.text=[NSString stringWithFormat:@"%@:",NSLocalizedString(@"lbl_Terms", @"lbl_Terms")];
        NSMutableAttributedString *text = [self.lbl_Terms.attributedText mutableCopy];
        [text addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, text.length)];
        self.lbl_Terms.attributedText = text;
        self.con_heightTerm.constant=27;
        self.constraintTopViewHeight.constant = 70.f;
        [self.searchBtn setTitle:self.marketBusinessName forState:UIControlStateNormal];
        
    }
    else{
        self.con_heightTerm.constant = 0.f;
        self.constraintTopViewHeight.constant = 43.f;
    }
        self.title = baseviewTitle;

        [super viewWillAppear:animated];
}
//Gauri Added
-(NSString *)dateformat
{
    NSString *str = expiredDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    
     NSDate *date = [dateFormatter dateFromString: str];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/YY"];
     // here set format which you want...
     NSString *convertedString = [dateFormatter stringFromDate:date];
    return convertedString;
}

-(void)getCatTopProduct:(NSString *)marketID
{
    [[ConnectionsManager sharedManager] getCatTopProduct_withdelegate:marketID delegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:YES];
}
-(void)viewDidLoad
{
    [super viewDidLoad];
	[self showBackButton:YES];
	//[self.navigationController setNavigationBarHidden:YES];
	NSMutableAttributedString *text = [self.lbl_Date.attributedText mutableCopy];
	[text addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, text.length)];

}

#pragma mark -- YSLContainerViewControllerDelegate
- (void)containerViewItemIndex:(NSInteger)indexY currentController:(UIViewController *)controller
{
    currentIndex = indexY;

	if([controller respondsToSelector:@selector(reloadScreenData)])
	{
		[(MarketProductVC *)controller reloadScreenData];
	}
   else
    [controller viewWillAppear:YES];
    
    
}

//-(void)loadData
//{
//    NSArray *bussineesListTemp = self.categoryList;
//    NSLog(@"bussinessListTemp%@",self.categoryList);
//    viewControllers= [[NSMutableArray alloc]init];
//    MarketProductVC *vc;
//    NSMutableArray *tempResponse = [[NSMutableArray alloc] init];
//    NSInteger index= 0;
//    
//    for (MarketDealsResponce *MDResponse in bussineesListTemp)
//    {
//        vc = [self getVCWithSB_ID:kCategoryForMealListVC];
//        if(MDResponse.nameB && MDResponse.nameB != nil)
//        {
//            vc.title = MDResponse.nameB;
//            vc.marketResponse = MDResponse;
//            vc.currentIndex = index;
//            [tempResponse addObject:MDResponse];
//            vc.listOfmarketItems = [[NSMutableArray alloc] initWithArray:bussineesListTemp];
//            
//            [viewControllers addObject:vc];
//            
//            index ++;
//        }
//    }
//    float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
//    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
//    
//    YSLContainerViewController *containerVC = [[YSLContainerViewController alloc]initWithControllers:viewControllers
//                                                                                        topBarHeight:statusHeight + navigationHeight
//                                                                                parentViewController:self];
//    containerVC.delegate = self;
//    containerVC.menuItemFont = [UIFont fontWithName:@"Arial" size:16];
//    
//    [self.view addSubview:containerVC.view];

-(void)success:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    
      if([response.method isEqualToString:mFetchCatTopProduct])
    {
		// NSLog(@"%@", response.respondeBody);
        NSArray *responseList = response.respondeBody;
        if(responseList && [responseList isKindOfClass:[NSArray class]] && [responseList count] > 0)
        {
            NSMutableArray *temp = [NSMutableArray array];
            @autoreleasepool {
                for(NSDictionary *dic in responseList)
                {
                    CatTopProduct *bussinessResponse = [[CatTopProduct alloc] initWithDictionary:dic];
                    if (bussinessResponse.products.count>0 )
                    {
                        [temp addObject:bussinessResponse];
                    }
                    else
                    {
                        NSLog(@"no data");
                   }
                }
            }
//            MarketProductVC *marketVC = [self getVCWithSB_ID:kMarketProductVC];
//            marketVC.listOfmarketItems = temp;
            NSInteger index= 0;
            
            viewControllers= [[NSMutableArray alloc]init];
            for (CatTopProduct *prodcut in temp)
            {
               // MarketProductVC *vc = [[MarketProductVC alloc]init];

                MarketProductVC *vc = [self getVCWithSB_ID:kMarketProductVC];
                vc.listOfmarketItems = prodcut.products;
                vc.title =prodcut.name;
                vc.currentIndex = index;
                vc.marketPlaceId = self.marketPlaceId; //SANC
                
                
                
                [viewControllers addObject:vc];
                
                index ++;
            }
            float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
            float navigationHeight = self.navigationController.navigationBar.frame.size.height;
            
            YSLContainerViewController *containerVC = [[YSLContainerViewController alloc]initWithControllers:viewControllers
                                                                                                topBarHeight:statusHeight + navigationHeight
                                                                                        parentViewController:self];
            containerVC.delegate = self;
            containerVC.menuItemFont = [UIFont fontWithName:@"Arial" size:16];
            containerVC.view.frame = CGRectMake(0, CGRectGetMaxY(self.topSearchBarView.frame), containerVC.view.frame.size.width, containerVC.view.frame.size.height);
            containerVC.isScrollShow = NO;
            containerVC.view.backgroundColor=[UIColor whiteColor];
            [self.view addSubview:containerVC.view];


        }
    }
    
}

-(void)failure:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    
    if([response.method isEqualToString:@"fetchCatMealForBusiness"])
    {
		// NSLog(@"%@", response.respondeBody);
    }
}

-(void)searchCouponsby:(NSString *)aStr
{
    
    if(!aStr || ![aStr isEmpty])
        //aStr = nil;
    
    
    
        if(aStr)
        {
            NSPredicate *cityPredicate = [NSPredicate predicateWithFormat:@"(SELF.Name contains[cd] %@)",aStr];
            
            MarketProductVC *vc = [viewControllers objectAtIndex:currentIndex];
            if([vc isKindOfClass:[MarketProductVC class]])
            {
                NSArray *filterArray = [vc.listOfmarketItems filteredArrayUsingPredicate:cityPredicate];
                vc.filteredList = filterArray;
                vc.isSearch = YES;
            }
            else
            {
                  vc.isSearch = NO;
                  vc.filteredList = self.listOfmarketItems;
            }
            [vc reloadScreenData];
}

    
//    // aStr = @"בימבה מכונית";
//    NSPredicate *cityPredicate = [NSPredicate predicateWithFormat:@"(SELF.Name contains[cd] %@)",aStr];
//   
//    MarketProductVC *vc = [viewControllers objectAtIndex:currentIndex];
//    if([vc isKindOfClass:[MarketProductVC class]])
//    {
//        NSArray *filterArray = [listOfmarketItems filteredArrayUsingPredicate:cityPredicate];
//        vc.filteredList = filterArray;
//        vc.isSearch = YES;
//        if(aStr == nil || [aStr isEmpty])
//            vc.filteredList = listOfmarketItems;
//        
//        [vc reloadScreenData];
//      
//    }
//
//    
//
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self searchCouponsby:self.txtFldSearch.text];
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *searchString = self.txtFldSearch.text;
    
    if([string isEqualToString:@""])
        searchString = [searchString substringToIndex:[searchString length] - 1];
    
    else
        searchString = [searchString stringByAppendingString:string];
    
    [self searchCouponsby:searchString];
    return YES;
}

-(void)onClickCancelButton
{
    MarketProductVC *vc = [viewControllers objectAtIndex:currentIndex];
    if([vc isKindOfClass:[MarketProductVC class]])
    {
        vc.filteredList = vc.marketResponse.products;
        vc.isSearch = NO;
        [vc viewWillAppear:YES];
        [self.txtFldSearch setText:@""];
      
    }
}



- (IBAction)onClickCancelButton:(id)sender {
}
- (IBAction)ClickTermBtn:(id)sender
{
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AboutBussinessVC *aboutVc = [storyBoard instantiateViewControllerWithIdentifier:@"SB_ID_AboutBussiness"];
    aboutVc.mLimitations=limitation1;
    aboutVc.flagForTerms=true;
    
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:aboutVc];
    
    [self presentViewController:nav animated:YES completion:^{
        
        
        
    }];
    
   // [self removeAnimate];


}
@end
