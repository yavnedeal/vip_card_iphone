//
//  MarketDealDetailVC.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 27/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "MarketDealDetailVC.h"
#import "MarketDealDetailCell.h"
#import "ReportError.h"

@interface MarketDealDetailVC ()
{
    NSInteger selectedDeal;
    ReportError *reportErr;
    
}
@end

@implementation MarketDealDetailVC
@synthesize cartprod,listOfmarketItems,selIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    selectedDeal = selIndex;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"listOfmarketItems: %@",listOfmarketItems);
    [self showBackButton:YES];
   
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [listOfmarketItems count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MarketDealDetailCell *dealcell = (MarketDealDetailCell *)[_collectionview dequeueReusableCellWithReuseIdentifier:@"marketdealcell" forIndexPath:indexPath];
    
    cartprod = [listOfmarketItems objectAtIndex:selectedDeal];
    
    //[self populatedDealOfTheDayCell:dealcell indePath:indexPath];
    [dealcell.btnterms setTitle:NSLocalizedString(@"Terms of network", nil) forState:UIControlStateNormal];
    [dealcell.btnadd setTitle:NSLocalizedString(@"Add to List", nil) forState:UIControlStateNormal];
    [dealcell.btnadd.layer setCornerRadius:8];
    [dealcell.btnterms.layer setCornerRadius:8];
    
    dealcell.lblprodname.text = cartprod.Name;
    dealcell.lblprice.text = cartprod.Price;
    dealcell.lbldesc.text = cartprod.Description;
    dealcell.lblsubdesc.text = cartprod.SubDescription;
    dealcell.lbllimit.text = cartprod.Limitations;
    
    NSURL *imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", mFetchMarketImages,cartprod.Image]];
    
    if(cartprod.Image.length > 0)
        [dealcell.imgprod sd_setImageWithURL:imgUrl placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",PLACE_HOLDER_IMAGE]]];
    else
        [dealcell.imgprod setImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
    
    NSURL *logoimgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", mFetchImages,cartprod.Logo]];
    
    if(cartprod.Logo.length > 0)
        [dealcell.imglogo sd_setImageWithURL:logoimgUrl placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",PLACE_HOLDER_IMAGE]]];
    else
        [dealcell.imglogo setImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
    
    return dealcell;
}

- (IBAction)touchPrev:(id)sender
{
    selectedDeal -- ;
    _btnNext.hidden = NO;
    
    [_collectionview scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:selectedDeal inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    if (selectedDeal == 0)
    {
        _btnPrev.hidden = YES;
        return;
    }
}

- (IBAction)touchNext:(id)sender {
    
    selectedDeal ++ ;
    _btnPrev.hidden = NO;
    
    [_collectionview scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:selectedDeal inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
    if (selectedDeal == listOfmarketItems.count-1)
    {
        _btnNext.hidden = YES;
        return;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)showinfo:(id)sender
{
    [appDelegate.window addSubview:[self getReportErrView]];
}
-(ReportError *) getReportErrView{
    
    reportErr=[ReportError loadFromNibWithFrame:CGRectMake(0, 0,appDelegate.window.frame.size.width,appDelegate.window.frame.size.height )];
    UITapGestureRecognizer *tapgest = [[UITapGestureRecognizer alloc] initWithTarget:reportErr action:@selector(hideKey)];
    [reportErr addGestureRecognizer:tapgest];
    tapgest.delegate = reportErr;
    [reportErr fillData:cartprod.Id];
    return reportErr;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
