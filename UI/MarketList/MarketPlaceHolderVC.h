//
//  MarketPlaceHolderVC.h
//  VIPCard
//
//  Created by Vishal Kolhe on 23/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "CatTopProduct.h"

@interface MarketPlaceHolderVC : BaseViewController<ServerResponseDelegate>
@property (nonatomic, strong) NSMutableArray *categoryList,*listOfmarketItems;
@property (strong, nonatomic) IBOutlet NSString *baseviewTitle;
@property (strong, nonatomic) IBOutlet NSString *marketPlaceId;
@property (strong, nonatomic) IBOutlet NSString *expiredDate;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_heightTerm;

@property (strong, nonatomic) IBOutlet NSString *limitation1;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Terms;


@property (nonatomic, strong) CatTopProduct *marketResponse;
@property (nonatomic, strong) UITableView *ProductListView;
@property (nonatomic, assign) NSInteger currentIndex;
@property (weak, nonatomic) IBOutlet UITextField *txtFldSearch;
- (IBAction)onClickCancelButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnTerms;
- (IBAction)ClickTermBtn:(id)sender;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintTopViewHeight;

-(NSString *)dateformat;
@property(nonatomic,strong)NSString *searchString;
@property(nonatomic,strong) NSString *marketBusinessName;
@end
