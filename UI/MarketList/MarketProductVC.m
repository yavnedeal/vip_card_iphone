//
//  MarketProductVC.m
//  VIPCard
//
//  Created by Vishal Kolhe on 20/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "MarketProductVC.h"
#import "CatTopProduct.h"
#import "MarketProductCell.h"
#import "MarketDealDetailVC.h"
#import "CatTopProductDetail.h"


#define CELL_HEIGHT 130
@interface MarketProductVC ()
{
NSMutableArray *viewControllers;
NSArray *marketList;
}

@end

@implementation MarketProductVC
@synthesize listOfmarketItems,currentIndex;

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadScreenData];

}

-(void) reloadScreenData{

    
    
    UINavigationBar *bar = [self.navigationController navigationBar];
    [bar setTintColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0]];
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:107.0/255.0 green:145.0/255.0 blue:35.0/255.0 alpha:1.0]];
    
  
    
    if(!_isSearch)
    {
        _filteredList = listOfmarketItems;
    }
       [self.ProductListView reloadData];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self showBackButton:YES];
    [self.ProductListView setDataSource:self];
    [self.ProductListView setDelegate:self];
   marketList = self.marketResponse.products;
   
   self.ProductListView.tableFooterView =  [[UIView alloc] initWithFrame:CGRectZero];
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //CatTopProduct *product = [self.listOfmarketItems objectAtIndex:section];
    if (_isSearch)
     {
         return [self.filteredList count];
    }
    return [self.listOfmarketItems count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MarketCell";
    MarketProductCell*cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
   
    if (cell == nil) {
        cell = [[MarketProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] ;
        cell.textLabel.lineBreakMode =NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
  
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   //CatTopProduct *marketObj = [self.listOfmarketItems objectAtIndex:indexPath.section];
    CatTopProductDetail *ctprod;
    if (_isSearch)
     {
         [cell populateData:[self.filteredList objectAtIndex:indexPath.row]];
         ctprod=[self.filteredList objectAtIndex:indexPath.row];
    }
    else
    {
        [cell populateData:[self.listOfmarketItems objectAtIndex:indexPath.row]];
        ctprod=[self.listOfmarketItems objectAtIndex:indexPath.row];
    }
    
    //-----------------------------------------------------
    if(IS_OS_8_OR_LATER)
    {
        cell.con_heightBname.active = TRUE;
        cell.con_lbldec.active = TRUE;
        cell.con_lbllimitation.active = TRUE;
    }
        //CODE TO SHOW MARKET Name
        if ([_marketPlaceId isEqualToString:@"-1"])
         {
             cell.lbl_MarketName.hidden = FALSE;
             cell.con_heightBname.constant=18;
             cell.lbl_MarketName.text = ctprod.bName;
        }
        else {
            cell.con_heightBname.constant=0;
            cell.lbl_MarketName.text = @"";
            cell.lbl_MarketName.hidden = TRUE;
        }
     //-----------------------------------------------------
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    MarketProductCell *cell = (MarketProductCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    CGSize toName = [self getStringSize:cell.lblName andString:cell.lblName.text];
    //CGSize toLimitation = [self getStringSize:cell.lbl_limitation andString:cell.lbl_limitation.text];
    
    CGSize toLimitation = CGSizeZero;
    float hLimitation=21.0;

    if ((cell.lbl_limitation.text.length > 0))
    {
        toLimitation = [self getStringSize:cell.lbl_limitation andString:cell.lbl_limitation.text];
        if(toLimitation.height > 21)
            hLimitation = toLimitation.height;
    }


    float hName = 21.0;
    float hDes = 16.0;
    
    if(toName.height >21)
        hName = toName.height;
    
    if ((cell.lblDesc.text.length > 0))
    {
        CGSize toDes = [self getStringSize:cell.lblDesc andString:cell.lblDesc.text];
        if(toDes.height > 16)
            hDes = toDes.height;
    }
    
    CGFloat cellHeight = CELL_HEIGHT;
    if (![_marketPlaceId isEqualToString:@"-1"])
        cellHeight = cellHeight - 18.f;
    
    if ((hDes + hName + hLimitation) > 113.0)
    {
        return cellHeight + (hDes + hName + hLimitation) - 110.0 ;
    }
    
    return cellHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//TODO will be implemented in future
    MarketDealDetailVC *marketDealDetailVC = [self getVCWithSB_ID:kMarketDealDetailVC_SB_ID];
   // CatTopProductDetail *ctprod = [self.listOfmarketItems objectAtIndex:indexPath.row];
    //marketDealDetailVC.cartprod = ctprod;
    marketDealDetailVC.selIndex = indexPath.row;
    marketDealDetailVC.listOfmarketItems = listOfmarketItems;
    //[self.navigationController pushViewController:marketDealDetailVC animated:YES];
}



@end
