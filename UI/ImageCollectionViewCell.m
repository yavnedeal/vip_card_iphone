//
//  ImageCollectionViewCell.m
//  VIPCard
//
//  Created by SanC on 13/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "ImageCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "HomeGalleryResponse.h"



@implementation ImageCollectionViewCell
-(void)populatedData:(NSDictionary*)data
{
NSURL *url;
if([data isKindOfClass:[NSDictionary class]])
	url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", mFetchImagesBYWeb,[data objectForKey:@"PathDirectory"] ,[data objectForKey:@"name"]]];
else if([data isKindOfClass:[HomeGalleryResponse class]])
{

	HomeGalleryResponse *homeGalleryResponse=(HomeGalleryResponse*)data;
	NSString *combined =[NSString stringWithFormat:@"%@%@%@", mFetchImagesBYWeb,homeGalleryResponse.PathDirectory ,homeGalleryResponse.name];
	url=[NSURL URLWithString:combined];
}
	[self.imgView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];

}



@end
