//
//  FieldCategoryDetailCell.h
//  VIPCard
//
//  Created by SanC on 03/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FieldCategoryDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_View;
@property (weak, nonatomic) IBOutlet UIButton *btn_ImgPopUp;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Phone;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Name;
@property (weak, nonatomic) IBOutlet UIButton *btn_Call;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Discription;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Address;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_NameconstraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_DiscriptionConstraintHeight;
@property (weak, nonatomic) IBOutlet UIView *view_BckGround;



- (IBAction)onClick_ImgPopUp:(id)sender;
- (IBAction)onClick_BtnCall:(id)sender;

@end
