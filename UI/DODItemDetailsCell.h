//
//  DODItemDetailsCell.h
//  VIPCard
//
//  Created by Andrei on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DODItemDetailsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgProduct;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblSubTitleDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblShekel;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UIView *viewDetail;
@property (strong, nonatomic) IBOutlet UILabel *lblItemDetails1;
@property (strong, nonatomic) IBOutlet UILabel *lblItemDetails2;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewItemDetailsHeightConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_titleDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_subTitleDescription;
@property (strong, nonatomic) IBOutlet UIView *view_Seprator;

@property (nonatomic, strong) UIView *baseHolderView;
@property (strong, nonatomic) UILabel *lblextrameal;
@end
