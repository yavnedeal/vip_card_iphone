//
//  MyOrdersVC.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 26/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "MyOrdersVC.h"
#import "ServiceManager.h"
#import "UIImageView+WebCache.h"
#import "JSONUtil.h"
#import "ShoppingCartSummaryVC.h"

#define CELL_HEIGHT 100

@interface MyOrdersVC ()
{
    AppGlobalData *appGlobalData;
}
@end

@implementation MyOrdersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showBackButton:YES];
    self.tblOrders.tableFooterView =  [[UIView alloc] initWithFrame:CGRectZero];
    listOfOrders = [[NSArray alloc] initWithObjects:@"Test",@"Test",@"Test",@"Test",@"Test", nil];
    
    appGlobalData = [AppGlobalData sharedManager];
    ServiceManager *serviceManager = [ServiceManager sharedInstance:self];
    listOfOrders = [serviceManager fetchMyOrders];
    if (listOfOrders.count == 0) {
        self.lblerr.text = @"No Records.";
    }
    [self.tblOrders reloadData];
    
    [self.navigationController.navigationBar setTintColor:[UIColor redColor]];
    [self.navigationController.navigationBar setHidden:NO];
    //self.navigationController.navigationBar.translucent = YES;
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listOfOrders count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"OrderCell";
    MyOrdersCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    MyOrders *order = [listOfOrders objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        cell = [[MyOrdersCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] ;
        cell.textLabel.lineBreakMode =NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        
    }
    cell.delegate = self;
    [cell populateData:order];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //CatTopProductDetail *ctprod;
    
    //[cell populateData:[self.listOfmarketItems objectAtIndex:indexPath.row]];
    //ctprod=[self.listOfmarketItems objectAtIndex:indexPath.row];
    //cell.btn_ordDetail.tag = indexPath.row;
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //TODO will be implemented in future
}
-(void) reloadScreenData{
    
    
    
    UINavigationBar *bar = [self.navigationController navigationBar];
    [bar setTintColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0]];
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:107.0/255.0 green:145.0/255.0 blue:35.0/255.0 alpha:1.0]];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //[self reloadScreenData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)onOrderDetailClicked:(MyOrders*)myOrder
{
    
    NSDictionary *dic = [JSONUtil JSONStrToDic:myOrder.orderObj];
    NSDictionary *dicBiz = [JSONUtil JSONStrToDic:myOrder.cartData];
    NSLog(@"detail dic:%@",dic);
    ShoppingCartSummaryVC *summaryVC = [self.storyboard instantiateViewControllerWithIdentifier:kShoppingCartSummaryVC_SB_ID];
    
    NSArray *arrcart = [[dicBiz objectForKey:@"cartObj"] objectForKey:@"Meals"];//[[[dic objectForKey:@"ordering"] objectForKey:@"DetailsOrder"] objectForKey:@"Meals"];
    NSMutableArray *cartArr = [NSMutableArray array];
    
    
    WSBussiness *business = [[WSBussiness alloc] init];
    [business populateData:[dicBiz objectForKey:@"selBZ"]];
    
    for (NSDictionary *mealdic in arrcart) {
        
        WSMealsResponse *mealsResponse = [[WSMealsResponse alloc]init];
        [mealsResponse populateData:mealdic];
        [cartArr addObject:mealsResponse];
        
        [appGlobalData addItemToCart:business.id cartItem:mealsResponse];
    }
    
    
    summaryVC.isFromCart = [dicBiz objectForKey:@"isFromCart"];
    summaryVC.businessId = [[dicBiz objectForKey:@"selBZ"] objectForKey:@"id"];
    summaryVC.selectedBussiness = business;
    summaryVC.cartArr = cartArr;
    summaryVC.isFromMyOrder = YES;
    summaryVC.myOrder = myOrder;
    summaryVC.myOrdDiscount = [[dic objectForKey:@"ordering"] objectForKey:@"PriceDiscount"];
    [self.navigationController.navigationBar setBarTintColor:[UIColor redColor]];
    [self.navigationController pushViewController:summaryVC animated:YES];
}
-(void)onOrderDeleteClicked:(MyOrders *)myOrder
{
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
  
    ServiceManager *serviceManager = [ServiceManager sharedInstance:self];
    [serviceManager deleteOrder:[NSString stringWithFormat:@"%@",myOrder.orderId]];
    
    listOfOrders = [serviceManager fetchMyOrders];
    [self.tblOrders reloadData];
    [DejalActivityView removeView];
}

@end
