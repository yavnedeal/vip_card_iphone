//
//  DODItemDetailsCell.h
//  VIPCard
//
//  Created by Andrei on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DODTotalCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UIView *viewMainTotal;
@property (strong, nonatomic) IBOutlet UILabel *lblMainTotal;
@property (strong, nonatomic) IBOutlet UILabel *lblDiscount;
@property (strong, nonatomic) IBOutlet UIButton *btnDeliveryType;
@property (strong, nonatomic) IBOutlet UILabel *lblDeliveryName;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryNameShekel;

@property (strong, nonatomic) IBOutlet UILabel *lblTotalAfterDiscount;
@property (strong, nonatomic) IBOutlet UITextField *txtMessage;
@property (strong, nonatomic) IBOutlet UILabel *lblOrderType;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewMailTotalHeigthConstraints;
@property (weak, nonatomic) IBOutlet UIImageView *img_BckOfTextfield;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_additionalViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lbl_mainTotalShekel;

@property (weak, nonatomic) IBOutlet UILabel *lbl_discountShekel;
@property (weak, nonatomic) IBOutlet UILabel *lbl_totalAfterDiscountShekel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_vipdiscount_ht;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_vipdiscount_shekel_ht;

@end
