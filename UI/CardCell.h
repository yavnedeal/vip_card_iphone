//
//  CardCell.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 02/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppBorderTextField.h"
#import "CardData.h"


@class CardCell;
@protocol CardCellDelegate <NSObject>

-(void) cardCell:(CardCell *) cardCell toggleScroll:(BOOL) flag;
-(void) chkboxFE:(NSInteger)isCheckedFE;
@end

@interface CardCell : UITableViewCell
{
    
}


@property (weak, nonatomic) IBOutlet UILabel *lblname;
@property (weak, nonatomic) IBOutlet UILabel *lblownid;
@property (weak, nonatomic) IBOutlet UILabel *lblcardtype;
@property (weak, nonatomic) IBOutlet UILabel *lblcardno;
@property (weak, nonatomic) IBOutlet UILabel *lblexpdate;
@property (weak, nonatomic) IBOutlet UILabel *lblsumtopay;
@property (weak, nonatomic) IBOutlet AppBorderTextField *txtname;
@property (weak, nonatomic) IBOutlet UITextField *txtownid;
@property (weak, nonatomic) IBOutlet UIButton *btncardtype;
@property (weak, nonatomic) IBOutlet UITextField *txtcardno;
@property (weak, nonatomic) IBOutlet UIButton *btnmonth;
@property (weak, nonatomic) IBOutlet UIButton *btnyear;
@property (weak, nonatomic) IBOutlet UITextField *txtsumtopay;
@property (weak, nonatomic) IBOutlet UILabel *lblcurency;

@property (weak, nonatomic) IBOutlet UIPickerView *pickcardtype;
@property (weak, nonatomic) IBOutlet UIView *pickview;
@property (weak, nonatomic) IBOutlet UIView *MPickView;
@property (weak, nonatomic) IBOutlet UIView *YPickView;
@property (weak, nonatomic) IBOutlet UIPickerView *MPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *YPicker;
//CVV Addition
@property (weak, nonatomic) IBOutlet UILabel *lblcardcvv;
@property (weak, nonatomic) IBOutlet UITextField *txtcardcvv;

@property(strong,nonatomic) CardData *cellData;

@property(strong,nonatomic) id<CardCellDelegate> cardCellDelegate;
@property (strong,nonatomic)NSDate *chkboxStartTime,*chkboxEndTime;

-(void)populateData:(CardData *)aData;
- (IBAction)showYearPicker:(UIButton *)sender;
- (IBAction)showMonthPicker:(UIButton *)sender;
- (IBAction)showCardTypeView:(UIButton *)sender;


@property (strong,nonatomic)NSMutableArray *lateOrderTime;
@property(strong,nonatomic) NSIndexPath *currentIndexPath;
@end
