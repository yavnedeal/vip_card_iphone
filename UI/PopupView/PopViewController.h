//
//  PopViewController.h
//  CheckNet
//
//  Created by Yogesh Bhamre on 03/12/15.
//  Copyright © 2015 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSFindBussinessListResponse.h"
#import "popUpCell.h"
#import "BaseViewController.h"

@protocol PopupViewDelegate <NSObject>

@optional
-(void)deleteRecord;
-(void)closeButton;
@end

//@end

@interface PopViewController : BaseViewController <PopupViewDelegate,UITextFieldDelegate>
{
	__weak IBOutlet UITableView *tbl_View;
	NSArray *array_time;

}

@property (assign) id value;
@property(strong,nonatomic)WSFindBussinessListResponse *bussinessResponse;


@property (weak, nonatomic) IBOutlet UITextField *txt_Field;
@property (nonatomic, weak) id<PopupViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
- (IBAction)onClickCloseButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
/*Button declaration*/
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
- (IBAction)onClickOkButton:(id)sender;
- (IBAction)onClickCancelButton:(id)sender;
/********************/



- (void)showInView:(UIView *)aView withMessage:(NSString *)msg animated:(BOOL)animated;
//-(void)ShowOnWindow:(UIView *)aView animated:(BOOL)animated;

//@gauri added


@property (weak, nonatomic) IBOutlet UILabel *lbl_msg;

@property (weak, nonatomic) IBOutlet UILabel *lbl_barcode;


- (IBAction)submite_btn:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *popupview;
@property (weak, nonatomic) IBOutlet UIView *btn_submite;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_heigh;





@end
