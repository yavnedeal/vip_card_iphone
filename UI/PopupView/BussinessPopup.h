//
//  BussinessPopup.h
//  VIPCard
//
//  Created by SanC on 24/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BussinessPopup;

@protocol BussinessPopupDelegate <NSObject>



-(void) businessPopup:(BussinessPopup *) bussPopup withButtonIndex:(NSInteger ) butonIndex;

@end

@interface BussinessPopup : UIView
@property (weak, nonatomic) IBOutlet UIButton *btn_FilterBussiness;
@property (weak, nonatomic) IBOutlet UIButton *btn_FindBussiness;
@property (strong,nonatomic) id<BussinessPopupDelegate> bussDelegate;
- (IBAction)onClick_FindBussiness:(id)sender;
- (IBAction)onClick_FilterBussiness:(id)sender;

+(BussinessPopup *) bussinessPopup:(CGRect) frame;

@end
