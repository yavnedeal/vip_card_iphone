//
//  PopViewController.m
//  CheckNet
//
//  Created by Yogesh Bhamre on 03/12/15.
//  Copyright © 2015 Enovate. All rights reserved.
//

#import "PopViewController.h"
#import "WSBaseData.h"
#import "WSOpeningHoursResponse.h"
#import "AppColorConstants.h"
#import "AppDelegate.h"
#import "UserLoginVC.h"


@interface PopViewController ()
{
	NSString *toastMessage;
}
@end
static PopViewController *alertManager = nil;
@implementation PopViewController

- (void)viewDidLoad {
	self.view.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:.6];
	tbl_View.layer.cornerRadius = 10;
	self.popupview.layer.shadowOpacity = 0.8;
	self.popupview.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
	array_time=_value;
	[self.lblMessage setText:toastMessage];
	self.popUpView.layer.cornerRadius = 5;
	_btnSignIn.layer.cornerRadius = 07.0;
	_btnSignUp.layer.cornerRadius = 07.0;
	_btnClose.layer.cornerRadius = 15.0;
	_btnClose.layer.masksToBounds = YES;
	_btnSignUp.layer.masksToBounds = YES;
	_btnSignIn.layer.masksToBounds = YES;
	self.txt_Field.delegate=self;


	[_btnSignUp setTitle:NSLocalizedString(@"lbl_Button2", @"lbl_Button2") forState:UIControlStateNormal];
	[_btnSignIn setTitle:NSLocalizedString(@"lbl_Button1", @"lbl_Button1") forState:UIControlStateNormal];
	_lblMessage.text = NSLocalizedString(@"lbl_Title", @"lbl_Title");


//@Gauri added//
    
    	[self.lbl_msg setText:toastMessage];
        self.popupview.layer.cornerRadius = 5;
    	_btn_submite.layer.cornerRadius = 07.0;
        _btn_submite.layer.masksToBounds = YES;
    
    //	[_btn_submite setTitle:NSLocalizedString(@"lbl_Button2", @"lbl_Button2") forState:UIControlStateNormal];
    
    	_lbl_msg.text = NSLocalizedString(@"lbl_Title", @"lbl_Title");

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle: @"Servers" style:UIBarButtonItemStylePlain target:self action:@selector(backBtn:)];
     self.navigationItem.backBarButtonItem = backButton;



	// Do any additional setup after loading the view from its nib.
	UITapGestureRecognizer *singleFingerTap =
	[[UITapGestureRecognizer alloc] initWithTarget:self
											action:@selector(handleSingleTap:)];
	[self.view addGestureRecognizer:singleFingerTap];
	[super viewDidLoad];

}
-(void)backBtn:(id)sender
{
    

}

+ (id)sharedInstance {

	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{

		alertManager = [[PopViewController alloc] init];
	});

	return alertManager;
}


-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
	[[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}




#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// Return the number of sections.
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of rows in the section.
	return array_time.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

	return 45.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";
	popUpCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"popUpCell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
	}
	WSOpeningHoursResponse *wsOpeningHoursResponse=[array_time objectAtIndex:indexPath.row];
	tableView.separatorColor=[UIColor clearColor];
	cell.lbl_Title.text=wsOpeningHoursResponse.Day;
    if (wsOpeningHoursResponse.HourClose.length>5)
     {
         NSString *closeTime=[wsOpeningHoursResponse.HourClose substringToIndex:5];
         cell.lbl_Time.text=[NSString stringWithFormat:@"%@-%@" ,wsOpeningHoursResponse.HourOpen,closeTime];
     }
     else if(wsOpeningHoursResponse.HourOpen.length>5)
     {
         NSString *openTime=[wsOpeningHoursResponse.HourOpen substringToIndex:5];
         cell.lbl_Time.text=[NSString stringWithFormat:@"%@-%@" ,wsOpeningHoursResponse.HourOpen,openTime];
     }
    else
	cell.lbl_Time.text=[NSString stringWithFormat:@"%@-%@" ,wsOpeningHoursResponse.HourOpen,wsOpeningHoursResponse.HourClose];
 return cell;
}

#pragma mark- UITableViewDelegate
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
	headerView.backgroundColor = [UIColor clearColor];
	UILabel *label = [[UILabel alloc] initWithFrame: CGRectMake(0,0, tableView.frame.size.width-10, 30)];
	label.textColor = [UIColor blackColor];
	label.text = NSLocalizedString(@"opening_Hour","opening_Hour");
	label.font = helveticaBoldFont(19.0);
	label.textAlignment = NSTextAlignmentCenter;
	label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
	[headerView addSubview:label];
	return headerView;
}

#pragma mark-UITextFieldDelegate
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
	[self.view endEditing:YES];
	return YES;
}



#pragma mark-UserDefineMethods
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
	[self removeAnimate];
}
- (void)showInView:(UIView *)aView withMessage:(NSString *)msg animated:(BOOL)animated
{
	toastMessage = msg;
	self.view.frame = aView.frame;
	[aView addSubview:self.view];
	if (animated) {
		[self showAnimate];
	}
}



- (void)showAnimate
{
	self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
	self.view.alpha = 0;
	[UIView animateWithDuration:.25 animations:^{
		self.view.alpha = 1;
		self.view.transform = CGAffineTransformMakeScale(1, 1);
	}];
}

- (void)removeAnimate
{
	[UIView animateWithDuration:.25 animations:^{
		self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
		self.view.alpha = 0.0;
	} completion:^(BOOL finished) {
		if (finished) {
			[self.view removeFromSuperview];
		}
	}];
}

- (IBAction)onClickOkButton:(id)sender
{
    /*UIViewController *dealVC = [self getVCWithSB_ID:kUserLoginFilterVC];
    [self.navigationController pushViewController:dealVC animated:YES];*/
//    AppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
//    [appdelegate navigateLoginView];

    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UserLoginVC *vc = [storyBoard instantiateViewControllerWithIdentifier:@"SB_ID_UserLoginVC"];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self presentViewController:nav animated:YES completion:^{
        
        
        
    }];
   
	[self removeAnimate];
}
- (IBAction)onClickCancelButton:(id)sender
{
	[self.delegate closeButton];
	[self removeAnimate];
}

- (IBAction)onClickCloseButton:(id)sender
{
	[self removeAnimate];
}
- (IBAction)submite_btn:(id)sender
{
	[self.view endEditing:YES];

}

@end
