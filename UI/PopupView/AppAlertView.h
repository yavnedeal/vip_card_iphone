//
//  AppAlertView.h
//  CheckNet
//
//  Created by Enovate Macbook Pro on 21/06/16.
//  Copyright © 2016 Enovate IT Outsourcing Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppAlertView;

@protocol AppAlertViewDelegate <NSObject>

@required

-(void) appAlertView:(AppAlertView *) alertView clickedButtonIndex:(NSInteger) buttonIndex;


@end

@interface AppAlertView : UIView
{
    UIView *_parentView;
    UIView *backgroundView;
    UIView *popupView;
    UIView *buttomView;
    
    
    BOOL rtlFlag;
    
}

@property(nonatomic,readonly) NSInteger cancelButtonIndex;

@property(nonatomic,strong) UIColor *titleTextColor;
@property(nonatomic,strong) UIColor *messageTextColor;
@property(nonatomic,strong) UIColor *buttonSeperatorColor;
@property(nonatomic,strong) UIColor *cancelButtonColor;
@property(nonatomic,strong) UIColor *otherButtonColor;
@property(nonatomic,strong) UIFont *titleFont;
@property(nonatomic,strong) UIFont *messageFont;
@property(nonatomic,strong) UIFont *buttonFont;

@property(nonatomic,strong) UIView *childView;

@property(nonatomic,strong) NSString *alertTitle;
@property(nonatomic,strong) NSString *alertMessage;



@property(nonatomic,strong) id<AppAlertViewDelegate> delegate;
//Method

//Static

+(AppAlertView *) createWithTitle:(NSString *) title message:(NSString *) msg cancelButton:(NSString *) cancelButtonTitle otherButtons:(NSString *) otherButtonTitle, ... NS_REQUIRES_NIL_TERMINATION;

+(AppAlertView *) createWithView:(UIView *) childView title:(NSString *) title message:(NSString *) msg cancelButton:(NSString *) cancelButtonTitle otherButtons:(NSString *) otherButtonTitle, ... NS_REQUIRES_NIL_TERMINATION;

-(id) initWithTitle:(NSString *) title message:(NSString *) msg cancelButton:(NSString *) cancelButtonTitle;

-(void) show:(UIView *) parentView rtlFlag:(BOOL) flag;
-(void) close;








@end
