//
//  FIndBussinessListCell.h
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindBussinessListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgProduct;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
@property (strong, nonatomic) IBOutlet UILabel *lblProductTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblProductDiscription;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_lblDiscriptionHeight;


@end
