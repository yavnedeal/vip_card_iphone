//
//  GalleryCell.h
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "WSConstants.h"
#import "AppGlobalConstant.h"

@interface GalleryCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
-(void)populatedData:(NSString*)data;



@end
