//
//  GalleryCell.m
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.O
//

#import "GalleryCell.h"
#import "GridGalleryResponse.h"

@implementation GalleryCell
-(void)populatedData:(NSDictionary*)data
{
	self.imgView.layer.shadowOffset = CGSizeMake(0, 2);
	self.imgView.layer.shadowRadius = 4.0;
	self.imgView.layer.shadowColor = [UIColor blackColor].CGColor;
	self.imgView.layer.shadowOpacity = 1;
	NSURL *url;
	if([data isKindOfClass:[NSDictionary class]])
	{
	url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", mFetchImagesBYWeb,[data objectForKey:@"PathDirectory"] ,[data objectForKey:@"name"]]];

	}
	else if([data isKindOfClass:[GridGalleryResponse class]])
	{

		GridGalleryResponse *gridGalleryResponse=(GridGalleryResponse*)data;
		NSString *combined =[NSString stringWithFormat:@"%@%@%@", mFetchImagesBYWeb,gridGalleryResponse.PathDirectory ,gridGalleryResponse.name];
		url=[NSURL URLWithString:combined];
	}
	[self.imgView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];


	
}

@end
