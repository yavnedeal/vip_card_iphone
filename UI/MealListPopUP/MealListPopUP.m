//
//  MealListPopUP.m
//  VIPCard
//
//  Created by SanC on 09/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
#define CheckedImage    @"checkbox_clicked"
#define UnCheckImage    @"checkbox"
#import "MealListPopUP.h"
#import "AppColorConstants.h"


@implementation MealListPopUP
@synthesize extraForMealsObj,isChild;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect {
	// Drawing code
	//self.backgroundColor=[UIColor clearColor];
}

+(MealListPopUP *) loadFromNibWithFrame:(CGRect) frame
{
    MealListPopUP *mlpu = [[[UINib nibWithNibName: @"MealListPopUP"
                                           bundle: [NSBundle bundleForClass: [MealListPopUP class]]]
                            
                            instantiateWithOwner: nil
                            options: nil] firstObject];
    mlpu.frame = frame;
    mlpu.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [mlpu papulateData];
    
    return mlpu;
    
}
/*
- (instancetype) initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
    if(self)
    {
        self = [[[UINib nibWithNibName: @"MealListPopUP"
                                bundle: [NSBundle bundleForClass: [MealListPopUP class]]]
                 
                 instantiateWithOwner: nil
                 options: nil] firstObject];
        
        self.frame = frame;
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [self papulateData];
    }
	return self;
}
 */

-(void)papulateData
{
	gesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
	[internalView addGestureRecognizer:gesture];
	[internalView setUserInteractionEnabled:TRUE];
	

}

#pragma mark-UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return listOfMeals.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";

	CatForMenuDataCell *cell =(CatForMenuDataCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil)
	{
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CatForMenuDataCell" owner:self options:nil];
		cell = [nib objectAtIndex:0];

	}
	//tableView.backgroundColor=[UIColor clearColor];
	tableView.separatorColor=[UIColor clearColor];
    
    [cell toggleCheckBox:!maelOrExtra];
	//[cell.imgCheckBox setHidden:!maelOrExtra];
	cell.backgroundColor=[UIColor whiteColor];

	WSExtraMealsResponse *extraMeals = [listOfMeals objectAtIndex:indexPath.row];

	if(maelOrExtra)
	{
		if([listOfItems containsObject:extraMeals])
		{
			[cell.imgCheckBox setImage:[UIImage imageNamed:CheckedImage]];
		}
		else
		{
			[cell.imgCheckBox setImage:[UIImage imageNamed:UnCheckImage]];
		}
	}
	[cell populateData:extraMeals Forprice:[extraForMealsObj.price intValue]];
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

	WSExtraMealsResponse *mealResponse = [listOfMeals objectAtIndex:indexPath.row];
	[self submitData:mealResponse];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//CatForMenuDataCell *cell = (CatForMenuDataCell*) [self tableView:tableView cellForRowAtIndexPath:indexPath];
    
	WSExtraMealsResponse *extraMeals = [listOfMeals objectAtIndex:indexPath.row];
    
    CGFloat w = self.tableView.bounds.size.width - 155;
    
    if (!maelOrExtra)
        w = w + 20.f;
    
    CGFloat height = [self heightForString:extraMeals.Name font:helveticaFont(17.0) width:w];

	//	CGSize size = [self getStringSize:cell.lblName andString:extraMeals.Name];
	if (height>21)
    {
		return 39 + height - 21;
	}
	else
		return 39;
}

-(CGFloat ) heightForString:(NSString *) str font:(UIFont *) font width:(CGFloat) w{
    
    CGSize maximumLabelSize = CGSizeMake(w,9999);
    
    CGSize labelSize = [str sizeWithFont:font
                             constrainedToSize:maximumLabelSize
                                 lineBreakMode:NSLineBreakByWordWrapping];
    return labelSize.height;
}

#pragma mark-UserDefineMethods
-(CGSize)getStringSize:(UILabel *)aLabel andString:(NSString *)aStr
{
	CGSize expectedLabelSize;
	if(!aStr ||[aStr isEqualToString:@""])
		return expectedLabelSize;
	NSMutableString *myString = [[NSMutableString alloc] initWithString:aStr];

	CGSize maximumLabelSize = CGSizeMake(aLabel.frame.size.width,9999);

	expectedLabelSize = [myString sizeWithFont:aLabel.font
							 constrainedToSize:maximumLabelSize
								 lineBreakMode:aLabel.lineBreakMode];

	//adjust the label the the new height.
	CGRect newFrame = aLabel.frame;
	newFrame.size.height = expectedLabelSize.height;
	aLabel.frame = newFrame;
	return expectedLabelSize;
}


- (IBAction)onClick_Close:(id)sender {
	[self removeFromSuperview];

}

- (IBAction)onClick_SubmitData:(id)sender
{
	if(listOfItems.allObjects.count)
	{
		[self.delegate mealListPopup:self selectedListofItems:listOfItems.allObjects];
		[self removeFromSuperview];
	}
}

#pragma mark-UserDefineMethods
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
	//[self removeFromSuperview];
}


-(void)submitData:(WSExtraMealsResponse *)aData
{
	if(maelOrExtra)
	{
		if([listOfItems containsObject:aData])
		{
			[listOfItems removeObject:aData];
		}
		else
		{
			[listOfItems addObject:aData];
		}

		[self.tableView reloadData];
	}
	else
	{
        aData.isChild = isChild;
        aData.Description = extraForMealsObj.description;
		[self.delegate mealListPopup:self selectedItem:aData];
		[self removeFromSuperview];
	}
}

-(void)refreshMealList
{
    [self setHidden:YES];
    NSLog(@"extraForMealsObj:%@",extraForMealsObj);
	self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	btnSubmit=[UIButton buttonWithType:UIButtonTypeCustom];
	listOfMeals = extraForMealsObj.extraMealsList;
	maelOrExtra = [extraForMealsObj.maelOrExtra boolValue];

	if(!maelOrExtra)
		btn_Submit.hidden = YES;
	else
		btn_Submit.hidden=NO;
	listOfItems = [NSMutableSet set];
	

	if(maelOrExtra)
	{
		if(extraForMealsObj.selectedExtraMealsResponseList != nil)
		{
			for (id item in extraForMealsObj.selectedExtraMealsResponseList) {

				[listOfItems addObject:item];
			}

		}
	}
	else {
		if (extraForMealsObj.selectedExtraMealsResponse != nil)
		{
			[listOfItems addObject:extraForMealsObj.selectedExtraMealsResponse];
		}
	}
    
       // added by Sarika [26-07-16]
    self.lblExtraDesc.text = extraForMealsObj.description;//@"Extra Desc";
    // ###########################
    
	[self.tableView reloadData];
    [self performSelector:@selector(updateScreenSize) withObject:nil afterDelay:0.2];
}
-(void) updateScreenSize{
    float h = self.tableView.contentSize.height;
    if(h > 370.0)
    {
        self.tableView.frame = CGRectMake(0, 35, self.tableView.frame.size.width, 370);
    }
    else
    {
        self.tableView.frame = CGRectMake(0, 35, self.tableView.frame.size.width, h);
    }
    NSLog(@"h:%f", h);
    
    if([extraForMealsObj isMultiChoice])
    {
        self.constraintSubmitHeight.constant = 40.0;
        self.constraintMainViewHeight.constant = CGRectGetMaxY(self.tableView.frame) + 40;
        
    }
    else{
        self.constraintSubmitHeight.constant = 0.0;
        self.constraintMainViewHeight.constant = CGRectGetMaxY(self.tableView.frame);
    }
    
   
    [self bringSubviewToFront:self.contentViewHolder];
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [self setHidden:NO];
    } completion:^(BOOL finished) {
        [self setHidden:NO];
    }];

}
@end
