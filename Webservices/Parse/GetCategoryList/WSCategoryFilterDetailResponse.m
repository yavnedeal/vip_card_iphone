//
//  WSCategoryFilterDetailResponse.m
//  VIPCard
//
//  Created by SanC on 03/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSCategoryFilterDetailResponse.h"
#import "WSOpeningHoursResponse.h"
#import "WSAddressResponse.h"
#import "WSCouponsResponse.h"

#import "WSCategoryMealResponse.h"
#import "WSCategorTopResponse.h"
#import "GridGalleryResponse.h"
#import "HomeGalleryResponse.h"
#import "WSAdditionalExtrasResponse.h"

@implementation WSCategoryFilterDetailResponse

/*
-(id)initWithDictionary:(NSDictionary *)dict
{
	if([super initWithDictionary:dict])
	{
		[self parseWithDictionary:dict];
	}
	return self;
}
 */

-(void)parseWithDictionary:(NSDictionary *)dict
{
	[super parseWithDictionary:dict];

	NSArray *tempOpeningHours = [dict objectForKey:@"openingHours"];
	if(tempOpeningHours.count)
	{
		NSMutableArray *tempOH = [NSMutableArray array];
		for(NSDictionary *dict in tempOpeningHours)
		{
			WSOpeningHoursResponse *openingHourResponse = [[WSOpeningHoursResponse alloc] initWithDictionary:dict];
			[tempOH addObject:openingHourResponse];
		}
		self.openingHours = tempOH;
	}

	NSArray *tempAddress = [dict objectForKey:@"address"];
	if(tempAddress.count)
	{
		NSMutableArray *tempAdd = [NSMutableArray array];
		for(NSDictionary *dict in tempAddress)
		{
			WSAddressResponse *openingHourResponse = [[WSAddressResponse alloc] initWithDictionary:dict];
			[tempAdd addObject:openingHourResponse];
		}
		self.address = tempAdd;
	}

	NSArray *tempCoupones = [dict objectForKey:@"coupones"];
	if(tempCoupones.count)
	{
		NSMutableArray *tempCoupon = [NSMutableArray array];
		for(NSDictionary *dict in tempCoupones)
		{
			WSCouponsResponse *openingHourResponse = [[WSCouponsResponse alloc] initWithDictionary:dict];
			[tempCoupon addObject:openingHourResponse];
		}
		self.coupones = tempCoupon;
	}

	NSArray *tempDeals = [dict objectForKey:@"deals"];
	if(tempDeals.count)
	{
		NSMutableArray *tempCoupones = [NSMutableArray array];
		for(NSDictionary *dict in tempDeals)
		{
			WSCouponsResponse *openingHourResponse = [[WSCouponsResponse alloc] initWithDictionary:dict];
			[tempCoupones addObject:openingHourResponse];
		}
		self.deals = tempCoupones;
	}



	NSArray *tempCategoryMeal = [dict objectForKey:@"categoryMeal"];
	if(tempDeals.count)
	{
		NSMutableArray *tempCoupones = [NSMutableArray array];
		for(NSDictionary *dict in tempCategoryMeal)
		{
			WSCategoryMealResponse *openingHourResponse = [[WSCategoryMealResponse alloc] initWithDictionary:dict];
			[tempCoupones addObject:openingHourResponse];
		}
		self.categoryMeal = tempCoupones;
	}


	NSArray *tempCategorTop = [dict objectForKey:@"categorTop"];
	if(tempDeals.count)
	{
		NSMutableArray *tempCoupones = [NSMutableArray array];
		for(NSDictionary *dict in tempCategoryMeal)
		{
			WSCategorTopResponse *openingHourResponse = [[WSCategorTopResponse alloc] initWithDictionary:dict];
			[tempCoupones addObject:openingHourResponse];
		}
		self.categorTop = tempCategorTop;
	}

	NSArray *tempGridGallery =[dict objectForKey:@"gridGalleries"];
	if(tempGridGallery.count > 0)
	{
		NSMutableArray *tempGallery =[NSMutableArray array];

		for(NSDictionary *dict in tempGridGallery)
		{
			GridGalleryResponse *openingHourResponse = [[GridGalleryResponse alloc] initWithDictionary:dict];
			[tempGallery addObject:openingHourResponse];
		}
		self.gridGalleries=tempGallery;
	}

	NSArray *tempHomeGallery =[dict objectForKey:@"homeGalleries"];
	if(tempHomeGallery.count > 0)
	{
		NSMutableArray *tempGallery =[NSMutableArray array];

		for(NSDictionary *dict in tempHomeGallery)
		{
			HomeGalleryResponse *openingHourResponse = [[HomeGalleryResponse alloc] initWithDictionary:dict];
			[tempGallery addObject:openingHourResponse];
		}
		self.homeGalleries=tempGallery;

	}

	  NSArray *tempAdditionalExtra = [dict objectForKey:@"aditionalExtras"];
	 if(tempAdditionalExtra.count)
	 {
	 NSMutableArray *tempAExtra = [NSMutableArray array];
	 for(NSDictionary *dict in tempAdditionalExtra)
	 {
	 WSAdditionalExtrasResponse *openingHourResponse = [[WSAdditionalExtrasResponse alloc] initWithDictionary:dict];
	 [tempAExtra addObject:openingHourResponse];
	 }
	 self.aditionalExtras = tempAExtra;
	 }
}


@end
