//
//  WSCategoryFilterResponse.h
//  VIPCard
//
//  Created by SanC on 21/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSCategoryFilterResponse : WSBaseData
@property (nonatomic, strong) NSArray *categoryList;
@property (nonatomic, strong) NSString *id,*name;
//id = 1;
//name = "\U05d0\U05d1\U05d9\U05d6\U05e8\U05d9 \U05d0\U05d5\U05e4\U05e0\U05d4";

@end
