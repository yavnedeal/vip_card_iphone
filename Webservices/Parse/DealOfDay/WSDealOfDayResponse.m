//
//  WSDealOfDayResponse.m
//  VIPCard
//
//  Created by Vishal Kolhe on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSDealOfDayResponse.h"
#import "WSCouponsResponse.h"

@implementation WSDealOfDayResponse
@synthesize name, id, couponsList;

/*
-(id)initWithDictionary:(NSDictionary *)dict
{
    if([super initWithDictionary:dict])
    {
        [self populateFromDictionary:dict];
    }
    return self;
}
 */

-(void)populateFromDictionary:(NSDictionary *)dict
{
    //Prase JSON
    //coupons
    [super populateFromDictionary:dict];
    
    NSArray *tempCouponsList = [dict objectForKey:kCouponsList];
    if(tempCouponsList.count)
    {
        NSMutableArray *tempHolder = [NSMutableArray array];
        //List exsists
        for(NSDictionary *dic in tempCouponsList)
        {
            WSCouponsResponse *couponObj = [[WSCouponsResponse alloc] initWithDictionary:dic];
            [tempHolder addObject:couponObj];
        }
        self.couponsList = tempHolder;
    }
}
@end