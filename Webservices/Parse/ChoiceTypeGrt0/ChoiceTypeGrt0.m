//
//  ChoiceTypeGrt0.m
//  VIPCard
//
//  Created by SanC on 16/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "ChoiceTypeGrt0.h"
#import "WSExtraForMealsResponse.h"

@implementation ChoiceTypeGrt0
@synthesize array_ChoiceTypeGrt0 = _array_ChoiceTypeGrt0,array_DistChoiceNames = _array_DistChoiceNames,isMendatory;


-(void) setArray_ChoiceTypeGrt0:(NSArray *)array_ChoiceTypeGrt0
{
	_array_ChoiceTypeGrt0 = array_ChoiceTypeGrt0;
	//now get distinct name
	_array_DistChoiceNames = [array_ChoiceTypeGrt0 valueForKeyPath:@"@distinctUnionOfObjects.nameChoice"];

}


//-(NSDictionary *)parseDataByNameChoice:(NSArray*)array_Choice:(NSArray*)array_name
//{
//
//	NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
//
//	for (int i=0; i<array_name.count; i++)
//    {
//		NSString *str_name=[array_name objectAtIndex:i];
//
//		for (int i=0; i<array_Choice.count; i++)
//		{
//			WSExtraForMealsResponse *response=[array_Choice objectAtIndex:i];
//			NSString *str_IsChoice=response.isChoice;
//			if ([str_name isEqualToString:str_IsChoice])
//			{
//				NSArray *filtered = [array_Choice filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(isChoice == %@)", str_name]];
//				[dict setObject:filtered forKey:str_name];
//			}
//		}
//	}
//	[self parseDataarray:array_Choice];
//	return dict;
//}


-(NSMutableDictionary *)parseDataarray:(NSArray*)array
{
	NSMutableDictionary *dic_final=[[NSMutableDictionary alloc] init];
	NSSet *array_choiceList;
	for (int i=0; i<array.count; i++)
    {
		WSExtraForMealsResponse *response=[array objectAtIndex:i];
		NSString *str_name=response.nameChoice;
		for (int i=0; i<array.count; i++)
		{
			WSExtraForMealsResponse *response=[array objectAtIndex:i];
			if ([response.nameChoice isEqualToString:str_name])
			{
				NSArray *filtered = [array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(nameChoice == %@)", str_name]];
				array_choiceList=[NSSet setWithArray:filtered];
			}
		}
        if(array_choiceList)
		  [dic_final setObject:array_choiceList forKey:str_name];
	}
	return dic_final;
}

-(void) resetSelection {
   if(_array_ChoiceTypeGrt0)
   {
       for (WSExtraForMealsResponse *item in _array_ChoiceTypeGrt0)
       {
           item.selectedExtraMealsResponse = nil;
           item.selectedExtraMealsResponseList = [NSArray array];
       }
   }

    
}




-(NSArray *) arrayByChoiceName:(NSString *)strChoice
{

   return  [_array_ChoiceTypeGrt0 filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(nameChoice == %@)", strChoice]];

}

@end
