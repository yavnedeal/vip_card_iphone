//
//  ChoiceTypeGrt0.h
//  VIPCard
//
//  Created by SanC on 16/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface ChoiceTypeGrt0 : WSBaseData
@property(nonatomic,strong)NSArray *array_ChoiceTypeGrt0;

@property(nonatomic,strong)NSArray *array_DistChoiceNames;

@property(nonatomic,strong)NSString *isChoiceValue;
@property(assign)BOOL isMendatory;

//-(NSDictionary *)parseDataByNameChoice:(NSArray*)array_Choice:(NSArray*)array_name;
-(NSDictionary *)parseDataarray:(NSArray*)array;

-(NSArray *) arrayByChoiceName:(NSString *)strChoice;

-(void) resetSelection;

@end
