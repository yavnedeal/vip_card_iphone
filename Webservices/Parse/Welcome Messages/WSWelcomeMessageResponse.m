//
//  WSWelcomeMessageResponse.m
//  Chain
//
//  Created by Andrei Boulgakov on 15/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WSWelcomeMessageResponse.h"

@implementation WSWelcomeMessageResponse
-(void) parseWithDictionary:(NSDictionary *)dict
{
    [super parseWithDictionary:dict];
    if(self.respondeBody && [self.respondeBody isKindOfClass:[NSArray class]])
    {
        NSMutableArray *array = [NSMutableArray array];
		for(NSDictionary *itemDic in self.respondeBody)
		{
			WelcomeMessagesWS *welcomeMessage = [[WelcomeMessagesWS alloc] initWithDictionary:itemDic];
			[array addObject:welcomeMessage];
		}
		self.welcomeMessageList = [NSArray arrayWithArray:array];
    }
    else if(self.respondeBody && [self.respondeBody isKindOfClass:[NSDictionary class]])
    {
        self.welcomeMessages = [[WelcomeMessagesWS alloc] initWithDictionary:self.respondeBody];
    }
}
@end
