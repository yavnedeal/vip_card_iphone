//
//  WSWelcomeMessageResponse.h
//  Chain
//
//  Created by Andrei Boulgakov on 15/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WSBaseResponse.h"
#import "WelcomeMessagesWS.h"

@interface WSWelcomeMessageResponse : WSBaseResponse
@property (nonatomic,strong) WelcomeMessagesWS *welcomeMessages;
@property (nonatomic, strong) NSArray *welcomeMessageList;
@end
