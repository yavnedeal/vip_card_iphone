//
//  WSDealsResponse.h
//  VIPCard
//
//  Created by Vishal Kolhe on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSDealsResponse : WSBaseData
//id : "1"
//name : "הכל"

@property (nonatomic, strong) NSString *id, *name;
@property (nonatomic, strong) NSArray *couponsList;

//-(id)initWithDictionary:(NSDictionary *)dict;

@end
