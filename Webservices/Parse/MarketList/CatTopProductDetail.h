//
//  CatTopProductDetail.h
//  VIPCard
//
//  Created by Vishal Kolhe on 20/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"
#import "CatTopProduct.h"

@interface CatTopProductDetail : WSBaseData

//Id : "1282"
//BarCode : "30"
//IdMarketDeals : "1"
//IdCategoryProduct : "47"
//Name : "מדליק פחם מוצק אינטרסאן"
//Image : ""
//Description : "easy light"
//SubDescription : "2 יח' ב-9 ₪"
//Limitations : ""
//Price : "9"
//Price2 : "0"
//id : "1354"
//idCategoryTop : "47"
//idMarket : "1"
//name : "יינות ביתן"
//Logo : "ybitan.png"
//DateExpert : "2016-05-21"
//limitations : "מבצעי הבשר/הדגים/העופות מוגבלים ל-3 ק”ג, אלא אם צוין אחרת, ומותנים בקנייה ב-100 ₪ ומעלה של הפריטים שלא משתתפים במבצע ואינם חלים על קניית מוצרי סיגריות וטבק . המחירים אינם תקפים לאינטרנט  .  מינימום 1,000 יח‘/ק“ג למבצע . אין מכירה בסיטונאות  . החברה רשאית להפסיק/לשנות את המבצע בכל עת  . מכירת אלכוהול מותרת למי שמלאו לו 18 שנים ומעלה  . המבצעים תקפים בין התאריכים 15-21.5.16 או עד גמר המלאי  . התמונות להמחשה בלב

//-(id)initWithDictionary:(NSDictionary *)dict;

@property(strong,nonatomic)NSString *Id,*BarCode,*IdMarketDeals,*IdCategoryProduct,*Name,*Image,*Description,*SubDescription,*Limitations,*Price,*Price2,*id,*idCategoryTop,*idMarket,*bName,*Logo,*DateExpert,*mLimitations;

@end
