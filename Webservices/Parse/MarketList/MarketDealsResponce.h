//
//  MarketDealsResponce.h
//  VIPCard
//
//  Created by Vishal Kolhe on 19/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface MarketDealsResponce : WSBaseData

//id : "1"
//idbusiness : "18"
//limitation : "מבצעי הבשר/הדגים/העופות מוגבלים ל-3 ק”ג, אלא אם צוין אחרת, ומותנים בקנייה ב-100 ₪ ומעלה של הפריטים שלא משתתפים במבצע ואינם חלים על קניית מוצרי סיגריות וטבק . המחירים אינם תקפים לאינטרנט  .  מינימום 1,000 יח‘/ק“ג למבצע . אין מכירה בסיטונאות  . החברה רשאית להפסיק/לשנות את המבצע בכל עת  . מכירת אלכוהול מותרת למי שמלאו לו 18 שנים ומעלה  . המבצעים תקפים בין התאריכים 15-21.5.16 או עד גמר המלאי  . התמונות להמחשה בלבד  . ט.ל.ח"
//dateStrat : "2016-05-14"
//dateExpert : "2016-05-21"
//nameB : "יינות ביתן"
//logoB : "ybitan.png"



@property (strong ,nonatomic) NSString *id,*idbusiness,*limitation,*dateStrat,*dateExpert,*nameB,*logoB;

@property(strong,nonatomic)NSArray *marketlist;
//-(id)initWithDictionary:(NSDictionary *)dict;
@end
