//
//  ChoiceType0.h
//  VIPCard
//
//  Created by SanC on 16/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface ChoiceType0 : WSBaseData
@property(nonatomic,strong)NSArray *array_Choicetype0;

@end
