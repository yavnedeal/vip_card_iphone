//
//  BusinessCatResponse.h
//  VIPCard
//
//  Created by SanC on 26/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"
@class WSBussiness;

@interface WSBusinessCatResponse : WSBaseData


@property(nonatomic,strong) NSMutableArray *meals;
@property(nonatomic,strong) WSBussiness *business;
@end
