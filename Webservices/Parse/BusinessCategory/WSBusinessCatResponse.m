//
//  BusinessCatResponse.m
//  VIPCard
//
//  Created by SanC on 26/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBusinessCatResponse.h"
#import "WSBussiness.h"
#import "WSExtraForMealsResponse.h"
#import "WSCategoryMealResponse.h"

@implementation WSBusinessCatResponse



-(void)populateFromDictionary:(NSDictionary *)dict
{
	//Prase JSON
	//coupons
    id param =  [dict objectForKey:@"business"];
    if(param)
    {
	 self.business = [[WSBussiness alloc] initWithDictionary:(NSDictionary *) param];
    }

	NSArray *mealsList = [dict objectForKey:@"meals"];
	if(mealsList && mealsList.count)
	{
		NSMutableArray *tempList = [NSMutableArray array];
		//List exsists
		//for(NSDictionary *dic in mealsList)
		//{
			//WSExtraForMealsResponse *couponObj = [[WSExtraForMealsResponse alloc] initWithDictionary:dic];
            //SanC - New only one category will come 
            WSCategoryMealResponse *couponObj = [[WSCategoryMealResponse alloc] initWithDictionary:dict];
			[tempList addObject:couponObj];
		//}

		self.meals = tempList;
	}
}



@end
