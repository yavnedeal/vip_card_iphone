//
//  WSMealsResponse.h
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"
#import "WSExtraForMealsResponse.h"

@interface WSMealsResponse : WSBaseData
//"id":"216",
//"name":"תמונת קנוואס ",
//"idCategoryForBusiness":"1",
//"image":"canvas-1.jpg",
//"description":"ניתן לשדרג במידות שונות",
//"price":"65",
//"extraForMeals":{
//}

@property (strong , nonatomic) NSString *id,*name,*idCategoryForBusiness,*image,*description,
*price;

@property (nonatomic, strong) NSArray *extraForMealsList;
@property (nonatomic, strong) NSDictionary *origDictionary;

-(WSMealsResponse *) copyObject;
-(NSDictionary *)dictionaryFromMenu:(WSMealsResponse*)menu;
-(void)populateData:(NSDictionary *)dict;

@end
