//
//  WSCategoryMealResponse.m
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSCategoryMealResponse.h"
#import "WSMealsResponse.h"

@implementation WSCategoryMealResponse
@synthesize id, idBusiness,idCategorySub,description,name;

/*
-(id)initWithDictionary:(NSDictionary *)dict
{
    if([super initWithDictionary:dict])
    {
        [self parseWithDictionary:dict];
    }
    return self;
}
*/
-(void)populateFromDictionary:(NSDictionary *)dict
{
    [super populateFromDictionary:dict];
    
    NSArray *tempOpeningHours = [dict objectForKey:@"meals"];
    if(tempOpeningHours.count)
    {
        NSMutableArray *tempOH = [NSMutableArray array];
        for(NSDictionary *dict in tempOpeningHours)
        {
            WSMealsResponse *openingHourResponse = [[WSMealsResponse alloc] initWithDictionary:dict];
            [tempOH addObject:openingHourResponse];
        }
        self.mealsList = tempOH;
    }
}
@end
