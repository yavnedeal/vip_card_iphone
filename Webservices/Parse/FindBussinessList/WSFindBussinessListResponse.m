//
//  WSFindBussinessListResponse.m
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSFindBussinessListResponse.h"
#import "WSOpeningHoursResponse.h"
#import "WSAdditionalExtrasResponse.h"
#import "WSAddressResponse.h"

#import "WSCategorTopResponse.h"
#import "WSCategoryMealResponse.h"

#import "WSAdditionalExtrasResponse.h"
#import "WSCouponsResponse.h"

#import "GridGalleryResponse.h"
#import "HomeGalleryResponse.h"


@implementation WSFindBussinessListResponse
@synthesize id, name, fax, phone, phone2, mobile, email, idAddress,idTypeBusiness,
codeBusiness,vipDiscount,facebook,site,sendFax,sendEmail,sendSms,takeAway,maxSeats,delivery,minDelivery,
event,logo,des,image,about;



-(void)populateFromDictionary:(NSDictionary *)dict
{
    [super populateFromDictionary:dict];
    NSArray *tempOpeningHours = [dict objectForKey:@"openingHours"];
    if(tempOpeningHours.count)
    {
        NSMutableArray *tempOH = [NSMutableArray array];
        for(NSDictionary *dict in tempOpeningHours)
        {
            WSOpeningHoursResponse *openingHourResponse = [[WSOpeningHoursResponse alloc] initWithDictionary:dict];
            [tempOH addObject:openingHourResponse];
        }
        self.openingHoursList = tempOH;
    }
    
    NSArray *tempAddress = [dict objectForKey:@"address"];
    if(tempAddress.count)
    {
        NSMutableArray *tempAdd = [NSMutableArray array];
        for(NSDictionary *dict in tempAddress)
        {
            WSAddressResponse *openingHourResponse = [[WSAddressResponse alloc] initWithDictionary:dict];
            [tempAdd addObject:openingHourResponse];
        }
        self.addressList = tempAdd;
    }
    
    NSArray *tempCoupones = [dict objectForKey:@"coupones"];
    if(tempCoupones.count)
    {
        NSMutableArray *tempCoupon = [NSMutableArray array];
        for(NSDictionary *dict in tempCoupones)
        {
            WSCouponsResponse *openingHourResponse = [[WSCouponsResponse alloc] initWithDictionary:dict];
            [tempCoupon addObject:openingHourResponse];
        }
        self.couponesList = tempCoupon;
    }
    
    NSArray *tempDeals = [dict objectForKey:@"deals"];
    if(tempDeals.count)
    {
        NSMutableArray *tempCoupones = [NSMutableArray array];
        for(NSDictionary *dict in tempDeals)
        {
            WSCouponsResponse *openingHourResponse = [[WSCouponsResponse alloc] initWithDictionary:dict];
            [tempCoupones addObject:openingHourResponse];
        }
        self.dealsList = tempCoupones;
    }
    
    NSArray *tempCategoryMeal = [dict objectForKey:@"categoryMeal"];
    if(tempDeals.count)
    {
        NSMutableArray *tempCoupones = [NSMutableArray array];
        for(NSDictionary *dict in tempCategoryMeal)
        {
            WSCategoryMealResponse *openingHourResponse = [[WSCategoryMealResponse alloc] initWithDictionary:dict];
            [tempCoupones addObject:openingHourResponse];
        }
        self.categoryMealList = tempCoupones;
    }
    
    NSArray *tempCategorTop = [dict objectForKey:@"categorTop"];
    if(tempDeals.count)
    {
        NSMutableArray *tempCoupones = [NSMutableArray array];
        for(NSDictionary *dict in tempCategoryMeal)
        {
            WSCategorTopResponse *openingHourResponse = [[WSCategorTopResponse alloc] initWithDictionary:dict];
            [tempCoupones addObject:openingHourResponse];
        }
        self.categorTopList = tempCategorTop;
    }
    
	NSArray *tempGridGallery =[dict objectForKey:@"gridGalleries"];
	if(tempGridGallery.count > 0)
	{
		NSMutableArray *tempGallery =[NSMutableArray array];

		for(NSDictionary *dict in tempGridGallery)
		{
			GridGalleryResponse *openingHourResponse = [[GridGalleryResponse alloc] initWithDictionary:dict];
			[tempGallery addObject:openingHourResponse];
		}
	}

	NSArray *tempHomeGallery =[dict objectForKey:@"homeGalleries"];
	if(tempHomeGallery.count > 0)
	{
		NSMutableArray *tempGallery =[NSMutableArray array];

		for(NSDictionary *dict in tempHomeGallery)
		{
			HomeGalleryResponse *openingHourResponse = [[HomeGalleryResponse alloc] initWithDictionary:dict];
			[tempGallery addObject:openingHourResponse];
		}
	}

    NSArray *tempAdditionalExtra = [dict objectForKey:@"aditionalExtras"];
    if(tempAdditionalExtra.count)
    {
        NSMutableArray *tempAExtra = [NSMutableArray array];
        for(NSDictionary *dict in tempAdditionalExtra)
        {
            WSAdditionalExtrasResponse *openingHourResponse = [[WSAdditionalExtrasResponse alloc] initWithDictionary:dict];
            [tempAExtra addObject:openingHourResponse];
        }
        self.aditionalExtrasList = tempAdditionalExtra;
    }
}
@end
