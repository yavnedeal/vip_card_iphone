//
//  HomeGalleryResponse.m
//  VIPCard
//
//  Created by SanC on 14/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#define SAFE(src) \
src!=nil?src:NSNULL

#define SAFE_DEF(value, default) ((value==nil)?(default):(value))
#define NSNULL [NSNull null]
#define IsNULL(val) (val==nil || [val isKindOfClass:[NSNull class]])

#import "GridGalleryResponse.h"

@implementation GridGalleryResponse
@synthesize id,name,NewName,IdBusiness,PathDirectory,TypeGallery;


/*
- (id) initWithDictionary:(NSDictionary *)dict
{
	self = [super init];
	if(self)
		[self parseWithDictionary:dict];

	return self;
}

-(void) parseWithDictionary:(NSDictionary *)dict
{
	[self populateFromDictionary:dict];
}

- (void)populateFromDictionary:(NSDictionary *)dict
{
	unsigned int outCount, i;
	objc_property_t *properties = class_copyPropertyList([self class], &outCount);
	for (i = 0; i < outCount; i++)
	{
		objc_property_t property = properties[i];
		NSString *propertyName = [[NSString alloc] initWithUTF8String:property_getName(property)];

		id propertyValue = [dict valueForKey:(NSString *)propertyName];
		//now check if value is empty then ignore field

		if(propertyValue)
		{
			if(IsNULL(propertyValue))
			{
				propertyValue = @"";
			}
			[self  setValue:SAFE_DEF(propertyValue, @"") forKey:propertyName];
		}
	}
}
*/

@end
