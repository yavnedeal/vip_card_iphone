//
//  WSDeliveryTimeResponse.m
//  VIPCard
//
//  Created by Enovate Macbook Pro on 04/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSDeliveryTimeResponse.h"
#import "WSBZDeliveryTime.h"

@implementation WSDeliveryTimeResponse

-(void) populateFromDictionary:(NSDictionary *)dict
{
    id param = [dict objectForKey:@"results"];
    
    if(param && [param isKindOfClass:[NSArray class]])
    {
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *d in param) {
            WSBZDeliveryTime *time = [[WSBZDeliveryTime alloc] initWithDictionary:d];
            [array addObject:time];
        }
        
        self.deliveryTimeList = [NSArray arrayWithArray:array];
        
    }
    else
    {
        self.deliveryTimeList = [NSArray array];
    }
    
}

@end
