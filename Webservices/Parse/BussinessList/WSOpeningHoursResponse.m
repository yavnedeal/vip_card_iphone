//
//  WSOpeningHoursResponse.m
//  VIPCard
//
//  Created by Vishal Kolhe on 27/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSOpeningHoursResponse.h"

@implementation WSOpeningHoursResponse
@synthesize Id, IdBusiness, HourOpen, HourClose, HourOpen2, HourClose2, IdDay,Day;

/*
-(id)initWithDictionary:(NSDictionary *)dict
{
    if([super initWithDictionary:dict])
    {
        [self parseWithDictionary:dict];
    }
    
    return self;
}

-(void)parseWithDictionary:(NSDictionary *)dict
{
    [super parseWithDictionary:dict];
}
 */

@end
