//
//  WSBussiness.m
//  VIPCard
//
//  Created by Vishal Kolhe on 27/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBussiness.h"
#import "WSOpeningHoursResponse.h"
#import "WSAdditionalExtrasResponse.h"
#import "WSAddressResponse.h"
#import "WSBussiness.h"
#import "WSBZDeliveryTime.h"

@implementation WSBussiness
@synthesize openingHoursList, addressList, aditionalExtrasList;
@synthesize id, name, fax, phone, phone2, mobile, idAddress, idTypeBusiness, codeBusiness, vipDiscount, facebook, site, sendFax, sendEmail, sendSms, takeAway, maxSeats, delivery, minDelivery, event, logo, des, image,orderphone;

/*
-(id)initWithDictionary:(NSDictionary *)dict
{
    if([super initWithDictionary:dict])
    {
        [self parseWithDictionary:dict];
    }
    return self;
}
 */

-(void)populateFromDictionary:(NSDictionary *)dict
{
    //CHECK WITH TEAM
   // [super populateFromDictionary:dict];
    
    NSDictionary *firstDict = [dict objectForKey:@"0"];
    if(firstDict)
      [super populateFromDictionary:firstDict];
    else
       [super populateFromDictionary:dict];
    
    NSArray *tempOpeningHours = [dict objectForKey:@"openingHours"];
    if(tempOpeningHours.count)
    {
        NSMutableArray *tempOH = [NSMutableArray array];
        for(NSDictionary *dict in tempOpeningHours)
        {
            WSOpeningHoursResponse *openingHourResponse = [[WSOpeningHoursResponse alloc] initWithDictionary:dict];
            [tempOH addObject:openingHourResponse];
        }
        self.openingHoursList = tempOH;
    }
    
    NSArray *tempAddress = [dict objectForKey:@"address"];
    if(tempAddress.count)
    {
        NSMutableArray *tempAddress = [NSMutableArray array];
        for(NSDictionary *dict in tempAddress)
        {
            WSAddressResponse *openingHourResponse = [[WSAddressResponse alloc] initWithDictionary:dict];
            [tempAddress addObject:openingHourResponse];
        }
        self.addressList = tempAddress;
    }
    
    NSArray *tempAdditionalExtra = [dict objectForKey:@"aditionalExtras"];
    if(tempAdditionalExtra && [tempAdditionalExtra count] > 0)
    {
        NSMutableArray *tempAExtra = [NSMutableArray array];
        for(NSDictionary *dict in tempAdditionalExtra)
        {
            WSAdditionalExtrasResponse *openingHourResponse = [[WSAdditionalExtrasResponse alloc] initWithDictionary:dict];
            [tempAExtra addObject:openingHourResponse];
        }
        self.aditionalExtrasList = tempAExtra;
    }
    else
    {
        self.aditionalExtrasList = [NSArray array];
    }
}

-(void)populateData:(NSDictionary *)dict
{
    //CHECK WITH TEAM
    // [super populateFromDictionary:dict];
    
    NSDictionary *firstDict = [dict objectForKey:@"0"];
    if(firstDict)
        [super populateFromDictionary:firstDict];
    else
        [super populateFromDictionary:dict];
    
    NSArray *tempOpeningHours = [dict objectForKey:@"openingHoursList"];
    if(tempOpeningHours.count)
    {
        NSMutableArray *tempOH = [NSMutableArray array];
        for(NSDictionary *dict in tempOpeningHours)
        {
            WSOpeningHoursResponse *openingHourResponse = [[WSOpeningHoursResponse alloc] initWithDictionary:dict];
            [tempOH addObject:openingHourResponse];
        }
        self.openingHoursList = tempOH;
    }
    
    NSArray *tempAddress = [dict objectForKey:@"addressList"];
    if(tempAddress.count)
    {
        NSMutableArray *tempAddress = [NSMutableArray array];
        for(NSDictionary *dict in tempAddress)
        {
            WSAddressResponse *openingHourResponse = [[WSAddressResponse alloc] initWithDictionary:dict];
            [tempAddress addObject:openingHourResponse];
        }
        self.addressList = tempAddress;
    }
    
    NSArray *tempAdditionalExtra = [dict objectForKey:@"aditionalExtrasList"];
    if(tempAdditionalExtra && [tempAdditionalExtra count] > 0)
    {
        NSMutableArray *tempAExtra = [NSMutableArray array];
        for(NSDictionary *dict in tempAdditionalExtra)
        {
            WSAdditionalExtrasResponse *openingHourResponse = [[WSAdditionalExtrasResponse alloc] initWithDictionary:dict];
            [tempAExtra addObject:openingHourResponse];
        }
        self.aditionalExtrasList = tempAExtra;
    }
    else
    {
        self.aditionalExtrasList = [NSArray array];
    }
    NSArray *deliveryTimeList = [dict objectForKey:@"deliveryTimeList"];
    if(deliveryTimeList.count)
    {
        NSMutableArray *tempDeliv = [NSMutableArray array];
        for(NSDictionary *dict in deliveryTimeList)
        {
            WSBZDeliveryTime *deliveryTime = [[WSBZDeliveryTime alloc] initWithDictionary:dict];
            [tempDeliv addObject:deliveryTime];
        }
        self.deliveryTimeList = tempDeliv;
    }
}

-(NSDictionary*)WSBusinessToDic:(WSBussiness*)business
{
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:business.id,@"id",business.name,@"name",business.fax,@"fax",business.phone,@"phone",business.phone2,@"phone2",business.mobile,@"mobile",business.idAddress,@"idAddress",business.idTypeBusiness,@"idTypeBusiness",business.codeBusiness,@"codeBusiness",business.vipDiscount,@"vipDiscount",business.facebook,@"facebook",business.site,@"site",business.sendFax,@"sendFax",business.sendEmail,@"sendEmail",business.sendSms,@"sendSms",business.takeAway,@"takeAway",business.maxSeats,@"maxSeats",business.delivery,@"delivery",business.minDelivery,@"minDelivery",business.event,@"event",business.logo,@"logo",business.des,@"des",business.image,@"image",business.orderphone,@"orderphone",business.addressList,@"addressList",business.seletectedListItem,@"seletectedListItem",nil];
    NSMutableDictionary *dicmain = [dic mutableCopy];
    
    if (business.aditionalExtrasList.count > 0) {
        NSMutableArray *arr = [NSMutableArray array];
        for (WSAdditionalExtrasResponse *extra in business.aditionalExtrasList) {
            [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:extra.id,@"id",extra.name,@"name",extra.price,@"price",extra.idBusiness,@"idBusiness",[NSNumber numberWithBool:extra.selected],@"selected", nil]];
        }
        [dicmain setObject:arr forKey:@"aditionalExtrasList"];
    }
    if (business.deliveryTimeList.count > 0) {
        NSMutableArray *arr = [NSMutableArray array];
        for (WSBZDeliveryTime *deliv in business.deliveryTimeList) {
            [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:deliv.Id,@"Id",deliv.IdBusiness,@"IdBusiness",deliv.OpenHour,@"OpenHour",deliv.CloseHour,@"CloseHour",deliv.IdDay,@"IdDay", nil]];
        }
        [dicmain setObject:arr forKey:@"deliveryTimeList"];
    }
    if (business.openingHoursList.count > 0) {
        NSMutableArray *arr = [NSMutableArray array];
        for (WSOpeningHoursResponse *openhr in business.openingHoursList) {
            [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:openhr.Id,@"Id",openhr.IdBusiness,@"IdBusiness",openhr.HourOpen,@"HourOpen",openhr.HourClose,@"HourClose",openhr.HourOpen2,@"HourOpen2",openhr.HourClose2,@"HourClose2",openhr.IdDay,@"IdDay",openhr.Day,@"Day", nil]];
        }
        [dicmain setObject:arr forKey:@"openingHoursList"];
    }
    
    return dicmain;
}
@end
