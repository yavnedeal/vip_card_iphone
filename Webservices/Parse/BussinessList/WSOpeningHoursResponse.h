//
//  WSOpeningHoursResponse.h
//  VIPCard
//
//  Created by Vishal Kolhe on 27/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSOpeningHoursResponse : WSBaseData

/*
 Id : "6"
 IdBusiness : "2"
 HourOpen : "11:30"
 HourClose : "23:59:00"
 HourOpen2 : null
 HourClose2 : null
 IdDay : "10"
 Day : "\U05d5"
*/

@property (nonatomic, strong) NSString *Id, *IdBusiness, *HourOpen, *HourClose, *HourOpen2, *HourClose2, *IdDay, *Day;

//-(id)initWithDictionary:(NSDictionary *)dict;

@end
