//
//  WSDeliveryTimeResponse.h
//  VIPCard
//
//  Created by Enovate Macbook Pro on 04/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSDeliveryTimeResponse : WSBaseData

@property(nonatomic,strong) NSArray *deliveryTimeList;

@end
