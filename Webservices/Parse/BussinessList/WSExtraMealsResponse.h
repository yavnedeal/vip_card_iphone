//
//  WSExtraMealsResponse.h
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"
#import "WSExtraForMealsResponse.h"

@interface WSExtraMealsResponse : WSBaseData

/*
 id	:	183
 
 Name	:	\\u05e4\\u05d9\\u05e6\\u05d4 \\u05d8\\u05d1\\u05e2\\u05d5\\u05e0\\u05d9\\u05ea
 
 price	:	3
 
 IdTypeExtra	:
*/

@property (nonatomic, strong) NSString *id, *Name, *price, *IdTypeExtra, *Description,*extraForMealPrice;
@property (nonatomic) BOOL isChild,selected;
//-(id)initWithDictionary:(NSDictionary *)dict;

@end
