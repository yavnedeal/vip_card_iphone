//
//  WSBussiness.h
//  VIPCard
//
//  Created by Vishal Kolhe on 27/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"


@interface WSBussiness : WSBaseData
/*
 id : "2"
 name : "סושי וויט"
 fax : null
 phone : "08-9328888"
 phone2 : null
 mobile : "050-4226036"
 email : null
 idAddress : "4"
 idTypeBusiness : "7"
 codeBusiness : "55555"
 vipDiscount : "10"
 facebook : "וויט סושי בר White Sushi Bar"
 site : null
 sendFax : "1"
 sendEmail : "0"
 sendSms : "1"
 takeAway : "1"
 maxSeats : "20"
 delivery : "15"
 minDelivery : "59"
 event : "0"
 logo : "whiteLogo.jpg"
 des : "אוכל אסיאתי"
 image : "whiteTop.jpg"
*/

@property (nonatomic, strong) NSString *id, *name, *fax, *phone, *phone2, *mobile, *idAddress, *idTypeBusiness, *codeBusiness, *vipDiscount, *facebook, *site, *sendFax, *sendEmail, *sendSms, *takeAway, *maxSeats, *delivery, *minDelivery, *event, *logo, *des, *image,*orderphone;

@property (nonatomic, strong) NSArray *openingHoursList, *addressList, *aditionalExtrasList,*deliveryTimeList;

@property (nonatomic, strong) NSArray *seletectedListItem;

-(NSDictionary*)WSBusinessToDic:(WSBussiness*)business;
-(void)populateData:(NSDictionary *)dict;
//-(id)initWithDictionary:(NSDictionary *)dict;
@end
