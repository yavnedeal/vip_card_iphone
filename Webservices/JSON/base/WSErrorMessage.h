//
//  WSErrorMessage.h
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WSBaseData.h"

@interface WSErrorMessage : WSBaseData

@property (nonatomic,strong) NSString *error_code;
@property (nonatomic,strong) NSString *error_msg;

-(NSString *) errorMessage;


@end
