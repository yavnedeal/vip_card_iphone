//
//  WSBaseRequest.m
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WSBaseRequest.h"

@implementation WSBaseRequest
@synthesize requestParams;

static WSResponseHeader *header;

-(id)initWithParameters:(NSDictionary *) params
{
    self = [super init];
    if(self)
    {
        self.requestParams = params;
    }
    return self;
}

+(void) setLoggedUserHeader:(WSResponseHeader *)resHeader
{
    header = resHeader;
}

- (NSMutableDictionary *)getRequestData {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    if(requestParams && [requestParams objectForKey:@"header"] != nil && [[requestParams objectForKey:@"header"] isKindOfClass:[NSDictionary class]])
    {
        return [NSMutableDictionary dictionaryWithDictionary:requestParams];
    }
    if(header)
    {
        [dict setObject:[header getDictionaryFromObject] forKey:@"header"];
        if(requestParams)
            [dict setObject:requestParams forKey:@"data"];
    }
    else if(requestParams)
        dict = [NSMutableDictionary dictionaryWithDictionary:self.requestParams];
    
    return dict;
}

@end
