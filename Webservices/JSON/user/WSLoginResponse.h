//
//  WSLoginResponse.h
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WSBaseResponse.h"
//#import "UserJSONWS.h"

@interface WSLoginResponse : WSBaseResponse

//@property (nonatomic,strong) UserJSONWS *user;

@end
