//
//  Fcatorycd+CoreDataProperties.m
//  VIPCard
//
//  Created by Syntel-Amargoal1 on 11/9/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Fcatorycd+CoreDataProperties.h"

@implementation Fcatorycd (CoreDataProperties)

@dynamic factoryID;
@dynamic name;

@end
