//
//  ConnectionsManager.m
//  Chain
//
//  Created by Nava Carmon on 4/25/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "ConnectionsManager.h"
#import "AppDelegate.h"
#import "WSBaseRequest.h"
#import "WSBaseResponse.h"
#import "DejalActivityView.h"


static NSString * const BaseURLString = ApplicationBaseUrl;
@interface ConnectionsManager ()

@property (nonatomic, strong) NSDictionary *response;

- (void) postToURL:(NSString *)url withParameters:(NSDictionary *)parameters delegate:(id<ServerResponseDelegate>)delegate;

@end

@implementation ConnectionsManager
// Singletone related

+ (instancetype)sharedManager {
    
    static ConnectionsManager *_sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] initWithBaseURL:[NSURL URLWithString:BaseURLString]];
    });
    
    return _sharedManager;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        AFSecurityPolicy *policy = [[AFSecurityPolicy alloc] init];
        [policy setAllowInvalidCertificates:YES];
        
        [self setSecurityPolicy:policy];
        self.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    return self;
}

-(void) endBackgroundTask{
    UIApplication *application = [UIApplication sharedApplication];
    AppDelegate *appDelegateRef = (AppDelegate *)application.delegate;
    if(appDelegateRef && appDelegateRef.backgroundTaskIdentifier && appDelegateRef.backgroundTaskIdentifier != UIBackgroundTaskInvalid)
        [application endBackgroundTask:appDelegateRef.backgroundTaskIdentifier];
}

- (void) postToURL:(NSString *)url withParameters:(NSDictionary *)parameters delegate:(id<ServerResponseDelegate>)delegate
{
    
    @autoreleasepool{
        NSString *urlString = url;
        //createGroup
        
        urlString = [NSString stringWithFormat:@"%@%@", BaseURLString, url];
        
        WSBaseRequest *request = [[WSBaseRequest alloc] initWithParameters:parameters];
        
        NSDictionary *tmpParameters = [request getRequestData];
        [self.requestSerializer setTimeoutInterval:60.0];
        
        
        [self POST:urlString parameters:tmpParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self endBackgroundTask];
            if (responseObject) {
                
                NSDictionary *response = [responseObject objectForKey:@"response"];
                
                if(!response || response == nil || response.allValues.count <= 0)
                {
                    response = responseObject; //[responseObject objectForKey:@"responseBody"];
                }
                if (response) {
                    
                    WSBaseResponse *baseResponse = [[WSBaseResponse alloc] initWithDictionary:response];
                    if(baseResponse.result && [baseResponse.result isSuccess])
                    {
                        [WSBaseRequest setLoggedUserHeader:baseResponse.header];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [delegate success:baseResponse];
                        });
                    }
                    else {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [delegate failure:baseResponse];
                        });
                    }
                }
                else {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self showErrorMessage];
                    });
                }
            }
            else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self showErrorMessage];
                });
            }
        }
           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               [self endBackgroundTask];
               
               NSLog(@"Post Error : %@",error);
               NSLog(@"Error Data:%@",[[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding]);
               
               dispatch_async(dispatch_get_main_queue(), ^{
                   
                   [DejalActivityView removeView];
                   
                   // If timeout then don't show the timeout error message.
                   
                   
                   if (delegate && [delegate respondsToSelector:@selector(failure:)]) {
                       
                       [delegate failure:nil];
                   }
               });
           }];
    }
}

#pragma mark - Internal Relogin on token expired

-(void) doSilentLoginInternal:(NSString *)url withParameters:(NSDictionary *)parameters delegate:(id<ServerResponseDelegate>)delegate
{
    
}

- (void) getToURL:(NSString *)url withParameters:(NSDictionary *)parameters delegate:(id<ServerResponseDelegate>)delegate
{
    @autoreleasepool
    {
        NSString *urlString = url;
        //createGroup
        
        urlString = [NSString stringWithFormat:@"%@%@", BaseURLString, url];
        
        WSBaseRequest *request = [[WSBaseRequest alloc] initWithParameters:parameters];
        
        NSDictionary *tmpParameters = [request getRequestData];
        [self.requestSerializer setTimeoutInterval:60.0];
        
        NSLog(@"URL:%@",urlString);
        
        [self GET:urlString parameters:tmpParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self endBackgroundTask];
            
            if (responseObject) {
                
                NSDictionary *response = responseObject; //[responseObject objectForKey:@"responseBody"];
                
                if (response) {
                    
                    WSBaseResponse *baseResponse = [[WSBaseResponse alloc] initWithDictionary:response];
                    if(baseResponse.result && [baseResponse.result isSuccess])
                    {
                        [WSBaseRequest setLoggedUserHeader:baseResponse.header];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [delegate success:baseResponse];
                        });
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [delegate failure:baseResponse];
                        });
                    }
                }
                else {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [self showErrorMessage];
                    });
                }
            }
            else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self showErrorMessage];
                });
            }
        }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              [self endBackgroundTask];
              NSLog(@"GET error : %@",error);
              NSLog(@"Error Data:%@",[[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding]);
              dispatch_async(dispatch_get_main_queue(), ^{
                  
                  [DejalActivityView removeView];
                  
                  // If timeout then don't show the timeout error message.
                  
                  if (delegate && [delegate respondsToSelector:@selector(failure:)]) {
                      
                      [delegate failure:nil];
                  }
              });
          }];
    }
}

//Get Streets and Towns
-(void)gettowns_withdelegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:@"gettowns" withParameters:nil delegate:delegate];
}
-(void)getstreets_withdelegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:@"getstreets" withParameters:nil delegate:delegate];
}

//Manoj kumar
-(void)getFactoryList:(NSDictionary *)params withdelegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:@"factoryList" withParameters:params delegate:delegate];
}

-(void)submitOrder:(NSDictionary *)params withdelegate:(id<ServerResponseDelegate>) delegate
{
    [self postToURL:@"submitorder" withParameters:params delegate:delegate];
}

-(void)getdeals_withdelegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:@"getdeals" withParameters:nil delegate:delegate];
}

-(void)fetchCoupons_withdelegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:@"getcoupons" withParameters:nil delegate:delegate];
}

-(void)fetchDealOfCoupons_withdelegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:@"getdaydeals" withParameters:nil delegate:delegate];
}

-(void)getCatMealforBussinesswithID:(NSString *)bussinessID delegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:[NSString stringWithFormat:@"getcatmealforbusiness/%@", bussinessID] withParameters:nil delegate:delegate];
}

-(void)getFindBussinessList_withdelegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:@"getfindbusinesslist" withParameters:nil delegate:delegate];
}

-(void)getFildCategoryFilterList_withdelegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:@"getcategoryfilters" withParameters:nil delegate:delegate];
}

-(void)getFieldCategoryDetail_withdelegate:(NSString *)filterID delegate:(id<ServerResponseDelegate>) delegate
{
    
    [self getToURL:[NSString stringWithFormat:@"getcategoryfiltersbusinesses/%@", filterID] withParameters:nil delegate:delegate];
    
}
-(void)getloginwithmember_withdelegate:(NSDictionary *)dic delegate:(id<ServerResponseDelegate>) delegate
{
    [self postToURL:@"memberlogin" withParameters:dic delegate:delegate];
}
-(void)login_withdelegate:(NSDictionary *)dicdata delegate:(id<ServerResponseDelegate>) delegate
{
    [self postToURL:@"login" withParameters:dicdata delegate:delegate];
    
}
-(void)getregistration_withdelegate:(NSDictionary *)dictdata delegate:(id<ServerResponseDelegate>) delegate
{
    [self postToURL:@"register" withParameters:dictdata delegate:delegate];
}
-(void)getMarketList_withdelegate:(id<ServerResponseDelegate>) delegate
{
    
    [self getToURL:[NSString stringWithFormat:@"getmarketdeals"] withParameters:nil delegate:delegate];
    
}
-(void)getCatTopProduct_withdelegate:(NSString *)marketID delegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:[NSString stringWithFormat:@"getcattopproduct/%@", marketID] withParameters:nil delegate:delegate];
}

//getbusinesslist

-(void)getBussinessList_withdelegate:(id<ServerResponseDelegate>)delegate
{
    [self getToURL:@"getbusinesslist" withParameters:nil delegate:delegate];
}


//fetch only bussiness without meals @shilpa 26/05/2016
-(void)fetchOnlyBussiness_withdelegate:(NSString *)businessId delegate:(id<ServerResponseDelegate>) delegate
{
    [self getToURL:[NSString stringWithFormat:@"getbusiness/%@",businessId] withParameters:nil delegate:delegate];
}

//fetchMealsOfBusiness  @shilpa 26/05/2016
-(void)fetchMealsOfBusiness_withdelegate:(NSString *)businessId subCategory:(NSString*)subCategory delegate:(id<ServerResponseDelegate>)delegate
{
    [self getToURL:[NSString stringWithFormat:@"getmealsforbusiness/%@/%@",businessId,subCategory] withParameters:nil delegate:delegate];
    
}

// phonecall API @Sarika 03/08/2016
-(void)markBusinessCallOrder_withdelegate:(NSDictionary*)dict delegate:(id<ServerResponseDelegate>)delegate
{
    [self postToURL:@"markBusinessCallOrder" withParameters:dict delegate:delegate];
}
//get delivery time @Sarika 03/08/2016
-(void)getBusinessDeliveryTime_withdelegate:(NSString *)businessId delegate:(id<ServerResponseDelegate>)delegate
{
    [self getToURL:[NSString stringWithFormat:@"getBusinessDeliveryTime/%@",businessId] withParameters:nil delegate:delegate];
}
-(void)submitOrderPayment:(NSDictionary *)params withdelegate:(id<ServerResponseDelegate>) delegate
{
    [self postToURL:@"submitOrderPayment" withParameters:params delegate:delegate];
}

//---------///Shabat api---//-----
-(void)getShabatData_withdelegate:(id<ServerResponseDelegate>)delegate
{
    [self getToURL:@"checkshabat" withParameters:nil delegate:delegate];
    
    
}

//-------//-----


- (void)success:(WSBaseResponse *)response
{
    
}

- (void)failure:(WSBaseResponse *)response
{
    
}

- (void)showErrorMessage {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No server response." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

-(void)sendMessage:(NSDictionary *) parameters delegate:(id<ServerResponseDelegate>)delegate
{
    [self postToURL:@"cfpuser/sendSMS" withParameters:parameters delegate:delegate];
}




@end
