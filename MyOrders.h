//
//  MyOrders.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 17/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyOrders : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "MyOrders+CoreDataProperties.h"
