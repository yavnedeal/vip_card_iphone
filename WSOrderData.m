//
//  WSOrderData.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 17/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSOrderData.h"

@implementation WSOrderData

-(void) parseWithDictionary:(NSMutableDictionary *)dict
{
    [super parseWithDictionary:dict];
}
-(id)populateCoreDataFromObject:(id)coreData
{
    myOrdersObj = (MyOrders*)coreData;
    myOrdersObj.orderId = self.orderId;
    myOrdersObj.orderDate = self.orderDate;
    myOrdersObj.orderSum = self.orderSum;
    myOrdersObj.logoURL = self.logoURL;
    myOrdersObj.orderObj = self.orderObj;
    myOrdersObj.cartData = self.cartData;
    
    return myOrdersObj;
}
-(void)populateObjectFromCorData:(id)coreData
{
    myOrdersObj = (MyOrders*)coreData;
    self.orderId = myOrdersObj.orderId;
    self.orderDate = myOrdersObj.orderDate;
    self.orderSum = myOrdersObj.orderSum;
    self.logoURL = myOrdersObj.logoURL;
    self.orderObj = myOrdersObj.orderObj;
    self.cartData = myOrdersObj.cartData;
}
@end
